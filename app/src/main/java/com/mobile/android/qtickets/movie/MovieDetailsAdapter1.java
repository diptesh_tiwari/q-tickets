package com.mobile.android.qtickets.movie;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.mobile.android.qtickets.R;

import java.util.ArrayList;

/**
 * Created by Satish - 360 on 11/22/2016.
 */

public class MovieDetailsAdapter1 extends RecyclerView.Adapter<MovieDetailsAdapter1.ViewHolder> implements View.OnClickListener {
	ArrayList<ShowTimeVO> showTimes;
	ShowDateVO showTimeVO;
	private Context mContext;
	private ArrayList<TheatreVO> movieList;
	private RecyclerView rv_showTimings;
	private RecyclerView.Adapter mTheatreAdapter;
	private RecyclerView.LayoutManager mTheatreLayoutManager;

	public MovieDetailsAdapter1(Context ctx, ArrayList<TheatreVO> listEvents) {
		mContext = ctx;
		movieList = listEvents;
//        showTimes = alShowTimes;
	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View v = LayoutInflater.from(mContext).inflate(R.layout.rvadapter_movie_details, parent, false);
		ViewHolder mViewHolder = new ViewHolder(v, viewType);
		return mViewHolder;
	}

	@Override
	public void onBindViewHolder(final ViewHolder holder, final int position) {
//        Picasso.with(mContext).load(dataBranches.get(position).getImageNews()).into(holder.iv_News_Image);
//        holder.tv_NewsTitle.setText(dataBranches.get(position).getNewsTitle());
//        holder.tv_NewsDescription.setText(dataBranches.get(position).getNewsDescription());

//        Picasso.with(mContext).load(R.drawable.restaurant).into(holder.iv_News_Image);
//        holder.tv_NewsTitle.setText("News Title");
//        holder.tv_NewsDescription.setText(R.string.sample_text);
		holder.tv_mall.setText(movieList.get(position).theatreName + "," + movieList.get(position).address);

		for (int i = 0; i < movieList.get(position).showDatesArr.size(); i++) {
			for (int j = 0; j < movieList.get(position).showDatesArr.get(i).showTimesArr.size(); j++) {
				//alshowdates.add(movievo.movieTheatresArr.get(i).showDatesArr.get(j).showDate);
				holder.btn1.setText(movieList.get(j).showDatesArr.get(i).showTimesArr.get(j).showTime);
				holder.btn1.setVisibility(View.VISIBLE);
			}
		}
//        ShowTimeVO std_vo = movieList.get(position).showDatesArr.get(position).showTimesArr.get(position);
//        holder.btn1.setText(showTimes.get(position).showTime);
//        holder.btn1.setVisibility(View.VISIBLE);

//        for (int j = 0; j < movieList.get(position).theatreName.length(); j++) {
//            for (int i = 0; i < showTimes.size(); i++) {
//                switch (i) {
//                    case 0:
//                        holder.btn1.setText(showTimes.get(position).showTime);
//                        holder.btn1.setVisibility(View.VISIBLE);
//
//                        break;
//                    /*case 1:
//                        holder.btn2.setText(showTimes.get(position).toString());
//                        holder.btn2.setVisibility(View.VISIBLE);
//
//                        break;
//                    case 2:
//                        holder.btn3.setText(showTimes.get(position).toString());
//                        holder.btn3.setVisibility(View.VISIBLE);
//
//                        break;
//                    case 3:
//                        holder.btn4.setText(showTimes.get(position).toString());
//                        holder.btn4.setVisibility(View.VISIBLE);
//
//                        break;
//                    case 4:
//                        holder.btn5.setText(showTimes.get(position).toString());
//                        holder.btn5.setVisibility(View.VISIBLE);
//
//                        break;
//                    case 5:
//                        holder.btn6.setText(showTimes.get(position).toString());
//                        holder.btn6.setVisibility(View.VISIBLE);
//
//                        break;
//                    case 6:
//                        holder.btn7.setText(showTimes.get(position).toString());
//                        holder.btn7.setVisibility(View.VISIBLE);
//
//                        break;
//                    case 7:
//                        holder.btn8.setText(showTimes.get(position).toString());
//                        holder.btn8.setVisibility(View.VISIBLE);
//
//                        break;*/
//
//                }
//
//            }
//        }
//        holder.btn2.setText(showTimes.get(1).toString());

//        holder.btn2.setVisibility(View.VISIBLE);

	}

	@Override
	public int getItemCount() {
		return movieList.size();
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		}

	}

	public class ViewHolder extends RecyclerView.ViewHolder {
		//        private RecyclerView rv_showTimings;
//        private RecyclerView.Adapter mTimingsAdapter;
//        private RecyclerView.LayoutManager mTimingsLayoutManager;
//        private ArrayList<String> timingsList;
		private TextView tv_mall;
		private Button btn1, btn2, btn3, btn4, btn5, btn6, btn7, btn8;

		public ViewHolder(View itemView, int viewType) {
			super(itemView);
			tv_mall = (TextView) itemView.findViewById(R.id.tv_mall_name);
			btn1 = (Button) itemView.findViewById(R.id.nortitle1);
			btn2 = (Button) itemView.findViewById(R.id.nortitle2);
			btn3 = (Button) itemView.findViewById(R.id.nortitle3);
			btn4 = (Button) itemView.findViewById(R.id.nortitle4);
			btn5 = (Button) itemView.findViewById(R.id.nortitle5);
			btn6 = (Button) itemView.findViewById(R.id.nortitle6);
			btn7 = (Button) itemView.findViewById(R.id.nortitle7);
			btn8 = (Button) itemView.findViewById(R.id.nortitle8);
//            rv_showTimings = (RecyclerView) itemView.findViewById(R.id.rv_show_timings);

//            setupTimingsAdapter();
		}

//        private void setupTimingsAdapter() {
//            rv_showTimings.setHasFixedSize(true);
//            rv_showTimings.setNestedScrollingEnabled(false);
//
//            mTimingsLayoutManager = new LinearLayoutManager(mContext);
//            rv_showTimings.setLayoutManager(mTimingsLayoutManager);
//
//            mTimingsAdapter = new MovieTimingsAdapter(mContext, timingsList);
//            rv_showTimings.setAdapter(mTimingsAdapter);
//        }
	}
}
