package com.mobile.android.qtickets.event.booking.activity;

/**
 * Created by hexalabssd on 24/03/17.
 */

public enum PaymentType {
	CREDIT_CARD_EVENT(2),
	NAPS(3),
	CREDIT_CARD_MOVIE(4),
	AMERICAN_EXPRESS_MOVIE(5),
	AMERICAN_EXPRESS_EVENT(6);

	private int mType;

	PaymentType(final int pI) {
		mType = pI;
	}

	public static PaymentType getPaymentType(final int type) {
		switch (type) {
			case 2: return CREDIT_CARD_EVENT;
			case 3: return NAPS;
			case 4: return CREDIT_CARD_MOVIE;
			case 5: return AMERICAN_EXPRESS_MOVIE;
			case 6: return AMERICAN_EXPRESS_EVENT;
			default:
				return null;
		}
	}
}
