package com.mobile.android.qtickets.parsers;

import com.mobile.android.qtickets.constants.AppConstants;
import com.mobile.android.qtickets.model.CountriesModel;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by Satish - 360 on 12/13/2016.
 */

public class TermsConditionsParseOperation {

	public ArrayList<CountriesModel> countryArr;
	protected XmlPullParser xmlpullparser;
	String TAG = "XmlPullParsing";
	CountriesModel country;
	StringBuffer sb;
	int holderForStartAndLength[] = new int[2];

	public TermsConditionsParseOperation(InputStream is) {
		XmlPullParserFactory factory = null;
		country = new CountriesModel();
		try {
			factory = XmlPullParserFactory.newInstance();
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		factory.setNamespaceAware(true);
		try {
			xmlpullparser = factory.newPullParser();
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			xmlpullparser.setInput(is, "UTF-8");
			processDocument(xmlpullparser);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void processDocument(XmlPullParser xpp) throws XmlPullParserException, IOException {
		int eventType = xpp.getEventType();
		do {
			if (eventType == XmlPullParser.START_DOCUMENT) {
//				System.out.println("Start document");
			} else if (eventType == XmlPullParser.END_DOCUMENT) {
//				System.out.println("End document");
			} else if (eventType == XmlPullParser.START_TAG) {
				processStartElement(xpp);
			} else if (eventType == XmlPullParser.END_TAG) {
				processEndElement(xpp);
			} else if (eventType == XmlPullParser.TEXT) {
				processText(xpp);
			}
			eventType = xpp.next();
		} while (eventType != XmlPullParser.END_DOCUMENT);
	}

	public void processStartElement(XmlPullParser xpp) {
		String name = xpp.getName();
		String uri = xpp.getNamespace();
		sb = new StringBuffer();

		if ("".equals(uri)) {
//			System.out.println("Start element: " + name);

			if (name.equals(AppConstants.Countriesinfo_TAG)) {
				countryArr = new ArrayList<CountriesModel>();
			}

			if (name.equals(AppConstants.Countriesinfo_COUNTRY_TAG)) {
				CountriesModel countrytemp = new CountriesModel();

				if (xpp.getAttributeValue(null, AppConstants.Countriesinfo_COUNTRYID_TAG) != null) {

					countrytemp.Countryid = xpp.getAttributeValue(null, AppConstants.Countriesinfo_COUNTRYID_TAG);
				} else {
					countrytemp.Countryid = "";
				}
				if (xpp.getAttributeValue(null, AppConstants.Countriesinfo_Countryname_TAG) != null) {
					countrytemp.Countryname = xpp.getAttributeValue(null, AppConstants.Countriesinfo_Countryname_TAG);
				} else {
					countrytemp.Countryname = "";
				}
				if (xpp.getAttributeValue(null, AppConstants.Countriesinfo_Countryprefix_TAG) != null) {
					countrytemp.Countryprefix = xpp.getAttributeValue(null, AppConstants.Countriesinfo_Countryprefix_TAG);
				} else {
					countrytemp.Countryprefix = "";
				}
				if (xpp.getAttributeValue(null, AppConstants.Countriesinfo_CountryNationality_TAG) != null) {
					countrytemp.CountryNationality = xpp.getAttributeValue(null, AppConstants.Countriesinfo_CountryNationality_TAG);
				} else {
					countrytemp.CountryNationality = "";
				}

				countryArr.add(countrytemp);
			}

		} else {
//			System.out.println("Start element: {" + uri + "}" + name);
		}
	}

	public void processEndElement(XmlPullParser xpp) {
		String name = xpp.getName();
		String uri = xpp.getNamespace();
		if ("".equals(uri)) {
//			System.out.println("End element: " + name);
			//			if (name.equals(SMSCountryConstants.SHOW_TIME_TAG)) {
			//				showDate.showTimesArr.addAll(showTimeArr);
			//
			//			}

			//			if (name.equals(SMSCountryConstants.SHOW_DATE_TAG)) {
			//				showDate.showTimesArr.addAll(showTimeArr);
			//
			//			}


		} else {
//			System.out.println("End element:   {" + uri + "}" + name);
		}
	}

	public void processText(XmlPullParser xpp) throws XmlPullParserException {
		char ch[] = xpp.getTextCharacters(holderForStartAndLength);
		int start = holderForStartAndLength[0];
		int length = holderForStartAndLength[1];
//		System.out.print("Characters:    \"");
		for (int i = start; i < start + length; i++) {
			switch (ch[i]) {
				case '\\':
					sb.append(ch[i]);
					break;

				case '"':
					sb.append(ch[i]);
					break;
				case '\n':
					sb.append(ch[i]);
					break;
				case '\r':
					sb.append(ch[i]);
					break;
				case '\t':
					sb.append(ch[i]);
					break;
				default:
					sb.append(ch[i]);
					break;
			}
		}
//		System.out.print("\"\n");
	}
}
