package com.mobile.android.qtickets.movie;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mobile.android.qtickets.R;

import java.util.ArrayList;

/**
 * Created by Satish - 360 on 11/22/2016.
 */

public class MovieTimingsAdapter extends RecyclerView.Adapter<MovieTimingsAdapter.ViewHolder> implements View.OnClickListener {
	private Context mContext;
	private ArrayList<String> eventsList;

	public MovieTimingsAdapter(Context ctx, ArrayList<String> listEvents) {
		mContext = ctx;
		eventsList = listEvents;
	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View v = LayoutInflater.from(mContext).inflate(R.layout.rvadapter_show_timings, parent, false);
		ViewHolder mViewHolder = new ViewHolder(v, viewType);
		return mViewHolder;
	}

	@Override
	public void onBindViewHolder(final ViewHolder holder, final int position) {
//        Picasso.with(mContext).load(dataBranches.get(position).getImageNews()).into(holder.iv_News_Image);
//        holder.tv_NewsTitle.setText(dataBranches.get(position).getNewsTitle());
//        holder.tv_NewsDescription.setText(dataBranches.get(position).getNewsDescription());

//        Picasso.with(mContext).load(R.drawable.restaurant).into(holder.iv_News_Image);
//        holder.tv_NewsTitle.setText("News Title");
//        holder.tv_NewsDescription.setText(R.string.sample_text);


	}

	@Override
	public int getItemCount() {
		return 5;
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		}

	}

	public class ViewHolder extends RecyclerView.ViewHolder {

		public ViewHolder(View itemView, int viewType) {
			super(itemView);


		}
	}

}
