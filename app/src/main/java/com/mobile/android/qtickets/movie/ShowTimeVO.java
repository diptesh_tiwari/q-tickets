package com.mobile.android.qtickets.movie;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Satish - 360 on 11/28/2016.
 */

public class ShowTimeVO implements Parcelable {
	public static final Creator<ShowTimeVO> CREATOR = new Creator<ShowTimeVO>() {

		public ShowTimeVO createFromParcel(Parcel in) {
			return new ShowTimeVO(in);
		}

		public ShowTimeVO[] newArray(int size) {
			return new ShowTimeVO[size];
		}

	};
	public int localShowTimeId, availableCount, total;
	public String serverId, showTime, showType, screenId, screenName;
	public String isEnable;

	public ShowTimeVO() {
		// TODO Auto-generated constructor stub

	}

	public ShowTimeVO(Parcel in) {
		// TODO Auto-generated constructor stub
		readFromParcel(in);
	}

	private void readFromParcel(Parcel in) {
		localShowTimeId = in.readInt();
		serverId = in.readString();
		showTime = in.readString();
		showType = in.readString();
		screenId = in.readString();
		screenName = in.readString();
		availableCount = in.readInt();
		isEnable = in.readString();
		total = in.readInt();

//		isEnable = in.readByte() != 0;     //myBoolean == true if byte != 0

	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		dest.writeInt(localShowTimeId);
		dest.writeString(serverId);
		dest.writeString(showTime);
		dest.writeString(showType);
		dest.writeString(screenId);
		dest.writeString(screenName);
		dest.writeInt(availableCount);
		dest.writeString(isEnable);
		dest.writeInt(total);

//		dest.writeByte((byte) (isEnable ? 1 : 0));     //if myBoolean == true, byte == 1


	}
}
