package com.mobile.android.qtickets.movie.moviedetails;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.util.ArraySet;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.mobile.android.qtickets.constants.AppConstants;
import com.mobile.android.qtickets.model.DateItem;
import com.mobile.android.qtickets.movie.moviedetails.activity.MovieDetailsActivity;
import com.mobile.android.qtickets.utils.QTUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * Created by hexalabssd on 02/03/17.
 */

public final class MovieShowDatesAdapter  extends RecyclerView.Adapter<MovieShowDatesViewHolder> {

	private final ArrayList<DateItem> mMovieDates;
	private Context mContext;
	private OnShowDateSelectedListener mOnShowDateSelectedListener;


	public MovieShowDatesAdapter(Context context) {
		mMovieDates = new ArrayList<>();
		this.mContext=context;
	}

	@Override
	public MovieShowDatesViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
		return new MovieShowDatesViewHolder(parent);
	}

	@Override
	public void onBindViewHolder(final MovieShowDatesViewHolder holder, final int position) {
		DateItem dateItem = mMovieDates.get(position);
		final Date movieDate = dateItem.getDatess();
		holder.setDateText(new SimpleDateFormat("EEE\ndd", Locale.getDefault()).format(movieDate));

		SharedPreferences prefmPrefs = PreferenceManager.getDefaultSharedPreferences(mContext);
		SharedPreferences.Editor editor = prefmPrefs.edit();
		editor.putString(AppConstants.MOVIE_BOOKING_DATE,new SimpleDateFormat("MMM dd,yyyy", Locale.getDefault()).format(mMovieDates.get(0).getDatess()
		));

		editor.commit();

		holder.setDrawable(mContext,dateItem.getAvailable());
        holder.itemView.setTag(dateItem);
		//QTUtils.getInstance().saveObjectToSharedPreference(mContext, AppConstants.MOVIE_BOOKING_DATE,new SimpleDateFormat("MMM dd,yyyy", Locale.getDefault()).format(movieDate));
		holder.itemView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(final View v) {
				if (mOnShowDateSelectedListener != null) {
                    DateItem dateItem= (DateItem) v.getTag();
                    Date date=dateItem.getDatess();
					mOnShowDateSelectedListener.onShowDateSelected(movieDate);
					SharedPreferences prefmPrefs = PreferenceManager.getDefaultSharedPreferences(mContext);
					SharedPreferences.Editor editor = prefmPrefs.edit();
					editor.putString(AppConstants.MOVIE_BOOKING_DATE,new SimpleDateFormat("MMM dd,yyyy", Locale.getDefault()).format(date));

					editor.commit();
					//QTUtils.getInstance().saveObjectToSharedPreference(mContext, AppConstants.MOVIE_BOOKING_DATE,new SimpleDateFormat("MMM dd,yyyy", Locale.getDefault()).format(movieDate));
				}
			}
		});
	}

	@Override
	public int getItemCount() {
		return mMovieDates.size();
	}

	public void setMovieDates(final ArrayList<DateItem> pMovieDates) {
		mMovieDates.clear();
		if (pMovieDates != null) {
			mMovieDates.addAll(pMovieDates);
		}
		notifyDataSetChanged();
	}

	public void setOnShowDateSelectedListener(final OnShowDateSelectedListener pOnShowDateSelectedListener) {
		mOnShowDateSelectedListener = pOnShowDateSelectedListener;
	}

	public interface OnShowDateSelectedListener {
		void onShowDateSelected(final Date selectedDate);
	}
}