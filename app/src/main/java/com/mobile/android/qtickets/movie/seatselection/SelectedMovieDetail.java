package com.mobile.android.qtickets.movie.seatselection;

import android.content.Intent;

import com.mobile.android.qtickets.activities.GuestActivity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Indglobal on 4/29/2017.
 */

public class SelectedMovieDetail implements Serializable{

    private String mMovieName;
    private String mMovieId;
    private String mMovieTheatre;
    private String mMovieDate;
    private String mMovieTime;
    private String mShowTimeId;
    private List<String> mSelectedSeatsList;
    private double totalCost;

    public double getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(double totalCost) {
        this.totalCost = totalCost;
    }

    public String getmMovieName() {
        return mMovieName;
    }

    public void setmMovieName(String mMovieName) {
        this.mMovieName = mMovieName;
    }

    public String getmMovieId() {
        return mMovieId;
    }

    public void setmMovieId(String mMovieId) {
        this.mMovieId = mMovieId;
    }

    public String getmMovieTheatre() {
        return mMovieTheatre;
    }

    public void setmMovieTheatre(String mMovieTheatre) {
        this.mMovieTheatre = mMovieTheatre;
    }

    public String getmMovieDate() {
        return mMovieDate;
    }

    public void setmMovieDate(String mMovieDate) {
        this.mMovieDate = mMovieDate;
    }

    public String getmMovieTime() {
        return mMovieTime;
    }

    public void setmMovieTime(String mMovieTime) {
        this.mMovieTime = mMovieTime;
    }

    public String getmShowTimeId() {
        return mShowTimeId;
    }

    public void setmShowTimeId(String mShowTimeId) {
        this.mShowTimeId = mShowTimeId;
    }

    public List<String> getmSelectedSeatsList() {
        return mSelectedSeatsList;
    }

    public void setmSelectedSeatsList(List<String> mSelectedSeatsList) {
        this.mSelectedSeatsList = mSelectedSeatsList;
    }


}
