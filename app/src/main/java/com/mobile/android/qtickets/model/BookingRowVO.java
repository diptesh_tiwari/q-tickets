package com.mobile.android.qtickets.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Satish - 360 on 12/7/2016.
 */

public class BookingRowVO implements Serializable {

	public int totalSeats, availableSeatsCount, noOfGangwaySeats;
	public String rowName; // letter
	public ArrayList<BookingSeatVO> seatsArr;
	public BookingClassVO bookingClassVO;
	public Boolean isFamily;
	public Boolean isInitialGangway;
	public int isInitialGangwayCount;

	//		public BookingRowVO(int totalSeats, int availableSeatsCount,
//				int noOfGangwaySeats, String rowName, Boolean isFamily) {
//			super();
//			this.totalSeats = totalSeats;
//			this.availableSeatsCount = availableSeatsCount;
//			this.noOfGangwaySeats = noOfGangwaySeats;
//			this.rowName = rowName;
//			this.isFamily = isFamily;
//		}
	public BookingRowVO() {
		super();
		bookingClassVO = new BookingClassVO();
		seatsArr = new ArrayList<BookingSeatVO>();
	}

	public BookingClassVO getBookingClassVO() {
		return bookingClassVO;
	}

	public void setBookingClassVO(BookingClassVO bookingClassVO) {
		this.bookingClassVO = bookingClassVO;
	}

	public ArrayList<BookingSeatVO> getSeatsArr() {
		return seatsArr;
	}

	public void setSeatsArr(ArrayList<BookingSeatVO> seatsArr) {
		this.seatsArr = seatsArr;
	}

	public Boolean getIsInitialGangway() {
		return isInitialGangway;
	}

	public void setIsInitialGangway(Boolean isInitialGangway) {
		this.isInitialGangway = isInitialGangway;
	}

	public int getIsInitialGangwayCount() {
		return isInitialGangwayCount;
	}

	public void setIsInitialGangwayCount(int isInitialGangwayCount) {
		this.isInitialGangwayCount = isInitialGangwayCount;
	}

	//		CGRect rowFrame;
	public int getTotalSeats() {
		return totalSeats;
	}

	public void setTotalSeats(int totalSeats) {
		this.totalSeats = totalSeats;
	}

	public int getAvailableSeatsCount() {
		return availableSeatsCount;
	}

	public void setAvailableSeatsCount(int availableSeatsCount) {
		this.availableSeatsCount = availableSeatsCount;
	}

	public int getNoOfGangwaySeats() {
		return noOfGangwaySeats;
	}

	public void setNoOfGangwaySeats(int noOfGangwaySeats) {
		this.noOfGangwaySeats = noOfGangwaySeats;
	}

	public String getRowName() {
		return rowName;
	}

	public void setRowName(String rowName) {
		this.rowName = rowName;
	}

	public Boolean getIsFamily() {
		return isFamily;
	}

	public void setIsFamily(Boolean isFamily) {
		this.isFamily = isFamily;
	}

}
