package com.mobile.android.qtickets.movie;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.afollestad.sectionedrecyclerview.SectionedRecyclerViewAdapter;
import com.mobile.android.qtickets.R;

import java.util.ArrayList;

/**
 * Created by Satish - 360 on 11/22/2016.
 */

public class MovieDetailsAdapter extends SectionedRecyclerViewAdapter<MovieDetailsAdapter.ViewHolder> implements View.OnClickListener {
	ArrayList<String> showTimes;
	ShowDateVO showTimeVO;
	private Context mContext;
	private ArrayList<TheatreVO> movieList;
	private RecyclerView rv_showTimings;
	private RecyclerView.Adapter mTheatreAdapter;
	private RecyclerView.LayoutManager mTheatreLayoutManager;

	public MovieDetailsAdapter(Context ctx, ArrayList<TheatreVO> listEvents, ArrayList<String> alShowTimes) {
		mContext = ctx;
		movieList = listEvents;
		showTimes = alShowTimes;
	}

	@Override
	public int getSectionCount() {
		return movieList.size();
	}

	@Override
	public int getItemCount(int section) {
		if (section % movieList.size() == 0) return movieList.size();
		return showTimes.size();// even sections get 4 items
	}

	@Override
	public void onBindHeaderViewHolder(ViewHolder holder, int section) {
		holder.title.setText(movieList.get(section).theatreName + "," + movieList.get(section).address);
	}

	@Override
	public void onBindViewHolder(ViewHolder holder, int section, int relativePosition, int absolutePosition) {
		holder.norText.setText(showTimes.get(0).toString());
		holder.norText.setVisibility(View.VISIBLE);
	}


	@Override
	public int getItemViewType(int section, int relativePosition, int absolutePosition) {
		if (section == 1)
			return 0; // VIEW_TYPE_HEADER is -2, VIEW_TYPE_ITEM is -1. You can return 0 or greater.
		return super.getItemViewType(section, relativePosition, absolutePosition);
	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
	  /*  int layout;
        switch (viewType) {
            case VIEW_TYPE_HEADER:
                layout = R.layout.list_item_header;
                break;
            case VIEW_TYPE_ITEM:
                layout = R.layout.list_item_main;
                break;
            default:
                layout = R.layout.list_item_main;
                break;
        }*/

		View v = LayoutInflater.from(parent.getContext()).inflate(viewType == VIEW_TYPE_HEADER ? R.layout.list_item_header : R.layout.list_item_main, parent, false);

       /* View v = LayoutInflater.from(parent.getContext())
                .inflate(layout, parent, false);*/
		return new ViewHolder(v);
	}

	@Override
	public void onClick(View view) {

	}

	public static class ViewHolder extends RecyclerView.ViewHolder {

		final TextView title;
		Button norText, btn02, btn03, btn04;

		public ViewHolder(View itemView) {
			super(itemView);
			title = (TextView) itemView.findViewById(R.id.tv_theatre_details);
			norText = (Button) itemView.findViewById(R.id.nortitle1);
		}
	}

   /* @Override
	public MovieDetailsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.sample_show_timings, parent, false);
        MovieDetailsAdapter.ViewHolder mViewHolder = new MovieDetailsAdapter.ViewHolder(v, viewType);
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(final MovieDetailsAdapter.ViewHolder holder, final int position) {
//        Picasso.with(mContext).load(dataBranches.get(position).getImageNews()).into(holder.iv_News_Image);
//        holder.tv_NewsTitle.setText(dataBranches.get(position).getNewsTitle());
//        holder.tv_NewsDescription.setText(dataBranches.get(position).getNewsDescription());

//        Picasso.with(mContext).load(R.drawable.restaurant).into(holder.iv_News_Image);
//        holder.tv_NewsTitle.setText("News Title");
//        holder.tv_NewsDescription.setText(R.string.sample_text);

        setUpTimingsAdapter(showTimeVO.showTimesArr);

        holder.tv_mall.setText(movieList.get(position).theatreName + ", " + movieList.get(position).address);

    }

    @Override
    public int getItemCount() {
        return movieList.size();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
        }

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_mall;
        //        private RecyclerView rv_showTimings;
//        private RecyclerView.Adapter mTimingsAdapter;
//        private RecyclerView.LayoutManager mTimingsLayoutManager;
//        private ArrayList<String> timingsList;

        public ViewHolder(View itemView, int viewType) {
            super(itemView);
            tv_mall = (TextView) itemView.findViewById(R.id.tv_theatre_details);
            rv_showTimings = (RecyclerView) itemView.findViewById(R.id.rv_timings_show);

//            rv_showTimings = (RecyclerView) itemView.findViewById(R.id.rv_show_timings);

//            setupTimingsAdapter();
        }


//        private void setupTimingsAdapter() {
//            rv_showTimings.setHasFixedSize(true);
//            rv_showTimings.setNestedScrollingEnabled(false);
//
//            mTimingsLayoutManager = new LinearLayoutManager(mContext);
//            rv_showTimings.setLayoutManager(mTimingsLayoutManager);
//
//            mTimingsAdapter = new MovieTimingsAdapter(mContext, timingsList);
//            rv_showTimings.setAdapter(mTimingsAdapter);
//        }
    }

    private void setUpTimingsAdapter(ArrayList<ShowTimeVO> showTime) {
        rv_showTimings.setHasFixedSize(true);
        rv_showTimings.setNestedScrollingEnabled(false);

        mTheatreLayoutManager = new GridLayoutManager(mContext, 3);
        rv_showTimings.setLayoutManager(mTheatreLayoutManager);

        mTheatreAdapter = new QTTimingsAdapter(mContext, showTime);
        rv_showTimings.setAdapter(mTheatreAdapter);
    }*/
}
