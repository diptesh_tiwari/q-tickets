package com.mobile.android.qtickets.movie.list;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;

import com.mobile.android.qtickets.movie.Movie;

import java.util.ArrayList;

/**
 * Created by Satish - 360 on 11/18/2016.
 */

class MoviesAdapter extends RecyclerView.Adapter<MovieItemViewHolder> {

	private static final int VIEW_TYPE_HORIZONTAL = 0;
	private static final int VIEW_TYPE_VERTICAL_SMALL = 1;

	private Context mContext;
	private ArrayList<Movie> moviesList;
	private MovieItemClickListener mMovieItemClickListener;
	private String movieType;

	public String getMovieType() {
		return movieType;
	}

	public void setMovieType(String movieType) {
		this.movieType = movieType;
	}

	MoviesAdapter(Context ctx, ArrayList<Movie> listMovies) {
		mContext = ctx;
		moviesList = listMovies;
	}

	@Override
	public MovieItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		switch (viewType) {
			case VIEW_TYPE_HORIZONTAL: {
				return new MovieHorizontalItemViewHolder(parent);
			}
			case VIEW_TYPE_VERTICAL_SMALL: {
				return new MovieVerticalSmallItemViewHolder(parent);
			}
			default:
				return null;
		}
	}

	@Override
	public void onBindViewHolder(final MovieItemViewHolder holder, final int position) {
		final Movie movie = moviesList.get(position);
		holder.setMovieName(movie.movieName);
		holder.setMovieType(movieType);
		if(!TextUtils.isEmpty(movie.duration)) {
			holder.setDuration(convertMinutesToHours(movie.duration != null ? movie.duration : ""));
		}

		final View.OnClickListener itemClickListener = new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				mMovieItemClickListener.onMovieItemClick(movie);
			}
		};
		String mvurl = "";
		holder.setMovieItemClickListener(itemClickListener);
		if (holder instanceof MovieHorizontalItemViewHolder) {
			final MovieHorizontalItemViewHolder horizontalHolder = (MovieHorizontalItemViewHolder) holder;
			horizontalHolder.setCensor(movie.censor);

			mvurl = movie.thumbnail;

			if(!TextUtils.isEmpty(movie.thumbnail)) {

				mvurl = movie.thumbnail;
			}

		} else if (holder instanceof MovieVerticalSmallItemViewHolder) {
			final MovieVerticalSmallItemViewHolder verticalSmallHolder = (MovieVerticalSmallItemViewHolder) holder;
			verticalSmallHolder.setMovieType(movieType);
			verticalSmallHolder.setLanguage(movie.language!=null?movie.language:"");
			verticalSmallHolder.setVisionType(movie.movieType!=null?movie.movieType:"");
			verticalSmallHolder.setDate(movie.releaseDate!=null?movie.releaseDate:"");
			if(!TextUtils.isEmpty(movie.IMDB_rating)){
				verticalSmallHolder.setVotes(Integer.parseInt(movie.IMDB_rating));
			}

			if(movie.movieTheatresArr!=null&& !movie.movieTheatresArr.isEmpty()){
				verticalSmallHolder.setTheater(String.valueOf(movie.movieTheatresArr!=null?movie.movieTheatresArr:""));

			}else {
				verticalSmallHolder.showVotes(movie);
			}





			if(!TextUtils.isEmpty(movie.ipadthumb)){
				mvurl = movie.ipadthumb;
			}else if(!TextUtils.isEmpty(movie.thumbnailURL)){
				mvurl = movie.thumbnailURL;
			}

		}

		mvurl = mvurl.replaceAll("http", "https");
		holder.setBanner(mvurl);
	}

	@Override
	public int getItemViewType(final int position) {
		return VIEW_TYPE_VERTICAL_SMALL;
	}

	@Override
	public int getItemCount() {
		return moviesList.size();
	}

	private String convertMinutesToHours(String mins) {
		int time = Integer.valueOf(mins);
		int hours = time / 60; //since both are ints, you get an int
		int minutes = time % 60;
		String duration = hours + " hr " + minutes + " min";
		return duration;
	}

	public void setMovieItemClickListener(final MovieItemClickListener pMovieItemClickListener) {
		mMovieItemClickListener = pMovieItemClickListener;
	}
}
