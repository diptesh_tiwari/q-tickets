package com.mobile.android.qtickets.CustomFonts;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.CheckedTextView;
import android.widget.TextView;

/**
 * Created by Indglobal on 4/28/2017.
 */

public class CheckedTextViewRegular  extends CheckedTextView {
    public CheckedTextViewRegular(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/SSPro_Regular.otf"));
    }
}