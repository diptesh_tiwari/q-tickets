package com.mobile.android.qtickets.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mobile.android.qtickets.R;
import com.mobile.android.qtickets.model.BookedItem;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Android on 4/27/17.
 */

public class BookedAdapter extends RecyclerView.Adapter<BookedAdapter.ViewHolder> {
    private Context mContext;
    ArrayList<BookedItem> bookedItemArrayList;

    public BookedAdapter(Context ctx, ArrayList<BookedItem> listEvents) {
        mContext = ctx;
        bookedItemArrayList = listEvents;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.rvadapter_booked, parent, false);
        ViewHolder mViewHolder = new ViewHolder(v, viewType);
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        String url = bookedItemArrayList.get(position).getMoviebannerURL();
        url = url.replace("App_Images", "movie_Images");
        Picasso.with(mContext).load(url).into(holder.iv_bookedmovie_image);
        holder.tv_title.setText(bookedItemArrayList.get(position).getMovie());
        holder.tv_theatre.setText(bookedItemArrayList.get(position).getTheater());
        holder.tv_seats.setText(bookedItemArrayList.get(position).getSeats());
        holder.tv_total.setText(bookedItemArrayList.get(position).getTotal_Cost()+" QAR");

        String date = bookedItemArrayList.get(position).getShowdate();
        Calendar calendar = Calendar.getInstance();
        try {
            Date dd = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(date);
            calendar.setTime(dd);
            String monthName = new SimpleDateFormat("MMM").format(calendar.getTime());
            String dateno = new SimpleDateFormat("dd").format(calendar.getTime());
            String day = new SimpleDateFormat("EEE").format(calendar.getTime());
            holder.tv_date.setText(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        //String newDate = format.format(calendar.getTime());


    }

    @Override
    public int getItemCount() {
        return bookedItemArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView iv_bookedmovie_image;
        private TextView tv_title,tv_lang,tv_type,tv_theatre,tv_date,tv_seats,tv_total;

        public ViewHolder(View itemView, int viewType) {
            super(itemView);
            iv_bookedmovie_image = (ImageView) itemView.findViewById(R.id.iv_bookedmovie_image);
            tv_title = (TextView) itemView.findViewById(R.id.tv_title);
            tv_lang = (TextView) itemView.findViewById(R.id.tv_lang);
            tv_type = (TextView)itemView.findViewById(R.id.tv_type);
            tv_theatre = (TextView)itemView.findViewById(R.id.tv_theatre);
            tv_date = (TextView)itemView.findViewById(R.id.tv_date);
            tv_seats = (TextView)itemView.findViewById(R.id.tv_seats);
            tv_total = (TextView)itemView.findViewById(R.id.tv_total);
        }
    }
}
