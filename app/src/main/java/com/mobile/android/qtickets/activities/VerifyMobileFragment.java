package com.mobile.android.qtickets.activities;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mobile.android.qtickets.R;
import com.mobile.android.qtickets.constants.AppConstants;
import com.mobile.android.qtickets.model.UserDetailsModel;

/**
 * Created by Android on 4/27/17.
 */

public class VerifyMobileFragment extends Fragment implements View.OnClickListener{

    LayoutInflater inflater;
    UserDetailsModel user;
    LinearLayout llSend,llVerify;
    TextView numberTV,prefixTV,tvGetVrfctnCode;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        inflater = (LayoutInflater) activity.getLayoutInflater();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.vrfy_mble_fragment, container, false);
        numberTV= (TextView) view.findViewById(R.id.numberTV);
        prefixTV= (TextView) view.findViewById(R.id.prefixTV);

        SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
        Gson gson = new Gson();
        String usrDetails = mPrefs.getString(AppConstants.SHARED_USER_TAG,null);

        if (usrDetails != null) {
            java.lang.reflect.Type type = new TypeToken<UserDetailsModel>(){}.getType();
            try {
                user = gson.fromJson(usrDetails,type);

                String  phoneNumber=user.phoneNumber;
                String userId = user.id;
                String prefix = user.prefix;

                numberTV.setText(phoneNumber!=null?phoneNumber:"");
                prefixTV.setText(prefix!=null?prefix:"");


            }catch (Exception e){
                e.printStackTrace();
            }

        }

        return view;
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        llSend = (LinearLayout)getActivity().findViewById(R.id.llSend);
        llVerify = (LinearLayout)getActivity().findViewById(R.id.llVerify);
        tvGetVrfctnCode = (TextView)getActivity().findViewById(R.id.tvGetVrfctnCode);

        llSend.setVisibility(View.VISIBLE);
        llVerify.setVisibility(View.GONE);

        tvGetVrfctnCode.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        switch (id){

            case R.id.tvGetVrfctnCode:
                llSend.setVisibility(View.GONE);
                llVerify.setVisibility(View.VISIBLE);
                break;
        }
    }
}