package com.mobile.android.qtickets.event.booking;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by hexalabssd on 20/03/17.
 */
public class EventTicket implements Parcelable {
	public static final Creator<EventTicket> CREATOR = new Creator<EventTicket>() {
		@Override
		public EventTicket createFromParcel(Parcel in) {
			return new EventTicket(in);
		}

		@Override
		public EventTicket[] newArray(int size) {
			return new EventTicket[size];
		}
	};
	private final String ticketType;
	private final double rate;
	private int count;

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	private int totalCount;

	public EventTicket(final String pTicketType, final double pRate) {
		ticketType = pTicketType;
		rate = pRate;
	}

	protected EventTicket(Parcel in) {
		ticketType = in.readString();
		rate = in.readDouble();
		count = in.readInt();
		totalCount = in.readInt();
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(ticketType);
		dest.writeDouble(rate);
		dest.writeInt(count);
		dest.writeInt(totalCount);
	}

	public String getTicketType() {
		return ticketType;
	}

	public double getRate() {
		return rate;
	}

	public int getCount() {
		return count;
	}

	public void setCount(final int pCount) {
		count = pCount;
	}
}
