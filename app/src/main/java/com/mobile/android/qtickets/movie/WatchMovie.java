package com.mobile.android.qtickets.movie;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * Created by Satish - 360 on 11/28/2016.
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class WatchMovie implements Parcelable {

	public int localMovieId;
	public String will_watch, will_not_watch,id;


	public WatchMovie() {
		// TODO Auto-generated constructor stub
//		movieTheatresArr = new ArrayList<MovieTheatreVO>();

	}

	public WatchMovie(Parcel in) {
		// TODO Auto-generated constructor stub
		readFromParcel(in);
	}

	public static final Creator<WatchMovie> CREATOR = new Creator<WatchMovie>() {
		@Override
		public WatchMovie createFromParcel(Parcel in) {
			return new WatchMovie(in);
		}

		@Override
		public WatchMovie[] newArray(int size) {
			return new WatchMovie[size];
		}
	};

	private void readFromParcel(Parcel in) {

		will_not_watch = in.readString();
		id = in.readString();
		will_watch = in.readString();

	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		dest.writeInt(localMovieId);
		dest.writeString(will_not_watch);
		dest.writeString(id);
		dest.writeString(will_watch);
	}
}
