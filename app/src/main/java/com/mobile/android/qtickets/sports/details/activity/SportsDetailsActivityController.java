package com.mobile.android.qtickets.sports.details.activity;

import android.widget.TextView;

import com.mobile.android.qtickets.R;

import in.hexalab.abstractcomponents.AppBarAbstractActivity;

/**
 * Created by hexalabssd on 21/03/17.
 */

public abstract class SportsDetailsActivityController extends AppBarAbstractActivity {
	private TextView mTitle;
	private TextView mDate;
	private TextView mTime;
	private TextView mVenue;
	private TextView mType;

	@Override
	protected void initContentView() {
		setContentView(R.layout.activity_sports_details);

	}

	@Override
	protected void initContentViewListeners() {

	}

	@Override
	protected void initActionBar() {

	}

	@Override
	protected void initNavigationDrawer() {

	}
}
