package com.mobile.android.qtickets.movie.moviedetails.activity;

import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.mobile.android.qtickets.R;
import com.mobile.android.qtickets.custom.MySpannable;
import com.mobile.android.qtickets.movie.TheatreVO;
import com.mobile.android.qtickets.movie.moviedetails.MovieShowDatesAdapter;
import com.mobile.android.qtickets.movie.moviedetails.MovieShowDatesViewHolder;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Locale;

import in.hexalab.abstractcomponents.AppBarAbstractActivity;

/**
 * Created by hexalabssd on 25/01/17.
 */

abstract class MovieDetailsActivityController extends AppBarAbstractActivity {
	protected LinearLayout mTheatreAndShowLinearLayout;
	private TextView tv_movieDescription;
	private TextView tv_movieTitle;
	private TextView tv_duration;
	private TextView tv_censor;
	private TextView mLanguageTextView;
	private TextView mVisionTypeTextView;
	private TextView mVotesTextView;
	private TextView btn_player;
	private Spinner sp_date_movie_details;
	private ImageView iv_movieBanner;
	public ImageView shareImageView,backImageView;
	private RadioGroup mDatesRadioGroup;

	private LinearLayout nowShowingLL,upComingLL;
	public TextView totalDontWatchTV,totalWatchTV,synopsisTV;
	private RelativeLayout appbar;
	private ImageView backBtn;
	private LinearLayout notWatchLL,watchLL;
	private ImageView watchImage;

	public abstract void onClickWatchTrailerButton(final View pView);

	@Override
	protected void onCreate(@Nullable final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		initTheatreAndShowRecyclerView();
	}

	@Override
	protected void initContentView() {
		setContentView(R.layout.activity_movie_details);

		iv_movieBanner = (ImageView) findViewById(R.id.iv_moviebanner_mdetails);
		shareImageView = (ImageView) findViewById(R.id.shareImageView);
		tv_movieTitle = (TextView) findViewById(R.id.textView_movieName);
		tv_censor = (TextView) findViewById(R.id.tv_censor_mdetails);
		tv_duration = (TextView) findViewById(R.id.textView_duration);
		mLanguageTextView = (TextView) findViewById(R.id.textView_language);
		mVisionTypeTextView = (TextView) findViewById(R.id.textView_visionType);
		mVotesTextView = (TextView) findViewById(R.id.textView_votes);
		btn_player = (TextView) findViewById(R.id.btn_trailer);
		sp_date_movie_details = (Spinner) findViewById(R.id.spn_selectDate);
		tv_movieDescription = (TextView) findViewById(R.id.tv_movie_details_description);
		mTheatreAndShowLinearLayout = (LinearLayout) findViewById(R.id.linearLayout_theatreAndShow);
		mDatesRadioGroup = (RadioGroup) findViewById(R.id.radioGroup_dates);



		nowShowingLL = (LinearLayout) findViewById(R.id.nowShowingLL);
		upComingLL = (LinearLayout) findViewById(R.id.upComingLL);
		totalDontWatchTV = (TextView) findViewById(R.id.totalDontWatchTV);
		totalWatchTV = (TextView) findViewById(R.id.totalWatchTV);
		synopsisTV = (TextView) findViewById(R.id.synopsisTV);
		//appbar = (RelativeLayout) findViewById(R.id.appbar);
		backImageView = (ImageView) findViewById(R.id.backImageView);
		//backBtn = (ImageView) findViewById(R.id.backBtn);

		//nowShowingLL,upComingLL,totalDontWatchTV,totalWatchTV

		notWatchLL = (LinearLayout) findViewById(R.id.notWatchLL);
		watchLL= (LinearLayout) findViewById(R.id.watchLL);
		watchImage= (ImageView) findViewById(R.id.watchImage);
		notWatchLL.setClickable( true );
		watchLL.setClickable( true );
		//notWatchLL.setOnClickListener(this);
		//watchLL.setOnClickListener(this);
		//watchImage.setOnClickListener(this);


	}

	@Override
	protected void initContentViewListeners() {
	}

	@Override
	protected void initActionBar() {
		final ActionBar actionBar = getSupportActionBar();
		if (actionBar != null) {
			actionBar.setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	protected void initNavigationDrawer() {

	}

	@Override
	protected void initArguments(@Nullable Bundle pBundle) {

	}

	@Override
	protected void fillContentViews() {

	}

	@CallSuper
	private void initTheatreAndShowRecyclerView() {
	}

	@Override
	public boolean onSupportNavigateUp() {
		onBackPressed();
		return super.onSupportNavigateUp();
	}

	protected void setPosterImageUrl(final String pPosterImageUrl) {
		Picasso.with(this).load(pPosterImageUrl).into(iv_movieBanner);
	}

	protected String getSelectedDate() {
		return null;
	}

	protected void setDateAdapter(final BaseAdapter pDateAdaper) {
		sp_date_movie_details.setAdapter(pDateAdaper);
	}

	protected void setShowDates(final MovieShowDatesAdapter pMovieShowDatesAdapter) {
		for (int i = 0; i < pMovieShowDatesAdapter.getItemCount(); i++) {
			final MovieShowDatesViewHolder viewHolder = pMovieShowDatesAdapter.createViewHolder(mDatesRadioGroup, 0);
			pMovieShowDatesAdapter.bindViewHolder(viewHolder, i);
			mDatesRadioGroup.addView(viewHolder.itemView);
		}
	}

	protected void setShowDateSelection(final int index) {
		if (mDatesRadioGroup != null && mDatesRadioGroup.getChildCount() > 0) {
			((RadioButton) mDatesRadioGroup.getChildAt(index)).setChecked(true);
		}
	}

	protected void setDateSelectedListener(AdapterView.OnItemSelectedListener pDateSelectedListener) {
		sp_date_movie_details.setOnItemSelectedListener(pDateSelectedListener);
	}

	protected void setSynopsis(final String synopsis) {
		synopsisTV.setText(synopsis);
		//appbar.setVisibility(View.GONE);
		//backImageView.setVisibility(View.VISIBLE);
	}

	protected void setTotalWatch(final String totalWatch) {

		tv_movieDescription.setText(totalWatch);
	}

	protected void setTotalDontWatch(final String totaDontlWatch) {
		tv_movieDescription.setText(totaDontlWatch);
	}

	protected void setDescription(final String pDescription) {
		tv_movieDescription.setText(pDescription);
		//appbar.setVisibility(View.VISIBLE);
		//backImageView.setVisibility(View.GONE);
	}

	protected void showDetailLayout(final ArrayList<TheatreVO> theatreVOArrayList) {
		if(!theatreVOArrayList.isEmpty()){
			nowShowingLL.setVisibility(View.VISIBLE);
			upComingLL.setVisibility(View.GONE);

		}else {
			nowShowingLL.setVisibility(View.GONE);
			upComingLL.setVisibility(View.VISIBLE);

		}

	}

	protected void setOnDescriptionClickListener(View.OnClickListener pOnDescriptionClickListener) {
		tv_movieDescription.setOnClickListener(pOnDescriptionClickListener);
	}

	protected void makeDescriptionResizable(final int maxLine, final String expandText, final boolean viewMore) {
		if (tv_movieDescription.getTag() == null) {
			tv_movieDescription.setTag(tv_movieDescription.getText());
		}
		ViewTreeObserver vto = tv_movieDescription.getViewTreeObserver();
		vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

			@SuppressWarnings("deprecation")
			@Override
			public void onGlobalLayout() {

				ViewTreeObserver obs = tv_movieDescription.getViewTreeObserver();
				obs.removeGlobalOnLayoutListener(this);
				if (maxLine == 0) {
					int lineEndIndex = tv_movieDescription.getLayout().getLineEnd(0);
					String text = tv_movieDescription.getText().subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
					tv_movieDescription.setText(text);
					tv_movieDescription.setMovementMethod(LinkMovementMethod.getInstance());
					tv_movieDescription.setText(addClickablePartTextViewResizable(Html.fromHtml(tv_movieDescription.getText().toString()), tv_movieDescription, maxLine, expandText, viewMore), TextView.BufferType.SPANNABLE);
				} else if (maxLine > 0 && tv_movieDescription.getLineCount() >= maxLine) {
					int lineEndIndex = tv_movieDescription.getLayout().getLineEnd(maxLine - 1);
					String text = tv_movieDescription.getText().subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
					tv_movieDescription.setText(text);
					tv_movieDescription.setMovementMethod(LinkMovementMethod.getInstance());
					tv_movieDescription.setText(addClickablePartTextViewResizable(Html.fromHtml(tv_movieDescription.getText().toString()), tv_movieDescription, maxLine, expandText, viewMore), TextView.BufferType.SPANNABLE);
				} else {
					int lineEndIndex = tv_movieDescription.getLayout().getLineEnd(tv_movieDescription.getLayout().getLineCount() - 1);
					String text = tv_movieDescription.getText().subSequence(0, lineEndIndex) + " " + expandText;
					tv_movieDescription.setText(text);
					tv_movieDescription.setMovementMethod(LinkMovementMethod.getInstance());
					tv_movieDescription.setText(addClickablePartTextViewResizable(Html.fromHtml(tv_movieDescription.getText().toString()), tv_movieDescription, lineEndIndex, expandText, viewMore), TextView.BufferType.SPANNABLE);
				}
			}
		});
	}

	private SpannableStringBuilder addClickablePartTextViewResizable(final Spanned strSpanned, final TextView tv, final int maxLine, final String spanableText, final boolean viewMore) {
		String str = strSpanned.toString();
		SpannableStringBuilder ssb = new SpannableStringBuilder(strSpanned);

		if (str.contains(spanableText)) {
			ssb.setSpan(new MySpannable(false) {
				@Override
				public void onClick(View widget) {
					if (viewMore) {
						tv.setLayoutParams(tv.getLayoutParams());
						tv.setText(tv.getTag().toString(), TextView.BufferType.SPANNABLE);
						tv.invalidate();
						String viewLess = "<font color='#4286f4'>. View Less</font>";
						makeDescriptionResizable(-1, ". View Less", false);
					} else {
						tv.setLayoutParams(tv.getLayoutParams());
						tv.setText(tv.getTag().toString(), TextView.BufferType.SPANNABLE);
						tv.invalidate();
						makeDescriptionResizable(2, ".....View More", true);
					}
				}
			}, str.indexOf(spanableText), str.indexOf(spanableText) + spanableText.length(), 0);

		}
		return ssb;

	}

	protected void setMovieTitle(final String pMovieTitle) {
		tv_movieTitle.setText(pMovieTitle);
	}

	protected void setCensor(final String pCensor) {
		tv_censor.setText(pCensor);
	}

	protected void setDuration(final String pDuration) {
		tv_duration.setText(pDuration);
	}

	protected void setLanguage(final String pLanguage) {
		mLanguageTextView.setText(pLanguage);
	}

	protected void setVisionType(final String pVisionType) {
		mVisionTypeTextView.setText(pVisionType);
	}

	protected void setVotes(final int pVotes) {
		//mVotesTextView.setText(String.format(Locale.getDefault(), "%d votes", pVotes));
		//mVotesTextView.setText(String.format(Locale.getDefault(), "%d votes", pVotes));
		mVotesTextView.setText("IMDB Rating"+": "+pVotes+"/"+10);
	}


	protected void setLikeVotes(Integer total) {
        //android:background="@drawable/background_movie_info_item"

		mVotesTextView.setText(String.format(Locale.getDefault(), "%d votes", total));
        mVotesTextView.setBackgroundDrawable(getResources().getDrawable(R.drawable.background_movie_info_item));
	}



}
