package com.mobile.android.qtickets;

import android.content.Context;

import com.mobile.android.qtickets.search.GetSearchResultRequest;

import in.hexalab.abstractvolley.ApiRequestManager;

/**
 * Created by hexalabssd on 23/01/17.
 */

public final class AppModel {
	private final Context mContext;

	public AppModel(final Context pContext) {
		mContext = pContext;
	}

	public void getSearchResult(final GetSearchResultRequest pGetSearchResultRequest) {
		ApiRequestManager.addToRequestQueue(mContext, pGetSearchResultRequest);
	}
}
