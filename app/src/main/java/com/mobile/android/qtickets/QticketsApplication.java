package com.mobile.android.qtickets;

import android.app.Application;
import android.os.Build;
import android.os.StrictMode;

//@ReportsCrashes(mailTo = "ram@orfeostory.com", mode = ReportingInteractionMode.TOAST, resToastText = R.string.crash_toast_text)
public class QticketsApplication extends Application {

    private static QticketsApplication qticketsApplication;


    public static QticketsApplication getInstance() {

        return qticketsApplication;
    }



    @Override
    public void onCreate() {
        super.onCreate();
       // ACRA.init(this);
        if (Config.DEVELOPER_MODE && Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectAll().penaltyDialog().build());
            StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder().detectAll().penaltyDeath().build());
        }
        qticketsApplication = this;


    }


    public class Config {
        public static final boolean DEVELOPER_MODE = false;
    }

}
