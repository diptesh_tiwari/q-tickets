package com.mobile.android.qtickets.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Satish - 360 on 11/25/2016.
 */

public class CountriesModel implements Parcelable {

	public static final Creator<CountriesModel> CREATOR = new Creator<CountriesModel>() {

		public CountriesModel createFromParcel(Parcel in) {
			return new CountriesModel(in);
		}

		public CountriesModel[] newArray(int size) {
			return new CountriesModel[size];
		}
	};
	public int localcutryid;
	public String country, Countryid, Countryname, Countryprefix, CountryNationality;

	public CountriesModel(Parcel in) {
		// TODO Auto-generated constructor stub
		readFromParcel(in);
	}

	private void readFromParcel(Parcel in) {

		localcutryid = in.readInt();
		country = in.readString();
		Countryid = in.readString();
		Countryname = in.readString();
		Countryprefix = in.readString();
		CountryNationality = in.readString();

	}

	public CountriesModel() {
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(localcutryid);
		dest.writeString(country);
		dest.writeString(Countryid);
		dest.writeString(Countryname);
		dest.writeString(Countryprefix);
		dest.writeString(CountryNationality);

	}
}
