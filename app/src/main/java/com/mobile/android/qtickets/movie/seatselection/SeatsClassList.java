package com.mobile.android.qtickets.movie.seatselection;

import android.net.Uri;

import java.util.ArrayList;

/**
 * Created by hexalabssd on 13/03/17.
 */

public final class SeatsClassList extends ArrayList<SeatsClass> {
	private int mMaxBooking;
	private int mBookingFees;
	private Uri mUrl;

	public int getMaxBooking() {
		return mMaxBooking;
	}

	public void setMaxBooking(final int pMaxBooking) {
		mMaxBooking = pMaxBooking;
	}

	public int getBookingFees() {
		return mBookingFees;
	}

	public void setBookingFees(final int pBookingFees) {
		mBookingFees = pBookingFees;
	}

	public Uri getUrl() {
		return mUrl;
	}

	public void setUrl(final Uri pUrl) {
		mUrl = pUrl;
	}

}
