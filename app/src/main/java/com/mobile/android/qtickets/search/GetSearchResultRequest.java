package com.mobile.android.qtickets.search;

import android.support.annotation.NonNull;

import com.mobile.android.qtickets.constants.AppConstants;

import in.hexalab.abstractvolley.base.HGetRequest;
import in.hexalab.abstractvolley.base.HResponseListener;

/**
 * Created by hexalabssd on 23/01/17.
 */

public final class GetSearchResultRequest extends HGetRequest<SearchResultResponse> {

	private static final String URL = AppConstants.SERVER_BASE_URL + "getsearchresult";

	/**
	 * Use this to make API request with {@code GET} method from {@link Method}.
	 *
	 * @param keyword
	 * @param listener
	 */
	public GetSearchResultRequest(@NonNull String keyword, @NonNull final HResponseListener<SearchResultResponse> listener) {
		super(URL, SearchResultResponse.class, listener);
		addUrlParam("search", keyword);
	}
}
