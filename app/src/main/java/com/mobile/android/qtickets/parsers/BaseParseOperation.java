package com.mobile.android.qtickets.parsers;

import org.xml.sax.helpers.DefaultHandler;

/**
 * Created by Satish - 360 on 11/23/2016.
 */

public class BaseParseOperation extends DefaultHandler {

	public String msg, errormsg, status, errorCode, randomCode, transId;
	String currentElement, currentElementCharacters;
	Boolean elementOn = false;
	StringBuffer sb;
}
