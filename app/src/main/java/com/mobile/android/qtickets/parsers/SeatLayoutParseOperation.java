package com.mobile.android.qtickets.parsers;

import com.mobile.android.qtickets.constants.AppConstants;
import com.mobile.android.qtickets.model.BookingClassVO;
import com.mobile.android.qtickets.model.BookingRowVO;
import com.mobile.android.qtickets.model.BookingSeatVO;
import com.mobile.android.qtickets.model.SeatLayoutVO;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by Satish - 360 on 12/7/2016.
 */

public class SeatLayoutParseOperation {
	public ArrayList<SeatLayoutVO> outputArr;
	protected XmlPullParser xmlpullparser;
	SeatLayoutVO seatLayoutVO;
	String TAG = "XmlPullParsing";
	StringBuffer sb;
	int holderForStartAndLength[] = new int[2];

	public SeatLayoutParseOperation(InputStream is) {
		outputArr = new ArrayList<SeatLayoutVO>();
		seatLayoutVO = new SeatLayoutVO();
		XmlPullParserFactory factory = null;
		try {
			factory = XmlPullParserFactory.newInstance();
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		factory.setNamespaceAware(true);
		try {
			xmlpullparser = factory.newPullParser();
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			xmlpullparser.setInput(is, "UTF-8");
			processDocument(xmlpullparser);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void processDocument(XmlPullParser xpp) throws XmlPullParserException, IOException {
		int eventType = xpp.getEventType();
		do {
			if (eventType == XmlPullParser.START_DOCUMENT) {
				//				System.out.println("Start document");
			} else if (eventType == XmlPullParser.END_DOCUMENT) {
				//				System.out.println("End document");
			} else if (eventType == XmlPullParser.START_TAG) {
				processStartElement(xpp);
			} else if (eventType == XmlPullParser.END_TAG) {
				processEndElement(xpp);
			} else if (eventType == XmlPullParser.TEXT) {
				processText(xpp);
			}
			eventType = xpp.next();
		} while (eventType != XmlPullParser.END_DOCUMENT);
	}

	public void processStartElement(XmlPullParser xpp) {
		String name = xpp.getName();
		//		System.out.println("\n"+name);
		String uri = xpp.getNamespace();
		sb = new StringBuffer();

		if ("".equals(uri)) {
			//			System.out.println("Start element: " + name);

			//			if(name.equals(SMSCountryConstants.CLASSES_TAG)){
			//				moviesArr= new ArrayList<MovieVO>();
			//			}

			if (name.equals(AppConstants.CLASSES_TAG)) {
				SeatLayoutVO tempBookingClassesVO = new SeatLayoutVO();
				if (xpp.getAttributeValue(null, AppConstants.CLASSES_MAX_BOOKING_TAG) != null) {
					tempBookingClassesVO.setMaxBooking(Integer.parseInt(xpp.getAttributeValue(null, AppConstants.CLASSES_MAX_BOOKING_TAG)));
				}
				if (xpp.getAttributeValue(null, AppConstants.CLASSES_BOOKING_FEES_TAG) != null) {
					tempBookingClassesVO.setBookingFees(Float.valueOf(xpp.getAttributeValue(null, AppConstants.CLASSES_BOOKING_FEES_TAG)));
				}
				if (xpp.getAttributeValue(null, AppConstants.CLASSES_URL_TAG) != null) {
					tempBookingClassesVO.setScreenURl(xpp.getAttributeValue(null, AppConstants.CLASSES_URL_TAG));
				}

				outputArr.add(tempBookingClassesVO);
			}

			if (name.equals(AppConstants.CLASS_TAG)) {
				SeatLayoutVO bClassesVORef = outputArr.get(outputArr.size() - 1);

				BookingClassVO bClassVO = new BookingClassVO();

				if (xpp.getAttributeValue(null, AppConstants.CLASS_ID_TAG) != null) {
					bClassVO.setClassId(xpp.getAttributeValue(null, AppConstants.CLASS_ID_TAG));
				}
				if (xpp.getAttributeValue(null, AppConstants.CLASS_NAME_TAG) != null) {
					bClassVO.setClassName(xpp.getAttributeValue(null, AppConstants.CLASS_NAME_TAG));
				}
				if (xpp.getAttributeValue(null, AppConstants.CLASS_COST_TAG) != null) {
					bClassVO.setCost(Float.valueOf(xpp.getAttributeValue(null, AppConstants.CLASS_COST_TAG)));
				}
				if (xpp.getAttributeValue(null, AppConstants.CLASS_NO_OF_ROWS_TAG) != null) {
					bClassVO.setNoOfRows(Integer.parseInt(xpp.getAttributeValue(null, AppConstants.CLASS_NO_OF_ROWS_TAG)));
				}


				bClassesVORef.getBookingClassesArr().add(bClassVO);
				//				movieTemp.thetresArr.addEventTicket(theatreTempVO);
				//				theatreArr.addEventTicket(theatreTempVO);

			}

			if (name.equals(AppConstants.ROW_TAG)) {

				SeatLayoutVO bClassesVORef = outputArr.get(outputArr.size() - 1);

				ArrayList<BookingClassVO> allClassesArr = bClassesVORef.getBookingClassesArr();

				BookingClassVO classVORef = allClassesArr.get(allClassesArr.size() - 1);

				// creating a new row

				BookingRowVO bRowVO = new BookingRowVO();

				bRowVO.setBookingClassVO(classVORef);


				if (xpp.getAttributeValue(null, AppConstants.ROW_LETTER_TAG) != null) {
					bRowVO.setRowName(xpp.getAttributeValue(null, AppConstants.ROW_LETTER_TAG));
				}
				if (xpp.getAttributeValue(null, AppConstants.ROW_NO_OF_SEATS_TAG) != null) {
					bRowVO.setTotalSeats(Integer.valueOf(xpp.getAttributeValue(null, AppConstants.ROW_NO_OF_SEATS_TAG)));
				}
				if (xpp.getAttributeValue(null, AppConstants.ROW_AVAILABLE_COUNT_TAG) != null) {
					bRowVO.setAvailableSeatsCount(Integer.parseInt(xpp.getAttributeValue(null, AppConstants.ROW_AVAILABLE_COUNT_TAG)));
				}
//				if (xpp.getAttributeValue(null, QTicketsConstants.ROW_GANGWAY_COUNTS_TAG) != null) {
//					bRowVO.setNoOfGangwaySeats(Integer.parseInt(xpp.getAttributeValue(null, QTicketsConstants.ROW_GANGWAY_COUNTS_TAG)));
//				}
				if (xpp.getAttributeValue(null, AppConstants.ROW_IS_FAMILY_TAG) != null) {
					if (xpp.getAttributeValue(null, AppConstants.ROW_IS_FAMILY_TAG).equals("1")) {
						bRowVO.setIsFamily(true);
					} else {
						bRowVO.setIsFamily(false);
					}
					//					bRowVO.setIsFamily(Boolean.valueOf(xpp.getAttributeValue(null, QTicketsConstants.ROW_IS_FAMILY_TAG)));
				}

				if (xpp.getAttributeValue(null, AppConstants.ROW_IS_INITIAL_GANGWAY_TAG) != null) {
					bRowVO.setIsInitialGangway(Boolean.valueOf(xpp.getAttributeValue(null, AppConstants.ROW_IS_INITIAL_GANGWAY_TAG)));
				}
				if (xpp.getAttributeValue(null, AppConstants.ROW_IS_INITIAL_GANGWAT_COUNT_TAG) != null) {
					bRowVO.setIsInitialGangwayCount(Integer.parseInt(xpp.getAttributeValue(null, AppConstants.ROW_IS_INITIAL_GANGWAT_COUNT_TAG)));
				}


				//				if (xpp.getAttributeValue(null, QTicketsConstants.ROW_AVAILABLE_SEATS_TAG) != null) {
				String availableSeatsStr = xpp.getAttributeValue(null, AppConstants.ROW_AVAILABLE_SEATS_TAG);
				//				}
				Boolean isFamilyBookingRow = Boolean.valueOf(xpp.getAttributeValue(null, AppConstants.ROW_IS_FAMILY_TAG));

				String[] availableSeatsArr = new String[]{};

				if (availableSeatsStr != null) {
					availableSeatsArr = availableSeatsStr.split(",");
				}

				if (isFamilyBookingRow) {
					//					System.out.println("Family booking seats");
				}
				// get the gangway seat numbers from the server if at all gangway is available
				String gangwaySeatsStr;
				String[] gangwaySeatsArr = new String[]{};

				// get the gangway count for each and every gangway
				String gangwayCountsStr;
				String[] gangwayCountsArr = new String[]{};

				if (Boolean.parseBoolean(xpp.getAttributeValue(null, AppConstants.ROW_IS_GANGWAY_TAG))) {
					gangwaySeatsStr = xpp.getAttributeValue(null, AppConstants.ROW_GANGWAY_SEATS_TAG);
					gangwaySeatsArr = gangwaySeatsStr.split(",");
					bRowVO.setNoOfGangwaySeats(gangwaySeatsArr.length);
					gangwayCountsStr = xpp.getAttributeValue(null, AppConstants.ROW_GANGWAY_COUNTS_TAG);
					gangwayCountsArr = gangwayCountsStr.split(",");
				}

				String allSeatsStr = xpp.getAttributeValue(null, AppConstants.ROW_ALL_SEATS_TAG);

				String[] allSeatsArr = new String[]{};

				if (allSeatsStr != null) {
					allSeatsArr = allSeatsStr.split(",");
				}
				// create all the seats, by default we set it the status as NOT_AVAILABLE
				for (int j = 0; j < allSeatsArr.length; j++) {

					BookingSeatVO tempSeatVO = new BookingSeatVO();
					tempSeatVO.row_is_family = bRowVO.isFamily;
					tempSeatVO.setBookingRowVO(bRowVO);
					tempSeatVO.setIsNextGangway(false);
					tempSeatVO.setGangwayCount(0);
					//										tempSeatVO.setIsGangwaySeat(false);
					tempSeatVO.setSeatNumber(allSeatsArr[j]);
					//					xpp.getAttributeValue(null, QTicketsConstants.ROW_IS_GANGWAY_TAG);

					if (availableSeatsArr.length > 0) {
						for (String availableServerSeat : availableSeatsArr) {
							tempSeatVO.setSeatStatus(SeatLayoutVO.SeatStatus.NOT_AVAILABLE);
							if (tempSeatVO.seatNumber.equals(availableServerSeat)) {
								tempSeatVO.setSeatStatus(SeatLayoutVO.SeatStatus.AVAILABLE);
								break;
							}
						}
					}

					if (Boolean.parseBoolean(xpp.getAttributeValue(null, AppConstants.ROW_IS_GANGWAY_TAG))) {

						for (int i = 0; i < gangwaySeatsArr.length; i++) {

							String gangWayServerSeat = gangwaySeatsArr[i];

							if (tempSeatVO.getSeatNumber().equals(gangWayServerSeat)) {
								tempSeatVO.setIsNextGangway(true);
								tempSeatVO.setGangwayCount(Integer.parseInt(gangwayCountsArr[i]));
								break;
							}
						}
					}
					bRowVO.getSeatsArr().add(tempSeatVO);
				}
				classVORef.getBookingRowsArr().add(bRowVO);

			}

		} else {
			//			System.out.println("Start element: {" + uri + "}" + name);
		}
	}

	public void processEndElement(XmlPullParser xpp) {
		String name = xpp.getName();
		String uri = xpp.getNamespace();
		if ("".equals(uri)) {
			//			System.out.println("End element: " + name);
			//			if (name.equals(SMSCountryConstants.SHOW_TIME_TAG)) {
			//				showDate.showTimesArr.addAll(showTimeArr);
			//
			//			}
			//			if (name.equals(SMSCountryConstants.SHOW_TIMES_TAG)) {
			//				showDatesTempVO.showTimesArr.addAll(showTimeArr);
			//				if (!showDatesArr.contains(showDatesTempVO)) {
			//					showDatesArr.addEventTicket(showDatesTempVO);
			//				}
			//			}
			//			if (name.equals(SMSCountryConstants.SHOW_DATE_TAG)) {
			//				showDate.showTimesArr.addAll(showTimeArr);
			//
			//			}
			//			if (name.equals(SMSCountryConstants.SHOW_DATES_TAG)) {
			//				theatreTempVO.showDatesArr.addAll(showDatesArr);
			//				if (!theatreArr.contains(theatreTempVO)) {
			//					theatreArr.addEventTicket(theatreTempVO);
			//
			//				}
			//			}
			//			if (name.equals(SMSCountryConstants.THEATRE_TAG)) {
			//				movieTempVO.thetresArr.addAll(theatreArr);
			//				if (!moviesArr.contains(movieTempVO)) {
			//					moviesArr.addEventTicket(movieTempVO);
			//
			//				}
			//			}

		} else {
			//			System.out.println("End element:   {" + uri + "}" + name);
		}
	}

	public void processText(XmlPullParser xpp) throws XmlPullParserException {
		char ch[] = xpp.getTextCharacters(holderForStartAndLength);
		int start = holderForStartAndLength[0];
		int length = holderForStartAndLength[1];
		//		System.out.print("Characters:    \"");
		for (int i = start; i < start + length; i++) {
			switch (ch[i]) {
				case '\\':
					sb.append(ch[i]);
					break;

				case '"':
					sb.append(ch[i]);
					break;
				case '\n':
					sb.append(ch[i]);
					break;
				case '\r':
					sb.append(ch[i]);
					break;
				case '\t':
					sb.append(ch[i]);
					break;
				default:
					sb.append(ch[i]);
					break;
			}
		}
		//		System.out.print("\"\n");
	}
}
