package com.mobile.android.qtickets.movie.moviedetails;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.RadioButton;

import com.mobile.android.qtickets.R;

/**
 * Created by hexalabssd on 02/03/17.
 */

public final class  MovieShowDatesViewHolder extends RecyclerView.ViewHolder {
	private final RadioButton mDateRadioButton;

	public MovieShowDatesViewHolder(final ViewGroup parent) {
		super(LayoutInflater.from(parent.getContext()).inflate(R.layout.view_date_radio_button, parent, false));
		mDateRadioButton = (RadioButton) itemView;
	}

	public void setDateText(final String pDateText) {
		mDateRadioButton.setText(pDateText);
	}

	public void setDrawable(Context context, String availabltext) {
		if (availabltext.equalsIgnoreCase("1")){
			mDateRadioButton.setBackground(context.getResources().getDrawable(R.drawable.background_date_radio_button));
			mDateRadioButton.setTextColor(context.getResources().getColor(R.color.text_date_radio_button));
			mDateRadioButton.setEnabled(true);
		}else {
			mDateRadioButton.setBackground(context.getResources().getDrawable(R.drawable.background_date_radio_button_disable));
			mDateRadioButton.setTextColor(context.getResources().getColor(R.color.colorGray));
			mDateRadioButton.setEnabled(false);
		}
	}

}
