package com.mobile.android.qtickets.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mobile.android.qtickets.R;
import com.mobile.android.qtickets.adapters.BookedAdapter;
import com.mobile.android.qtickets.constants.AppConstants;
import com.mobile.android.qtickets.model.BookedItem;
import com.mobile.android.qtickets.model.CountriesModel;
import com.mobile.android.qtickets.model.UserDetailsModel;
import com.mobile.android.qtickets.network.InternetConnectionDetector;
import com.mobile.android.qtickets.parsers.CountryParseOperation;
import com.mobile.android.qtickets.parsers.LoginParseOperation;
import com.mobile.android.qtickets.parsers.ProfileParseOpration;
import com.mobile.android.qtickets.utils.QTUtils;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;

import static com.mobile.android.qtickets.utils.QTUtils.getASCIIContentFromEntity;

/**
 * Created by Android on 4/27/17.
 */

public class MyProfileFragment extends Fragment implements View.OnClickListener{

    LayoutInflater inflater;

    LinearLayout llChangePass;
    ImageView ivCross;
    TextView tvChangePass,tvName,tvEmail,tvSavePrfl,tvSavePass;
    EditText etName,etEmail,etMobile,etOldPass,etNewPass,etConfrmPass;
    Spinner spinCountryCode,spinNationality;

    UserDetailsModel user;
    ArrayList<CountriesModel> countryArr = new ArrayList<>();
    ArrayList<String> allCountryNationality = new ArrayList<>();
    ArrayList<String> allPrefix = new ArrayList<>();
    String prefix="",name,email,phone,nationality,userId="",oldPass,newPass,confPass;
    private String status, error_msg, errorcode;
    int slctdSpin = 0,slctdNtn = 0;

    InternetConnectionDetector connectionDetector;
    private ProgressDialog dialog = null;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        inflater = (LayoutInflater) activity.getLayoutInflater();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.my_prfle_fragment, container, false);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        connectionDetector = new InternetConnectionDetector(getActivity());

        llChangePass = (LinearLayout)getActivity().findViewById(R.id.llChangePass);
        ivCross = (ImageView)getActivity().findViewById(R.id.ivCross);
        tvChangePass = (TextView)getActivity().findViewById(R.id.tvChangePass);
        tvName = (TextView)getActivity().findViewById(R.id.tvName);
        tvEmail = (TextView)getActivity().findViewById(R.id.tvEmail);
        tvSavePrfl = (TextView)getActivity().findViewById(R.id.tvSavePrfl);
        etName = (EditText) getActivity().findViewById(R.id.etName);
        etEmail = (EditText) getActivity().findViewById(R.id.etEmail);
        etMobile = (EditText) getActivity().findViewById(R.id.etMobile);
        spinCountryCode = (Spinner)getActivity().findViewById(R.id.spinCountryCode);
        spinNationality = (Spinner)getActivity().findViewById(R.id.spinNationality);

        tvSavePass = (TextView)getActivity().findViewById(R.id.tvSavePass);
        etOldPass = (EditText) getActivity().findViewById(R.id.etOldPass);
        etNewPass = (EditText) getActivity().findViewById(R.id.etNewPass);
        etConfrmPass = (EditText) getActivity().findViewById(R.id.etConfrmPass);

        SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
        Gson gson = new Gson();
        String usrDetails = mPrefs.getString(AppConstants.SHARED_USER_TAG,null);

        if (usrDetails != null) {
            java.lang.reflect.Type type = new TypeToken<UserDetailsModel>(){}.getType();
            try {
                user = gson.fromJson(usrDetails,type);

                tvName.setText(user.userName);
                etName.setText(user.userName);
                tvEmail.setText(user.emailId);
                etEmail.setText(user.emailId);
                etMobile.setText(user.phoneNumber);
                userId = user.id;
                prefix = user.prefix;
                nationality = user.nationality;


            }catch (Exception e){
                e.printStackTrace();
            }

        }


        initNationalitySpinner();

        tvChangePass.setOnClickListener(this);
        ivCross.setOnClickListener(this);
        tvSavePrfl.setOnClickListener(this);
        tvSavePass.setOnClickListener(this);

    }

    private void initializeProgressDialog() {
        dialog = new ProgressDialog(getActivity(), R.style.progress_bar_style);
        dialog.getWindow().setGravity(Gravity.CENTER);
        WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
        dialog.getWindow().setAttributes(params);
        dialog.show();
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
    }

    private void initNationalitySpinner() {

        SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
        Gson gson = new Gson();
        String country = mPrefs.getString(AppConstants.SHARED_COUNTRIES_TAG, null);
        if (country != null) {
            java.lang.reflect.Type type = new TypeToken<List<CountriesModel>>() {
            }.getType();
            countryArr = gson.fromJson(country, type);
            if (countryArr.size() != 0) {
                bindSpinners();
            }else {
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (connectionDetector.isConnectedToInternet()) {
                            // Do something after 2s = 2000ms
                            new CountryListUrl().execute();
                        }
                    }
                }, 300);
            }
        }else {
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (connectionDetector.isConnectedToInternet()) {
                        // Do something after 2s = 2000ms
                        new CountryListUrl().execute();
                    }
                }
            }, 300);
        }

    }

    private void bindSpinners() {
        for (int i = 0; i < countryArr.size(); i++) {
            allCountryNationality.add(countryArr.get(i).CountryNationality);

            if (nationality.equalsIgnoreCase(countryArr.get(i).CountryNationality)){
                slctdNtn = i;
            }

            String getPrfx = "+" + countryArr.get(i).Countryprefix;
            allPrefix.add(getPrfx);
            if (prefix.equalsIgnoreCase(countryArr.get(i).Countryprefix)){
                slctdSpin = i;
            }

        }

        setupNationalityAdapter(allCountryNationality);
        setupPrefixAdapter(allPrefix);

    }

    private void setupNationalityAdapter(ArrayList<String> allNationality) {
        // Creating adapter for spinner
        ArrayAdapter<String> countryAdapter = new ArrayAdapter<String>(getActivity().getBaseContext(), R.layout.simple_spinner_item_black, allNationality);
        // Drop down layout style - list view with radio button
        countryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        spinNationality.setAdapter(countryAdapter);
        spinNationality.setSelection(slctdNtn);
    }

    private void setupPrefixAdapter(ArrayList<String> allPrefixes) {
        // Creating adapter for spinner
        ArrayAdapter<String> countryAdapter = new ArrayAdapter<String>(getActivity().getBaseContext(), R.layout.simple_spinner_item_black, allPrefixes);
        // Drop down layout style - list view with radio button
        countryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        spinCountryCode.setAdapter(countryAdapter);
        spinCountryCode.setSelection(slctdSpin);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        switch (id){

            case R.id.tvChangePass:
                llChangePass.setVisibility(View.VISIBLE);
                break;

            case R.id.ivCross:
                llChangePass.setVisibility(View.GONE);
                break;

            case R.id.tvSavePrfl:
                if (connectionDetector.isConnectedToInternet()) {
                    if (validateAllFields()) {
                        new SavePrflUrl(getActivity().getApplicationContext()).execute();
                    }
                } else {
                    new AlertDialog.Builder(getActivity()).setMessage("No Internet Connection").setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).show();
                }
                break;

            case R.id.tvSavePass:
                if (connectionDetector.isConnectedToInternet()) {
                    if (validateChangePassFields()) {
                        new SavePassUrl(getActivity().getApplicationContext()).execute();
                    }
                } else {
                    new AlertDialog.Builder(getActivity()).setMessage("No Internet Connection").setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).show();
                }
                break;
        }
    }

    private boolean validateChangePassFields() {
        // TODO Auto-generated method stub
        if (!etOldPass.getText().toString().isEmpty()) {
            if (!etNewPass.getText().toString().isEmpty()) {
                if (!etConfrmPass.getText().toString().isEmpty()) {
                    if ((etConfrmPass.getText().toString()).equalsIgnoreCase(etNewPass.getText().toString())){
                        return true;
                    }else {
                        etConfrmPass.requestFocus();
                        etConfrmPass.setError("Password didn't matched!");
                        return false;
                    }

                } else {
                    etConfrmPass.requestFocus();
                    etConfrmPass.setError("Re-Enter New Password");
                    return false;
                }
            } else {
                etNewPass.requestFocus();
                etNewPass.setError("Enter New Password");
                return false;
            }
        } else {
            etOldPass.requestFocus();
            etOldPass.setError("Enter Your Old Password");
            return false;
        }

    }

    private class SavePassUrl extends AsyncTask<String, Void, String> {
        private Context mContext;

        public SavePassUrl(final Context pContext) {
            mContext = pContext;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            oldPass = etOldPass.getText().toString().trim();
            newPass = etNewPass.getText().toString().trim();
            confPass = etConfrmPass.getText().toString().trim();

            initializeProgressDialog();
        }

        @Override
        protected String doInBackground(String... urls) {

            try {
                String registration_url = AppConstants.SERVER_URL_CHANGE_PASS + "userid=" + userId + "&oldpwd=" + oldPass
                        + "&newpwd=" + newPass ;
                registration_url = registration_url.replaceAll("\\s+", "%20");
                HttpClient httpClient = new DefaultHttpClient();
                HttpContext localContext = new BasicHttpContext();
                HttpGet httpGet = new HttpGet(registration_url);
                //httpGet.addHeader(BasicScheme.authenticate(new UsernamePasswordCredentials(getResources().getString(R.string.auth_username), getResources().getString(R.string.auth_password)), "UTF-8", false));
                HttpResponse response = httpClient.execute(httpGet, localContext);
                HttpEntity entity = response.getEntity();
                String response_str = QTUtils.getASCIIContentFromEntity(entity);
                return response_str;
            } catch (Exception e) {

            }
            return null;
        }


        protected void onPostExecute(String result) {
            dialog.dismiss();
            try {

                if (result !=null ){
                    JSONObject jsonObj = null;
                    try {
                        jsonObj = XML.toJSONObject(result);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    if (jsonObj != null){

                        JSONObject response = jsonObj.getJSONObject("response");
                        JSONObject jobjresult= response.getJSONObject("result");
                        String status = jobjresult.getString("status");

                        if (status.equalsIgnoreCase("true")){

                            String errormsg = jobjresult.getString("errormsg");
                            dialog.dismiss();
                            Toast.makeText(getActivity(),errormsg,Toast.LENGTH_SHORT).show();
                            Intent setIntent = new Intent(getActivity(), LoginActivity.class);
                            setIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            startActivity(setIntent);

                        }else {

                            String errormsg = jobjresult.getString("errormsg");
                            Toast.makeText(getActivity(),errormsg,Toast.LENGTH_SHORT).show();
                        }
                    }
                }

            } catch (Exception e) {
                Toast.makeText(getActivity().getApplicationContext(), "Server Not responding..", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        }
    }

    private boolean validateAllFields() {
        // TODO Auto-generated method stub
        if (!etName.getText().toString().isEmpty()) {
            if (!etEmail.getText().toString().isEmpty()) {
                if (!etMobile.getText().toString().isEmpty()) {
                    return true;
                } else {
                    etMobile.requestFocus();
                    etMobile.setError("Enter Phone Number");
                    return false;
                }
            } else {
                etEmail.requestFocus();
                etEmail.setError("Enter Email Address");
                return false;
            }
        } else {
            etName.requestFocus();
            etName.setError("Enter Your Name");
            return false;
        }

    }

    private class SavePrflUrl extends AsyncTask<Void, Void, Void> {
        private Context mContext;

        public SavePrflUrl(final Context pContext) {
            mContext = pContext;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            name = etName.getText().toString().trim();
            email = etEmail.getText().toString().trim();
            phone = etMobile.getText().toString().trim();
            prefix = spinCountryCode.getSelectedItem().toString();
            prefix = prefix.replaceAll("[-+.^:,]", "");
            nationality = spinNationality.getSelectedItem().toString();

            initializeProgressDialog();
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            String resp;
            try {
                String registration_url = AppConstants.SERVER_URL_UPDATE_PROFILE + "uid=" + userId + "&name=" + name
                        + "&email=" + email + "&mno=" + phone + "&prefix=" + prefix + "&nationality=" + nationality ;
                registration_url = registration_url.replaceAll("\\s+", "%20");
                HttpClient httpClient = new DefaultHttpClient();
                HttpContext localContext = new BasicHttpContext();
                HttpGet httpGet = new HttpGet(registration_url);
                //httpGet.addHeader(BasicScheme.authenticate(new UsernamePasswordCredentials(getResources().getString(R.string.auth_username), getResources().getString(R.string.auth_password)), "UTF-8", false));
                HttpResponse response = httpClient.execute(httpGet, localContext);
                HttpEntity entity = response.getEntity();
                resp = getASCIIContentFromEntity(entity);
                parseProfileResponse(resp);
            } catch (Exception e) {

            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            dialog.dismiss();
            try {
                if (status.equalsIgnoreCase(AppConstants.RESPONSE_TRUE_TAG)) {
                    SharedPreferences prefmPrefs = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
                    SharedPreferences.Editor editor = prefmPrefs.edit();
                    Gson gson = new Gson();
                    String userdata = gson.toJson(user);
                    editor.putString(AppConstants.SHARED_USER_TAG, userdata);
                    editor.commit();
//                    new AlertDialog.Builder(mContext).setTitle("Success").setMessage("Registration has done. Now please login with your email address and password.").setNeutralButton("Ok", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(final DialogInterface dialog, final int which) {
//                            Intent intent = new Intent(getActivity().getApplicationContext(), LoginActivity.class);
//                            startActivity(intent);
//                            getActivity().finish();
//                        }
//                    }).create().show();
                    String usrDetails = prefmPrefs.getString(AppConstants.SHARED_USER_TAG,null);

                    if (usrDetails != null) {
                        java.lang.reflect.Type type = new TypeToken<UserDetailsModel>(){}.getType();
                        try {
                            user = gson.fromJson(usrDetails,type);

                            tvName.setText(user.userName);
                            tvEmail.setText(user.emailId);
                            userId = user.id;
                            prefix = user.prefix;
                            nationality = user.nationality;


                        }catch (Exception e){
                            e.printStackTrace();
                        }

                    }
                    Toast.makeText(getActivity().getApplicationContext(), "Profile Successfully Updated!", Toast.LENGTH_SHORT).show();
                } else if (status.equalsIgnoreCase(AppConstants.RESPONSE_FALSE_TAG)) {
//                    new AlertDialog.Builder(getActivity()).setMessage(error_msg).setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                        public void onClick(DialogInterface dialog, int which) {
//
//                        }
//                    }).show();
                    Toast.makeText(getActivity().getApplicationContext(), error_msg, Toast.LENGTH_SHORT).show();

                }
            } catch (Exception e) {
                Toast.makeText(getActivity().getApplicationContext(), "Server Not responding..", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        }
    }


    protected void parseProfileResponse(String result) {
        // TODO Auto-generated method stub
        try {
            //two lines of code for xmlpullparsing
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(result.getBytes());
            ProfileParseOpration xmlpull = new ProfileParseOpration(byteArrayInputStream);
            user = xmlpull.user;
            status = xmlpull.status;
            errorcode = xmlpull.errorCode;
            error_msg = xmlpull.errormsg;
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
    }

    public class CountryListUrl extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... arg0) {
            try {
                String resp;
                try {
                    String country_url = AppConstants.SERVER_URL_GET_ALL_COUNTRIES;
                    HttpClient httpClient = new DefaultHttpClient();
                    HttpContext localContext = new BasicHttpContext();
                    HttpGet httpGet = new HttpGet(country_url);
                    httpGet.addHeader(BasicScheme.authenticate(new UsernamePasswordCredentials(getResources().getString(R.string.auth_username), getResources().getString(R.string.auth_password)), "UTF-8", false));
                    HttpResponse response = httpClient.execute(httpGet, localContext);
                    HttpEntity entity = response.getEntity();
                    resp = QTUtils.getASCIIContentFromEntity(entity);
                    countryArr = parseCountryResponse(resp);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {

            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            initializeProgressDialog();

        }


        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            dialog.dismiss();
            try {
                if (countryArr.size() > 0) {
                    SharedPreferences prefmPrefs = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
                    SharedPreferences.Editor editor = prefmPrefs.edit();
                    Gson gson = new Gson();
                    String countryjson = gson.toJson(countryArr);
                    editor.putString(AppConstants.SHARED_COUNTRIES_TAG, countryjson);
                    editor.commit();

                } else {
                    countryArr.clear();
                    new CountryListUrl().execute();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            initNationalitySpinner();
        }

    }

    protected ArrayList<CountriesModel> parseCountryResponse(String result) {
        ArrayList<CountriesModel> conty = null;
        try {
            //two lines of code for xmlpullparsing
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(result.getBytes());
            CountryParseOperation xmlpull = new CountryParseOperation(byteArrayInputStream);
            conty = xmlpull.countryArr;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return conty;
    }
}