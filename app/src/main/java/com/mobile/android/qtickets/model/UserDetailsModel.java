package com.mobile.android.qtickets.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.util.ArrayList;

/**
 * Created by Satish - 360 on 11/23/2016.
 */


public class UserDetailsModel implements Parcelable {
	public static final Creator<UserDetailsModel> CREATOR = new Creator<UserDetailsModel>() {

		public UserDetailsModel createFromParcel(Parcel in) {
			return new UserDetailsModel(in);
		}

		public UserDetailsModel[] newArray(int size) {
			return new UserDetailsModel[size];
		}

	};
	public int localUserId, status;
	public String id, userName, prefix, phoneNumber, address, emailId, password, verify, nationality;
	private ArrayList<String> bookingHistoryArr;

	public UserDetailsModel() {
		super();
		bookingHistoryArr = new ArrayList<String>();
	}

	public UserDetailsModel(Parcel in) {
		// TODO Auto-generated constructor stub
		readFromParcel(in);
	}

	private void readFromParcel(Parcel in) {
		bookingHistoryArr = new ArrayList<String>();
		localUserId = in.readInt();
		id = in.readString();
		userName = in.readString();
		prefix = in.readString();
		phoneNumber = in.readString();
		address = in.readString();
		emailId = in.readString();
		password = in.readString();
		status = in.readInt();
		verify = in.readString();
		nationality = in.readString();
		in.readList(bookingHistoryArr, getClass().getClassLoader());
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		dest.writeInt(localUserId);
		dest.writeString(id);
		dest.writeString(userName);
		dest.writeString(prefix);
		dest.writeString(phoneNumber);
		dest.writeString(address);
		dest.writeString(emailId);
		dest.writeString(password);
		dest.writeInt(status);
		dest.writeString(verify);
		dest.writeString(nationality);
		dest.writeList(bookingHistoryArr);

	}

}
