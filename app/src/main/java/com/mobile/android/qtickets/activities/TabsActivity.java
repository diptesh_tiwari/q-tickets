package com.mobile.android.qtickets.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.mobile.android.qtickets.R;
import com.mobile.android.qtickets.event.EventsFragment;
import com.mobile.android.qtickets.leisure.LeisureFragment;
import com.mobile.android.qtickets.movie.list.MoviesListFragment;
import com.mobile.android.qtickets.movie.list.SearchableActivity;
import com.mobile.android.qtickets.search.SearchActivity;
import com.mobile.android.qtickets.sports.SportsFragment;

import static com.mobile.android.qtickets.movie.list.MoviesListFragment.nowShowTV;
import static com.mobile.android.qtickets.movie.list.MoviesListFragment.upComingShowTV;

/**
 * Created by Satish - 360 on 11/18/2016.
 */

public class TabsActivity extends AppCompatActivity implements View.OnClickListener, TextView.OnEditorActionListener {



	private final TextView.OnEditorActionListener mOnEditorActionSearch = new TextView.OnEditorActionListener() {
		@Override
		public boolean onEditorAction(final TextView v, final int actionId, final KeyEvent event) {
			switch (actionId) {
				case EditorInfo.IME_ACTION_SEARCH: {
					final String keyword = v.getText().toString();
					if (!keyword.isEmpty()) {
						startSearchActivity(keyword);
						return true;
					}
				}
				break;
			}
			return false;
		}
	};
	private static final String EXTRA_TAB_POSITION = TabsActivity.class.getName() + ".extra.TAB_POSITION";

	boolean flag = false;
	private Toolbar toolbar;
	private TabLayout tabLayout;
	private ViewPager viewPager;
	private  ViewPagerAdapter adapter;
	private ImageView backBtn,serchBtn;
	private EditText editText_search;

	public static void start(Context context, final int tabPosition) {
		final Bundle bundle = new Bundle();
		bundle.putInt(EXTRA_TAB_POSITION, tabPosition);
		start(context, bundle);
	}

	private static void start(Context context, final Bundle pBundle) {
		final Intent starter = new Intent(context, TabsActivity.class);
		if (pBundle != null) {
			starter.putExtras(pBundle);
		}
		context.startActivity(starter);
	}

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tabs);

		viewPager = (ViewPager) findViewById(R.id.viewpager);
		backBtn = (ImageView) findViewById(R.id.backBtn);
		serchBtn = (ImageView) findViewById(R.id.serchBtn);
		editText_search = (EditText) findViewById(R.id.editText_search);


		adapter = new ViewPagerAdapter(getSupportFragmentManager());
		viewPager.setAdapter(adapter);
		backBtn.setOnClickListener(this);
		serchBtn.setOnClickListener(this);

		tabLayout = (TabLayout) findViewById(R.id.tabs);
		tabLayout.setupWithViewPager(viewPager);
		for (int i = 0; i < tabLayout.getTabCount(); i++) {
			final TabLayout.Tab tab = tabLayout.getTabAt(i);
			switch (i) {
				case 0: {
					tab.setIcon(R.drawable.ic_movie);
				}
				break;
				case 1: {
					tab.setIcon(R.drawable.ic_events);
				}
				break;
				case 2: {
					tab.setIcon(R.drawable.ic_sports);
				}
				break;
				case 3: {
					tab.setIcon(R.drawable.ic_leisure);
				}
				break;
			}
		}

		viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
			@Override
			public void onPageScrolled(final int position, final float positionOffset, final int positionOffsetPixels) {
				int color = ContextCompat.getColor(TabsActivity.this, R.color.colorMovie);
				switch (position) {
					case 1: {
						color = ContextCompat.getColor(TabsActivity.this, R.color.event);
					}
					break;
					case 2: {
						color = ContextCompat.getColor(TabsActivity.this, R.color.colorSports);
					}
					break;
					case 3: {
						color = ContextCompat.getColor(TabsActivity.this, R.color.leisure);
					}
					break;
				}

				tabLayout.setSelectedTabIndicatorColor(color);
			}

			@Override
			public void onPageSelected(final int position) {

				Fragment mFragment = null;
				int index = viewPager.getCurrentItem();
				//chanageBackground(nowShowTV);
				adapter = ((ViewPagerAdapter) viewPager.getAdapter());
				mFragment = adapter.getItem(index);

				if (mFragment instanceof MoviesListFragment){
					if (mFragment instanceof MoviesListFragment){
						((MoviesListFragment) mFragment).setMovieType("nowShow");
					}
				}

			}

			@Override
			public void onPageScrollStateChanged(final int state) {

			}
		});

		viewPager.setCurrentItem(getIntent().getIntExtra(EXTRA_TAB_POSITION, 0), true);

		tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
			@Override
			public void onTabSelected(TabLayout.Tab tab) {
				changeMovieType();
			}

			@Override
			public void onTabUnselected(TabLayout.Tab tab) {

			}

			@Override
			public void onTabReselected(TabLayout.Tab tab) {

			}
		});

	}

	private void changeMovieType() {
		try {
			nowShowTV.setBackgroundDrawable(getResources().getDrawable(R.drawable.square_button));
			upComingShowTV.setBackgroundDrawable(getResources().getDrawable(R.drawable.square_button_f));
		}catch (Exception e){
			e.printStackTrace();
		}
	}

	private void initActionbar() {
		setSupportActionBar(toolbar);
		final ActionBar actionBar = getSupportActionBar();
		if (actionBar != null) {
			actionBar.setDisplayHomeAsUpEnabled(true);
			actionBar.setHomeAsUpIndicator(R.drawable.back_btn);
		}
	}

	@Override
	protected void onResume() {
		super.onResume();

	}

	@Override
	public boolean onSupportNavigateUp() {
		onBackPressed();
		return super.onSupportNavigateUp();
	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {
			case R.id.backBtn:
				finish();
				break;
			case R.id.serchBtn:
				Intent intent=new Intent(this,SearchableActivity.class);
				startActivity(intent);

		}
	}

	public void nowShowClick(){
		Fragment mFragment = null;
		int index = viewPager.getCurrentItem();
		chanageBackground(nowShowTV);

		adapter = ((ViewPagerAdapter) viewPager.getAdapter());
		mFragment = adapter.getItem(index);

		if (mFragment instanceof MoviesListFragment){
			if (mFragment instanceof MoviesListFragment){
				((MoviesListFragment) mFragment).setMovieType("nowShow");
			}
		}

		mFragment = getSupportFragmentManager().findFragmentById(R.id.fragment_reference);

	}

	public void upcmngClick(){
		Fragment mFragment = null;
		int index = viewPager.getCurrentItem();
		chanageBackground(upComingShowTV);
		adapter = ((ViewPagerAdapter) viewPager.getAdapter());
		mFragment = adapter.getItem(index);

		if (mFragment instanceof MoviesListFragment){
			if (mFragment instanceof MoviesListFragment){
				((MoviesListFragment) mFragment).setMovieType("upComingShow");
			}
		}
	}

	private void startSearchActivity(final String keyword) {
		SearchActivity.start(this, keyword);
	}

	private void chanageBackground(TextView clickTextView) {
		nowShowTV.setBackgroundDrawable(getResources().getDrawable(R.drawable.square_button_f));
		upComingShowTV.setBackgroundDrawable(getResources().getDrawable(R.drawable.square_button_f));
		clickTextView.setBackgroundDrawable(getResources().getDrawable(R.drawable.square_button));


	}

	@Override
	public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
		return false;
	}

	class ViewPagerAdapter extends FragmentPagerAdapter {

		private final MoviesListFragment mMoviesListFragment;
		private final EventsFragment mEventsFragment;
		private final SportsFragment mSportsFragment;
		private final LeisureFragment mLeisureFragment;

		ViewPagerAdapter(FragmentManager manager) {
			super(manager);
			mMoviesListFragment = new MoviesListFragment();
			mEventsFragment = new EventsFragment();
			mSportsFragment = new SportsFragment();
			mLeisureFragment = new LeisureFragment();
		}

		@Override
		public Fragment getItem(int position) {
			switch (position) {
				case 0: {
					return mMoviesListFragment;
				}
				case 1: {
					return mEventsFragment;
				}
				case 2: {
					return mSportsFragment;
				}
				case 3: {
					return mLeisureFragment;
				}
				default: {
					return null;
				}
			}
		}

		@Override
		public int getCount() {
			return 4;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			switch (position) {
				case 0: {
					return "Movies";
				}
				case 1: {
					return "Events";
				}
				case 2: {
					return "Sports";
				}
				case 3: {
					return "Leisure";
				}
				default:
					return "";
			}
		}
	}
}
