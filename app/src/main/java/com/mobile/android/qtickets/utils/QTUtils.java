package com.mobile.android.qtickets.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.preference.PreferenceManager;
import android.util.TypedValue;

import org.apache.http.HttpEntity;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Satish - 360 on 11/23/2016.
 */

public class QTUtils {

	private static QTUtils utils;

	public static QTUtils getInstance() {
		if (utils == null) {
			utils = new QTUtils();
		}
		return utils;
	}

	public static String getASCIIContentFromEntity(HttpEntity entity) {
		try {
			InputStream in = entity.getContent();
			StringBuffer out = new StringBuffer();
			int n = 1;
			while (n > 0) {
				byte[] b = new byte[4096];
				n = in.read(b);
				if (n > 0) out.append(new String(b, 0, n));
			}
			return out.toString();
		} catch (Exception ep) {
		}
		return "error";
	}


	public int dpToPx(float dp, Resources resources) {
		float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
				resources.getDisplayMetrics());
		return (int) px;
	}

	public String saveObjectToSharedPreference(Context context, String key, Object object) {
		try {
			String json = Api.toJson(object);
			QTUtils.getInstance().saveToSharedPreference(context, key, json);
			return json;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public Object getObjectFromSharedPreference(Context context, String key, Class<?> clazz) {
		try {
			String json = QTUtils.getInstance().getFromSharedPreference(context, key);
			if (json != null) {
				Object object = Api.fromJson(json, clazz);
				return object;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public void saveToSharedPreference(Context context, String key,
									   Object value) {
		String localValue;
		if (value != null && context != null) {
			localValue = value.toString();
		} else {
			localValue = null;
		}
		SharedPreferences sharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(context);
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putString(key, localValue);
		editor.commit();
	}

	/**
	 * Get String value from Shared Preference
	 *
	 * @param context
	 * @param key     The name of the preference to retrieve
	 * @return Value for the given preference if exist else null.
	 */
	public String getFromSharedPreference(Context context, String key) {

		SharedPreferences sharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(context);
		return sharedPreferences.getString(key, null);
	}
}
