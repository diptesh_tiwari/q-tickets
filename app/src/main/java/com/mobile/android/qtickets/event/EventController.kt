package com.mobile.android.qtickets.event

import `in`.hexalab.abstractvolley.base.HResponseListener
import android.content.Context
import com.mobile.android.qtickets.event.booking.BookEventRequest
import org.xmlpull.v1.XmlPullParser

/**
 * Created by hexalabssd on 27/03/17.
 */
class EventController(mContext: Context) {
    private val mEventModel = EventModel(mContext)

    fun bookEvent(listener: HResponseListener<XmlPullParser>) {
        listener.onRequestSubmitted()
        mEventModel.bookEvent(BookEventRequest(listener))
    }
}