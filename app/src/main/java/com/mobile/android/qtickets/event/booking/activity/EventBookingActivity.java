package com.mobile.android.qtickets.event.booking.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mobile.android.qtickets.R;
import com.mobile.android.qtickets.constants.AppConstants;
import com.mobile.android.qtickets.event.EventDetailsVO;
import com.mobile.android.qtickets.event.EventsDetailsActivity;
import com.mobile.android.qtickets.model.BookedDetal;
import com.mobile.android.qtickets.model.SelectEventType;
import com.mobile.android.qtickets.model.UserDetailsModel;
import com.mobile.android.qtickets.network.InternetConnectionDetector;
import com.mobile.android.qtickets.utils.Api;
import com.mobile.android.qtickets.utils.QTUtils;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.XML;

import java.util.ArrayList;
import java.util.List;

//import com.mobile.android.qtickets.PaymentGatewaysActivity;

public class EventBookingActivity extends EventBookingActivityController {
    //private static final String EXTRA_EVENT_BOOKING_INFO = EventBookingActivity.class.getName() + ".extra.EVENT_BOOKING_INFO";
    //private static final String EXTRA_EVENT_DETAILS = EventBookingActivity.class.getName() + ".extra.EVENT_DETAILS";
    EventDetailsVO eventDetailsVO;
    Double price = null;
    Integer person = null;
    UserDetailsModel user;
    String name, number, email, prifix;
    InternetConnectionDetector connectionDetector;
    boolean fromguest = false;
    private ProgressDialog dialog = null;
    private String noOftktPerid;
    private String eventMasterId;
    private Integer serverPrice;
    private BookedDetal bookedDetal;


    @Override
    public void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setPaymentDetailsListener(mPaymentDetailsListener);


    }


    @Override
    protected void initArguments(@Nullable final Bundle pBundle) {
        if (pBundle != null) {


            // mEventBookingInfo = pBundle.getParcelable(EXTRA_EVENT_BOOKING_INFO);
            //mEventDetailsVO = pBundle.getParcelable(EXTRA_EVENT_DETAILS);
        }
    }

    @Override
    protected void fillContentViews() {
        SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String pEventBookingInfoJson = mPrefs.getString("pEventBookingInfo", null);
        String selectedSeatJson = mPrefs.getString("Selected_Seat", null);
        List<SelectEventType> selectEventTypes = new ArrayList();


        String usrDetails = mPrefs.getString(AppConstants.SHARED_USER_TAG, null);


        List<String> strings = new ArrayList<>();
        try {
            //JSONObject  jsonObject= new JSONObject(selectedSeatJson);
            JSONArray json = new JSONArray(selectedSeatJson);

            for (int i = 0; i < json.length(); i++) {
                SelectEventType selectEventType = (SelectEventType) Api.fromJson(json.getString(i), SelectEventType.class);
                selectEventTypes.add(selectEventType);
                if(i==0){

                }
                if(selectEventType.getTktpriceid()!=null&& selectEventType.getCount()!=null){
                    eventMasterId = selectEventType.getEventMasterId();
                    String data = (selectEventType.getTktpriceid() + "-" + selectEventType.getCount() + "");
                    if(data!=null){
                        strings.add(data);
                    }
                }


            }

        } catch (Exception e) {
            e.printStackTrace();
        }


        noOftktPerid = getListAsCsvString(strings);

        eventDetailsVO = getIntent().getParcelableExtra(AppConstants.EVENT_DETAIL);

        Intent ii = getIntent();
        String from = ii.getStringExtra("fromGuest");


        if (usrDetails != null) {
            fromguest = false;
            Gson gson = new Gson();
            if (usrDetails != null) {
                java.lang.reflect.Type type = new TypeToken<UserDetailsModel>() {
                }.getType();
                try {
                    user = gson.fromJson(usrDetails, type);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }


        }else {
            fromguest = true;
            name = ii.getStringExtra("name");
            number = ii.getStringExtra("number");
            email = ii.getStringExtra("email");
            prifix = ii.getStringExtra("prifix");

        }

        try {
            JSONObject obj = new JSONObject(pEventBookingInfoJson);
            //price = obj.getDouble("totalCost");
            String totalCount= QTUtils.getInstance().getFromSharedPreference(this,AppConstants.TOTAL_COUNT);
            String totalCost= QTUtils.getInstance().getFromSharedPreference(this,AppConstants.TOTAL_PRICE);
            price=Double.parseDouble(totalCost);
            person=Integer.parseInt(totalCount);
            String coupanAmount=QTUtils.getInstance().getFromSharedPreference(EventBookingActivity.this,AppConstants.COUPAN_AMOUNT);

            if(coupanAmount!=null){
                int reduceAmountBal= Integer.parseInt(coupanAmount);
                price=price-reduceAmountBal;
                setTotalCost(price);
                serverPrice = price.intValue();
            }else {
                setTotalCost(price);
                serverPrice = price.intValue();
            }


          /*  setTotalCost(price);
            serverPrice = price.intValue();*/
            //person = obj.getInt("totalTicketsCount");
            setEventTitle(eventDetailsVO.eventname);
            setLocation(eventDetailsVO.Venue);
            if(QTUtils.getInstance().getFromSharedPreference(this,AppConstants.EVENET_SELECTED_DATE)!=null){
                String selectedDate=QTUtils.getInstance().getFromSharedPreference(this,AppConstants.EVENET_SELECTED_DATE);
                setDate(selectedDate,eventDetailsVO.StartTime);
            }else {
                setDate(eventDetailsVO.startDate,eventDetailsVO.StartTime);
            }


            //setTime(eventDetailsVO.StartTime);

            setNumberOfPersons(person);
        } catch (Exception e) {
            e.printStackTrace();
        }
        connectionDetector = new InternetConnectionDetector(this);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (connectionDetector.isConnectedToInternet()) {
                    // Do something after 2s = 2000ms
                    // new CountryListUrl().execute();

                    //new EventPaymentConfirmation().execute();
                }
            }
        }, 300);

    }


    public String getListAsCsvString(List<String> list) {

        StringBuilder sb = new StringBuilder();
        for (String str : list) {
            if (sb.length() != 0) {
                sb.append(",");
            }
            sb.append(str);
        }
        return sb.toString();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonLoginTV:
                new EventPaymentConfirmation().execute();
                break;
            case R.id.backBtn:
                onBackPressed();
                //finish();
                break;
        }
    }


    public void onBackPressed() {
        Intent intent = new Intent(this, EventsDetailsActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);

        finish();

    }

    public class EventPaymentConfirmation extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {


            String AppVersion = null;
            try {
                AppVersion = getApplicationContext().getPackageManager()
                        .getPackageInfo(getApplicationContext().getPackageName(), 0).versionName;
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }

            //  eventid=698&ticket_id=660&amount=100&tkt_count=2&NoOftktPerid=2224-2&email=ios@indglobal-consulting.com&name=rahul
            // &phone=1234567890&prefix=+355&bdate=2017-05-08&btime=6:30PM&balamount=0&camount=0&
            // couponcodes=&AppSource=3&AppVersion=2.3.2
            https:
//api.q-tickets.com/v2.1//eventbookings?
            try {
                String event_paymnet = "";
                if (user == null) {
                    event_paymnet = AppConstants.SERVER_URL_EVENT_PAYMENT + "eventid=" + eventDetailsVO.eventid + "&" + "ticket_id=" + eventMasterId + "&" + "amount=" + serverPrice +
                            "&" + "tkt_count=" + person + "&" + "NoOftktPerid=" + noOftktPerid + "&" + "camount=" + "0"
                            + "&" + "email=" + email + "&" + "name=" + name + "&" + "phone=" + number + "&" + "prefix=" + prifix + "&" + "bdate=" + eventDetailsVO.startDate + "&" +
                            "btime=" + eventDetailsVO.StartTime + "&" + "balamount=" + "0" + "&" + "couponcodes=" + "" + "&" + "AppSource=" + "3" + "&" + "AppVersion=" + AppVersion;
                } else {
                    event_paymnet = AppConstants.SERVER_URL_EVENT_PAYMENT + "eventid=" + eventDetailsVO.eventid + "&" + "ticket_id=" + eventMasterId + "&" + "amount=" + serverPrice +
                            "&" + "tkt_count=" + person + "&" + "NoOftktPerid=" + noOftktPerid + "&" + "camount=" + "0"
                            + "&" + "email=" + user.emailId + "&" + "name=" + user.userName + "&" + "phone=" + user.phoneNumber + "&" + "prefix=" + user.prefix + "&" + "bdate=" + eventDetailsVO.startDate + "&" +
                            "btime=" + eventDetailsVO.StartTime + "&" + "balamount=" + "0" + "&" + "couponcodes=" + "" + "&" + "AppSource=" + "3" + "&" + "AppVersion=" + AppVersion;

                }
                // showid=397665&seats=F04,F03&AppSource=3&movie_id=12800&schedule_date=04/27/2017&show_time=07:30 PM

                String resp;
                if (connectionDetector.isConnectedToInternet()) {
                    try {
                        event_paymnet = event_paymnet.replaceAll("\\s+", "%20");
                        HttpClient httpClient = new DefaultHttpClient();
                        HttpContext localContext = new BasicHttpContext();
                        HttpGet httpGet = new HttpGet(event_paymnet);
                        httpGet.addHeader(BasicScheme.authenticate(new UsernamePasswordCredentials(getResources().getString(R.string.auth_username), getResources().getString(R.string.auth_password)), "UTF-8", false));
                        HttpResponse response = httpClient.execute(httpGet, localContext);
                        HttpEntity entity = response.getEntity();
                        resp = QTUtils.getASCIIContentFromEntity(entity);
                        return resp;
                    } catch (Exception e) {
                        resp = "error";
                    }
                }
            } catch (Exception e) {

            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            initializeProgressDialog();

        }


        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            dialog.dismiss();
            try {
                if (result != null) {
                    // movieSeatBlockResult = new MovieSeatBlockResult();
                    JSONObject json = XML.toJSONObject(result);
                    JSONObject jsonObject = json.getJSONObject("response");
                    JSONObject resultRes = jsonObject.getJSONObject("result");
                    if (resultRes != null) {

                        if (resultRes.getString("status").equalsIgnoreCase("true")) {
                            String orderid = resultRes.getString("orderid");
                            String balanceamount = resultRes.getString("balanceamount");
                            bookedDetal = new BookedDetal();
                            bookedDetal.setOrderInfo(orderid);
                            bookedDetal.setBalance(Integer.valueOf(balanceamount));


                            if (bookedDetal != null && bookedDetal.getOrderInfo() != null) {
                                if (bookedDetal != null) {
                                    Intent intent = new Intent(EventBookingActivity.this, EventPaymentActivity.class);
                                    intent.putExtra(AppConstants.BOOKED_DETAIL, bookedDetal);
                                    startActivity(intent);
                                } else {
                                    Toast.makeText(EventBookingActivity.this, "Booking details are empty please try again later!", Toast.LENGTH_SHORT).show();
                                }
                            }


                        } else {
                            String errormsg = resultRes.getString("errormsg");
                            Log.d("MSG2", errormsg);
                            Toast.makeText(EventBookingActivity.this, errormsg, Toast.LENGTH_SHORT).show();
                        }


                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            //initNationalitySpinner();
        }


        private void initializeProgressDialog() {
            dialog = new ProgressDialog(EventBookingActivity.this, R.style.progress_bar_style);
            dialog.getWindow().setGravity(Gravity.CENTER);
            WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
            dialog.getWindow().setAttributes(params);
            dialog.show();
            dialog.setCancelable(true);
            dialog.setCanceledOnTouchOutside(true);
        }

    }

}
