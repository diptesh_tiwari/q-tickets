package com.mobile.android.qtickets.event.booking;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mobile.android.qtickets.R;
import com.mobile.android.qtickets.constants.AppConstants;
import com.mobile.android.qtickets.model.CountriesModel;
import com.mobile.android.qtickets.model.UserDetailsModel;
import com.mobile.android.qtickets.network.InternetConnectionDetector;
import com.mobile.android.qtickets.parsers.CountryParseOperation;
import com.mobile.android.qtickets.parsers.LoginParseOperation;
import com.mobile.android.qtickets.utils.QTUtils;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by hexalabssd on 21/03/17.
 */

public final class BookingLoginFragment extends Fragment {
	public static final String TAG = BookingLoginFragment.class.getName() + ".TAG";
	UserDetailsModel user;
	InternetConnectionDetector connectionDetector;
	ArrayList<CountriesModel> countryArr = new ArrayList<>();
	ArrayList<String> allCountryNationality = new ArrayList<>();
	ArrayList<String> allPrefix = new ArrayList<>();
	//private Button mProceedToPaymentButton;
	private TextView mLoginButton;
	private EditText et_username;
	private EditText et_password;
	private final View.OnClickListener mOnClickLoginButton = new View.OnClickListener() {
		@Override
		public void onClick(final View v) {
			final String emailAddresStr = et_username.getText().toString();
			if (TextUtils.isEmpty(emailAddresStr)) {
				et_username.setError("Please enter email address");
			} else if (!emailAddresStr.matches(AppConstants.REGEX_EMAIL_ADDRESS)) {
				et_username.setError("Please enter a valid email address");
			} else if (et_password.getText().toString().length() == 0) {
				et_password.setError("Please enter password");
			} else {
				new LoginUrl().execute();
			}
		}
	};
	private EditText et_name;
	private EditText et_phoneNumber;
	private Spinner spn_prefix;
	private TextView mPasswordWrapper;
	private OnProceedForPaymentListener mOnProceedForPaymentListener;
	private final View.OnClickListener mOnClickProceedPaymentButton = new View.OnClickListener() {
		@Override
		public void onClick(final View v) {
			if (mOnProceedForPaymentListener != null) {
				mOnProceedForPaymentListener.onClickProceedPaymentButton(user);
			}
		}
	};
	private String status, error_msg, errorcode;
	private ProgressDialog dialog = null;

	public static BookingLoginFragment newInstance() {
		return newInstance(null);
	}

	private static BookingLoginFragment newInstance(@Nullable final Bundle args) {
		final BookingLoginFragment fragment = new BookingLoginFragment();
		if (args != null) {
			fragment.setArguments(args);
		}
		return fragment;
	}

	@Override
	public void onCreate(@Nullable final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		connectionDetector = new InternetConnectionDetector(getContext());
		final Handler handler = new Handler();
		handler.postDelayed(new Runnable() {
			@Override
			public void run() {
				if (connectionDetector.isConnectedToInternet()) {
					// Do something after 2s = 2000ms
					new CountryListUrl().execute();
				}
			}
		}, 300);

	}

	@Nullable
	@Override
	public View onCreateView(final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable final Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_book_tickets_login, container, false);
	}

	@Override
	public void onViewCreated(final View view, @Nullable final Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		et_username = (EditText) view.findViewById(R.id.et_emailid_login);
		et_password = (EditText) view.findViewById(R.id.et_password_login);
		mLoginButton = (TextView) view.findViewById(R.id.button_login);
		et_name = (EditText) view.findViewById(R.id.et_name);
		et_phoneNumber = (EditText) view.findViewById(R.id.et_phoneNumber);
		spn_prefix = (Spinner) view.findViewById(R.id.spn_prefix);
		//mProceedToPaymentButton = (Button) view.findViewById(R.id.button_proceedPayment);
		mPasswordWrapper = (TextView) view.findViewById(R.id.wrapper_password);

		initContentViewListeners();
	}

	private void initContentViewListeners() {
		mLoginButton.setOnClickListener(mOnClickLoginButton);
		//mProceedToPaymentButton.setOnClickListener(mOnClickProceedPaymentButton);
	}

	public void setOnProceedForPaymentListener(final OnProceedForPaymentListener pOnProceedForPaymentListener) {
		mOnProceedForPaymentListener = pOnProceedForPaymentListener;
	}

	private void initializeProgressDialog() {
		dialog = new ProgressDialog(getContext(), R.style.progress_bar_style);
		dialog.getWindow().setGravity(Gravity.CENTER);
		WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
		dialog.getWindow().setAttributes(params);
		dialog.show();
		dialog.setCancelable(true);
		dialog.setCanceledOnTouchOutside(true);
	}

	private void initPrefixSpinner() {

		SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(getContext());
		Gson gson = new Gson();
		String country = mPrefs.getString(AppConstants.SHARED_COUNTRIES_TAG, null);
		if (country != null) {
			java.lang.reflect.Type type = new TypeToken<List<CountriesModel>>() {
			}.getType();
			countryArr = gson.fromJson(country, type);
			if (countryArr.size() != 0) {
				bindSpinners();
			}
		}
	}

	private void bindSpinners() {
		for (int i = 0; i < countryArr.size(); i++) {
			allCountryNationality.add(countryArr.get(i).CountryNationality);
			allPrefix.add("+" + countryArr.get(i).Countryprefix);
		}
		setupPrefixAdapter(allPrefix);
	}

	private void setupPrefixAdapter(ArrayList<String> allPrefixes) {
		// Creating adapter for spinner
		ArrayAdapter<String> countryAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, allPrefixes);
		// Drop down layout style - list view with radio button
		countryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		// attaching data adapter to spinner
		spn_prefix.setAdapter(countryAdapter);
	}

	private void hideLoginOptions() {
		mPasswordWrapper.setVisibility(View.GONE);
		mLoginButton.setVisibility(View.GONE);
		et_username.setEnabled(false);
	}

	protected void parseUserLoginResponse(String result) {
		// TODO Auto-generated method stub
		try {
			//two lines of code for xmlpullparsing
			ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(result.getBytes());
			LoginParseOperation xmlpull = new LoginParseOperation(byteArrayInputStream);
			user = xmlpull.user;
			status = xmlpull.status;
			errorcode = xmlpull.errorCode;
			error_msg = xmlpull.errormsg;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	protected ArrayList<CountriesModel> parseCountryResponse(String result) {
		// TODO Auto-generated method stub
		ArrayList<CountriesModel> conty = null;
		try {
			//two lines of code for xmlpullparsing
			ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(result.getBytes());
			CountryParseOperation xmlpull = new CountryParseOperation(byteArrayInputStream);
			conty = xmlpull.countryArr;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return conty;
	}

	public interface OnProceedForPaymentListener {
		void onClickProceedPaymentButton(@Nullable final UserDetailsModel pUserDetailsModel);
	}

	private class LoginUrl extends AsyncTask<Void, Void, Void> {
		private String username;
		private String password;

		@Override
		protected Void doInBackground(Void... arg0) {
			String resp;
			try {
				final String GCMTOCKEN = "";
				String login_url = AppConstants.SERVICE_URL_GET_USER_LOGIN + AppConstants.SERVICE_MSG_GET_USER_LOGIN_EMAIL + username + AppConstants.SERVICE_MSG_GET_USER_LOGIN_PASSWORD + password + AppConstants.SERVICE_MSG_GET_USER_LOGIN_SOURCE + "4" + AppConstants.SERVICE_MSG_GET_USER_LOGIN_TOCKEN + GCMTOCKEN;
				login_url = login_url.replaceAll("\\s+", "%20");
				HttpClient httpClient = new DefaultHttpClient();
				HttpContext localContext = new BasicHttpContext();
				HttpGet httpGet = new HttpGet(login_url);
				httpGet.addHeader(BasicScheme.authenticate(new UsernamePasswordCredentials(getResources().getString(R.string.auth_username), getResources().getString(R.string.auth_password)), "UTF-8", false));
				HttpResponse response = httpClient.execute(httpGet, localContext);
				HttpEntity entity = response.getEntity();
				resp = QTUtils.getASCIIContentFromEntity(entity);
				parseUserLoginResponse(resp);
			} catch (Exception e) {

			}
			return null;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			initializeProgressDialog();
			username = et_username.getText().toString().trim();
			password = et_password.getText().toString().trim();
		}


		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			dialog.dismiss();
			try {
				if (status.equalsIgnoreCase(AppConstants.RESPONSE_TRUE_TAG)) {
					SharedPreferences prefmPrefs = PreferenceManager.getDefaultSharedPreferences(getContext());
					SharedPreferences.Editor editor = prefmPrefs.edit();
					Gson gson = new Gson();
					String userdata = gson.toJson(user);
					editor.putString(AppConstants.SHARED_USER_TAG, userdata);
					editor.commit();

					hideLoginOptions();
					et_name.setText(user.userName);
					et_phoneNumber.setText(user.phoneNumber);
					final ArrayAdapter<String> prefixAdapter = (ArrayAdapter<String>) spn_prefix.getAdapter();
					spn_prefix.setSelection(prefixAdapter.getPosition("+" + user.prefix));
				} else if (status.equalsIgnoreCase(AppConstants.RESPONSE_FALSE_TAG)) {
					new AlertDialog.Builder(getContext()).setMessage(error_msg).setPositiveButton("OK", null).show();
				}

			} catch (Exception e) {
				Toast.makeText(getContext(), "Server Not responding..", Toast.LENGTH_SHORT).show();
				e.printStackTrace();
			}

		}

	}

	public class CountryListUrl extends AsyncTask<Void, Void, Void> {
		@Override
		protected Void doInBackground(Void... arg0) {
			try {
				String resp;
				try {
					String country_url = AppConstants.SERVER_URL_GET_ALL_COUNTRIES;
					HttpClient httpClient = new DefaultHttpClient();
					HttpContext localContext = new BasicHttpContext();
					HttpGet httpGet = new HttpGet(country_url);
					httpGet.addHeader(BasicScheme.authenticate(new UsernamePasswordCredentials(getResources().getString(R.string.auth_username), getResources().getString(R.string.auth_password)), "UTF-8", false));
					HttpResponse response = httpClient.execute(httpGet, localContext);
					HttpEntity entity = response.getEntity();
					resp = QTUtils.getASCIIContentFromEntity(entity);
					countryArr = parseCountryResponse(resp);
				} catch (Exception e) {
					e.printStackTrace();
				}
			} catch (Exception e) {

			}
			return null;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			initializeProgressDialog();

		}


		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			dialog.dismiss();
			try {
				if (countryArr.size() > 0) {
					SharedPreferences prefmPrefs = PreferenceManager.getDefaultSharedPreferences(getContext());
					SharedPreferences.Editor editor = prefmPrefs.edit();
					Gson gson = new Gson();
					String countryjson = gson.toJson(countryArr);
					editor.putString(AppConstants.SHARED_COUNTRIES_TAG, countryjson);
					editor.commit();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			initPrefixSpinner();
		}

	}


}
