package com.mobile.android.qtickets.utils;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.TextView;

import com.mobile.android.qtickets.PdfViewer;
import com.mobile.android.qtickets.R;
import com.mobile.android.qtickets.activities.LoginActivity;
import com.mobile.android.qtickets.constants.AppConstants;
import com.mobile.android.qtickets.event.EventsDetailsActivity;
import com.mobile.android.qtickets.interfes.OnEventCallBack;


public class MyDialog {

    public static ProgressDialog showProgress(Context context) {
        ProgressDialog mdialog = ProgressDialog.show(context, "", "", true);
        mdialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View localView = inflater.inflate(R.layout.progress_bar, null);
        mdialog.setContentView(localView);
        return mdialog;
    }

    public static void showLogInDialog(final Context context, final OnEventCallBack onEventCallBack) {
        final Dialog accountDialog = new Dialog(context);
        accountDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        accountDialog.setContentView(R.layout.alert_dialog);
        accountDialog.setCanceledOnTouchOutside(false);
        TextView loginTV = (TextView) accountDialog.findViewById(R.id.loginTV);
        TextView singnUpTV = (TextView) accountDialog.findViewById(R.id.singnUpTV);
        TextView facebookLoginTV = (TextView) accountDialog.findViewById(R.id.facebookLoginTV);
        TextView tvGuest = (TextView)accountDialog.findViewById(R.id.tvGuest);

        loginTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onEventCallBack.onEvent(R.id.loginTV);
               accountDialog.dismiss();
            }
        });
        singnUpTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onEventCallBack.onEvent(R.id.singnUpTV);
                accountDialog.dismiss();
            }
        });

        facebookLoginTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onEventCallBack.onEvent(R.id.loginTV);
                accountDialog.dismiss();
            }
        });

        tvGuest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onEventCallBack.onEvent(R.id.tvGuest);
                accountDialog.dismiss();
            }
        });
        accountDialog.show();

        accountDialog.show();
        Display display = ((Activity) context).getWindowManager()
                .getDefaultDisplay();
        int width = display.getWidth() - 80;
        int height = display.getHeight() - 200;
        Window window = accountDialog.getWindow();
        window.setLayout(width, ActionBar.LayoutParams.WRAP_CONTENT);
    }





    public static void showWatchDontWatchDialog(final Context context, final OnEventCallBack onEventCallBack) {
        final Dialog accountDialog = new Dialog(context);
        accountDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        accountDialog.setContentView(R.layout.watch_dont_watch_alert_dialog);
        accountDialog.setCanceledOnTouchOutside(false);
        TextView loginTV = (TextView) accountDialog.findViewById(R.id.loginTV);
        TextView singnUpTV = (TextView) accountDialog.findViewById(R.id.singnUpTV);
        TextView facebookLoginTV = (TextView) accountDialog.findViewById(R.id.facebookLoginTV);
       // TextView tvGuest = (TextView)accountDialog.findViewById(R.id.tvGuest);

        loginTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onEventCallBack.onEvent(R.id.loginTV);
                accountDialog.dismiss();
            }
        });
        singnUpTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onEventCallBack.onEvent(R.id.singnUpTV);
                accountDialog.dismiss();
            }
        });

        facebookLoginTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onEventCallBack.onEvent(R.id.loginTV);
                accountDialog.dismiss();
            }
        });
        accountDialog.show();

        accountDialog.show();
        Display display = ((Activity) context).getWindowManager()
                .getDefaultDisplay();
        int width = display.getWidth() - 80;
        int height = display.getHeight() - 200;
        Window window = accountDialog.getWindow();
        window.setLayout(width, ActionBar.LayoutParams.WRAP_CONTENT);
    }


    public static void showRegistrationFrom(final Context context) {


        final Dialog accountDialog = new Dialog(context);
        accountDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        accountDialog.setContentView(R.layout.registration_form_layot);
        accountDialog.setCanceledOnTouchOutside(false);
        TextView individualApplicationTV = (TextView) accountDialog.findViewById(R.id.individualApplicationTV);
        TextView compositionApplicationTV = (TextView) accountDialog.findViewById(R.id.compositionApplicationTV);
        TextView choirEnsembleApplicationTV = (TextView) accountDialog.findViewById(R.id.choirEnsembleApplicationTV);

        individualApplicationTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(context, PdfViewer.class);
                intent.putExtra(AppConstants.DATA,"http://qnmc.org/wp-content/uploads/2017/05/Application-1.pdf");
                context.startActivity(intent);
                accountDialog.dismiss();

            }
        });

        compositionApplicationTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent= new Intent(context, PdfViewer.class);
                intent.putExtra(AppConstants.DATA,"http://qnmc.org/wp-content/uploads/2017/05/Application-22-2.pdf");
                context.startActivity(intent);
                accountDialog.dismiss();
            }

        });

        choirEnsembleApplicationTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(context, PdfViewer.class);
                intent.putExtra(AppConstants.DATA,"http://qnmc.org/wp-content/uploads/2017/05/Application-3.pdf");
                context.startActivity(intent);
                accountDialog.dismiss();

            }
        });

        accountDialog.show();
        Display display = ((Activity) context).getWindowManager()
                .getDefaultDisplay();
        int width = display.getWidth() - 80;
        int height = display.getHeight() - 200;
        Window window = accountDialog.getWindow();
        window.setLayout(width, ActionBar.LayoutParams.WRAP_CONTENT);
    }

    public static void showBoatDialog(final Context context,final OnEventCallBack onEventCallBack) {

        final Dialog accountDialog = new Dialog(context);
        accountDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        accountDialog.setContentView(R.layout.registration_form_layot_single);
        accountDialog.setCanceledOnTouchOutside(false);
        TextView boatOption1TV = (TextView) accountDialog.findViewById(R.id.boatOption1TV);
        TextView boatOption2TV = (TextView) accountDialog.findViewById(R.id.boatOption2TV);
        TextView boatOption3TV = (TextView) accountDialog.findViewById(R.id.boatOption3TV);

        boatOption1TV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onEventCallBack.onEvent(R.id.boatOption1TV);
                accountDialog.dismiss();

            }
        });
        boatOption2TV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onEventCallBack.onEvent(R.id.boatOption2TV);
                accountDialog.dismiss();

            }
        });

        boatOption3TV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onEventCallBack.onEvent(R.id.boatOption3TV);
                accountDialog.dismiss();

            }
        });

        accountDialog.show();
        Display display = ((Activity) context).getWindowManager()
                .getDefaultDisplay();
        int width = display.getWidth() - 80;
        int height = display.getHeight() - 200;
        Window window = accountDialog.getWindow();
        window.setLayout(width, ActionBar.LayoutParams.WRAP_CONTENT);
    }

    public static void showtimeDialog(final Context context,final OnEventCallBack onEventCallBack) {
        final Dialog accountDialog = new Dialog(context);
        accountDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        accountDialog.setContentView(R.layout.select_time_dialog);
        accountDialog.setCanceledOnTouchOutside(false);
        TextView firstTimeTV = (TextView) accountDialog.findViewById(R.id.firstTimeTV);
        TextView secondTimeTV = (TextView) accountDialog.findViewById(R.id.secondTimeTV);


        firstTimeTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onEventCallBack.onEvent(R.id.firstTimeTV);
                accountDialog.dismiss();

            }
        });
        secondTimeTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onEventCallBack.onEvent(R.id.secondTimeTV);
                accountDialog.dismiss();

            }
        });

        accountDialog.show();
        Display display = ((Activity) context).getWindowManager()
                .getDefaultDisplay();
        int width = display.getWidth() - 80;
        int height = display.getHeight() - 200;
        Window window = accountDialog.getWindow();
        window.setLayout(width, ActionBar.LayoutParams.WRAP_CONTENT);
    }

    public static void showDayTypeDialog(final Context context,final OnEventCallBack onEventCallBack) {

        final Dialog accountDialog = new Dialog(context);
        accountDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        accountDialog.setContentView(R.layout.select_day_type_dialog);
        accountDialog.setCanceledOnTouchOutside(false);
        //firstHalfDayTV,secondHalfDayTV,firstFullDayTV,secondFullDayTV,nightTV
        TextView firstHalfDayTV = (TextView) accountDialog.findViewById(R.id.firstHalfDayTV);
        TextView secondHalfDayTV = (TextView) accountDialog.findViewById(R.id.secondHalfDayTV);
        TextView firstFullDayTV = (TextView) accountDialog.findViewById(R.id.firstFullDayTV);
        TextView secondFullDayTV = (TextView) accountDialog.findViewById(R.id.secondFullDayTV);
        TextView nightTV = (TextView) accountDialog.findViewById(R.id.nightTV);



        firstHalfDayTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onEventCallBack.onEvent(R.id.firstHalfDayTV);
                accountDialog.dismiss();

            }
        });
        secondHalfDayTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onEventCallBack.onEvent(R.id.secondHalfDayTV);
                accountDialog.dismiss();

            }
        });

        firstFullDayTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onEventCallBack.onEvent(R.id.firstFullDayTV);
                accountDialog.dismiss();

            }
        });
        secondFullDayTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onEventCallBack.onEvent(R.id.secondFullDayTV);
                accountDialog.dismiss();

            }
        });








        nightTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onEventCallBack.onEvent(R.id.nightTV);
                accountDialog.dismiss();

            }
        });

        accountDialog.show();
        Display display = ((Activity) context).getWindowManager()
                .getDefaultDisplay();
        int width = display.getWidth() - 80;
        int height = display.getHeight() - 200;
        Window window = accountDialog.getWindow();
        window.setLayout(width, ActionBar.LayoutParams.WRAP_CONTENT);
    }

    public static void showDatesDialog(final Context context,final OnEventCallBack onEventCallBack ,String[] arrFriday) {

        final Dialog accountDialog = new Dialog(context);
        accountDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        accountDialog.setContentView(R.layout.date_dialog);
        accountDialog.setCanceledOnTouchOutside(false);


       GridView  gridView = (GridView)accountDialog.findViewById(R.id.gridView1);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context,
                android.R.layout.simple_list_item_1, arrFriday);

        gridView.setAdapter(adapter);


        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
                String selectedEvents = parent.getItemAtPosition(position).toString();

                QTUtils.getInstance().saveToSharedPreference(context, AppConstants.EVENET_SELECTED_DATE, selectedEvents);
                onEventCallBack.onEvent(R.id.gridView1,selectedEvents);
                accountDialog.dismiss();
            }
        });



        accountDialog.show();
        Display display = ((Activity) context).getWindowManager()
                .getDefaultDisplay();
        int width = display.getWidth() - 80;
        int height = display.getHeight() - 200;
        Window window = accountDialog.getWindow();
        window.setLayout(width, ActionBar.LayoutParams.WRAP_CONTENT);
    }
}
