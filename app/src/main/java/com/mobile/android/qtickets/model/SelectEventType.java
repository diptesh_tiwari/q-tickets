package com.mobile.android.qtickets.model;

import java.io.Serializable;

/**
 * Created by Indglobal on 5/1/2017.
 */

public class SelectEventType implements Serializable {
    private  String tktpriceid;
    private  Integer count;
    private String eventMasterId;

    public String getEventMasterId() {
        return eventMasterId;
    }

    public void setEventMasterId(String eventMasterId) {
        this.eventMasterId = eventMasterId;
    }

    public String getTktpriceid() {
        return tktpriceid;
    }

    public void setTktpriceid(String tktpriceid) {
        this.tktpriceid = tktpriceid;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}
