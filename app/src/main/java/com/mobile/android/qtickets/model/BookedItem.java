package com.mobile.android.qtickets.model;


/**
 * Created by Android on 4/27/17.
 */

public class BookedItem {

    public String id, movie, theater, area, bookedtime, showdate,seats, seatsCount, confirmationCode,
            barcodeURL, ticket_Cost, total_Cost, bookingfees,movieImageURL,moviebannerURL,movieServerID,
            checkcancelstatus;

    public BookedItem(String id, String movie, String theater, String area, String bookedtime, String showdate,
                      String seats, String seatsCount, String confirmationCode, String barcodeURL,
                      String ticket_Cost, String total_Cost, String bookingfees, String movieImageURL,
                      String moviebannerURL, String movieServerID, String checkcancelstatus) {
        this.id = id;
        this.movie = movie;
        this.theater = theater;
        this.area = area;
        this.bookedtime = bookedtime;
        this.showdate = showdate;
        this.seats = seats;
        this.seatsCount = seatsCount;
        this.confirmationCode = confirmationCode;
        this.barcodeURL = barcodeURL;
        this.ticket_Cost = ticket_Cost;
        this.total_Cost = total_Cost;
        this.bookingfees = bookingfees;
        this.movieImageURL = movieImageURL;
        this.moviebannerURL = moviebannerURL;
        this.movieServerID = movieServerID;
        this.checkcancelstatus = checkcancelstatus;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMovie() {
        return movie;
    }

    public void setMovie(String movie) {
        this.movie = movie;
    }

    public String getTheater() {
        return theater;
    }

    public void setTheater(String theater) {
        this.theater = theater;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getBookedtime() {
        return bookedtime;
    }

    public void setBookedtime(String bookedtime) {
        this.bookedtime = bookedtime;
    }

    public String getShowdate() {
        return showdate;
    }

    public void setShowdate(String showdate) {
        this.showdate = showdate;
    }

    public String getSeats() {
        return seats;
    }

    public void setSeats(String seats) {
        this.seats = seats;
    }

    public String getSeatsCount() {
        return seatsCount;
    }

    public void setSeatsCount(String seatsCount) {
        this.seatsCount = seatsCount;
    }

    public String getConfirmationCode() {
        return confirmationCode;
    }

    public void setConfirmationCode(String confirmationCode) {
        this.confirmationCode = confirmationCode;
    }

    public String getBarcodeURL() {
        return barcodeURL;
    }

    public void setBarcodeURL(String barcodeURL) {
        this.barcodeURL = barcodeURL;
    }

    public String getTicket_Cost() {
        return ticket_Cost;
    }

    public void setTicket_Cost(String ticket_Cost) {
        this.ticket_Cost = ticket_Cost;
    }

    public String getTotal_Cost() {
        return total_Cost;
    }

    public void setTotal_Cost(String total_Cost) {
        this.total_Cost = total_Cost;
    }

    public String getBookingfees() {
        return bookingfees;
    }

    public void setBookingfees(String bookingfees) {
        this.bookingfees = bookingfees;
    }

    public String getMovieImageURL() {
        return movieImageURL;
    }

    public void setMovieImageURL(String movieImageURL) {
        this.movieImageURL = movieImageURL;
    }

    public String getMoviebannerURL() {
        return moviebannerURL;
    }

    public void setMoviebannerURL(String moviebannerURL) {
        this.moviebannerURL = moviebannerURL;
    }

    public String getMovieServerID() {
        return movieServerID;
    }

    public void setMovieServerID(String movieServerID) {
        this.movieServerID = movieServerID;
    }

    public String getCheckcancelstatus() {
        return checkcancelstatus;
    }

    public void setCheckcancelstatus(String checkcancelstatus) {
        this.checkcancelstatus = checkcancelstatus;
    }
}
