package com.mobile.android.qtickets.movie.list;

import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mobile.android.qtickets.R;
import com.mobile.android.qtickets.movie.Movie;

import java.util.Locale;

/**
 * Created by hexalabssd on 01/03/17.
 */

final class MovieVerticalSmallItemViewHolder extends MovieItemViewHolder {
	public TextView mLanguageTextView,mVisionTypeTextView,mVotesTextView,bookTV,ratingTV;
	private LinearLayout ratingLL;

	private String movieType;
	private Movie movie;

	public Movie getMovie() {
		return movie;
	}

	public void setMovie(Movie movie) {
		this.movie = movie;
	}

	public String getMovieType() {

		return movieType;
	}

	public void setMovieType(String movieType) {

		this.movieType = movieType;
	}

	MovieVerticalSmallItemViewHolder(final ViewGroup parent) {
		super(R.layout.item_view_movie_vertical_small, parent);

		mLanguageTextView = (TextView) itemView.findViewById(R.id.textView_language);
		mVisionTypeTextView = (TextView) itemView.findViewById(R.id.textView_visionType);
		mVotesTextView = (TextView) itemView.findViewById(R.id.textView_votes);
		bookTV = (TextView) itemView.findViewById(R.id.bookTV);
		ratingTV = (TextView) itemView.findViewById(R.id.ratingTV);
		ratingLL = (LinearLayout) itemView.findViewById(R.id.ratingLL);

		/*if(movieType!=null&&movieType.equalsIgnoreCase("nowShow")){
			mLanguageTextView.setVisibility(View.VISIBLE);
			mVisionTypeTextView.setVisibility(View.VISIBLE);
			mVotesTextView.setVisibility(View.VISIBLE);
			bookTV.setVisibility(View.VISIBLE);


		}else if(movieType!=null&&movieType.equalsIgnoreCase("upComingShow")) {
			mLanguageTextView.setVisibility(View.VISIBLE);
			mVisionTypeTextView.setVisibility(View.GONE);
			mVotesTextView.setVisibility(View.GONE);
			bookTV.setVisibility(View.GONE);
		}else {
			mLanguageTextView.setVisibility(View.VISIBLE);
			mVisionTypeTextView.setVisibility(View.VISIBLE);
			mVotesTextView.setVisibility(View.VISIBLE);
			bookTV.setVisibility(View.VISIBLE);
		}*/
	}

	void setLanguage(final String pLanguage) {
		mLanguageTextView.setText(pLanguage);
	}

	void setVisionType(final String pVisionType) {

		mVisionTypeTextView.setText(pVisionType);
	}

	void setVotes(final int pVotes) {
		mVotesTextView.setVisibility(View.GONE);
		if(pVotes==0){
			mVotesTextView.setVisibility(View.GONE);
		}else {
			mVotesTextView.setText(String.format(Locale.getDefault(), "%d votes", pVotes));
			ratingTV.setText(pVotes+"/"+10);
		}

	}

	public void setDate(String s) {

		if (!TextUtils.isEmpty(s)) {

			ratingLL.setVisibility(View.VISIBLE);

		} else {
			ratingLL.setVisibility(View.GONE);
		}
	}

	public void setTheater(String s) {

		if(!TextUtils.isEmpty(s)&& s.length()!=0){
			bookTV.setVisibility(View.VISIBLE);
			mVotesTextView.setVisibility(View.GONE);
		}else {
			bookTV.setVisibility(View.GONE);
		}
/*



//7/6/2017 12:00:00 AM

		Long releaseDate=(long) 0.0;
		long todayDate =System.currentTimeMillis();
		//Long getInMillies= (long) 0.0;
		try {
			releaseDate=DateTime.getInMillies(s);

		} catch (ParseException e) {
			e.printStackTrace();
		}

		Date release= new Date();
		release.setTime(releaseDate);
		Date today= new Date();
		today.setTime(todayDate);


		if(!release.after(today)){
			bookTV.setVisibility(View.GONE);
		}else {
			bookTV.setVisibility(View.VISIBLE);
		}*/

	}

	public void showVotes(Movie movie) {
		int watch=0;
		int notwatch=0;
		int total=0;
		if(!TextUtils.isEmpty(movie.willwatch)){
			watch = Integer.parseInt(movie.willwatch);
		}
		if(!TextUtils.isEmpty(movie.willnotwatch)){
			notwatch = Integer.parseInt(movie.willnotwatch);
		}
		total=watch+notwatch;
		mVotesTextView.setText(""+total+" votes");
		mVotesTextView.setVisibility(View.VISIBLE);

	}
}
