package com.mobile.android.qtickets.movie.seatselection.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.gson.Gson;
import com.mobile.android.qtickets.R;
import com.mobile.android.qtickets.activities.GuestActivity;
import com.mobile.android.qtickets.activities.LoginActivity;
import com.mobile.android.qtickets.activities.RegisterActivity;
import com.mobile.android.qtickets.constants.AppConstants;
import com.mobile.android.qtickets.interfes.OnEventCallBack;
import com.mobile.android.qtickets.movie.seatselection.SeatLayoutResponse;
import com.mobile.android.qtickets.movie.seatselection.SeatsClass;
import com.mobile.android.qtickets.movie.seatselection.SeatsClassList;
import com.mobile.android.qtickets.movie.seatselection.SeatsRow;
import com.mobile.android.qtickets.movie.seatselection.SelectedMovieDetail;
import com.mobile.android.qtickets.utils.MyDialog;

import java.util.ArrayList;
import java.util.List;

import in.hexalab.abstractvolley.base.HResponseListener;

/**
 * Created by hexalabssd on 02/02/17.
 */

public final class MovieSeatsSelectionActivity extends MovieSeatsSelectionActivityController implements OnEventCallBack{
	private static final String EXTRA_MOVIE_NAME = MovieSeatsSelectionActivity.class.getName() + "extra.MOVIE_NAME";
	private static final String EXTRA_MOVIE_THEATRE = MovieSeatsSelectionActivity.class.getName() + "extra.MOVIE_THEATRE";
	private static final String EXTRA_MOVIE_DATE = MovieSeatsSelectionActivity.class.getName() + "extra.MOVIE_DATE";
	private static final String EXTRA_MOVIE_TIME = MovieSeatsSelectionActivity.class.getName() + "extra.MOVIE_TIME";
	private static final String EXTRA_MOVIE_ID = MovieSeatsSelectionActivity.class.getName() + "extra.MOVIE_ID";
	private static final String EXTRA_SHOW_TIME_ID = MovieSeatsSelectionActivity.class.getName() + "extra.SHOW_TIME_ID";
	private final HResponseListener<SeatLayoutResponse> mSeatLayoutResponse = new HResponseListener<SeatLayoutResponse>() {
		@Override
		public void onRequestSubmitted() {
			showLoadingDialog("");
		}

		@Override
		public void onErrorResponse(final VolleyError error) {
			hideLoadingDialog();
			error.printStackTrace();
		}

		@Override
		public void onResponse(final SeatLayoutResponse response) {
			if (response.getStatus().contentEquals(Boolean.TRUE.toString())) {
				populateSeats(response.getClasses());
			}
			hideLoadingDialog();
		}
	};
	private String mMovieName;
	private String mMovieId;
	private String mMovieTheatre;
	private String mMovieDate;
	private String mMovieTime;
	private String mShowTimeId;
	/**
	 * ATTENTION: This was auto-generated to implement the App Indexing API. See
	 * https://g.co/AppIndexing/AndroidStudio for more information.
	 */
	private GoogleApiClient client;
	private List<String> mSelectedSeatsList;
	private List<Double> mCostList;
	private OnSeatSelectionChangedListener mOnSeatSelectionChangedListener = new OnSeatSelectionChangedListener() {
	/*	@Override
		public void onSeatSelected(final String seat) {
			mSelectedSeatsList.add(seat);
			tvSeatSlctdNmbrs.setText(mSelectedSeatsList.size()+"");
		}


		@Override
		public void onSeatUnselected(final String seat) {

			mSelectedSeatsList.remove(seat);
			tvSeatSlctdNmbrs.setText(mSelectedSeatsList.size()+"");
		}*/

		@Override
		public void onSeatSelected(String seat, Double mCost) {
			mSelectedSeatsList.add(seat);
			mCostList.add(mCost);
			tvSeatSlctdNmbrs.setText(mSelectedSeatsList.size()+"");
		}

		@Override
		public void onSeatUnselected(final String seat, Double price) {

			mSelectedSeatsList.remove(seat);
			mCostList.remove(price);
			tvSeatSlctdNmbrs.setText(mSelectedSeatsList.size()+"");
		}
	};

	public static void start(Context context, String movie_id, String movieName, String movieTheatre, String movieTime, String showTimeId) {
		final Bundle bundle = new Bundle();
		bundle.putString(EXTRA_MOVIE_NAME, movieName);
		bundle.putString(EXTRA_MOVIE_ID, movie_id);
		bundle.putString(EXTRA_MOVIE_NAME, movieName);
		bundle.putString(EXTRA_MOVIE_THEATRE, movieTheatre);
		//bundle.putString(EXTRA_MOVIE_DATE, movieDate);
		bundle.putString(EXTRA_MOVIE_TIME, movieTime);
		bundle.putString(EXTRA_SHOW_TIME_ID, showTimeId);
		start(context, bundle);


	}

	private static void start(Context context, Bundle pBundle) {
		final Intent starter = new Intent(context, MovieSeatsSelectionActivity.class);
		starter.putExtras(pBundle);
		context.startActivity(starter);
	}

	@Override
	public void onClickConfirmBookingButton(final View pView) {

		SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		String usrDetails = mPrefs.getString(AppConstants.SHARED_USER_TAG,null);

		Intent intent = new Intent(this, GuestActivity.class);

		double mMoviesCost = Double.valueOf(00);

		for (int i = 0; i < mCostList.size(); i++) {
			mMoviesCost = mMoviesCost + mCostList.get(i).doubleValue();

		}
		intent.putExtra("TOTAL Amount", mMoviesCost);

		if(mSelectedSeatsList.isEmpty()) {
			Toast.makeText(this, "Select at least one seat to proceed.", Toast.LENGTH_SHORT).show();
		}else {

			if (usrDetails != null) {
				if (mSelectedSeatsList.isEmpty()) {
					Toast.makeText(this, "Select at least one seat to proceed.", Toast.LENGTH_SHORT).show();
				} else {
					intent = new Intent(this, GuestActivity.class);
					intent.putExtra("EXTRA_MOVIE_NAME", mMovieName);
					intent.putExtra("EXTRA_MOVIE_ID", mMovieId);
					intent.putExtra("EXTRA_MOVIE_THEATRE", mMovieTheatre);
					//intent.putExtra("EXTRA_MOVIE_DATE", mMovieDate);
					intent.putExtra("EXTRA_MOVIE_TIME", mMovieTime);
					intent.putExtra("EXTRA_SHOW_TIME_ID", mShowTimeId);
					intent.putExtra("TOTAL PERSON", (ArrayList) mSelectedSeatsList);
					SharedPreferences prefmPrefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
					SharedPreferences.Editor editor = prefmPrefs.edit();
					editor.putString(AppConstants.TYPE, "MOVIE");
					editor.commit();

					intent.putExtra("TOTAL Amount", mMoviesCost);
					startActivity(intent);
				}
			} else {
				MyDialog.showLogInDialog(this, this);
			}
		}

	}

	@Override
	protected void onCreate(@Nullable final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// ATTENTION: This was auto-generated to implement the App Indexing API.
		// See https://g.co/AppIndexing/AndroidStudio for more information.
		client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
		fetchSeatLayout(mShowTimeId);
		mSelectedSeatsList = new ArrayList<>();
		mCostList = new ArrayList<>();
		setOnSeatSelectionChangedListener(mOnSeatSelectionChangedListener);
	}

	@Override
	protected void initActionBar() {

	}

	private void fetchSeatLayout(final String pShowTimeId) {
		mMovieController.fetchSeatLayout(pShowTimeId, mSeatLayoutResponse);
	}

	@Override
	protected void initArguments(@Nullable final Bundle pBundle) {
		mMovieName = pBundle.getString(EXTRA_MOVIE_NAME);
		mMovieId = pBundle.getString(EXTRA_MOVIE_ID);
		mMovieTheatre = pBundle.getString(EXTRA_MOVIE_THEATRE);
		mMovieDate = pBundle.getString(EXTRA_MOVIE_DATE);
		mMovieTime = pBundle.getString(EXTRA_MOVIE_TIME);
		mShowTimeId = pBundle.getString(EXTRA_SHOW_TIME_ID);
	}

	@Override
	protected void fillContentViews() {
	}

	@Override
	public void onStart() {
		super.onStart();

		// ATTENTION: This was auto-generated to implement the App Indexing API.
		// See https://g.co/AppIndexing/AndroidStudio for more information.
		client.connect();
		AppIndex.AppIndexApi.start(client, getIndexApiAction());
	}

	/**
	 * ATTENTION: This was auto-generated to implement the App Indexing API. See
	 * https://g.co/AppIndexing/AndroidStudio for more information.
	 */
	public Action getIndexApiAction() {
		Thing object = new Thing.Builder().setName("QTBookMovieTickets Page") // TODO: Define a title for the content shown.
				// TODO: Make sure this auto-generated URL is correct.
				.setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]")).build();
		return new Action.Builder(Action.TYPE_VIEW).setObject(object).setActionStatus(Action.STATUS_TYPE_COMPLETED).build();
	}

	@Override
	public void onStop() {
		super.onStop();

		// ATTENTION: This was auto-generated to implement the App Indexing API.
		// See https://g.co/AppIndexing/AndroidStudio for more information.
		AppIndex.AppIndexApi.end(client, getIndexApiAction());
		client.disconnect();
	}

	private void populateSeats(final SeatsClassList pSeatsClassList) {
		for (SeatsClass seatClass : pSeatsClassList) {
			final List<SeatsRow> rows = seatClass.getRows();
			Double cost=seatClass.getCost();
			for (SeatsRow row : rows) {
				row.setmCost(cost);
				addSeatRow(row);
			}
		}
	}

	@Override
	public void onEvent(int id) {
		if(id== R.id.loginTV){
			startActivity(id);
		}else if(R.id.singnUpTV==id){
			startActivity(id);
		}else if (R.id.tvGuest==id){
			Intent intent= new Intent(this, GuestActivity.class);
			intent.putExtra("EXTRA_MOVIE_NAME", mMovieName);
			intent.putExtra("EXTRA_MOVIE_ID", mMovieId);
			intent.putExtra("EXTRA_MOVIE_THEATRE", mMovieTheatre);
			intent.putExtra("EXTRA_MOVIE_DATE", mMovieDate);
			intent.putExtra("EXTRA_MOVIE_TIME", mMovieTime);
			intent.putExtra("EXTRA_SHOW_TIME_ID", mShowTimeId);
			intent.putExtra("TOTAL PERSON",(ArrayList)mSelectedSeatsList);
			SharedPreferences prefmPrefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
			SharedPreferences.Editor editor = prefmPrefs.edit();
			editor.putString(AppConstants.TYPE, "MOVIE");
			editor.commit();


			double mMoviesCost= Double.valueOf(00);

			for (int i = 0; i <mCostList.size() ; i++) {
				mMoviesCost=mMoviesCost+mCostList.get(i).doubleValue();

			}
			intent.putExtra("TOTAL Amount",mMoviesCost);
			startActivity(intent);
		}
	}

	private  void startActivity(int id) {
		Intent intent;
		switch (id){
			case R.id.loginTV:
			 intent= new Intent(this, LoginActivity.class);
				startActivityForResult(intent,AppConstants.LOGIN);
				break;

			case R.id.singnUpTV:
				intent= new Intent(this, RegisterActivity.class);
				startActivityForResult(intent,AppConstants.REGISTER);
				break;
		}


	}


	@Override
	public void onEvent(int id, Object object) {

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode== Activity.RESULT_OK){
			if (requestCode==AppConstants.LOGIN){
				sendMovieDetail();
			}else if(requestCode==AppConstants.REGISTER){
				sendMovieDetail();
			}
		}
	}

	private void sendMovieDetail() {
		Intent intent= new Intent(this, GuestActivity.class);
		intent.putExtra("EXTRA_MOVIE_NAME", mMovieName);
		intent.putExtra("EXTRA_MOVIE_ID", mMovieId);
		intent.putExtra("EXTRA_MOVIE_THEATRE", mMovieTheatre);
		intent.putExtra("EXTRA_MOVIE_DATE", mMovieDate);
		intent.putExtra("EXTRA_MOVIE_TIME", mMovieTime);
		intent.putExtra("EXTRA_SHOW_TIME_ID", mShowTimeId);
		intent.putExtra("TOTAL PERSON",(ArrayList)mSelectedSeatsList);


		SharedPreferences prefmPrefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		SharedPreferences.Editor editor = prefmPrefs.edit();
		editor.putString(AppConstants.TYPE, "MOVIE");
		editor.commit();



		double mMoviesCost= Double.valueOf(00);

		for (int i = 0; i <mCostList.size() ; i++) {
			mMoviesCost=mMoviesCost+mCostList.get(i).doubleValue();

		}
		intent.putExtra("TOTAL Amount",mMoviesCost);
		startActivity(intent);
	}


	@Override
	protected void onResume() {
		super.onResume();
		SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(this);
		String GuestBack = mPrefs.getString("GuestBack",null);
		if (GuestBack!=null){
			if (GuestBack.equalsIgnoreCase("1")){
				mSelectedSeatsList.clear();
				mCostList.clear();
				tvSeatSlctdNmbrs.setText(mSelectedSeatsList.size()+"");
				mSeatsTableLayout.removeAllViews();
				fetchSeatLayout(mShowTimeId);
				setOnSeatSelectionChangedListener(mOnSeatSelectionChangedListener);

				SharedPreferences prefmPrefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
				SharedPreferences.Editor editor = prefmPrefs.edit();
				editor.putString("GuestBack", "0");
				editor.commit();
			}
		}

	}
}
