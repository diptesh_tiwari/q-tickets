package com.mobile.android.qtickets.event;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.renderscript.Type;
import android.support.annotation.Nullable;
import android.support.v4.util.ArrayMap;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mobile.android.qtickets.QticketsApplication;
import com.mobile.android.qtickets.R;
import com.mobile.android.qtickets.activities.GuestActivity;
import com.mobile.android.qtickets.activities.LoginActivity;
import com.mobile.android.qtickets.activities.RegisterActivity;
import com.mobile.android.qtickets.constants.AppConstants;
import com.mobile.android.qtickets.event.booking.EventBookingInfo;
import com.mobile.android.qtickets.event.booking.EventTicket;
import com.mobile.android.qtickets.interfes.OnEventCallBack;
import com.mobile.android.qtickets.model.SelectEventType;
import com.mobile.android.qtickets.model.UserDetailsModel;
import com.mobile.android.qtickets.utils.Api;
import com.mobile.android.qtickets.utils.MyDialog;
import com.mobile.android.qtickets.utils.QTUtils;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.net.Proxy;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Created by Satish - 360 on 11/21/2016.
 */

public class EventsDetailsActivity extends AppCompatActivity implements View.OnClickListener, OnEventCallBack{
    EventDetailsVO eventvo;
    private RecyclerView mEventDetailsRecyclerView;
    private QTEventDetailsAdapter mEventDetailsAdapter;
    private RecyclerView.LayoutManager mEventDetailsLayoutManager;
    private ArrayList<EventDetailsVO> detailsList;
    private TextView totalPersonTV, tv_eventDate, tv_eventTite, tv_eventDetails, tv_eventTimings, tv_eventLocation;
    private DatePickerDialog datePicker;
    private SimpleDateFormat dateFormatter;
    private ImageView iv_eventImage;
    private Map<String, EventTicket> mEventTicketCostMap = new ArrayMap<>();
    private EventBookingInfo mEventBookingInfo;
    private RelativeLayout ticketTypeLL;
    private ImageView shareImageView, backImageView;
    private TextView bookNowTV, termsShowButtonTV, eventTermCondition, totalPriceTV, termshowTV;
    LinearLayout termshowLL;
    Integer ticketCount;
    ScrollView scrlBar;
    private ArrayList<SelectEventType> selectEventTypes = new ArrayList<>();
    private ArrayList<EventTicketDetails> mainEvntTktDetilas = new ArrayList<>();
    private ArrayList<EventTicketDetails> templist1 = new ArrayList<>();
    private ArrayList<EventTicketDetails> templist2 = new ArrayList<>();
    private ArrayList<EventTicketDetails> templist3 = new ArrayList<>();
    private RelativeLayout ticketsLL;
    String currencyType;
    private RelativeLayout ticketsTypeLL;
    private String startDate, endDate;
    private LinearLayout selectDatLL;
    private TextView selectedDateTV, registrationformTV, boatTV, selectTimeTV, selectDayTypeTV;
    private String eventSelectedDate;
    private int eventID = 0;
    private int boatType = 0;
    private int minCountBoat1 = 19;
    private int minPeapleCountBoat1 = 19;
    private int minCountBoat2 = 14;
    private int minPeapleCountBoat2 = 14;
    private int minCountDhow = 19;
    private int minPeapleCountDhow = 19;
    private int showPopup=0;
    private int totalCount;
    private double totalPrice;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        currencyType = mPrefs.getString(AppConstants.COUNTRY_TYPE_CURRENCY, null);
        setDateTimeField();
        setContentView(R.layout.activity_events_details);

        mEventDetailsAdapter = new QTEventDetailsAdapter(this, mainEvntTktDetilas);


        iv_eventImage = (ImageView) findViewById(R.id.iv_eventImage);
        mEventDetailsRecyclerView = (RecyclerView) findViewById(R.id.rv_events_prices);
        //tv_date_select = (TextView) findViewById(R.id.tv_date_event_details);
        tv_eventTite = (TextView) findViewById(R.id.tv_edetails_title);
        tv_eventDate = (TextView) findViewById(R.id.tv_edetails_date);
        tv_eventTimings = (TextView) findViewById(R.id.tv_edetails_time);
        tv_eventLocation = (TextView) findViewById(R.id.tv_edetails_location);
        tv_eventDetails = (TextView) findViewById(R.id.tv_event_description);
        totalPersonTV = (TextView) findViewById(R.id.totalPersonTV);
        scrlBar = (ScrollView) findViewById(R.id.scrlBar);
        ticketsTypeLL = (RelativeLayout) findViewById(R.id.ticketsTypeLL);
        selectDatLL = (LinearLayout) findViewById(R.id.selectDatLL);
        selectedDateTV = (TextView) findViewById(R.id.selectedDateTV);
        registrationformTV = (TextView) findViewById(R.id.registrationformTV);
        boatTV = (TextView) findViewById(R.id.boatTV);
        selectTimeTV = (TextView) findViewById(R.id.selectTimeTV);
        selectDayTypeTV = (TextView) findViewById(R.id.selectDayTypeTV);



        //shareImageView,backImageView
        shareImageView = (ImageView) findViewById(R.id.shareImageView);
        backImageView = (ImageView) findViewById(R.id.backImageView);
        bookNowTV = (TextView) findViewById(R.id.bookNowTV);
        termsShowButtonTV = (TextView) findViewById(R.id.termsShowButtonTV);
        eventTermCondition = (TextView) findViewById(R.id.eventTermCondition);
        termshowTV = (TextView) findViewById(R.id.termshowTV);
        termshowLL = (LinearLayout) findViewById(R.id.termshowLL);
        totalPriceTV = (TextView) findViewById(R.id.totalPriceTV);
        ticketsLL = (RelativeLayout) findViewById(R.id.ticketsLL);


        shareImageView.setOnClickListener(this);
        backImageView.setOnClickListener(this);
        bookNowTV.setOnClickListener(this);
        termsShowButtonTV.setOnClickListener(this);
        termshowTV.setOnClickListener(this);
        tv_eventDate.setOnClickListener(this);
        selectDatLL.setOnClickListener(this);
        registrationformTV.setOnClickListener(this);
        boatTV.setOnClickListener(this);
        selectTimeTV.setOnClickListener(this);
        selectDayTypeTV
                .setOnClickListener(this);

        QTUtils.getInstance().saveToSharedPreference(this, AppConstants.BOAT_TYPE, null);
        QTUtils.getInstance().saveToSharedPreference(EventsDetailsActivity.this, AppConstants.EVENET_SELECTED_DATE, null);

        Gson gson = new Gson();
        Intent myInt = getIntent();
        String events = myInt.getStringExtra(AppConstants.INTENT_SHARED_EVENT_TAG);
        if (events != null) {
            eventvo = gson.fromJson(events, EventDetailsVO.class);
            QTUtils.getInstance().saveToSharedPreference(EventsDetailsActivity.this, AppConstants.EVENT_ID, eventvo.eventid);
            if (eventvo.eventid != null && eventvo.eventid.equalsIgnoreCase("706")) {
                eventID = 706;
                registrationformTV.setVisibility(View.VISIBLE);
                boatTV.setVisibility(View.GONE);
                selectTimeTV.setVisibility(View.GONE);
                selectDayTypeTV.setVisibility(View.GONE);
            } else if (eventvo.eventid != null && eventvo.eventid.equalsIgnoreCase("704")) {
                eventID = 704;
                registrationformTV.setVisibility(View.VISIBLE);
                boatTV.setVisibility(View.GONE);
                selectTimeTV.setVisibility(View.GONE);
                selectDayTypeTV.setVisibility(View.GONE);
            } else if (eventvo.eventid != null && eventvo.eventid.equalsIgnoreCase("421")) {
                eventID = 421;
                registrationformTV.setVisibility(View.GONE);
                boatTV.setVisibility(View.VISIBLE);
                selectTimeTV.setVisibility(View.GONE);
                selectDayTypeTV.setVisibility(View.GONE);

            } else if (eventvo.eventid != null && eventvo.eventid.equalsIgnoreCase("420")) {
                eventID = 420;
                registrationformTV.setVisibility(View.GONE);
                boatTV.setVisibility(View.GONE);
                selectTimeTV.setVisibility(View.VISIBLE);
                selectDayTypeTV.setVisibility(View.GONE);

            } else if (eventvo.eventid != null && eventvo.eventid.equalsIgnoreCase("354")) {
                eventID = 354;
                registrationformTV.setVisibility(View.GONE);
                boatTV.setVisibility(View.GONE);
                selectTimeTV.setVisibility(View.GONE);
                selectDayTypeTV.setVisibility(View.VISIBLE);

            } else {
                registrationformTV.setVisibility(View.GONE);
                boatTV.setVisibility(View.GONE);
                selectTimeTV.setVisibility(View.GONE);
                selectDayTypeTV.setVisibility(View.GONE);
                eventID = Integer.parseInt(eventvo.eventid);
            }
        } else if (QTUtils.getInstance().getFromSharedPreference(EventsDetailsActivity.this, AppConstants.EVENT_ITEM) != null) {
            events = QTUtils.getInstance().getFromSharedPreference(EventsDetailsActivity.this, AppConstants.EVENT_ITEM);

            eventvo = gson.fromJson(events, EventDetailsVO.class);
            QTUtils.getInstance().saveToSharedPreference(EventsDetailsActivity.this, AppConstants.EVENT_ID, eventvo.eventid);
            if (eventvo.eventid != null && eventvo.eventid.equalsIgnoreCase("706")) {
                eventID = 706;
                registrationformTV.setVisibility(View.VISIBLE);
                boatTV.setVisibility(View.GONE);
            } else if (eventvo.eventid != null && eventvo.eventid.equalsIgnoreCase("704")) {
                eventID = 704;
                registrationformTV.setVisibility(View.VISIBLE);
                boatTV.setVisibility(View.GONE);
            } else if (eventvo.eventid != null && eventvo.eventid.equalsIgnoreCase("421")) {
                eventID = 421;
                registrationformTV.setVisibility(View.GONE);
                boatTV.setVisibility(View.VISIBLE);

            } else {
                registrationformTV.setVisibility(View.GONE);
                boatTV.setVisibility(View.GONE);
                eventID = Integer.parseInt(eventvo.eventid);
            }
        }

        //initActionBar();
        if (eventvo != null) {
            String url = eventvo.bannerURL;
            url = url.replace("App_Images", "movie_Images");

            startDate = eventvo.startDate;
            endDate = eventvo.endDate;
            SimpleDateFormat dfEventDate = new SimpleDateFormat("yyyy-MM-dd");
            try {
                Date newDate = dfEventDate.parse(startDate);
                Date endDateEvent = dfEventDate.parse(endDate);
                dfEventDate = new SimpleDateFormat("dd MMM yyyy");
                startDate = dfEventDate.format(newDate);
                endDate = dfEventDate.format(endDateEvent);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            tv_eventDate.setText(startDate + " - " + endDate);


            tv_eventTite.setText(eventvo.eventname);
            tv_eventTimings.setText(eventvo.StartTime + " - " + eventvo.endTime);
            tv_eventLocation.setText(eventvo.Venue);
            String a = String.valueOf(Html.fromHtml(eventvo.EDescription));
            tv_eventDetails.setText(a);
            // eventTermCondition.setText(eventvo.t);
            Picasso.with(this).load(url).fit().into(iv_eventImage);

            Calendar c = Calendar.getInstance();
            System.out.println("Current time => " + c.getTime());

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MMM-dd");
            String formattedDate = sdf.format(c.getTime());

            if (eventvo.evntTktDetilas != null && !eventvo.evntTktDetilas.isEmpty()) {
                setUpEventPriceAdapter(eventvo.evntTktDetilas);
                mEventDetailsRecyclerView.setVisibility(View.VISIBLE);
                ticketsLL.setVisibility(View.VISIBLE);
                bookNowTV.setVisibility(View.VISIBLE);
                ticketsTypeLL.setVisibility(View.VISIBLE);
                if (startDate.equalsIgnoreCase(endDate)) {
                    selectDatLL.setVisibility(View.GONE);
                    eventSelectedDate = startDate;
                } else {
                    selectDatLL.setVisibility(View.VISIBLE);
                    eventSelectedDate = null;
                }

            } else {
                mEventDetailsRecyclerView.setVisibility(View.GONE);
                ticketsLL.setVisibility(View.GONE);
                bookNowTV.setVisibility(View.GONE);
                ticketsTypeLL.setVisibility(View.GONE);
                selectDatLL.setVisibility(View.GONE);
            }


        }

    }

    private void setDateTimeField() {
        dateFormatter = new SimpleDateFormat("dd MMM yyyy", Locale.US);
        Calendar newCalendar = Calendar.getInstance();
        datePicker = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                eventSelectedDate = dateFormatter.format(newDate.getTime());
                selectedDateTV.setText(eventSelectedDate);
                QTUtils.getInstance().saveToSharedPreference(EventsDetailsActivity.this, AppConstants.EVENET_SELECTED_DATE, eventSelectedDate);
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
    }

    private void setUpEventPriceAdapter(ArrayList<EventTicketDetails> evntTktDetilas) {
        //mEventDetailsRecyclerView.setHasFixedSize(true);
        mEventDetailsRecyclerView.setNestedScrollingEnabled(false);
        mEventDetailsLayoutManager = new LinearLayoutManager(this);
        mEventDetailsRecyclerView.setLayoutManager(mEventDetailsLayoutManager);


        if (eventID == 421) {

            try {
                String eventsDetail = Api.toJson(evntTktDetilas);
                QTUtils.getInstance().saveToSharedPreference(this, AppConstants.EVENTS_DETAIL, eventsDetail);
            } catch (IOException e) {
                e.printStackTrace();
            }

            for (int i = 0; i < evntTktDetilas.size(); i++) {
                if (i == 0) {
                    EventTicketDetails eventTicketDetail = evntTktDetilas.get(0);
                    templist1.add(eventTicketDetail);
                } else if (i == 1) {
                    EventTicketDetails eventTicketDetail = evntTktDetilas.get(1);
                    templist2.add(eventTicketDetail);
                } else if (i == 2) {
                    EventTicketDetails eventTicketDetail = evntTktDetilas.get(2);
                    templist3.add(eventTicketDetail);
                }
            }

        } else {
            mEventDetailsAdapter = new QTEventDetailsAdapter(this, evntTktDetilas);

        }


        mEventDetailsAdapter.setTicketCountSelectionChangeListener(new TicketCountSelectionChangeListener() {
            @Override
            public void onTicketCountChanged(final EventTicket pEventTicketCost) {
                mEventTicketCostMap.put(pEventTicketCost.getTicketType(), pEventTicketCost);

                mEventBookingInfo = new EventBookingInfo();

                for (final Map.Entry<String, EventTicket> costEntry : mEventTicketCostMap.entrySet()) {
                    final EventTicket eventTicket = costEntry.getValue();
                    String type=eventTicket.getTicketType();

                    if (eventTicket.getCount() > 0) {
                        if(type.equalsIgnoreCase("Per Person")&&eventTicket.getCount()==19){

                        }else {
                            mEventBookingInfo.addEventTicket(eventTicket);
                        }

                    }
                }
                final int totalTicketsCount = mEventBookingInfo.getTotalTicketsCount();
                final double totalCost = mEventBookingInfo.getTotalCost();

                setEventTotalInfo(totalTicketsCount, totalCost);
                if (totalTicketsCount > 0 && totalCost > 0) {
                    showEventTotalInfoText();
                } else {
                    hideEventTotalInfoText();
                }
            }
        });


        mEventDetailsRecyclerView.setAdapter(mEventDetailsAdapter);

    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }


    private void setEventTotalInfo(final int ticketsCount, final double totalCost) {
        if (ticketsCount >= 1) {
            totalPersonTV.setText(ticketsCount + " Persons");
            ticketCount = ticketsCount;
            totalPriceTV.setText(totalCost + " " + currencyType);
            totalCount=ticketsCount;
            totalPrice=totalCost;
            QTUtils.getInstance().saveToSharedPreference(this,AppConstants.TOTAL_COUNT,totalCount);
            QTUtils.getInstance().saveToSharedPreference(this,AppConstants.TOTAL_PRICE,totalPrice);

            

        } else {
            totalPersonTV.setText(ticketsCount + " Person");
            ticketCount = ticketsCount;
            totalPriceTV.setText(totalCost + " " + currencyType);
        }

    }

    private void showEventTotalInfoText() {
        totalPersonTV.setVisibility(View.VISIBLE);
    }

    private void hideEventTotalInfoText() {
        totalPersonTV.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.tv_date_event_details:
               /* datePicker.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                datePicker.show();*/
                break;

            case R.id.selectDatLL:
                if(eventID==585){
                    List<Calendar> dayslist= new LinkedList<>();
                    final Calendar[] daysArray;
                    Calendar cAux = Calendar.getInstance();
                    cAux.add(Calendar.DAY_OF_MONTH,+1);
                    int timer=0;
                    while ( cAux.getTimeInMillis() >= System.currentTimeMillis() && timer<225) {
                        if (cAux.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY ) {
                            Calendar c = Calendar.getInstance();
                            c.setTimeInMillis(cAux.getTimeInMillis());
                            dayslist.add(c);
                        }
                        timer++;
                        cAux.setTimeInMillis(cAux.getTimeInMillis() + (24*60*60*1000));
                    }
                    daysArray = new Calendar[dayslist.size()];
                    String []arrFriday=new String[daysArray.length];
                    for (int i = 0; i<daysArray.length;i++)
                    {
                        daysArray[i]=dayslist.get(i);
                        arrFriday[i]=daysArray[i].get(Calendar.YEAR)+"-"+(daysArray[i].get(Calendar.MONTH)+1)+"-"+daysArray[i].get(Calendar.DAY_OF_MONTH);
                        final SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd");
                        final SimpleDateFormat output = new SimpleDateFormat("dd MMM yyyy");
                        try {
                            arrFriday[i]=output.format(input.parse(arrFriday[i]));

                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                    }

                    Calendar date = Calendar.getInstance();

                    for (int i = 0; i <arrFriday.length ; i++) {
                        SimpleDateFormat f = new SimpleDateFormat("dd MMM yyyy");

                        try {
                            Date day = f.parse(arrFriday[i]);
                            date.setTime(day);

                        } catch (ParseException e) {
                            e.printStackTrace();
                        }


                    }

                    MyDialog.showDatesDialog(this,this,arrFriday);



                }else {

                    long currntTime = System.currentTimeMillis();
                    String today = dateFormatter.format(currntTime);

                    SimpleDateFormat f = new SimpleDateFormat("dd MMM yyyy");
                    long millisecondsEndDate = 0;
                    long millisecondStartDate = 0;
                    try {
                        Date endDay = f.parse(endDate);
                        Date startDay = f.parse(startDate);
                        Date currentDay = f.parse(today);

                        if (startDay.after(currentDay)) {
                            //startDay
                            millisecondStartDate = startDay.getTime();
                            System.out.println("Date1 is after Date2");
                        }

                        if (startDay.before(currentDay)) {
                            //CurrentDay
                            millisecondStartDate = currentDay.getTime();
                            System.out.println("Date1 is before Date2");
                        }

                        if (startDay.equals(currentDay)) {
                            //startDay
                            millisecondStartDate = startDay.getTime();
                            System.out.println("Date1 is equal Date2");
                        }


                        millisecondsEndDate = endDay.getTime();

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }


                    datePicker.getDatePicker().setMinDate(millisecondStartDate);
                    datePicker.getDatePicker().setMaxDate(millisecondsEndDate);
                    datePicker.show();
                }


                break;

            case R.id.shareImageView:
                intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                String url = eventvo.EventUrl;
                intent.putExtra(Intent.EXTRA_TEXT, url);
                startActivity(Intent.createChooser(intent, "Share with"));
                break;
            case R.id.backImageView:
                finish();
                break;
            case R.id.bookNowTV:
                startEventBookingPage(mEventBookingInfo);
                break;
            case R.id.termsShowButtonTV:
                if (termshowLL.getVisibility() == View.VISIBLE) {
                    termshowLL.setVisibility(View.GONE);
                    termsShowButtonTV.setText("+");

                } else {
                    termshowLL.setVisibility(View.VISIBLE);
                    termsShowButtonTV.setText("-");
                }
                break;
            case R.id.termshowTV:
                if (termshowLL.getVisibility() == View.VISIBLE) {
                    termshowLL.setVisibility(View.GONE);
                    termsShowButtonTV.setText("+");
                } else {
                    termshowLL.setVisibility(View.VISIBLE);
                    termsShowButtonTV.setText("-");
                }
                break;
            case R.id.registrationformTV:
                if (eventID == 706) {
                    MyDialog.showRegistrationFrom(this);
                } else if (eventID == 704) {

                    Intent intent1 = new Intent(Intent.ACTION_VIEW);
                    intent1.setData(Uri.parse("http://qfrs.leagueapps.com/pages/ramadan"));
                    startActivity(intent1);
                }

                break;

            case R.id.boatTV:
                MyDialog.showBoatDialog(this, this);
                break;

            case R.id.selectTimeTV:
                MyDialog.showtimeDialog(this, this);
                break;
            case R.id.selectDayTypeTV:
                MyDialog.showDayTypeDialog(this, this);
                break;

        }
    }




    private void refreshTicketsDetail() {
        minCountBoat1 = 19;
        minPeapleCountBoat1 = 19;
        minCountBoat2 = 14;
        minPeapleCountBoat2 = 14;
        minCountDhow = 19;
        minPeapleCountDhow = 19;
        totalPersonTV.setText("");
        ticketCount = 0;
        totalPriceTV.setText(null);
        QTUtils.getInstance().saveToSharedPreference(this,AppConstants.TOTAL_COUNT,null);
        QTUtils.getInstance().saveToSharedPreference(this,AppConstants.TOTAL_PRICE,null);


    }

    private void changeListener() {

        mEventDetailsAdapter.setTicketCountSelectionChangeListener(new TicketCountSelectionChangeListener() {
            @Override
            public void onTicketCountChanged(final EventTicket pEventTicketCost) {
                mEventTicketCostMap.put(pEventTicketCost.getTicketType(), pEventTicketCost);

                mEventBookingInfo = new EventBookingInfo();

                for (final Map.Entry<String, EventTicket> costEntry : mEventTicketCostMap.entrySet()) {
                    final EventTicket eventTicket = costEntry.getValue();
                    if (eventTicket.getCount() > 0) {
                        mEventBookingInfo.addEventTicket(eventTicket);
                    }
                }
                final int totalTicketsCount;
                final double totalCost;
                String boat_type=null;
                if (QTUtils.getInstance().getFromSharedPreference(EventsDetailsActivity.this, AppConstants.BOAT_TYPE) != null){
                    boat_type = QTUtils.getInstance().getFromSharedPreference(EventsDetailsActivity.this, AppConstants.BOAT_TYPE);
                }

                if (boat_type != null && boat_type.equalsIgnoreCase("3")) {
                    // eventID == 421 && boat_type.equalsIgnoreCase("3")
                    totalTicketsCount = pEventTicketCost.getTotalCount();
                    totalCost = pEventTicketCost.getRate();
                    if (totalTicketsCount == 0) {
                        double bal = 0;
                        setEventTotalInfo(totalTicketsCount, bal);
                    } else {
                        setEventTotalInfo(totalTicketsCount, totalCost);
                    }


                } else if (eventID == 421) {
                    totalTicketsCount = pEventTicketCost.getTotalCount();
                    totalCost = pEventTicketCost.getRate() * totalTicketsCount;
                    if (boat_type.equalsIgnoreCase("2")&&totalTicketsCount == 14) {
                        double bal = 0;
                        setEventTotalInfo(0, bal);
                    } else if (boat_type.equalsIgnoreCase("1")&&totalTicketsCount == 19) {
                        double bal = 0;
                        setEventTotalInfo(0, bal);
                    } else {
                        setEventTotalInfo(totalTicketsCount, totalCost);
                    }

                } else if (eventID == 363) {
                    totalTicketsCount = pEventTicketCost.getTotalCount();
                    totalCost = pEventTicketCost.getRate() * totalTicketsCount;
                    if (totalTicketsCount == 19) {
                        double bal = 0;
                        setEventTotalInfo(0, bal);
                    } else {
                        setEventTotalInfo(totalTicketsCount, totalCost);
                    }

                } else {
                    totalTicketsCount = mEventBookingInfo.getTotalTicketsCount();
                    totalCost = mEventBookingInfo.getTotalCost() * totalTicketsCount;
                    setEventTotalInfo(totalTicketsCount, totalCost);
                }

                if (totalTicketsCount > 0 && totalCost > 0) {
                    showEventTotalInfoText();
                } else {
                    hideEventTotalInfoText();
                }
            }
        });
    }

    private void startActivity(int id) {
        Intent intent;
        switch (id) {
            case R.id.loginTV:
                intent = new Intent(this, LoginActivity.class);
                startActivityForResult(intent, AppConstants.LOGIN);
                break;

            case R.id.singnUpTV:
                intent = new Intent(this, RegisterActivity.class);
                startActivityForResult(intent, AppConstants.REGISTER);
                break;
        }


    }
    @Override
    public void onEvent(int id) {
        if (id == R.id.boatOption1TV) {
            QTUtils.getInstance().saveToSharedPreference(this, AppConstants.BOAT_TYPE, "1");
            boatType = 1;
            boatTV.setText("Boat Option-1");
            mEventDetailsAdapter = new QTEventDetailsAdapter(this, templist1);
            changeListener();
            mEventDetailsRecyclerView.setAdapter(mEventDetailsAdapter);
            mEventDetailsAdapter.notifyDataSetChanged();
            refreshTicketsDetail();


        } else if (id == R.id.boatOption2TV) {
            QTUtils.getInstance().saveToSharedPreference(this, AppConstants.BOAT_TYPE, "2");
            boatType = 2;
            boatTV.setText("Boat Option-2");
            mEventDetailsAdapter = new QTEventDetailsAdapter(this, templist2);
            changeListener();
            mEventDetailsRecyclerView.setAdapter(mEventDetailsAdapter);
            mEventDetailsAdapter.notifyDataSetChanged();
            refreshTicketsDetail();

        } else if (id == R.id.boatOption3TV) {

            QTUtils.getInstance().saveToSharedPreference(this, AppConstants.BOAT_TYPE, "3");
            boatType = 3;
            boatTV.setText("Boat Option-3");
            mEventDetailsAdapter = new QTEventDetailsAdapter(this, templist3);
            changeListener();
            mEventDetailsRecyclerView.setAdapter(mEventDetailsAdapter);
            mEventDetailsAdapter.notifyDataSetChanged();
            refreshTicketsDetail();

        }
        else if (id == R.id.firstHalfDayTV) {
            selectDayTypeTV.setText("Half Day(9:00AM - 1:00PM)");
        } else if (id == R.id.secondHalfDayTV) {
            selectDayTypeTV.setText("Half Day(2:30PM - 6:30PM)");
        } else if (id == R.id.firstFullDayTV) {
            selectDayTypeTV.setText("Full Day(9:00AM - 4:30PM)");
        } else if (R.id.secondFullDayTV == id) {
            selectDayTypeTV.setText("Full Day(2:30PM - 9:30PM)");
        }else if (R.id.nightTV == id) {
            selectDayTypeTV.setText("OverNight(2:30PM - 8:30AM Next Day)");
        }



        else if (id == R.id.firstTimeTV) {
            selectTimeTV.setText("9.00 AM");
        } else if (id == R.id.secondTimeTV) {
            selectTimeTV.setText("2.00 PM");
        } else if (id == R.id.loginTV) {
            startActivity(id);
        } else if (R.id.singnUpTV == id) {
            startActivity(id);
        } else if (R.id.tvGuest == id) {
            SharedPreferences prefmPrefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            SharedPreferences.Editor editor = prefmPrefs.edit();
            editor.putString(AppConstants.TYPE, "EVENTS");
            editor.commit();

            Intent intent = new Intent(this, GuestActivity.class);

            intent.putExtra("Events", mEventBookingInfo);
            //startEventBookingPage(mEventBookingInfo);
            startActivity(intent);
        }
    }

    @Override
    public void onEvent(int id, Object object) {

        if(id==R.id.gridView1&&object instanceof String){
            String selected= (String) object;
            eventSelectedDate=selected;
            selectedDateTV.setText(selected);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == AppConstants.LOGIN) {
                sendEventDetail();
            } else if (requestCode == AppConstants.REGISTER) {
                sendEventDetail();
            }
        }
    }

    private void sendEventDetail() {
        SharedPreferences prefmPrefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = prefmPrefs.edit();
        editor.putString(AppConstants.TYPE, "EVENTS");
        editor.commit();
        Intent intent = new Intent(this, GuestActivity.class);
        startActivity(intent);
    }


    private void startEventBookingPage(final EventBookingInfo pEventBookingInfo) {

        SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = mPrefs.edit();


        String usrDetails = mPrefs.getString(AppConstants.SHARED_USER_TAG, null);

        String pEventBookingInfoJson = null;
        String eventvooJson = null;

        try {
            pEventBookingInfoJson = Api.toJson(pEventBookingInfo);
            eventvooJson = Api.toJson(eventvo);
            editor.putString("pEventBookingInfo", pEventBookingInfoJson);
            editor.putString("Selected_Seat", Api.toJson(selectEventTypes));
            editor.putString("eventvo", eventvooJson);
            editor.commit();
        } catch (IOException e) {
            e.printStackTrace();
        }


        if (ticketCount != null && ticketCount != 0) {
            if (usrDetails != null) {
                sendEventDetail();
                //EventBookingActivity.start(this, pEventBookingInfo, eventvo);
            } else {
                MyDialog.showLogInDialog(this, this);
            }

        } else {
            Toast.makeText(this, "Select at least one ticket", Toast.LENGTH_SHORT).show();
        }


    }


    private interface TicketCountSelectionChangeListener {
        void onTicketCountChanged(final EventTicket pEventTicketCost);
    }

    private class QTEventDetailsAdapter extends RecyclerView.Adapter<QTEventDetailsAdapter.ViewHolder> {
        private Context mContext;
        private ArrayList<EventTicketDetails> eventsList;
        private ArrayList<Integer> counts = new ArrayList<>();


        private TicketCountSelectionChangeListener mTicketCountSelectionChangeListener;

        public QTEventDetailsAdapter(Context ctx, ArrayList<EventTicketDetails> listEvents) {
            mContext = ctx;
            eventsList = listEvents;
            for (int i = 0; i < eventsList.size(); i++) {
                SelectEventType selectEventType = new SelectEventType();
                counts.add(i);
                selectEventTypes.add(selectEventType);
            }

        }

        public void setTicketCountSelectionChangeListener(final TicketCountSelectionChangeListener pTicketCountSelectionChangeListener) {
            mTicketCountSelectionChangeListener = pTicketCountSelectionChangeListener;
        }


        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(mContext).inflate(R.layout.rvadapter_event_details, parent, false);
            ViewHolder mViewHolder = new ViewHolder(v, viewType);
            return mViewHolder;
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            final EventTicketDetails eventTicketDetails = eventsList.get(position);

            final String ticketType = eventTicketDetails.TicketName;
            final String ticketPrice = eventTicketDetails.TicketPrice;
            holder.tv_ticketType.setText(ticketType);
            holder.tv_price.setText(currencyType + " " + ticketPrice);

            final EventTicket eventTicketCost = new EventTicket(ticketType, Double.parseDouble(ticketPrice));
            holder.decImageButton.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    if (eventSelectedDate != null) {


                        if (eventID == 417) {
                            if (eventTicketCost.getCount() != 0 && position == 0) {
                                int count = eventTicketCost.getCount();
                                int totalPeapleCount = eventTicketCost.getTotalCount();
                                count = count - 1;
                                totalPeapleCount = totalPeapleCount - 1;
                                holder.totalPeapleCountTV.setText("" + totalPeapleCount);
                                eventTicketCost.setCount(count);
                                eventTicketCost.setTotalCount(count);
                                String tktpriceid = eventTicketDetails.tktpriceid;
                                SelectEventType selectEventType = selectEventTypes.get(position);
                                selectEventType.setCount(count);
                                selectEventType.setEventMasterId(eventTicketDetails.tktmasterid);
                                selectEventType.setTktpriceid(tktpriceid);
                                mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);

                            } else if (eventTicketCost.getCount() != 0 && position == 1) {
                                int count = eventTicketCost.getCount();
                                int totalPeapleCount = eventTicketCost.getTotalCount();
                                count = count - 2;
                                totalPeapleCount = totalPeapleCount - 2;
                                holder.totalPeapleCountTV.setText("" + totalPeapleCount);
                                eventTicketCost.setCount(count);
                                eventTicketCost.setTotalCount(count);
                                String tktpriceid = eventTicketDetails.tktpriceid;
                                SelectEventType selectEventType = selectEventTypes.get(position);
                                selectEventType.setCount(count);
                                selectEventType.setEventMasterId(eventTicketDetails.tktmasterid);
                                selectEventType.setTktpriceid(tktpriceid);
                                mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);

                            } else if (eventTicketCost.getCount() != 0 && position == 2) {
                                int count = eventTicketCost.getCount();
                                int totalPeapleCount = eventTicketCost.getTotalCount();
                                count = count - 3;
                                totalPeapleCount = totalPeapleCount - 3;
                                holder.totalPeapleCountTV.setText("" + totalPeapleCount);
                                eventTicketCost.setCount(count);
                                eventTicketCost.setTotalCount(count);
                                String tktpriceid = eventTicketDetails.tktpriceid;
                                SelectEventType selectEventType = selectEventTypes.get(position);
                                selectEventType.setCount(count);
                                selectEventType.setEventMasterId(eventTicketDetails.tktmasterid);
                                selectEventType.setTktpriceid(tktpriceid);
                                mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);

                            } else if (eventTicketCost.getCount() != 0 && position == 3) {
                                int count = eventTicketCost.getCount();
                                int totalPeapleCount = eventTicketCost.getTotalCount();
                                count = count - 4;
                                totalPeapleCount = totalPeapleCount - 4;
                                holder.totalPeapleCountTV.setText("" + totalPeapleCount);
                                eventTicketCost.setCount(count);
                                eventTicketCost.setTotalCount(count);
                                String tktpriceid = eventTicketDetails.tktpriceid;
                                SelectEventType selectEventType = selectEventTypes.get(position);
                                selectEventType.setCount(count);
                                selectEventType.setEventMasterId(eventTicketDetails.tktmasterid);
                                selectEventType.setTktpriceid(tktpriceid);
                                mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);

                            } else if (eventTicketCost.getCount() != 0 && position == 4) {
                                int count = eventTicketCost.getCount();
                                int totalPeapleCount = eventTicketCost.getTotalCount();
                                count = count - 5;
                                totalPeapleCount = totalPeapleCount - 5;
                                holder.totalPeapleCountTV.setText("" + totalPeapleCount);
                                eventTicketCost.setCount(count);
                                eventTicketCost.setTotalCount(count);
                                String tktpriceid = eventTicketDetails.tktpriceid;
                                SelectEventType selectEventType = selectEventTypes.get(position);
                                selectEventType.setCount(count);
                                selectEventType.setEventMasterId(eventTicketDetails.tktmasterid);
                                selectEventType.setTktpriceid(tktpriceid);
                                mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);
                            } else if (eventTicketCost.getCount() != 0 && position == 5) {
                                int count = eventTicketCost.getCount();
                                int totalPeapleCount = eventTicketCost.getTotalCount();
                                count = count - 6;
                                totalPeapleCount = totalPeapleCount - 6;
                                holder.totalPeapleCountTV.setText("" + totalPeapleCount);
                                eventTicketCost.setCount(count);
                                eventTicketCost.setTotalCount(count);
                                String tktpriceid = eventTicketDetails.tktpriceid;
                                SelectEventType selectEventType = selectEventTypes.get(position);
                                selectEventType.setCount(count);
                                selectEventType.setEventMasterId(eventTicketDetails.tktmasterid);
                                selectEventType.setTktpriceid(tktpriceid);
                                mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);
                            } else if (eventTicketCost.getCount() != 0 && position == 6) {
                                int count = eventTicketCost.getCount();
                                int totalPeapleCount = eventTicketCost.getTotalCount();
                                count = count - 7;
                                totalPeapleCount = totalPeapleCount - 7;
                                holder.totalPeapleCountTV.setText("" + totalPeapleCount);
                                eventTicketCost.setCount(count);
                                eventTicketCost.setTotalCount(count);
                                String tktpriceid = eventTicketDetails.tktpriceid;
                                SelectEventType selectEventType = selectEventTypes.get(position);
                                selectEventType.setCount(count);
                                selectEventType.setEventMasterId(eventTicketDetails.tktmasterid);
                                selectEventType.setTktpriceid(tktpriceid);
                                mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);
                            }


                        }
                        if (eventID == 420) {
                            if (eventTicketCost.getCount() != 0 && position == 0) {
                                int count = eventTicketCost.getCount();
                                int totalPeapleCount = eventTicketCost.getTotalCount();
                                count = count - 1;
                                totalPeapleCount = totalPeapleCount - 1;
                                holder.totalPeapleCountTV.setText("" + totalPeapleCount);
                                eventTicketCost.setCount(count);
                                eventTicketCost.setTotalCount(count);
                                String tktpriceid = eventTicketDetails.tktpriceid;
                                SelectEventType selectEventType = selectEventTypes.get(position);
                                selectEventType.setCount(count);
                                selectEventType.setEventMasterId(eventTicketDetails.tktmasterid);
                                selectEventType.setTktpriceid(tktpriceid);
                                mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);

                            } else if (eventTicketCost.getCount() != 0 && position == 1) {
                                int count = eventTicketCost.getCount();
                                int totalPeapleCount = eventTicketCost.getTotalCount();
                                count = count - 2;
                                totalPeapleCount = totalPeapleCount - 2;
                                holder.totalPeapleCountTV.setText("" + totalPeapleCount);
                                eventTicketCost.setCount(count);
                                eventTicketCost.setTotalCount(count);
                                String tktpriceid = eventTicketDetails.tktpriceid;
                                SelectEventType selectEventType = selectEventTypes.get(position);
                                selectEventType.setCount(count);
                                selectEventType.setEventMasterId(eventTicketDetails.tktmasterid);
                                selectEventType.setTktpriceid(tktpriceid);
                                mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);

                            } else if (eventTicketCost.getCount() != 0 && position == 2) {
                                int count = eventTicketCost.getCount();
                                int totalPeapleCount = eventTicketCost.getTotalCount();
                                count = count - 3;
                                totalPeapleCount = totalPeapleCount - 3;
                                holder.totalPeapleCountTV.setText("" + totalPeapleCount);
                                eventTicketCost.setCount(count);
                                eventTicketCost.setTotalCount(count);
                                String tktpriceid = eventTicketDetails.tktpriceid;
                                SelectEventType selectEventType = selectEventTypes.get(position);
                                selectEventType.setCount(count);
                                selectEventType.setEventMasterId(eventTicketDetails.tktmasterid);
                                selectEventType.setTktpriceid(tktpriceid);
                                mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);

                            } else if (eventTicketCost.getCount() != 0 && position == 3) {
                                int count = eventTicketCost.getCount();
                                int totalPeapleCount = eventTicketCost.getTotalCount();
                                count = count - 4;
                                totalPeapleCount = totalPeapleCount - 4;
                                holder.totalPeapleCountTV.setText("" + totalPeapleCount);
                                eventTicketCost.setCount(count);
                                eventTicketCost.setTotalCount(count);
                                String tktpriceid = eventTicketDetails.tktpriceid;
                                SelectEventType selectEventType = selectEventTypes.get(position);
                                selectEventType.setCount(count);
                                selectEventType.setEventMasterId(eventTicketDetails.tktmasterid);
                                selectEventType.setTktpriceid(tktpriceid);
                                mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);

                            } else if (eventTicketCost.getCount() != 0 && position == 4) {
                                int count = eventTicketCost.getCount();
                                int totalPeapleCount = eventTicketCost.getTotalCount();
                                count = count - 5;
                                totalPeapleCount = totalPeapleCount - 5;
                                holder.totalPeapleCountTV.setText("" + totalPeapleCount);
                                eventTicketCost.setCount(count);
                                eventTicketCost.setTotalCount(count);
                                String tktpriceid = eventTicketDetails.tktpriceid;
                                SelectEventType selectEventType = selectEventTypes.get(position);
                                selectEventType.setCount(count);
                                selectEventType.setEventMasterId(eventTicketDetails.tktmasterid);
                                selectEventType.setTktpriceid(tktpriceid);
                                mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);
                            } else if (eventTicketCost.getCount() != 0 && position == 5) {
                                int count = eventTicketCost.getCount();
                                int totalPeapleCount = eventTicketCost.getTotalCount();
                                count = count - 6;
                                totalPeapleCount = totalPeapleCount - 6;
                                holder.totalPeapleCountTV.setText("" + totalPeapleCount);
                                eventTicketCost.setCount(count);
                                eventTicketCost.setTotalCount(count);
                                String tktpriceid = eventTicketDetails.tktpriceid;
                                SelectEventType selectEventType = selectEventTypes.get(position);
                                selectEventType.setCount(count);
                                selectEventType.setEventMasterId(eventTicketDetails.tktmasterid);
                                selectEventType.setTktpriceid(tktpriceid);
                                mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);
                            }


                        } else if (eventID == 421) {
                            if (boatType == 1) {
                                if (eventTicketCost.getCount() != 0) {
                                    if (eventTicketCost.getCount() != 19 && position == 0) {
                                        int count = eventTicketCost.getCount();
                                        int totalPeapleCount = eventTicketCost.getTotalCount();
                                        count = count - 1;
                                        totalPeapleCount = totalPeapleCount - 1;
                                        if (count == 19) {
                                            holder.totalPeapleCountTV.setText("" + 0);
                                            eventTicketCost.setCount(count);
                                            eventTicketCost.setTotalCount(count);
                                            String tktpriceid = eventTicketDetails.tktpriceid;
                                            SelectEventType selectEventType = new SelectEventType();
                                            // SelectEventType selectEventType = selectEventTypes.get(position);
                                            selectEventType.setCount(0);
                                            selectEventType.setEventMasterId("0");
                                            selectEventType.setTktpriceid("0");
                                            mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);
                                            minCountBoat1 = 19;
                                            minPeapleCountBoat1 = 19;
                                        } else {

                                            holder.totalPeapleCountTV.setText("" + totalPeapleCount);
                                            eventTicketCost.setCount(count);
                                            eventTicketCost.setTotalCount(count);
                                            String tktpriceid = eventTicketDetails.tktpriceid;
                                            SelectEventType selectEventType = selectEventTypes.get(position);
                                            selectEventType.setCount(count);
                                            selectEventType.setEventMasterId(eventTicketDetails.tktmasterid);
                                            selectEventType.setTktpriceid(tktpriceid);
                                            mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);
                                            minCountBoat1 = count;
                                            minPeapleCountBoat1 = totalPeapleCount;
                                        }


                                    }
                                }

                            } else if (boatType == 2) {
                                if (eventTicketCost.getCount() != 0) {
                                    if (eventTicketCost.getCount() != 14 && position == 0) {
                                        int count = eventTicketCost.getCount();
                                        int totalPeapleCount = eventTicketCost.getTotalCount();
                                        count = count - 1;
                                        totalPeapleCount = totalPeapleCount - 1;
                                        if (count == 14) {
                                            holder.totalPeapleCountTV.setText("" + 0);
                                            eventTicketCost.setCount(count);
                                            eventTicketCost.setTotalCount(count);
                                            String tktpriceid = eventTicketDetails.tktpriceid;
                                            SelectEventType selectEventType = new SelectEventType();
                                            // SelectEventType selectEventType = selectEventTypes.get(position);
                                            selectEventType.setCount(0);
                                            selectEventType.setEventMasterId("0");
                                            selectEventType.setTktpriceid("0");
                                            mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);
                                            minCountBoat2 = 14;
                                            minPeapleCountBoat2 = 14;
                                        } else {
                                            holder.totalPeapleCountTV.setText("" + totalPeapleCount);
                                            eventTicketCost.setCount(count);
                                            eventTicketCost.setTotalCount(count);
                                            String tktpriceid = eventTicketDetails.tktpriceid;
                                            SelectEventType selectEventType = selectEventTypes.get(position);
                                            selectEventType.setCount(count);
                                            selectEventType.setEventMasterId(eventTicketDetails.tktmasterid);
                                            selectEventType.setTktpriceid(tktpriceid);
                                            mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);

                                            minCountBoat2 = count;
                                            minPeapleCountBoat2 = totalPeapleCount;
                                        }

                                    }
                                }

                            } else if (boatType == 3) {
                                if (eventTicketCost.getCount() != 0) {
                                    int count = eventTicketCost.getCount();
                                    int totalPeapleCount = eventTicketCost.getTotalCount();
                                    count = count - 1;
                                    totalPeapleCount = totalPeapleCount - 1;
                                    holder.totalPeapleCountTV.setText("" + totalPeapleCount);
                                    eventTicketCost.setCount(count);
                                    eventTicketCost.setTotalCount(count);
                                    String tktpriceid = eventTicketDetails.tktpriceid;
                                    SelectEventType selectEventType = selectEventTypes.get(position);
                                    selectEventType.setCount(count);
                                    selectEventType.setEventMasterId(eventTicketDetails.tktmasterid);
                                    selectEventType.setTktpriceid(tktpriceid);
                                    mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);
                                }

                            }


                        }else if(eventID==354){
                            if (eventTicketCost.getCount() != 0) {
                                int count = eventTicketCost.getCount();
                                int totalPeapleCount = eventTicketCost.getTotalCount();
                                count = count - 1;
                                totalPeapleCount = totalPeapleCount - 1;
                                holder.totalPeapleCountTV.setText("" + totalPeapleCount);
                                eventTicketCost.setCount(count);
                                eventTicketCost.setTotalCount(count);
                                String tktpriceid = eventTicketDetails.tktpriceid;
                                SelectEventType selectEventType = selectEventTypes.get(position);
                                selectEventType.setCount(count);
                                selectEventType.setEventMasterId(eventTicketDetails.tktmasterid);
                                selectEventType.setTktpriceid(tktpriceid);
                                mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);
                            }
                        } else if (eventID == 363) {
                            if (position == 0) {
                                if (eventTicketCost.getCount() != 0) {
                                    if (eventTicketCost.getCount() != 19 && position == 0) {
                                        int count = eventTicketCost.getCount();
                                        int totalPeapleCount = eventTicketCost.getTotalCount();
                                        count = count - 1;
                                        totalPeapleCount = totalPeapleCount - 1;
                                        if (count == 19) {
                                            holder.totalPeapleCountTV.setText("" + 0);
                                            eventTicketCost.setCount(count);
                                            eventTicketCost.setTotalCount(count);
                                            String tktpriceid = eventTicketDetails.tktpriceid;
                                            SelectEventType selectEventType = new SelectEventType();
                                            // SelectEventType selectEventType = selectEventTypes.get(position);
                                            selectEventType.setCount(0);
                                            selectEventType.setEventMasterId("0");
                                            selectEventType.setTktpriceid("0");
                                            mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);
                                            minCountDhow = 19;
                                            minPeapleCountDhow = 19;
                                            showPopup=0 ;

                                           // refreshTicketsDetail();
                                        } else {

                                            holder.totalPeapleCountTV.setText("" + totalPeapleCount);
                                            eventTicketCost.setCount(count);
                                            eventTicketCost.setTotalCount(count);
                                            String tktpriceid = eventTicketDetails.tktpriceid;
                                            SelectEventType selectEventType = selectEventTypes.get(position);
                                            selectEventType.setCount(count);
                                            selectEventType.setEventMasterId(eventTicketDetails.tktmasterid);
                                            selectEventType.setTktpriceid(tktpriceid);
                                            mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);
                                            minCountDhow = count;
                                            minPeapleCountDhow = totalPeapleCount;
                                            minCountDhow = count;
                                            minPeapleCountDhow = totalPeapleCount;


                                            minCountBoat1 = count;
                                            minPeapleCountBoat1 = totalPeapleCount;
                                        }


                                    }
                                }


                            } else if (position == 1) {
                                if (eventTicketCost.getCount() != 0) {
                                    int count = eventTicketCost.getCount();
                                    int totalPeapleCount = eventTicketCost.getTotalCount();
                                    count = count - 1;
                                    totalPeapleCount = totalPeapleCount - 1;
                                    holder.totalPeapleCountTV.setText("" + totalPeapleCount);
                                    eventTicketCost.setCount(count);
                                    eventTicketCost.setTotalCount(count);
                                    String tktpriceid = eventTicketDetails.tktpriceid;
                                    SelectEventType selectEventType = selectEventTypes.get(position);
                                    selectEventType.setCount(count);
                                    selectEventType.setEventMasterId(eventTicketDetails.tktmasterid);
                                    selectEventType.setTktpriceid(tktpriceid);
                                    mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);
                                    //refreshTicketsDetail();
                                }
                            } else if (position == 2) {
                                if (eventTicketCost.getCount() != 0) {
                                    int count = eventTicketCost.getCount();
                                    int totalPeapleCount = eventTicketCost.getTotalCount();
                                    count = count - 1;
                                    totalPeapleCount = totalPeapleCount - 1;
                                    holder.totalPeapleCountTV.setText("" + totalPeapleCount);
                                    eventTicketCost.setCount(count);
                                    eventTicketCost.setTotalCount(count);
                                    String tktpriceid = eventTicketDetails.tktpriceid;
                                    SelectEventType selectEventType = selectEventTypes.get(position);
                                    selectEventType.setCount(count);
                                    selectEventType.setEventMasterId(eventTicketDetails.tktmasterid);
                                    selectEventType.setTktpriceid(tktpriceid);
                                    mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);
                                    //refreshTicketsDetail();
                                }
                            }

                        } else if (eventID == 422) {
                            if (eventTicketCost.getCount() != 0 && position == 0) {
                                int count = eventTicketCost.getCount();
                                int totalPeapleCount = eventTicketCost.getTotalCount();
                                count = count - 1;
                                totalPeapleCount = totalPeapleCount - 1;
                                holder.totalPeapleCountTV.setText("" + totalPeapleCount);
                                eventTicketCost.setCount(count);
                                eventTicketCost.setTotalCount(count);
                                String tktpriceid = eventTicketDetails.tktpriceid;
                                SelectEventType selectEventType = selectEventTypes.get(position);
                                selectEventType.setCount(count);
                                selectEventType.setEventMasterId(eventTicketDetails.tktmasterid);
                                selectEventType.setTktpriceid(tktpriceid);
                                mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);

                            } else if (eventTicketCost.getCount() != 0 && position == 1) {
                                int count = eventTicketCost.getCount();
                                int totalPeapleCount = eventTicketCost.getTotalCount();
                                count = count - 2;
                                totalPeapleCount = totalPeapleCount - 2;
                                holder.totalPeapleCountTV.setText("" + totalPeapleCount);
                                eventTicketCost.setCount(count);
                                eventTicketCost.setTotalCount(count);
                                String tktpriceid = eventTicketDetails.tktpriceid;
                                SelectEventType selectEventType = selectEventTypes.get(position);
                                selectEventType.setCount(count);
                                selectEventType.setEventMasterId(eventTicketDetails.tktmasterid);
                                selectEventType.setTktpriceid(tktpriceid);
                                mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);

                            } else if (eventTicketCost.getCount() != 0 && position == 2) {
                                int count = eventTicketCost.getCount();
                                int totalPeapleCount = eventTicketCost.getTotalCount();
                                count = count - 3;
                                totalPeapleCount = totalPeapleCount - 3;
                                holder.totalPeapleCountTV.setText("" + totalPeapleCount);
                                eventTicketCost.setCount(count);
                                eventTicketCost.setTotalCount(count);
                                String tktpriceid = eventTicketDetails.tktpriceid;
                                SelectEventType selectEventType = selectEventTypes.get(position);
                                selectEventType.setCount(count);
                                selectEventType.setEventMasterId(eventTicketDetails.tktmasterid);
                                selectEventType.setTktpriceid(tktpriceid);
                                mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);

                            } else if (eventTicketCost.getCount() != 0 && position == 3) {
                                int count = eventTicketCost.getCount();
                                int totalPeapleCount = eventTicketCost.getTotalCount();
                                count = count - 4;
                                totalPeapleCount = totalPeapleCount - 4;
                                holder.totalPeapleCountTV.setText("" + totalPeapleCount);
                                eventTicketCost.setCount(count);
                                eventTicketCost.setTotalCount(count);
                                String tktpriceid = eventTicketDetails.tktpriceid;
                                SelectEventType selectEventType = selectEventTypes.get(position);
                                selectEventType.setCount(count);
                                selectEventType.setEventMasterId(eventTicketDetails.tktmasterid);
                                selectEventType.setTktpriceid(tktpriceid);
                                mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);

                            } else if (eventTicketCost.getCount() != 0 && position == 4) {
                                int count = eventTicketCost.getCount();
                                int totalPeapleCount = eventTicketCost.getTotalCount();
                                count = count - 5;
                                totalPeapleCount = totalPeapleCount - 5;
                                holder.totalPeapleCountTV.setText("" + totalPeapleCount);
                                eventTicketCost.setCount(count);
                                eventTicketCost.setTotalCount(count);
                                String tktpriceid = eventTicketDetails.tktpriceid;
                                SelectEventType selectEventType = selectEventTypes.get(position);
                                selectEventType.setCount(count);
                                selectEventType.setEventMasterId(eventTicketDetails.tktmasterid);
                                selectEventType.setTktpriceid(tktpriceid);
                                mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);
                            } else if (eventTicketCost.getCount() != 0 && position == 5) {
                                int count = eventTicketCost.getCount();
                                int totalPeapleCount = eventTicketCost.getTotalCount();
                                count = count - 6;
                                totalPeapleCount = totalPeapleCount - 6;
                                holder.totalPeapleCountTV.setText("" + totalPeapleCount);
                                eventTicketCost.setCount(count);
                                eventTicketCost.setTotalCount(count);
                                String tktpriceid = eventTicketDetails.tktpriceid;
                                SelectEventType selectEventType = selectEventTypes.get(position);
                                selectEventType.setCount(count);
                                selectEventType.setEventMasterId(eventTicketDetails.tktmasterid);
                                selectEventType.setTktpriceid(tktpriceid);
                                mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);
                            }

                        } else if (eventID == 423) {

                            if (eventTicketCost.getCount() != 0 && position == 0) {
                                int count = eventTicketCost.getCount();
                                int totalPeapleCount = eventTicketCost.getTotalCount();
                                count = count - 1;
                                totalPeapleCount = totalPeapleCount - 1;
                                holder.totalPeapleCountTV.setText("" + totalPeapleCount);
                                eventTicketCost.setCount(count);
                                eventTicketCost.setTotalCount(count);
                                String tktpriceid = eventTicketDetails.tktpriceid;
                                SelectEventType selectEventType = selectEventTypes.get(position);
                                selectEventType.setCount(count);
                                selectEventType.setEventMasterId(eventTicketDetails.tktmasterid);
                                selectEventType.setTktpriceid(tktpriceid);
                                mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);

                            } else if (eventTicketCost.getCount() != 0 && position == 1) {
                                int count = eventTicketCost.getCount();
                                int totalPeapleCount = eventTicketCost.getTotalCount();
                                count = count - 2;
                                totalPeapleCount = totalPeapleCount - 2;
                                holder.totalPeapleCountTV.setText("" + totalPeapleCount);
                                eventTicketCost.setCount(count);
                                eventTicketCost.setTotalCount(count);
                                String tktpriceid = eventTicketDetails.tktpriceid;
                                SelectEventType selectEventType = selectEventTypes.get(position);
                                selectEventType.setCount(count);
                                selectEventType.setEventMasterId(eventTicketDetails.tktmasterid);
                                selectEventType.setTktpriceid(tktpriceid);
                                mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);

                            } else if (eventTicketCost.getCount() != 0 && position == 2) {
                                int count = eventTicketCost.getCount();
                                int totalPeapleCount = eventTicketCost.getTotalCount();
                                count = count - 3;
                                totalPeapleCount = totalPeapleCount - 3;
                                holder.totalPeapleCountTV.setText("" + totalPeapleCount);
                                eventTicketCost.setCount(count);
                                eventTicketCost.setTotalCount(count);
                                String tktpriceid = eventTicketDetails.tktpriceid;
                                SelectEventType selectEventType = selectEventTypes.get(position);
                                selectEventType.setCount(count);
                                selectEventType.setEventMasterId(eventTicketDetails.tktmasterid);
                                selectEventType.setTktpriceid(tktpriceid);
                                mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);

                            } else if (eventTicketCost.getCount() != 0 && position == 3) {
                                int count = eventTicketCost.getCount();
                                int totalPeapleCount = eventTicketCost.getTotalCount();
                                count = count - 4;
                                totalPeapleCount = totalPeapleCount - 4;
                                holder.totalPeapleCountTV.setText("" + totalPeapleCount);
                                eventTicketCost.setCount(count);
                                eventTicketCost.setTotalCount(count);
                                String tktpriceid = eventTicketDetails.tktpriceid;
                                SelectEventType selectEventType = selectEventTypes.get(position);
                                selectEventType.setCount(count);
                                selectEventType.setEventMasterId(eventTicketDetails.tktmasterid);
                                selectEventType.setTktpriceid(tktpriceid);
                                mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);

                            } else if (eventTicketCost.getCount() != 0 && position == 4) {
                                int count = eventTicketCost.getCount();
                                int totalPeapleCount = eventTicketCost.getTotalCount();
                                count = count - 5;
                                totalPeapleCount = totalPeapleCount - 5;
                                holder.totalPeapleCountTV.setText("" + totalPeapleCount);
                                eventTicketCost.setCount(count);
                                eventTicketCost.setTotalCount(count);
                                String tktpriceid = eventTicketDetails.tktpriceid;
                                SelectEventType selectEventType = selectEventTypes.get(position);
                                selectEventType.setCount(count);
                                selectEventType.setEventMasterId(eventTicketDetails.tktmasterid);
                                selectEventType.setTktpriceid(tktpriceid);
                                mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);
                            }


                        } else if (eventTicketCost.getCount() != 0) {
                            int count = eventTicketCost.getCount();
                            int totalPeapleCount = eventTicketCost.getTotalCount();
                            count = count - 1;
                            totalPeapleCount = totalPeapleCount - 1;
                            holder.totalPeapleCountTV.setText("" + totalPeapleCount);
                            eventTicketCost.setCount(count);
                            eventTicketCost.setTotalCount(count);
                            String tktpriceid = eventTicketDetails.tktpriceid;
                            SelectEventType selectEventType = selectEventTypes.get(position);
                            selectEventType.setCount(count);
                            selectEventType.setEventMasterId(eventTicketDetails.tktmasterid);
                            selectEventType.setTktpriceid(tktpriceid);
                            mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);
                        }

                    } else {
                        Toast.makeText(EventsDetailsActivity.this, "Please select date for book Tickets", Toast.LENGTH_SHORT).show();

                    }

                }
            });

            holder.incImageButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String currentCount = holder.totalPeapleCountTV.getText().toString().trim();

                    int currentCountLength = Integer.parseInt(currentCount);
                    if (eventSelectedDate != null) {

                        if (eventID == 421) {
                            if (boatType == 1) {
                                if (currentCountLength < 35) {
                                    int count = minCountBoat1;
                                    int totalPeapleCount = minPeapleCountBoat1;
                                    count = count + 1;
                                    totalPeapleCount = totalPeapleCount + 1;
                                    holder.totalPeapleCountTV.setText("" + totalPeapleCount);
                                    eventTicketCost.setCount(count);
                                    eventTicketCost.setTotalCount(totalPeapleCount);
                                    mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);
                                    String tktpriceid = eventTicketDetails.tktpriceid;
                                    SelectEventType selectEventType = selectEventTypes.get(position);
                                    selectEventType.setCount(count);
                                    selectEventType.setTktpriceid(tktpriceid);
                                    selectEventType.setEventMasterId(eventTicketDetails.tktmasterid);

                                    minCountBoat1 = count;
                                    minPeapleCountBoat1 = totalPeapleCount;

                                }

                            } else if (boatType == 2) {
                                if (currentCountLength < 19) {
                                    int count = minCountBoat2;
                                    int totalPeapleCount = minPeapleCountBoat2;
                                    count = count + 1;
                                    totalPeapleCount = totalPeapleCount + 1;
                                    holder.totalPeapleCountTV.setText("" + totalPeapleCount);
                                    eventTicketCost.setCount(count);
                                    eventTicketCost.setTotalCount(totalPeapleCount);
                                    mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);
                                    String tktpriceid = eventTicketDetails.tktpriceid;
                                    SelectEventType selectEventType = selectEventTypes.get(position);
                                    selectEventType.setCount(count);
                                    selectEventType.setTktpriceid(tktpriceid);
                                    selectEventType.setEventMasterId(eventTicketDetails.tktmasterid);
                                    minCountBoat2 = count;
                                    minPeapleCountBoat2 = totalPeapleCount;

                                }

                            } else if (boatType == 3) {
                                if (currentCountLength < 13) {
                                    int count = eventTicketCost.getCount();
                                    int totalPeapleCount = eventTicketCost.getTotalCount();
                                    count = count + 1;
                                    totalPeapleCount = totalPeapleCount + 1;
                                    holder.totalPeapleCountTV.setText("" + totalPeapleCount);
                                    eventTicketCost.setCount(count);
                                    eventTicketCost.setTotalCount(totalPeapleCount);
                                    mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);
                                    String tktpriceid = eventTicketDetails.tktpriceid;
                                    SelectEventType selectEventType = selectEventTypes.get(position);
                                    selectEventType.setCount(count);
                                    selectEventType.setTktpriceid(tktpriceid);
                                    selectEventType.setEventMasterId(eventTicketDetails.tktmasterid);
                                }

                            }


                        } else if (eventID == 420) {
                            if (position == 0 && currentCountLength < 1) {
                                int count = eventTicketCost.getCount();
                                int totalPeapleCount = eventTicketCost.getTotalCount();
                                count = count + 1;
                                totalPeapleCount = totalPeapleCount + 1;
                                holder.totalPeapleCountTV.setText("" + totalPeapleCount);
                                eventTicketCost.setCount(count);
                                eventTicketCost.setTotalCount(totalPeapleCount);
                                mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);
                                String tktpriceid = eventTicketDetails.tktpriceid;
                                SelectEventType selectEventType = selectEventTypes.get(position);
                                selectEventType.setCount(count);
                                selectEventType.setTktpriceid(tktpriceid);
                                selectEventType.setEventMasterId(eventTicketDetails.tktmasterid);

                            } else if (position == 1 && currentCountLength < 2) {
                                int count = eventTicketCost.getCount();
                                int totalPeapleCount = eventTicketCost.getTotalCount();
                                count = count + 2;
                                totalPeapleCount = totalPeapleCount + 2;
                                holder.totalPeapleCountTV.setText("" + totalPeapleCount);
                                eventTicketCost.setCount(count);
                                eventTicketCost.setTotalCount(totalPeapleCount);
                                mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);
                                String tktpriceid = eventTicketDetails.tktpriceid;
                                SelectEventType selectEventType = selectEventTypes.get(position);
                                selectEventType.setCount(count);
                                selectEventType.setTktpriceid(tktpriceid);
                                selectEventType.setEventMasterId(eventTicketDetails.tktmasterid);

                            } else if (position == 2 && currentCountLength < 3) {
                                int count = eventTicketCost.getCount();
                                int totalPeapleCount = eventTicketCost.getTotalCount();
                                count = count + 3;
                                totalPeapleCount = totalPeapleCount + 3;
                                holder.totalPeapleCountTV.setText("" + totalPeapleCount);
                                eventTicketCost.setCount(count);
                                eventTicketCost.setTotalCount(totalPeapleCount);
                                mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);
                                String tktpriceid = eventTicketDetails.tktpriceid;
                                SelectEventType selectEventType = selectEventTypes.get(position);
                                selectEventType.setCount(count);
                                selectEventType.setTktpriceid(tktpriceid);
                                selectEventType.setEventMasterId(eventTicketDetails.tktmasterid);

                            } else if (position == 3 && currentCountLength < 4) {
                                int count = eventTicketCost.getCount();
                                int totalPeapleCount = eventTicketCost.getTotalCount();
                                count = count + 4;
                                totalPeapleCount = totalPeapleCount + 4;
                                holder.totalPeapleCountTV.setText("" + totalPeapleCount);
                                eventTicketCost.setCount(count);
                                eventTicketCost.setTotalCount(totalPeapleCount);
                                mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);
                                String tktpriceid = eventTicketDetails.tktpriceid;
                                SelectEventType selectEventType = selectEventTypes.get(position);
                                selectEventType.setCount(count);
                                selectEventType.setTktpriceid(tktpriceid);
                                selectEventType.setEventMasterId(eventTicketDetails.tktmasterid);

                            } else if (position == 4 && currentCountLength < 5) {
                                int count = eventTicketCost.getCount();
                                int totalPeapleCount = eventTicketCost.getTotalCount();
                                count = count + 5;
                                totalPeapleCount = totalPeapleCount + 5;
                                holder.totalPeapleCountTV.setText("" + totalPeapleCount);
                                eventTicketCost.setCount(count);
                                eventTicketCost.setTotalCount(totalPeapleCount);
                                mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);
                                String tktpriceid = eventTicketDetails.tktpriceid;
                                SelectEventType selectEventType = selectEventTypes.get(position);
                                selectEventType.setCount(count);
                                selectEventType.setTktpriceid(tktpriceid);
                                selectEventType.setEventMasterId(eventTicketDetails.tktmasterid);

                            }else if (position == 5 && currentCountLength < 6) {
                                int count = eventTicketCost.getCount();
                                int totalPeapleCount = eventTicketCost.getTotalCount();
                                count = count + 6;
                                totalPeapleCount = totalPeapleCount + 6;
                                holder.totalPeapleCountTV.setText("" + totalPeapleCount);
                                eventTicketCost.setCount(count);
                                eventTicketCost.setTotalCount(totalPeapleCount);
                                mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);
                                String tktpriceid = eventTicketDetails.tktpriceid;
                                SelectEventType selectEventType = selectEventTypes.get(position);
                                selectEventType.setCount(count);
                                selectEventType.setTktpriceid(tktpriceid);
                                selectEventType.setEventMasterId(eventTicketDetails.tktmasterid);

                            }

                        } else if (eventID == 363) {

                            if (position == 0) {

                                if (currentCountLength < 30) {
                                    int count = minCountDhow;
                                    int totalPeapleCount = minPeapleCountDhow;
                                    count = count + 1;
                                    totalPeapleCount = totalPeapleCount + 1;
                                    holder.totalPeapleCountTV.setText("" + totalPeapleCount);
                                    eventTicketCost.setCount(count);
                                    eventTicketCost.setTotalCount(totalPeapleCount);
                                    mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);
                                    String tktpriceid = eventTicketDetails.tktpriceid;
                                    SelectEventType selectEventType = selectEventTypes.get(position);
                                    selectEventType.setCount(count);
                                    selectEventType.setTktpriceid(tktpriceid);
                                    selectEventType.setEventMasterId(eventTicketDetails.tktmasterid);
                                    minCountDhow = count;
                                    minPeapleCountDhow = totalPeapleCount;
                                    if(showPopup==0){
                                        showPopup=1;
                                        new AlertDialog.Builder(EventsDetailsActivity.this).setMessage("This ticket price is valid per person only when the number of people is a minimum of 20 and maximum of 30 ( Inclusive of Meals & Soft Drinks)").setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();
                                            }
                                        }).show();

                                    }

                                }

                            } else if (position == 1) {
                                if (currentCountLength < 1) {
                                    int count = eventTicketCost.getCount();
                                    int totalPeapleCount = eventTicketCost.getTotalCount();
                                    count = count + 1;
                                    totalPeapleCount = totalPeapleCount + 1;
                                    holder.totalPeapleCountTV.setText("" + totalPeapleCount);
                                    eventTicketCost.setCount(count);
                                    eventTicketCost.setTotalCount(totalPeapleCount);
                                    mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);
                                    String tktpriceid = eventTicketDetails.tktpriceid;
                                    SelectEventType selectEventType = selectEventTypes.get(position);
                                    selectEventType.setCount(count);
                                    selectEventType.setTktpriceid(tktpriceid);
                                    selectEventType.setEventMasterId(eventTicketDetails.tktmasterid);
                                }

                            } else if (position == 2) {
                                if (currentCountLength < 1) {
                                    int count = eventTicketCost.getCount();
                                    int totalPeapleCount = eventTicketCost.getTotalCount();
                                    count = count + 1;
                                    totalPeapleCount = totalPeapleCount + 1;
                                    holder.totalPeapleCountTV.setText("" + totalPeapleCount);
                                    eventTicketCost.setCount(count);
                                    eventTicketCost.setTotalCount(totalPeapleCount);
                                    mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);
                                    String tktpriceid = eventTicketDetails.tktpriceid;
                                    SelectEventType selectEventType = selectEventTypes.get(position);
                                    selectEventType.setCount(count);
                                    selectEventType.setTktpriceid(tktpriceid);
                                    selectEventType.setEventMasterId(eventTicketDetails.tktmasterid);
                                }

                            }


                        } else if (eventID == 422) {
                            if (position == 0 && currentCountLength < 1) {
                                int count = eventTicketCost.getCount();
                                int totalPeapleCount = eventTicketCost.getTotalCount();
                                count = count + 1;
                                totalPeapleCount = totalPeapleCount + 1;
                                holder.totalPeapleCountTV.setText("" + totalPeapleCount);
                                eventTicketCost.setCount(count);
                                eventTicketCost.setTotalCount(totalPeapleCount);
                                mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);
                                String tktpriceid = eventTicketDetails.tktpriceid;
                                SelectEventType selectEventType = selectEventTypes.get(position);
                                selectEventType.setCount(count);
                                selectEventType.setTktpriceid(tktpriceid);
                                selectEventType.setEventMasterId(eventTicketDetails.tktmasterid);

                            } else if (position == 1 && currentCountLength < 2) {
                                int count = eventTicketCost.getCount();
                                int totalPeapleCount = eventTicketCost.getTotalCount();
                                count = count + 2;
                                totalPeapleCount = totalPeapleCount + 2;
                                holder.totalPeapleCountTV.setText("" + totalPeapleCount);
                                eventTicketCost.setCount(count);
                                eventTicketCost.setTotalCount(totalPeapleCount);
                                mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);
                                String tktpriceid = eventTicketDetails.tktpriceid;
                                SelectEventType selectEventType = selectEventTypes.get(position);
                                selectEventType.setCount(count);
                                selectEventType.setTktpriceid(tktpriceid);
                                selectEventType.setEventMasterId(eventTicketDetails.tktmasterid);

                            } else if (position == 2 && currentCountLength < 3) {
                                int count = eventTicketCost.getCount();
                                int totalPeapleCount = eventTicketCost.getTotalCount();
                                count = count + 3;
                                totalPeapleCount = totalPeapleCount + 3;
                                holder.totalPeapleCountTV.setText("" + totalPeapleCount);
                                eventTicketCost.setCount(count);
                                eventTicketCost.setTotalCount(totalPeapleCount);
                                mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);
                                String tktpriceid = eventTicketDetails.tktpriceid;
                                SelectEventType selectEventType = selectEventTypes.get(position);
                                selectEventType.setCount(count);
                                selectEventType.setTktpriceid(tktpriceid);
                                selectEventType.setEventMasterId(eventTicketDetails.tktmasterid);

                            } else if (position == 3 && currentCountLength < 4) {
                                int count = eventTicketCost.getCount();
                                int totalPeapleCount = eventTicketCost.getTotalCount();
                                count = count + 4;
                                totalPeapleCount = totalPeapleCount + 4;
                                holder.totalPeapleCountTV.setText("" + totalPeapleCount);
                                eventTicketCost.setCount(count);
                                eventTicketCost.setTotalCount(totalPeapleCount);
                                mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);
                                String tktpriceid = eventTicketDetails.tktpriceid;
                                SelectEventType selectEventType = selectEventTypes.get(position);
                                selectEventType.setCount(count);
                                selectEventType.setTktpriceid(tktpriceid);
                                selectEventType.setEventMasterId(eventTicketDetails.tktmasterid);

                            } else if (position == 4 && currentCountLength < 5) {
                                int count = eventTicketCost.getCount();
                                int totalPeapleCount = eventTicketCost.getTotalCount();
                                count = count + 5;
                                totalPeapleCount = totalPeapleCount + 5;
                                holder.totalPeapleCountTV.setText("" + totalPeapleCount);
                                eventTicketCost.setCount(count);
                                eventTicketCost.setTotalCount(totalPeapleCount);
                                mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);
                                String tktpriceid = eventTicketDetails.tktpriceid;
                                SelectEventType selectEventType = selectEventTypes.get(position);
                                selectEventType.setCount(count);
                                selectEventType.setTktpriceid(tktpriceid);
                                selectEventType.setEventMasterId(eventTicketDetails.tktmasterid);

                            } else if (position == 5 && currentCountLength < 6) {
                                int count = eventTicketCost.getCount();
                                int totalPeapleCount = eventTicketCost.getTotalCount();
                                count = count + 6;
                                totalPeapleCount = totalPeapleCount + 6;
                                holder.totalPeapleCountTV.setText("" + totalPeapleCount);
                                eventTicketCost.setCount(count);
                                eventTicketCost.setTotalCount(totalPeapleCount);
                                mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);
                                String tktpriceid = eventTicketDetails.tktpriceid;
                                SelectEventType selectEventType = selectEventTypes.get(position);
                                selectEventType.setCount(count);
                                selectEventType.setTktpriceid(tktpriceid);
                                selectEventType.setEventMasterId(eventTicketDetails.tktmasterid);

                            }


                        } else if (eventID == 423) {
                            if (position == 0 && currentCountLength < 1) {
                                int count = eventTicketCost.getCount();
                                int totalPeapleCount = eventTicketCost.getTotalCount();
                                count = count + 1;
                                totalPeapleCount = totalPeapleCount + 1;
                                holder.totalPeapleCountTV.setText("" + totalPeapleCount);
                                eventTicketCost.setCount(count);
                                eventTicketCost.setTotalCount(totalPeapleCount);
                                mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);
                                String tktpriceid = eventTicketDetails.tktpriceid;
                                SelectEventType selectEventType = selectEventTypes.get(position);
                                selectEventType.setCount(count);
                                selectEventType.setTktpriceid(tktpriceid);
                                selectEventType.setEventMasterId(eventTicketDetails.tktmasterid);

                            } else if (position == 1 && currentCountLength < 2) {
                                int count = eventTicketCost.getCount();
                                int totalPeapleCount = eventTicketCost.getTotalCount();
                                count = count + 2;
                                totalPeapleCount = totalPeapleCount + 2;
                                holder.totalPeapleCountTV.setText("" + totalPeapleCount);
                                eventTicketCost.setCount(count);
                                eventTicketCost.setTotalCount(totalPeapleCount);
                                mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);
                                String tktpriceid = eventTicketDetails.tktpriceid;
                                SelectEventType selectEventType = selectEventTypes.get(position);
                                selectEventType.setCount(count);
                                selectEventType.setTktpriceid(tktpriceid);
                                selectEventType.setEventMasterId(eventTicketDetails.tktmasterid);

                            } else if (position == 2 && currentCountLength < 3) {
                                int count = eventTicketCost.getCount();
                                int totalPeapleCount = eventTicketCost.getTotalCount();
                                count = count + 3;
                                totalPeapleCount = totalPeapleCount + 3;
                                holder.totalPeapleCountTV.setText("" + totalPeapleCount);
                                eventTicketCost.setCount(count);
                                eventTicketCost.setTotalCount(totalPeapleCount);
                                mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);
                                String tktpriceid = eventTicketDetails.tktpriceid;
                                SelectEventType selectEventType = selectEventTypes.get(position);
                                selectEventType.setCount(count);
                                selectEventType.setTktpriceid(tktpriceid);
                                selectEventType.setEventMasterId(eventTicketDetails.tktmasterid);

                            } else if (position == 3 && currentCountLength < 4) {
                                int count = eventTicketCost.getCount();
                                int totalPeapleCount = eventTicketCost.getTotalCount();
                                count = count + 4;
                                totalPeapleCount = totalPeapleCount + 4;
                                holder.totalPeapleCountTV.setText("" + totalPeapleCount);
                                eventTicketCost.setCount(count);
                                eventTicketCost.setTotalCount(totalPeapleCount);
                                mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);
                                String tktpriceid = eventTicketDetails.tktpriceid;
                                SelectEventType selectEventType = selectEventTypes.get(position);
                                selectEventType.setCount(count);
                                selectEventType.setTktpriceid(tktpriceid);
                                selectEventType.setEventMasterId(eventTicketDetails.tktmasterid);

                            } else if (position + 1 == 5 && currentCountLength < 5) {
                                int count = eventTicketCost.getCount();
                                int totalPeapleCount = eventTicketCost.getTotalCount();
                                count = count + 5;
                                totalPeapleCount = totalPeapleCount + 5;
                                holder.totalPeapleCountTV.setText("" + totalPeapleCount);
                                eventTicketCost.setCount(count);
                                eventTicketCost.setTotalCount(totalPeapleCount);
                                mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);
                                String tktpriceid = eventTicketDetails.tktpriceid;
                                SelectEventType selectEventType = selectEventTypes.get(position);
                                selectEventType.setCount(count);
                                selectEventType.setTktpriceid(tktpriceid);
                                selectEventType.setEventMasterId(eventTicketDetails.tktmasterid);

                            }

                        } else if (eventID == 417) {
                            if (position == 0 && currentCountLength < 1) {
                                int count = eventTicketCost.getCount();
                                int totalPeapleCount = eventTicketCost.getTotalCount();
                                count = count + 1;
                                totalPeapleCount = totalPeapleCount + 1;
                                holder.totalPeapleCountTV.setText("" + totalPeapleCount);
                                eventTicketCost.setCount(count);
                                eventTicketCost.setTotalCount(totalPeapleCount);
                                mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);
                                String tktpriceid = eventTicketDetails.tktpriceid;
                                SelectEventType selectEventType = selectEventTypes.get(position);
                                selectEventType.setCount(count);
                                selectEventType.setTktpriceid(tktpriceid);
                                selectEventType.setEventMasterId(eventTicketDetails.tktmasterid);

                            } else if (position == 1 && currentCountLength < 2) {
                                int count = eventTicketCost.getCount();
                                int totalPeapleCount = eventTicketCost.getTotalCount();
                                count = count + 2;
                                totalPeapleCount = totalPeapleCount + 2;
                                holder.totalPeapleCountTV.setText("" + totalPeapleCount);
                                eventTicketCost.setCount(count);
                                eventTicketCost.setTotalCount(totalPeapleCount);
                                mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);
                                String tktpriceid = eventTicketDetails.tktpriceid;
                                SelectEventType selectEventType = selectEventTypes.get(position);
                                selectEventType.setCount(count);
                                selectEventType.setTktpriceid(tktpriceid);
                                selectEventType.setEventMasterId(eventTicketDetails.tktmasterid);

                            } else if (position == 2 && currentCountLength < 3) {
                                int count = eventTicketCost.getCount();
                                int totalPeapleCount = eventTicketCost.getTotalCount();
                                count = count + 3;
                                totalPeapleCount = totalPeapleCount + 3;
                                holder.totalPeapleCountTV.setText("" + totalPeapleCount);
                                eventTicketCost.setCount(count);
                                eventTicketCost.setTotalCount(totalPeapleCount);
                                mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);
                                String tktpriceid = eventTicketDetails.tktpriceid;
                                SelectEventType selectEventType = selectEventTypes.get(position);
                                selectEventType.setCount(count);
                                selectEventType.setTktpriceid(tktpriceid);
                                selectEventType.setEventMasterId(eventTicketDetails.tktmasterid);

                            } else if (position == 3 && currentCountLength < 4) {
                                int count = eventTicketCost.getCount();
                                int totalPeapleCount = eventTicketCost.getTotalCount();
                                count = count + 4;
                                totalPeapleCount = totalPeapleCount + 4;
                                holder.totalPeapleCountTV.setText("" + totalPeapleCount);
                                eventTicketCost.setCount(count);
                                eventTicketCost.setTotalCount(totalPeapleCount);
                                mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);
                                String tktpriceid = eventTicketDetails.tktpriceid;
                                SelectEventType selectEventType = selectEventTypes.get(position);
                                selectEventType.setCount(count);
                                selectEventType.setTktpriceid(tktpriceid);
                                selectEventType.setEventMasterId(eventTicketDetails.tktmasterid);

                            } else if (position + 1 == 5 && currentCountLength < 5) {
                                int count = eventTicketCost.getCount();
                                int totalPeapleCount = eventTicketCost.getTotalCount();
                                count = count + 5;
                                totalPeapleCount = totalPeapleCount + 5;
                                holder.totalPeapleCountTV.setText("" + totalPeapleCount);
                                eventTicketCost.setCount(count);
                                eventTicketCost.setTotalCount(totalPeapleCount);
                                mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);
                                String tktpriceid = eventTicketDetails.tktpriceid;
                                SelectEventType selectEventType = selectEventTypes.get(position);
                                selectEventType.setCount(count);
                                selectEventType.setTktpriceid(tktpriceid);
                                selectEventType.setEventMasterId(eventTicketDetails.tktmasterid);

                            } else if (position + 1 == 6 && currentCountLength < 6) {
                                int count = eventTicketCost.getCount();
                                int totalPeapleCount = eventTicketCost.getTotalCount();
                                count = count + 6;
                                totalPeapleCount = totalPeapleCount + 6;
                                holder.totalPeapleCountTV.setText("" + totalPeapleCount);
                                eventTicketCost.setCount(count);
                                eventTicketCost.setTotalCount(totalPeapleCount);
                                mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);
                                String tktpriceid = eventTicketDetails.tktpriceid;
                                SelectEventType selectEventType = selectEventTypes.get(position);
                                selectEventType.setCount(count);
                                selectEventType.setTktpriceid(tktpriceid);
                                selectEventType.setEventMasterId(eventTicketDetails.tktmasterid);

                            } else if (position + 1 == 7 && currentCountLength < 7) {
                                int count = eventTicketCost.getCount();
                                int totalPeapleCount = eventTicketCost.getTotalCount();
                                count = count + 7;
                                totalPeapleCount = totalPeapleCount + 7;
                                holder.totalPeapleCountTV.setText("" + totalPeapleCount);
                                eventTicketCost.setCount(count);
                                eventTicketCost.setTotalCount(totalPeapleCount);
                                mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);
                                String tktpriceid = eventTicketDetails.tktpriceid;
                                SelectEventType selectEventType = selectEventTypes.get(position);
                                selectEventType.setCount(count);
                                selectEventType.setTktpriceid(tktpriceid);
                                selectEventType.setEventMasterId(eventTicketDetails.tktmasterid);

                            }

                        }else if(eventID == 354){
                            if(position==0){
                                if (currentCountLength < 5) {
                                    int count = eventTicketCost.getCount();
                                    int totalPeapleCount = eventTicketCost.getTotalCount();
                                    count = count + 1;
                                    totalPeapleCount = totalPeapleCount + 1;
                                    holder.totalPeapleCountTV.setText("" + totalPeapleCount);
                                    eventTicketCost.setCount(count);
                                    eventTicketCost.setTotalCount(totalPeapleCount);
                                    mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);
                                    String tktpriceid = eventTicketDetails.tktpriceid;
                                    SelectEventType selectEventType = selectEventTypes.get(position);
                                    selectEventType.setCount(count);
                                    selectEventType.setTktpriceid(tktpriceid);
                                    selectEventType.setEventMasterId(eventTicketDetails.tktmasterid);
                                }
                            }else   if(position==1){
                                if (currentCountLength < 4) {
                                    int count = eventTicketCost.getCount();
                                    int totalPeapleCount = eventTicketCost.getTotalCount();
                                    count = count + 1;
                                    totalPeapleCount = totalPeapleCount + 1;
                                    holder.totalPeapleCountTV.setText("" + totalPeapleCount);
                                    eventTicketCost.setCount(count);
                                    eventTicketCost.setTotalCount(totalPeapleCount);
                                    mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);
                                    String tktpriceid = eventTicketDetails.tktpriceid;
                                    SelectEventType selectEventType = selectEventTypes.get(position);
                                    selectEventType.setCount(count);
                                    selectEventType.setTktpriceid(tktpriceid);
                                    selectEventType.setEventMasterId(eventTicketDetails.tktmasterid);
                                }
                            }else   if(position==2){
                                if (currentCountLength < 3) {
                                    int count = eventTicketCost.getCount();
                                    int totalPeapleCount = eventTicketCost.getTotalCount();
                                    count = count + 1;
                                    totalPeapleCount = totalPeapleCount + 1;
                                    holder.totalPeapleCountTV.setText("" + totalPeapleCount);
                                    eventTicketCost.setCount(count);
                                    eventTicketCost.setTotalCount(totalPeapleCount);
                                    mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);
                                    String tktpriceid = eventTicketDetails.tktpriceid;
                                    SelectEventType selectEventType = selectEventTypes.get(position);
                                    selectEventType.setCount(count);
                                    selectEventType.setTktpriceid(tktpriceid);
                                    selectEventType.setEventMasterId(eventTicketDetails.tktmasterid);
                                }
                            }else   if(position==3){
                                if (currentCountLength < 2) {
                                    int count = eventTicketCost.getCount();
                                    int totalPeapleCount = eventTicketCost.getTotalCount();
                                    count = count + 1;
                                    totalPeapleCount = totalPeapleCount + 1;
                                    holder.totalPeapleCountTV.setText("" + totalPeapleCount);
                                    eventTicketCost.setCount(count);
                                    eventTicketCost.setTotalCount(totalPeapleCount);
                                    mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);
                                    String tktpriceid = eventTicketDetails.tktpriceid;
                                    SelectEventType selectEventType = selectEventTypes.get(position);
                                    selectEventType.setCount(count);
                                    selectEventType.setTktpriceid(tktpriceid);
                                    selectEventType.setEventMasterId(eventTicketDetails.tktmasterid);
                                }
                            }else   if(position==4){
                                if (currentCountLength < 1) {
                                    int count = eventTicketCost.getCount();
                                    int totalPeapleCount = eventTicketCost.getTotalCount();
                                    count = count + 1;
                                    totalPeapleCount = totalPeapleCount + 1;
                                    holder.totalPeapleCountTV.setText("" + totalPeapleCount);
                                    eventTicketCost.setCount(count);
                                    eventTicketCost.setTotalCount(totalPeapleCount);
                                    mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);
                                    String tktpriceid = eventTicketDetails.tktpriceid;
                                    SelectEventType selectEventType = selectEventTypes.get(position);
                                    selectEventType.setCount(count);
                                    selectEventType.setTktpriceid(tktpriceid);
                                    selectEventType.setEventMasterId(eventTicketDetails.tktmasterid);
                                }
                            }else  if(position==5){
                                if (currentCountLength < 5) {
                                    int count = eventTicketCost.getCount();
                                    int totalPeapleCount = eventTicketCost.getTotalCount();
                                    count = count + 1;
                                    totalPeapleCount = totalPeapleCount + 1;
                                    holder.totalPeapleCountTV.setText("" + totalPeapleCount);
                                    eventTicketCost.setCount(count);
                                    eventTicketCost.setTotalCount(totalPeapleCount);
                                    mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);
                                    String tktpriceid = eventTicketDetails.tktpriceid;
                                    SelectEventType selectEventType = selectEventTypes.get(position);
                                    selectEventType.setCount(count);
                                    selectEventType.setTktpriceid(tktpriceid);
                                    selectEventType.setEventMasterId(eventTicketDetails.tktmasterid);
                                }
                            }else   if(position==6){
                                if (currentCountLength < 4) {
                                    int count = eventTicketCost.getCount();
                                    int totalPeapleCount = eventTicketCost.getTotalCount();
                                    count = count + 1;
                                    totalPeapleCount = totalPeapleCount + 1;
                                    holder.totalPeapleCountTV.setText("" + totalPeapleCount);
                                    eventTicketCost.setCount(count);
                                    eventTicketCost.setTotalCount(totalPeapleCount);
                                    mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);
                                    String tktpriceid = eventTicketDetails.tktpriceid;
                                    SelectEventType selectEventType = selectEventTypes.get(position);
                                    selectEventType.setCount(count);
                                    selectEventType.setTktpriceid(tktpriceid);
                                    selectEventType.setEventMasterId(eventTicketDetails.tktmasterid);
                                }
                            }else   if(position==7){
                                if (currentCountLength < 3) {
                                    int count = eventTicketCost.getCount();
                                    int totalPeapleCount = eventTicketCost.getTotalCount();
                                    count = count + 1;
                                    totalPeapleCount = totalPeapleCount + 1;
                                    holder.totalPeapleCountTV.setText("" + totalPeapleCount);
                                    eventTicketCost.setCount(count);
                                    eventTicketCost.setTotalCount(totalPeapleCount);
                                    mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);
                                    String tktpriceid = eventTicketDetails.tktpriceid;
                                    SelectEventType selectEventType = selectEventTypes.get(position);
                                    selectEventType.setCount(count);
                                    selectEventType.setTktpriceid(tktpriceid);
                                    selectEventType.setEventMasterId(eventTicketDetails.tktmasterid);
                                }
                            }else   if(position==8){
                                if (currentCountLength < 2) {
                                    int count = eventTicketCost.getCount();
                                    int totalPeapleCount = eventTicketCost.getTotalCount();
                                    count = count + 1;
                                    totalPeapleCount = totalPeapleCount + 1;
                                    holder.totalPeapleCountTV.setText("" + totalPeapleCount);
                                    eventTicketCost.setCount(count);
                                    eventTicketCost.setTotalCount(totalPeapleCount);
                                    mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);
                                    String tktpriceid = eventTicketDetails.tktpriceid;
                                    SelectEventType selectEventType = selectEventTypes.get(position);
                                    selectEventType.setCount(count);
                                    selectEventType.setTktpriceid(tktpriceid);
                                    selectEventType.setEventMasterId(eventTicketDetails.tktmasterid);
                                }
                            }else   if(position==9){
                                if (currentCountLength < 1) {
                                    int count = eventTicketCost.getCount();
                                    int totalPeapleCount = eventTicketCost.getTotalCount();
                                    count = count + 1;
                                    totalPeapleCount = totalPeapleCount + 1;
                                    holder.totalPeapleCountTV.setText("" + totalPeapleCount);
                                    eventTicketCost.setCount(count);
                                    eventTicketCost.setTotalCount(totalPeapleCount);
                                    mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);
                                    String tktpriceid = eventTicketDetails.tktpriceid;
                                    SelectEventType selectEventType = selectEventTypes.get(position);
                                    selectEventType.setCount(count);
                                    selectEventType.setTktpriceid(tktpriceid);
                                    selectEventType.setEventMasterId(eventTicketDetails.tktmasterid);
                                }
                            }else      if(position==10){
                                if (currentCountLength < 5) {
                                    int count = eventTicketCost.getCount();
                                    int totalPeapleCount = eventTicketCost.getTotalCount();
                                    count = count + 1;
                                    totalPeapleCount = totalPeapleCount + 1;
                                    holder.totalPeapleCountTV.setText("" + totalPeapleCount);
                                    eventTicketCost.setCount(count);
                                    eventTicketCost.setTotalCount(totalPeapleCount);
                                    mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);
                                    String tktpriceid = eventTicketDetails.tktpriceid;
                                    SelectEventType selectEventType = selectEventTypes.get(position);
                                    selectEventType.setCount(count);
                                    selectEventType.setTktpriceid(tktpriceid);
                                    selectEventType.setEventMasterId(eventTicketDetails.tktmasterid);
                                }
                            }else   if(position==11){
                                if (currentCountLength < 4) {
                                    int count = eventTicketCost.getCount();
                                    int totalPeapleCount = eventTicketCost.getTotalCount();
                                    count = count + 1;
                                    totalPeapleCount = totalPeapleCount + 1;
                                    holder.totalPeapleCountTV.setText("" + totalPeapleCount);
                                    eventTicketCost.setCount(count);
                                    eventTicketCost.setTotalCount(totalPeapleCount);
                                    mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);
                                    String tktpriceid = eventTicketDetails.tktpriceid;
                                    SelectEventType selectEventType = selectEventTypes.get(position);
                                    selectEventType.setCount(count);
                                    selectEventType.setTktpriceid(tktpriceid);
                                    selectEventType.setEventMasterId(eventTicketDetails.tktmasterid);
                                }
                            }else   if(position==12){
                                if (currentCountLength < 3) {
                                    int count = eventTicketCost.getCount();
                                    int totalPeapleCount = eventTicketCost.getTotalCount();
                                    count = count + 1;
                                    totalPeapleCount = totalPeapleCount + 1;
                                    holder.totalPeapleCountTV.setText("" + totalPeapleCount);
                                    eventTicketCost.setCount(count);
                                    eventTicketCost.setTotalCount(totalPeapleCount);
                                    mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);
                                    String tktpriceid = eventTicketDetails.tktpriceid;
                                    SelectEventType selectEventType = selectEventTypes.get(position);
                                    selectEventType.setCount(count);
                                    selectEventType.setTktpriceid(tktpriceid);
                                    selectEventType.setEventMasterId(eventTicketDetails.tktmasterid);
                                }
                            }else   if(position==13){
                                if (currentCountLength < 2) {
                                    int count = eventTicketCost.getCount();
                                    int totalPeapleCount = eventTicketCost.getTotalCount();
                                    count = count + 1;
                                    totalPeapleCount = totalPeapleCount + 1;
                                    holder.totalPeapleCountTV.setText("" + totalPeapleCount);
                                    eventTicketCost.setCount(count);
                                    eventTicketCost.setTotalCount(totalPeapleCount);
                                    mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);
                                    String tktpriceid = eventTicketDetails.tktpriceid;
                                    SelectEventType selectEventType = selectEventTypes.get(position);
                                    selectEventType.setCount(count);
                                    selectEventType.setTktpriceid(tktpriceid);
                                    selectEventType.setEventMasterId(eventTicketDetails.tktmasterid);
                                }
                            }else   if(position==14){
                                if (currentCountLength < 1) {
                                    int count = eventTicketCost.getCount();
                                    int totalPeapleCount = eventTicketCost.getTotalCount();
                                    count = count + 1;
                                    totalPeapleCount = totalPeapleCount + 1;
                                    holder.totalPeapleCountTV.setText("" + totalPeapleCount);
                                    eventTicketCost.setCount(count);
                                    eventTicketCost.setTotalCount(totalPeapleCount);
                                    mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);
                                    String tktpriceid = eventTicketDetails.tktpriceid;
                                    SelectEventType selectEventType = selectEventTypes.get(position);
                                    selectEventType.setCount(count);
                                    selectEventType.setTktpriceid(tktpriceid);
                                    selectEventType.setEventMasterId(eventTicketDetails.tktmasterid);
                                }
                            }else   if(position==15){
                                if (currentCountLength < 6) {
                                    int count = eventTicketCost.getCount();
                                    int totalPeapleCount = eventTicketCost.getTotalCount();
                                    count = count + 1;
                                    totalPeapleCount = totalPeapleCount + 1;
                                    holder.totalPeapleCountTV.setText("" + totalPeapleCount);
                                    eventTicketCost.setCount(count);
                                    eventTicketCost.setTotalCount(totalPeapleCount);
                                    mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);
                                    String tktpriceid = eventTicketDetails.tktpriceid;
                                    SelectEventType selectEventType = selectEventTypes.get(position);
                                    selectEventType.setCount(count);
                                    selectEventType.setTktpriceid(tktpriceid);
                                    selectEventType.setEventMasterId(eventTicketDetails.tktmasterid);
                                }
                            }
                        } else if (currentCountLength < 10) {
                            int count = eventTicketCost.getCount();
                            int totalPeapleCount = eventTicketCost.getTotalCount();
                            count = count + 1;
                            totalPeapleCount = totalPeapleCount + 1;
                            holder.totalPeapleCountTV.setText("" + totalPeapleCount);
                            eventTicketCost.setCount(count);
                            eventTicketCost.setTotalCount(totalPeapleCount);
                            mTicketCountSelectionChangeListener.onTicketCountChanged(eventTicketCost);
                            String tktpriceid = eventTicketDetails.tktpriceid;
                            SelectEventType selectEventType = selectEventTypes.get(position);
                            selectEventType.setCount(count);
                            selectEventType.setTktpriceid(tktpriceid);
                            selectEventType.setEventMasterId(eventTicketDetails.tktmasterid);
                        }


                    } else {
                        Toast.makeText(EventsDetailsActivity.this, "Please select date for book Tickets", Toast.LENGTH_SHORT).show();
                    }
                }
            });

        }

        @Override
        public int getItemCount() {
            return eventsList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private Spinner sp_eventDetails;
            private TextView tv_ticketType, tv_price, totalPeapleCountTV;
            private ImageView incImageButton, decImageButton;

            public ViewHolder(View itemView, int viewType) {
                super(itemView);
                tv_ticketType = (TextView) itemView.findViewById(R.id.tv_ticketType);
                tv_price = (TextView) itemView.findViewById(R.id.tv_price_edetails);
                totalPeapleCountTV = (TextView) itemView.findViewById(R.id.totalPeapleCountTV);
                incImageButton = (ImageView) itemView.findViewById(R.id.incImageButton);
                decImageButton = (ImageView) itemView.findViewById(R.id.decImageButton);

            }

        }
    }
}
