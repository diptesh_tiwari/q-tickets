package com.mobile.android.qtickets.activities;

import android.content.ContentResolver;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.VideoView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mobile.android.qtickets.R;
import com.mobile.android.qtickets.adapters.NavigationDrawerAdapter;
import com.mobile.android.qtickets.constants.AppConstants;
import com.mobile.android.qtickets.model.UserDetailsModel;
import com.mobile.android.qtickets.search.SearchActivity;
import com.mobile.android.qtickets.utils.QTUtils;

public class DashboardActivity extends AppCompatActivity implements NavigationDrawerAdapter.NavigationDrawerListener, AdapterView.OnItemSelectedListener {
    private final TextView.OnEditorActionListener mOnEditorActionSearch = new TextView.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(final TextView v, final int actionId, final KeyEvent event) {
            switch (actionId) {
                case EditorInfo.IME_ACTION_SEARCH: {
                    final String keyword = v.getText().toString();
                    if (!keyword.isEmpty()) {
                        startSearchActivity(keyword);
                        return true;
                    }
                }
                break;
            }
            return false;
        }
    };
    private DrawerLayout mDrawerLayout; // Declaring DrawerLayout
    private Toolbar toolbar;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private TextView tv_movies, tv_events, tv_sports, tv_leisure, title_template;
    private EditText mSearchEditText;
    private VideoView mVideoView;
    private Spinner spinner_countryFilter;
    UserDetailsModel user;

    SharedPreferences prefmPrefs;
    SharedPreferences.Editor editor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prefmPrefs = PreferenceManager.getDefaultSharedPreferences(this);
         editor = prefmPrefs.edit();
        editor.putString(AppConstants.COUNTRY_TYPE_CURRENCY,AppConstants.COUNTRY_TYPE_QA);
        editor.commit();
        initContentView();
        initContentViewListeners();
        setupActionBar();
        setupNavigationDrawer();
        setupBackgroundVideoPlay();
    }

    private void initContentView() {
        setContentView(R.layout.activity_dashboard);

        mVideoView = (VideoView) findViewById(R.id.videoView);
        mSearchEditText = (EditText) findViewById(R.id.editText_search);
        tv_movies = (TextView) findViewById(R.id.tv_movies_home);
        tv_events = (TextView) findViewById(R.id.tv_events_home);
        tv_sports = (TextView) findViewById(R.id.tv_sports);
        tv_leisure = (TextView) findViewById(R.id.tv_leisure);
        title_template = (TextView) findViewById(R.id.title_template);
        toolbar = (Toolbar) findViewById(R.id.toolbar_dashboard);
        spinner_countryFilter = (Spinner) findViewById(R.id.spinner_countryFilter);
        String lang[] = getResources().getStringArray(R.array.countries_filter);
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, R.layout.simple_spinner_item, lang);
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        spinner_countryFilter.setAdapter(dataAdapter);
        spinner_countryFilter.setOnItemSelectedListener(this);
        //Qatar,Doha
    }

    private void setupBackgroundVideoPlay() {
        mVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(final MediaPlayer mp) {
                mp.setVolume(0, 0);
                mp.setLooping(true);
                mp.setScreenOnWhilePlaying(false);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    //mp.setVideoScalingMode(MediaPlayer.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING);
                }
            }
        });
        mVideoView.setVideoURI(Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + getPackageName() + "/"
                + R.raw.dashboard_background_video));
        mVideoView.start();
    }

    private void initContentViewListeners() {
        mSearchEditText.setOnEditorActionListener(mOnEditorActionSearch);
    }

    private void setupActionBar() {
        setSupportActionBar(toolbar);
        setTitle("");
    }

    private void setupNavigationDrawer() {
        mRecyclerView = (RecyclerView) findViewById(R.id.RecyclerView);
        mRecyclerView.setHasFixedSize(true);
//		String TITLES[] = {getResources().getString(R.string.nav_gift_vouchers), getResources().getString(R.string.nav_favorites), getResources().getString(R.string.nav_purchasehistory), getResources().getString(R.string.nav_rateapp), getResources().getString(R.string.nav_contactus), getResources().getString(R.string.nav_policies), getResources().getString(R.string.nav_shareapp)};

        //String user = (String) QTUtils.getInstance().getObjectFromSharedPreference(this, AppConstants.SHARED_USER_TAG, String.class);

        SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        Gson gson = new Gson();
        String usrDetails = mPrefs.getString(AppConstants.SHARED_USER_TAG, null);

        if (usrDetails != null) {
            java.lang.reflect.Type type = new TypeToken<UserDetailsModel>() {
            }.getType();
            try {
                user = gson.fromJson(usrDetails, type);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        String login = mPrefs.getString("login", null);
        if (login != null) {
            if (login.equalsIgnoreCase("1")) {
                String LOGIN[] = {"My Account", "Forgot Password", "Contact Us", "Log Out"};
                mAdapter = new NavigationDrawerAdapter(this, LOGIN, this);
            } else {
                String TITLES[] = {"Login", "Sign up", "Forgot Password", "Contact Us"};
                mAdapter = new NavigationDrawerAdapter(this, TITLES, this);
            }
        } else {
            String TITLES[] = {"Login", "Sign Up", "Forgot Password", "Contact Us"};
            mAdapter = new NavigationDrawerAdapter(this, TITLES, this);
        }


        mRecyclerView.setAdapter(mAdapter);

        mLayoutManager = new LinearLayoutManager(this);

        mRecyclerView.setLayoutManager(mLayoutManager);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        final ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.nav_open, R.string.nav_close) {

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                // code here will execute once the drawer is opened( As I dont
                // want anything happened whe drawer is
                // open I am not going to put anything here)
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                // Code here will execute once drawer is closed
            }
        };
        drawerToggle.setDrawerIndicatorEnabled(false);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                mDrawerLayout.openDrawer(Gravity.LEFT);
            }
        });
        mDrawerLayout.addDrawerListener(drawerToggle);
        drawerToggle.syncState();

    }

    @Override
    protected void onStart() {
        super.onStart();
        mVideoView.start();
    }

    @Override
    public void closeNavigationDrawer() {
        mDrawerLayout.closeDrawers();
    }

    private void startSearchActivity(final String keyword) {
        SearchActivity.start(this, keyword);
    }

    public void onClickMoviesOption(View pView) {
        TabsActivity.start(pView.getContext(), 0);
    }

    public void onClickEventsOption(View pView) {
        TabsActivity.start(pView.getContext(), 1);
    }

    public void onClickSportsOption(View pView) {
        TabsActivity.start(pView.getContext(), 2);
    }

    public void onClickLeisureOption(View pView) {
        TabsActivity.start(pView.getContext(), 3);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        if (position == 0) {
            title_template.setText(getResources().getString(R.string.message_whatIsQTickets) + " Qatar");
            editor.putString(AppConstants.COUNTRY_TYPE_CURRENCY,AppConstants.COUNTRY_TYPE_QA);
            editor.commit();
        } else if (position == 1) {
            title_template.setText(getResources().getString(R.string.message_whatIsQTickets) + " Bahrain");
            editor.putString(AppConstants.COUNTRY_TYPE_CURRENCY,AppConstants.COUNTRY_TYPE_BH);
            editor.commit();
        } else if (position == 2) {
            title_template.setText(getResources().getString(R.string.message_whatIsQTickets) + " UAE");
            editor.putString(AppConstants.COUNTRY_TYPE_CURRENCY,AppConstants.COUNTRY_TYPE_UAE);
            editor.commit();
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        Gson gson = new Gson();
        String usrDetails = mPrefs.getString(AppConstants.SHARED_USER_TAG, null);

        if (usrDetails != null) {
            java.lang.reflect.Type type = new TypeToken<UserDetailsModel>() {
            }.getType();
            try {
                user = gson.fromJson(usrDetails, type);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        String login = mPrefs.getString("login", null);
        if (login != null) {
            if (login.equalsIgnoreCase("1")) {
                String LOGIN[] = {"My Account", "Forgot Password", "Contact Us", "Log Out"};
                mAdapter = new NavigationDrawerAdapter(this, LOGIN, this);
            } else {
                String TITLES[] = {"Login", "Sign Up", "Forgot Password", "Contact Us"};
                mAdapter = new NavigationDrawerAdapter(this, TITLES, this);
            }
        } else {
            String TITLES[] = {"Login", "Sign Up", "Forgot Password", "Contact Us"};
            mAdapter = new NavigationDrawerAdapter(this, TITLES, this);
        }

        mRecyclerView.setAdapter(null);
        mRecyclerView.setAdapter(mAdapter);

        mLayoutManager = new LinearLayoutManager(this);

        mRecyclerView.setLayoutManager(mLayoutManager);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        onResume();
    }
}
