package com.mobile.android.qtickets.movie.moviedetails.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.v4.util.ArraySet;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubePlayer;
import com.google.gson.Gson;
import com.mobile.android.qtickets.R;
import com.mobile.android.qtickets.activities.LoginActivity;
import com.mobile.android.qtickets.activities.RegisterActivity;
import com.mobile.android.qtickets.activities.TabsActivity;
import com.mobile.android.qtickets.constants.AppConstants;
import com.mobile.android.qtickets.interfes.OnEventCallBack;
import com.mobile.android.qtickets.model.DateItem;
import com.mobile.android.qtickets.model.MovieSeatBlockResult;
import com.mobile.android.qtickets.movie.Movie;
import com.mobile.android.qtickets.movie.MovieParseOperation;
import com.mobile.android.qtickets.movie.ShowDateVO;
import com.mobile.android.qtickets.movie.TheatreVO;
import com.mobile.android.qtickets.movie.moviedetails.MovieShowDatesAdapter;
import com.mobile.android.qtickets.movie.moviedetails.Theatre;
import com.mobile.android.qtickets.movie.moviedetails.TheatreAndShowsAdapter;
import com.mobile.android.qtickets.movie.seatselection.activity.MovieBookingActivity;
import com.mobile.android.qtickets.movie.seatselection.activity.MovieSeatsSelectionActivity;
import com.mobile.android.qtickets.parsers.LoginParseOperation;
import com.mobile.android.qtickets.parsers.WillWatchParseOperation;
import com.mobile.android.qtickets.utils.MyDialog;
import com.mobile.android.qtickets.utils.QTUtils;
import com.thefinestartist.ytpa.YouTubePlayerActivity;
import com.thefinestartist.ytpa.enums.Orientation;
import com.thefinestartist.ytpa.utils.YouTubeUrlParser;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;

import java.io.ByteArrayInputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

/**
 * Created by Satish - 360 on 11/22/2016.
 */

public final class MovieDetailsActivity extends MovieDetailsActivityController implements View.OnClickListener, Spinner.OnItemSelectedListener, OnEventCallBack {
    private Movie movievo;
    private Integer will_watch = 0;
    private Integer will_not_watch = 0;
    private ProgressDialog dialog = null;
    private String status, error_msg, errorcode;
    private final TheatreAndShowsAdapter.OnShowTimeItemClickListener mOnShowTimeItemClickListener = new TheatreAndShowsAdapter.OnShowTimeItemClickListener() {
        @Override
        public void OnShowTimeItemClick(final String theatreName, final String showTime, final String showTimeId) {
            startBookMovieTicketsActivity(movievo.serverId, movievo.movieName, theatreName, showTime, showTimeId);
            //movievo.localMovieId
        }
    };
    private ArrayList<String> alshowdates;
    private ArrayList<String> sortedTheatres;
    private ArrayList<String> sortedMoviesShows = new ArrayList<String>();
    private String mvurl;
    private TheatreAndShowsAdapter mTheatreAndShowsAdapter;
    final RecyclerView.AdapterDataObserver adapterDataObserver = new RecyclerView.AdapterDataObserver() {
        @Override
        public void onChanged() {
            super.onChanged();
            mTheatreAndShowLinearLayout.removeAllViews();
            for (int i = 0; i < mTheatreAndShowsAdapter.getItemCount(); i++) {
                final RecyclerView.ViewHolder viewHolder = mTheatreAndShowsAdapter.onCreateViewHolder(mTheatreAndShowLinearLayout, 0);
                mTheatreAndShowLinearLayout.addView(viewHolder.itemView);
                mTheatreAndShowsAdapter.onBindViewHolder(viewHolder, i);
            }
        }
    };
    private String id;
    private String willNotWatch, willWatch;


    public static void start(Context pContext, final String sharedMovies) {
        SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(pContext);
        String movies = mPrefs.getString(AppConstants.SELECTED_MOVIE_DETAIL, null);


        Gson gson = new Gson();
        Movie  movievo=null;
        //String movies = getIntent().getStringExtra(AppConstants.INTENT_SHARED_MOVIES_TAG);
        if (movies != null) {
           movievo = gson.fromJson(movies, Movie.class);
        }


        final Bundle bundle = new Bundle();
        bundle.putString(AppConstants.INTENT_SHARED_MOVIES_TAG, movies);
        start(pContext, MovieDetailsActivity.class, bundle);

    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setNavigationUpIcon(R.drawable.back_btn);
        SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        String moviesjson = mPrefs.getString(AppConstants.SELECTED_MOVIE_DETAIL, null);

        Gson gson = new Gson();
       // String movies = getIntent().getStringExtra(AppConstants.INTENT_SHARED_MOVIES_TAG);
        if (moviesjson != null) {
            movievo = gson.fromJson(moviesjson, Movie.class);
        }

        if (!TextUtils.isEmpty(movievo.bannerurl)) {
            mvurl = movievo.thumbnailURL;
        }
        mvurl = mvurl.replaceAll("http", "https");
        setPosterImageUrl(mvurl);
        setDateSelectedListener(this);

        setMovieTitle(movievo.movieName);
        setCensor(movievo.censor);
        setDuration(convertMinutesToHours(movievo.duration));
        setLanguage(movievo.language);
        setVisionType(movievo.movieType);


        if (!movievo.movieTheatresArr.isEmpty()) {
            setTheatreAndShowAdapter();
            initDateSpinner();
            showDetailLayout(movievo.movieTheatresArr);
            setDescription(movievo.description);
            setOnDescriptionClickListener(this);
            makeDescriptionResizable(3, ".....View More", true);
            if (!TextUtils.isEmpty(movievo.IMDB_rating)) {
                setVotes(Integer.parseInt(movievo.IMDB_rating));
            }

        } else {
            setSynopsis(movievo.synopsis);
            //new GetWatch().execute();
            //String willNotWatch,willWatch

            if(!TextUtils.isEmpty(movievo.willwatch)){
                willWatch = movievo.willwatch;
            }
            if(!TextUtils.isEmpty(movievo.willnotwatch)){
                willNotWatch = movievo.willnotwatch;
            }


            if (willWatch != null & willNotWatch != null) {
                totalWatchTV.setText("" + willWatch);
                totalDontWatchTV.setText("" + willNotWatch);
                setLikes();
            }

            //  new WatchUrl(id, will_watch, will_not_watch).execute();

        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

        }
    }

    private void startBookMovieTicketsActivity(String movie_id, final String movieName, final String theatreName, final String movieTime, final String showTimeId) {


        MovieSeatsSelectionActivity.start(this, movie_id, movieName, theatreName, movieTime, showTimeId);


       /* //SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        //String movieBookingDate = mPrefs.getString(AppConstants.MOVIE_BOOKING_DATE,null);
        if (movieBookingDate != null) {
           // String date = QTUtils.getInstance().getFromSharedPreference(this, AppConstants.MOVIE_BOOKING_DATE);
            MovieSeatsSelectionActivity.start(this, movie_id, movieName, theatreName, movieBookingDate, movieTime, showTimeId);
        }*/
    }

    @Override
    public void onClickWatchTrailerButton(final View pView) {
        SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String usrDetails = mPrefs.getString(AppConstants.SHARED_USER_TAG, null);


        switch (pView.getId()) {
            case R.id.btn_trailer:


                if (TextUtils.isEmpty(movievo.TrailerURL)) {
                    Toast.makeText(this, "Trailer video is not available.", Toast.LENGTH_SHORT).show();
                    return;
                }

                Intent intent = new Intent(this, YouTubePlayerActivity.class);

// Youtube video ID (Required, You can use YouTubeUrlParser to parse Video Id from url)
                intent.putExtra(YouTubePlayerActivity.EXTRA_VIDEO_ID, YouTubeUrlParser.getVideoId(movievo.TrailerURL));

// Youtube player style (DEFAULT as default)
                intent.putExtra(YouTubePlayerActivity.EXTRA_PLAYER_STYLE, YouTubePlayer.PlayerStyle.DEFAULT);

// Screen Orientation Setting (AUTO for default)
// AUTO, AUTO_START_WITH_LANDSCAPE, ONLY_LANDSCAPE, ONLY_PORTRAIT
                intent.putExtra(YouTubePlayerActivity.EXTRA_ORIENTATION, Orientation.ONLY_LANDSCAPE);

// TheatreAndShowsAdapter audio interface when watchMovie adjust volume (true for default)
                intent.putExtra(YouTubePlayerActivity.EXTRA_SHOW_AUDIO_UI, true);

// If the video is not playable, use Youtube app or Internet Browser to play it
// (true for default)
                intent.putExtra(YouTubePlayerActivity.EXTRA_HANDLE_ERROR, true);

                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                break;
            case R.id.watchLL:
                will_watch = 1;
                will_not_watch = 0;

                if (usrDetails != null) {
                    try {
                        JSONObject json = new JSONObject(usrDetails);
                        id = json.getString("id");
                        Log.d("", "");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    new WatchUrl(id, will_watch, will_not_watch).execute();
                } else {
                    MyDialog.showWatchDontWatchDialog(this, this);
                }
                break;
            case R.id.notWatchLL:
                will_watch = 0;
                will_not_watch = 1;

                if (usrDetails != null) {
                    try {
                        JSONObject json = new JSONObject(usrDetails);
                        id = json.getString("id");
                        Log.d("", "");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    new WatchUrl(id, will_watch, will_not_watch).execute();
                } else {
                    MyDialog.showWatchDontWatchDialog(this, this);
                }
                break;

            case R.id.shareImageView:

                intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                String url = movievo.TrailerURL;
                intent.putExtra(Intent.EXTRA_TEXT, url);
                startActivity(Intent.createChooser(intent, "Share with"));
                break;

            case R.id.backImageView:
                finish();
                break;
        }

    }

    @Override
    public void onEvent(int id) {
        if (id == R.id.loginTV) {
            startActivity(id);
        } else if (R.id.singnUpTV == id) {
            startActivity(id);
        }
    }

    @Override
    public void onEvent(int id, Object object) {

    }

    private void startActivity(int id) {
        Intent intent;
        switch (id) {
            case R.id.loginTV:
                intent = new Intent(this, LoginActivity.class);
                startActivityForResult(intent, AppConstants.LOGIN);
                break;

            case R.id.singnUpTV:
                intent = new Intent(this, RegisterActivity.class);
                startActivityForResult(intent, AppConstants.REGISTER);
                break;
        }


    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == AppConstants.LOGIN) {
                //WatchUrl.
            } else if (requestCode == AppConstants.REGISTER) {
                //sendMovieDetail();
            }
        }
    }


    public class WatchUrl extends AsyncTask<String, String, String> {
        private String id;
        private Integer will_watch;
        private Integer will_not_watch;


        public WatchUrl(String id, int will_watch, int will_not_watch) {
            this.id = id;
            this.will_watch = will_watch;
            this.will_not_watch = will_not_watch;
        }

        @Override
        protected String doInBackground(String... arg0) {
            String resp;
            try {
                //movieid=123&will_watch=12&will_not_watch=0&userid=1
                String watch_url =
                        AppConstants.SERVER_URL_WATCH + "movieid=" + movievo.serverId + "&" + "will_watch=" + will_watch + "&" + "will_not_watch=" + will_not_watch + "&" + "userid=" + id;
                watch_url = watch_url.replaceAll("\\s+", "%20");
                HttpClient httpClient = new DefaultHttpClient();
                HttpContext localContext = new BasicHttpContext();
                HttpGet httpGet = new HttpGet(watch_url);
                httpGet.addHeader(BasicScheme.authenticate(new UsernamePasswordCredentials(getResources().getString(R.string.auth_username), getResources().getString(R.string.auth_password)), "UTF-8", false));
                HttpResponse response = httpClient.execute(httpGet, localContext);
                HttpEntity entity = response.getEntity();
                resp = QTUtils.getASCIIContentFromEntity(entity);
                return resp;

            } catch (Exception e) {

            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            initializeProgressDialog();

        }


        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            dialog.dismiss();
            try {
                if (result != null) {
                    //movieSeatBlockResult = new MovieSeatBlockResult();
                    JSONObject json = XML.toJSONObject(result);
                    JSONObject jsonObject = json.getJSONObject("response");
                    //JSONObject resultRes = jsonObject.getJSONObject("result");
                    if (jsonObject != null) {
                        if (jsonObject.getString("status").equalsIgnoreCase("True")) {
                            if (will_not_watch == 1) {
                                int notwatch = Integer.parseInt(willNotWatch);
                                notwatch = notwatch + 1;
                                totalWatchTV.setText("" + notwatch);
                                totalDontWatchTV.setText("" + willNotWatch + 1);
                                willNotWatch = String.valueOf(notwatch);

                            } else if (will_watch == 1) {

                                int watch = Integer.parseInt(willWatch);
                                watch = watch + 1;

                                will_watch = will_watch + 1;
                                totalWatchTV.setText("" + watch);
                                totalDontWatchTV.setText(willNotWatch);
                                willWatch = String.valueOf(watch);
                            }

                        } else {
                            String massege = jsonObject.getString("errorcode");
                            Toast.makeText(MovieDetailsActivity.this, massege.toString(), Toast.LENGTH_SHORT).show();
                        }
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                totalWatchTV.setText("" + willWatch);
                                totalDontWatchTV.setText("" + willNotWatch);
                                setLikes();

                            }
                        });
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }


    protected void parseWatchResponse(String result) {
        // TODO Auto-generated method stub
        try {
            //two lines of code for xmlpullparsing
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(result.getBytes());
            WillWatchParseOperation xmlpull = new WillWatchParseOperation(byteArrayInputStream);
            status = xmlpull.status;
            errorcode = xmlpull.errorCode;
            error_msg = xmlpull.errormsg;
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
    }

    public void onClickShareButton(final View view) {

        switch (view.getId()) {


        }

    }


    private void initializeProgressDialog() {
        dialog = new ProgressDialog(this, R.style.progress_bar_style);
        dialog.getWindow().setGravity(Gravity.CENTER);
        WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
        dialog.getWindow().setAttributes(params);
        dialog.show();
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
    }



    private void setLikes() {
        Integer witllwatch = Integer.valueOf(willWatch);
        Integer willnotWatch = Integer.valueOf(willNotWatch);
        Integer total = witllwatch + willnotWatch;
        Log.d("", "" + total);
        setLikeVotes(total);
    }



    public class GetWatch extends AsyncTask<Void, Void, Void> {


        @Override
        protected Void doInBackground(Void... arg0) {
            String resp;
            try {
                //movieid=123&will_watch=12&will_not_watch=0&userid=1
                String login_url =
                        AppConstants.SERVER_URL_GET_WATCH_WILL + "movieid=" + movievo.serverId;
                HttpClient httpClient = new DefaultHttpClient();
                HttpContext localContext = new BasicHttpContext();
                HttpGet httpGet = new HttpGet(login_url);
                httpGet.addHeader(BasicScheme.authenticate(new UsernamePasswordCredentials(getResources().getString(R.string.auth_username), getResources().getString(R.string.auth_password)), "UTF-8", false));
                HttpResponse response = httpClient.execute(httpGet, localContext);
                HttpEntity entity = response.getEntity();
                resp = QTUtils.getASCIIContentFromEntity(entity);

                Log.e("resp", resp);
                // parseWatchResponse(resp);
                parseMoviesResponse(resp);
            } catch (Exception e) {

            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            initializeProgressDialog();

        }


        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            dialog.dismiss();
        }

    }

    private ArrayList<Movie> parseMoviesResponse(String result) {
        // TODO Auto-generated method stub
        ArrayList<Movie> moviesList = new ArrayList<Movie>();
        try {
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(result.getBytes());
            // To get from raw folder
            //       InputStream is =   getResources().openRawResource(R.raw.getmoviesbylangandtheatreid);
            MovieParseOperation xmlpull = new MovieParseOperation(byteArrayInputStream);
            moviesList = xmlpull.outputArr;
            Movie movie = moviesList.get(0);
            if(!TextUtils.isEmpty(movie.willwatch)){
                willWatch = movie.willwatch;
            }
            if(!TextUtils.isEmpty(movie.willnotwatch)){
                willNotWatch = movie.willnotwatch;
            }
            status = xmlpull.status;
            error_msg = xmlpull.errormsg;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    totalWatchTV.setText("" + willWatch);
                    totalDontWatchTV.setText("" + willNotWatch);
                    setLikes();

                }
            });

            //lastmodified = xmlpull.last_modified;
        } catch (Exception e) {
            e.printStackTrace();
            // TODO: handle exception
        }
        return moviesList;

    }

    protected void parseUserLoginResponse(String result) {
        // TODO Auto-generated method stub
        try {
            //two lines of code for xmlpullparsing
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(result.getBytes());
            LoginParseOperation xmlpull = new LoginParseOperation(byteArrayInputStream);
            //watchMovie = xmlpull.watchMovie;
            status = xmlpull.status;
            errorcode = xmlpull.errorCode;
            error_msg = xmlpull.errormsg;
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
    }


    private void setNavigationUpIcon(@DrawableRes final int drawableResId) {
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeAsUpIndicator(drawableResId);
        }
    }

    private void initDateSpinner() {
        //final ArraySet<Date> dateList = new ArraySet<>();
        final ArrayList<DateItem> datesArrayList = new ArrayList<>();
        ArrayList<String> values = new ArrayList<String>();
        DateItem dateItem;
        alshowdates = new ArrayList<String>();
        for (int i = 0; i < movievo.movieTheatresArr.size(); i++) {
            for (int j = 0; j < movievo.movieTheatresArr.get(i).showDatesArr.size(); j++) {
                String startDate = movievo.movieTheatresArr.get(i).showDatesArr.get(j).showDate;
                SimpleDateFormat dfEventDate = new SimpleDateFormat("MM/dd/yyyy");
                try {
                    Date newDate = dfEventDate.parse(startDate);

                    //dateList.add(newDate);
                    dateItem = new DateItem(newDate, "1");
                    if (!values.contains(newDate + "")) {
                        values.add(newDate + "");
                        datesArrayList.add(dateItem);
                    }
                    dfEventDate = new SimpleDateFormat("EEE, dd MMMM");
                    startDate = dfEventDate.format(newDate);
                    alshowdates.add(startDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }

        Collections.sort(datesArrayList, new Comparator<DateItem>() {
            public int compare(DateItem o1, DateItem o2) {
                if (o1.getDatess() == null || o2.getDatess() == null)
                    return 0;
                return o1.getDatess().compareTo(o2.getDatess());
            }
        });

//        final ArrayList<DateItem> tempDete = new ArrayList<>();
//
//        if(datesArrayList!=null&&!datesArrayList.isEmpty()) {
//
//            for (int i = 0; i < datesArrayList.size(); i++) {
//                DateItem dateItem1 = datesArrayList.get(i);
//                Date date = dateItem1.getDatess();
//                Date date1 =tempDete.get(i).getDatess();
//
//                if (!tempDete.isEmpty()&&!date.equals(date1)){
//                    tempDete.add(dateItem1);
//                }else {
//                    tempDete.add(dateItem1);
//                }
//
//
//            }
//        }


        if (datesArrayList.size() < 7) {

            for (int i = datesArrayList.size(); i < 7; i++) {
                Date dt = datesArrayList.get(i - 1).getDatess();
                Calendar c = Calendar.getInstance();
                c.setTime(dt);
                c.add(Calendar.DATE, 1);  // number of days to add
                dt = c.getTime();
                dateItem = new DateItem(dt, "2");
                datesArrayList.add(dateItem);
            }


        }

        Set<String> hs = new HashSet<String>();
        hs.addAll(alshowdates);
        alshowdates.clear();
        alshowdates.addAll(hs);
        alshowdates.add(0, "-- Select Date --");

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, R.layout.spin_text_item, alshowdates);
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        setDateAdapter(dataAdapter);

        final MovieShowDatesAdapter movieShowDatesAdapter = new MovieShowDatesAdapter(this);
        movieShowDatesAdapter.setMovieDates(datesArrayList);
        movieShowDatesAdapter.setOnShowDateSelectedListener(new MovieShowDatesAdapter.OnShowDateSelectedListener() {
            @Override
            public void onShowDateSelected(final Date selectedDate) {
                setTheatreListToAdapter(selectedDate);
            }
        });
        setShowDates(movieShowDatesAdapter);
        setShowDateSelection(0);
        setTheatreListToAdapter(datesArrayList.get(0).getDatess());
    }

    private void setTheatreListToAdapter(final Date dateSelected) {
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEE, dd MMMM", Locale.getDefault());
        setTheatreListToAdapter(simpleDateFormat.format(dateSelected));
    }

    private void setTheatreListToAdapter(final String dateSelected) {
        mTheatreAndShowsAdapter.setTheatreList(getTheatreList(dateSelected));
    }

    private List<Theatre> getTheatreList(final String selectedDateStr) {
        final List<Theatre> theatreList = new ArrayList<>();
        try {
            for (final TheatreVO theatreVO : movievo.movieTheatresArr) {
                for (final ShowDateVO showDateVO : theatreVO.showDatesArr) {
                    final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy", Locale.getDefault());
                    final Date showDate = simpleDateFormat.parse(showDateVO.showDate);
                    simpleDateFormat.applyPattern("EEE, dd MMMM");
                    final String showDateStr = simpleDateFormat.format(showDate);
                    if (showDateStr.equalsIgnoreCase(selectedDateStr)) {
                        final Theatre theatre = new Theatre(theatreVO.theatreName);
                        theatre.setShowTimes(showDateVO.showTimesArr);
                        theatreList.add(theatre);
                        break;
                    }
                }
            }
        } catch (ParseException pE) {
            pE.printStackTrace();
        }
        return theatreList;
    }

    private void setTheatreAndShowAdapter() {
        mTheatreAndShowsAdapter = new TheatreAndShowsAdapter();
        mTheatreAndShowsAdapter.setOnShowTimeItemClickListener(mOnShowTimeItemClickListener);
        mTheatreAndShowsAdapter.registerAdapterDataObserver(adapterDataObserver);
//    mTheatreAndShowRecyclerView.setAdapter(mTheatreAndShowsAdapter);
    }

    private String convertMinutesToHours(String mins) {
        int time = Integer.valueOf(mins);
        int hours = time / 60; //since both are ints, you get an int
        int minutes = time % 60;
        String duration = hours + " hr " + minutes + " min";
        return duration;
    }

    @Override
    protected void initArguments(@Nullable final Bundle pBundle) {

    }

    @Override
    protected void fillContentViews() {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mTheatreAndShowsAdapter != null) {
            mTheatreAndShowsAdapter.unregisterAdapterDataObserver(adapterDataObserver);
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        String mSelectDate = null, selectedTheatre = null;
        switch (adapterView.getId()) {
            case R.id.spn_selectDate:
                if (i > 0) {
                    mSelectDate = adapterView.getItemAtPosition(i).toString();
//          sortByTheatre(mSelectDate);
                    setTheatreListToAdapter(mSelectDate);
                }
                break;

        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    private void sortByShows(String mDate, String theatre) {

        sortedTheatres = new ArrayList<String>();
        sortedMoviesShows = new ArrayList<String>();

        for (int i = 0; i < movievo.movieTheatresArr.size(); i++) {
            String s = movievo.movieTheatresArr.get(i).theatreName;
            if (s.equalsIgnoreCase(theatre)) {
                sortedTheatres.add(movievo.movieTheatresArr.get(i).theatreName);
                for (int j = 0; j < movievo.movieTheatresArr.get(i).showDatesArr.size(); j++) {
                    String s1 = movievo.movieTheatresArr.get(i).showDatesArr.get(j).showDate;
                    SimpleDateFormat dfEventDate = new SimpleDateFormat("MM/dd/yyyy");
                    try {
                        Date newDate = dfEventDate.parse(s1);
                        dfEventDate = new SimpleDateFormat("EEE, dd MMMM");
                        s1 = dfEventDate.format(newDate);
                        Log.v("Date", s1);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    if (s1.equalsIgnoreCase(mDate)) {
                        for (int m = 0; m < movievo.movieTheatresArr.get(i).showDatesArr.get(j).showTimesArr.size(); m++) {
                            Calendar c = Calendar.getInstance();

                            SimpleDateFormat df = new SimpleDateFormat("EEE, dd MMMM hh:mm aaa");
                            String formattedDate = df.format(c.getTime());
                            System.out.println("Current time => " + formattedDate);
                            try {
                                if (df.parse(formattedDate).before(df.parse(s1 + " " + movievo.movieTheatresArr.get(i).showDatesArr.get(j).showTimesArr.get(m).showTime))) {
                                    sortedMoviesShows.add(movievo.movieTheatresArr.get(i).showDatesArr.get(j).showTimesArr.get(m).showTime + " ," + movievo.movieTheatresArr.get(i).showDatesArr.get(j).showTimesArr.get(m).serverId);
//                                    showTimeID = movievo.movieTheatresArr.get(i).showDatesArr.get(j).showTimesArr.get(m).id;
                                }
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
//                            sortedMoviesShows.addEventTicket(movievo.movieTheatresArr.get(i).showDatesArr.get(j).showTimesArr.get(m).showTime);
                        }

                    }

                }

            }
        }

        initShowsSpinner(sortedMoviesShows);
    }

    private void initShowsSpinner(ArrayList<String> sortedMoviesShows) {
        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, R.layout.spin_text_item, sortedMoviesShows);
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
    }

    private void sortByTheatre(String mDate) {

        sortedTheatres = new ArrayList<String>();

        for (int i = 0; i < movievo.movieTheatresArr.size(); i++) {
            for (int j = 0; j < movievo.movieTheatresArr.get(i).showDatesArr.size(); j++) {
                String s1 = movievo.movieTheatresArr.get(i).showDatesArr.get(j).showDate;
                SimpleDateFormat dfEventDate = new SimpleDateFormat("MM/dd/yyyy");
                try {
                    Date newDate = dfEventDate.parse(s1);
                    dfEventDate = new SimpleDateFormat("EEE, dd MMMM");
                    s1 = dfEventDate.format(newDate);
                    Log.v("Date", s1);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                if (s1.equalsIgnoreCase(mDate)) {
                    sortedTheatres.add(movievo.movieTheatresArr.get(i).theatreName);

                }

            }
        }
        initTheartreSpinner(sortedTheatres);
    }

    private void initTheartreSpinner(ArrayList<String> sortedMoviesTemp) {
        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, R.layout.spin_text_item, sortedMoviesTemp);
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner

    }


}