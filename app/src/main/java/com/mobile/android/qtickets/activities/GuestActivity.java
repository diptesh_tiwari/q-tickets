package com.mobile.android.qtickets.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mobile.android.qtickets.R;
import com.mobile.android.qtickets.constants.AppConstants;
import com.mobile.android.qtickets.event.EventDetailsVO;
import com.mobile.android.qtickets.event.booking.EventBookingInfo;
import com.mobile.android.qtickets.event.booking.activity.EventBookingActivity;
import com.mobile.android.qtickets.model.CountriesModel;
import com.mobile.android.qtickets.model.MovieSeatBlockResult;
import com.mobile.android.qtickets.model.UserDetailsModel;
import com.mobile.android.qtickets.movie.seatselection.activity.MovieBookingActivity;
import com.mobile.android.qtickets.network.InternetConnectionDetector;
import com.mobile.android.qtickets.parsers.CountryParseOperation;
import com.mobile.android.qtickets.utils.Api;
import com.mobile.android.qtickets.utils.CountryCodePicker;
import com.mobile.android.qtickets.utils.QTUtils;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONObject;
import org.json.XML;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Indglobal on 4/28/2017.
 */

public class GuestActivity extends Activity implements View.OnClickListener {

    ImageView ivCross;
    UserDetailsModel user;
    ArrayList<CountriesModel> countryArr = new ArrayList<>();
    ArrayList<String> allCountryNationality = new ArrayList<>();
    ArrayList<String> allPrefix = new ArrayList<>();
    InternetConnectionDetector connectionDetector;
    private ProgressDialog dialog = null;
    Spinner spn_prefix;
    private EditText et_guest_name, et_guest_phone, et_guest_email, et_broucher;
    private TextView totalCostTV, applyTV;
    LinearLayout continueTV;
    private EventBookingInfo pEventBookingInfo;
    private EventDetailsVO eventvo;
    private String mMovieName, mMovieID, mMovieTheatre, mMovieDate, mMovieTime, mShowTimeId;
    private double totalAmount;
    ArrayList<String> mSelectedSeatsList;
    String prefix;
    int slctdPrfx = 0;
    int typeOfBooking;
    EventDetailsVO eventDetailsVO;
    Double price = null;
    Integer person = null;
    private CountryCodePicker spinLoginCountry;
    private String countryCode;
    private String type;
    private String seats;
    private MovieSeatBlockResult movieSeatBlockResult;
    private String errorMassege;
    private RelativeLayout serviceLL;
    private TextView servicechargesTV;
    private String currencyType;
    private CheckBox tickCheckBox;
    String check;
    private String emailId;
    private String broucherCode;
    private int voucher = 0;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        QTUtils.getInstance().saveToSharedPreference(GuestActivity.this, AppConstants.COUPAN_AMOUNT, null);

        SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        currencyType = mPrefs.getString(AppConstants.COUNTRY_TYPE_CURRENCY, null);


        setContentView(R.layout.guest_layout);
        connectionDetector = new InternetConnectionDetector(this);

        ivCross = (ImageView) findViewById(R.id.ivCross);
        spn_prefix = (Spinner) findViewById(R.id.spn_prefix);
        et_guest_name = (EditText) findViewById(R.id.et_guest_name);
        et_guest_phone = (EditText) findViewById(R.id.et_guest_phone);
        et_guest_email = (EditText) findViewById(R.id.et_guest_email);
        et_broucher = (EditText) findViewById(R.id.et_broucher);
        continueTV = (LinearLayout) findViewById(R.id.continueTV);
        totalCostTV = (TextView) findViewById(R.id.totalCostTV);
        applyTV = (TextView) findViewById(R.id.applyTV);
        serviceLL = (RelativeLayout) findViewById(R.id.serviceLL);
        servicechargesTV = (TextView) findViewById(R.id.servicechargesTV);
        spinLoginCountry = (CountryCodePicker) findViewById(R.id.spinLoginCountry);
        tickCheckBox = (CheckBox) findViewById(R.id.tickCheckBox);
        tickCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {

                if (isChecked == true) {
                    check = "Check";
                } else {
                    check = "not Check";
                }
            }
        });

        continueTV.setOnClickListener(this);
        applyTV.setOnClickListener(this);

        Gson gson = new Gson();
        String usrDetails = mPrefs.getString(AppConstants.SHARED_USER_TAG, null);
        type = mPrefs.getString(AppConstants.TYPE, null);

        if (usrDetails != null) {
            java.lang.reflect.Type type = new TypeToken<UserDetailsModel>() {
            }.getType();
            try {
                user = gson.fromJson(usrDetails, type);
                et_guest_name.setText(user.userName);
                et_guest_phone.setText(user.phoneNumber);
                et_guest_email.setText(user.emailId);
                prefix = user.prefix;

            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        String pEventBookingInfoJson = mPrefs.getString("pEventBookingInfo", null);
        String eventvo = mPrefs.getString("eventvo", null);


        try {
           String totalCount= QTUtils.getInstance().getFromSharedPreference(this,AppConstants.TOTAL_COUNT);
            String totalCost= QTUtils.getInstance().getFromSharedPreference(this,AppConstants.TOTAL_PRICE);
            price=Double.parseDouble(totalCost);
            person=Integer.parseInt(totalCount);
            JSONObject obj = new JSONObject(pEventBookingInfoJson);
            //price = obj.getDouble("totalCost");
           // person = obj.getInt("totalTicketsCount");
            eventDetailsVO = (EventDetailsVO) Api.fromJson(eventvo, EventDetailsVO.class);

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (type.equalsIgnoreCase("EVENTS")) {
            //totalAmount = pEventBookingInfo.getTotalCost();
            totalCostTV.setText(price + " " + currencyType);
            SharedPreferences prefmPrefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            SharedPreferences.Editor editor = prefmPrefs.edit();
            editor.putString("fromEvent", "fromEvent");
            editor.commit();
            typeOfBooking = 1;
        } else if (type.equalsIgnoreCase("MOVIE")) {
            mMovieName = getIntent().getStringExtra("EXTRA_MOVIE_NAME");
            mMovieID = getIntent().getStringExtra("EXTRA_MOVIE_ID");
            mMovieTheatre = getIntent().getStringExtra("EXTRA_MOVIE_THEATRE");
            // mMovieDate = getIntent().getStringExtra("EXTRA_MOVIE_DATE");
            mMovieTime = getIntent().getStringExtra("EXTRA_MOVIE_TIME");
            mShowTimeId = getIntent().getStringExtra("EXTRA_SHOW_TIME_ID");
            mSelectedSeatsList = getIntent().getStringArrayListExtra("TOTAL PERSON");
            totalAmount = getIntent().getDoubleExtra("TOTAL Amount", 0);
            totalCostTV.setText(totalAmount + " " + currencyType);
            typeOfBooking = 2;

        }
        if (mSelectedSeatsList != null && !mSelectedSeatsList.isEmpty()) {
            seats = getListAsCsvString(mSelectedSeatsList);

        }


        if (type.equalsIgnoreCase("MOVIE")) {
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (connectionDetector.isConnectedToInternet()) {
                        // Do something after 2s = 2000ms
                        //new CountryListUrl().execute();
                        new setBlockURL().execute();
                    }
                }
            }, 300);
        }


        ivCross.setOnClickListener(this);

        countryCode = spinLoginCountry.getSelectedCountryCode();
        spinLoginCountry.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected() {
                countryCode = spinLoginCountry.getSelectedCountryCode();

            }

        });
    }


    public String getListAsCsvString(List<String> list) {

        StringBuilder sb = new StringBuilder();
        for (String str : list) {
            if (sb.length() != 0) {
                sb.append(",");
            }
            sb.append(str);
        }
        return sb.toString();

    }

    private void initNationalitySpinner() {

        SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        Gson gson = new Gson();
        String country = mPrefs.getString(AppConstants.SHARED_COUNTRIES_TAG, null);
        if (country != null) {
            java.lang.reflect.Type type = new TypeToken<List<CountriesModel>>() {
            }.getType();
            countryArr = gson.fromJson(country, type);
            if (countryArr.size() != 0) {
                bindSpinners();
            }
        }

    }

    private void bindSpinners() {
        for (int i = 0; i < countryArr.size(); i++) {
            // allCountryNationality.add(countryArr.get(i).CountryNationality);
//            alphonecodes.addEventTicket("+" + countryArr.get(i).Countryprefix + "  " + countryArr.get(i).Countryname);
            allPrefix.add("+" + countryArr.get(i).Countryprefix);
            if (prefix != null && prefix.equalsIgnoreCase("countryArr.get(i).Countryprefix")) {
                slctdPrfx = i;
            }
        }

//        setupNationalityAdapter(allCountryNationality);
//        Log.v("Countries :", allCountryNationality.toString());

        setupPrefixAdapter(allPrefix);

    }

    private void setupPrefixAdapter(ArrayList<String> allPrefixes) {
        // Creating adapter for spinner
        ArrayAdapter<String> countryAdapter = new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_spinner_item, allPrefixes);
        // Drop down layout style - list view with radio button
        countryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        spn_prefix.setAdapter(countryAdapter);
        spn_prefix.setSelection(slctdPrfx);
    }

    private void initializeProgressDialog() {
        dialog = new ProgressDialog(this, R.style.progress_bar_style);
        dialog.getWindow().setGravity(Gravity.CENTER);
        WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
        params.y = 200;
        dialog.getWindow().setAttributes(params);
        dialog.show();
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
    }

    protected ArrayList<CountriesModel> parseCountryResponse(String result) {
        ArrayList<CountriesModel> conty = null;
        try {
            //two lines of code for xmlpullparsing
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(result.getBytes());
            CountryParseOperation xmlpull = new CountryParseOperation(byteArrayInputStream);
            conty = xmlpull.countryArr;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return conty;
    }

    public class CountryListUrl extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... arg0) {
            try {
                String resp;
                try {
                    String country_url = AppConstants.SERVER_URL_GET_ALL_COUNTRIES;
                    HttpClient httpClient = new DefaultHttpClient();
                    HttpContext localContext = new BasicHttpContext();
                    HttpGet httpGet = new HttpGet(country_url);
                    httpGet.addHeader(BasicScheme.authenticate(new UsernamePasswordCredentials(getResources().getString(R.string.auth_username), getResources().getString(R.string.auth_password)), "UTF-8", false));
                    HttpResponse response = httpClient.execute(httpGet, localContext);
                    HttpEntity entity = response.getEntity();
                    resp = QTUtils.getASCIIContentFromEntity(entity);
                    countryArr = parseCountryResponse(resp);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {

            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            initializeProgressDialog();

        }


        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            dialog.dismiss();
            try {
                if (countryArr.size() > 0) {
                    SharedPreferences prefmPrefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                    SharedPreferences.Editor editor = prefmPrefs.edit();
                    Gson gson = new Gson();
                    String countryjson = gson.toJson(countryArr);
                    editor.putString(AppConstants.SHARED_COUNTRIES_TAG, countryjson);
                    editor.commit();

                } else {
                    countryArr.clear();
                    new CountryListUrl().execute();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            initNationalitySpinner();
        }

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {

            case R.id.ivCross:
                onBackPressed();
                break;
            case R.id.applyTV:

                if (!TextUtils.isEmpty(et_guest_email.getText().toString())) {
                    emailId = et_guest_email.getText().toString();
                    if (!isValidEmail(emailId)) {
                        Toast.makeText(this, "Enter valid email",
                                Toast.LENGTH_LONG).show();
                        return;
                    }
                }

                if (!TextUtils.isEmpty(et_broucher.getText().toString())) {
                    broucherCode = et_broucher.getText().toString();
                } else {
                    Toast.makeText(this, "Enter voucherCode",
                            Toast.LENGTH_LONG).show();
                    return;
                }

                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (connectionDetector.isConnectedToInternet()) {
                            new VoucherCodeValidateUrl().execute();
                        }
                    }
                }, 300);


                break;
            case R.id.continueTV:
                if (check != null && check.equalsIgnoreCase("Check")) {

                    String name, email, number = null;
                    if (!TextUtils.isEmpty(et_guest_name.getText().toString())) {
                        name = et_guest_name.getText().toString();
                        if (!TextUtils.isEmpty(et_guest_email.getText().toString())) {
                            email = et_guest_email.getText().toString();
                            if (!isValidEmail(email)) {
                                Toast.makeText(this, "Enter valid email",
                                        Toast.LENGTH_LONG).show();
                                return;
                            }
                            if (!TextUtils.isEmpty(et_guest_phone.getText().toString())) {

                                if (et_guest_phone.getText().toString().length() >= 7 && et_guest_phone.getText().toString().length() <= 10) {
                                    if (currencyType.equalsIgnoreCase(AppConstants.COUNTRY_TYPE_UAE)) {
                                        if (et_guest_phone.getText().toString().length() == 7) {
                                            if (et_guest_phone.getText().toString().indexOf(0) != 0) {
                                                number = et_guest_phone.getText().toString();
                                            } else {
                                                Toast.makeText(this, "mobile no should not be started with zero", Toast.LENGTH_SHORT).show();
                                                return;
                                            }

                                        } else {
                                            Toast.makeText(this, "Enter valid mob no", Toast.LENGTH_SHORT).show();
                                            return;
                                        }
                                    } else if (currencyType.equalsIgnoreCase(AppConstants.COUNTRY_TYPE_UAE)) {
                                        if (et_guest_phone.getText().toString().length() == 7) {
                                            if (et_guest_phone.getText().toString().indexOf(0) != 0) {
                                                number = et_guest_phone.getText().toString();
                                            } else {
                                                Toast.makeText(this, "mobile no should not be started with zero", Toast.LENGTH_SHORT).show();
                                                return;
                                            }

                                        } else {
                                            Toast.makeText(this, "Enter valid mob no", Toast.LENGTH_SHORT).show();
                                            return;

                                        }
                                    } else if (currencyType.equalsIgnoreCase(AppConstants.COUNTRY_TYPE_QA)) {
                                        if (et_guest_phone.getText().toString().length() == 8) {
                                            if (et_guest_phone.getText().toString().indexOf(0) != 0) {
                                                number = et_guest_phone.getText().toString();
                                            } else {
                                                Toast.makeText(this, "mobile no should not be started with zero", Toast.LENGTH_SHORT).show();
                                                return;
                                            }

                                        } else {
                                            Toast.makeText(this, "Enter valid mob no", Toast.LENGTH_SHORT).show();
                                            return;

                                        }
                                    } else if (currencyType.equalsIgnoreCase(AppConstants.COUNTRY_TYPE_BH)) {
                                        if (et_guest_phone.getText().toString().length() == 8) {
                                            if (et_guest_phone.getText().toString().indexOf(0) != 0) {
                                                number = et_guest_phone.getText().toString();
                                            } else {
                                                Toast.makeText(this, "mobile no should not be started with zero", Toast.LENGTH_SHORT).show();
                                                return;
                                            }

                                        } else {
                                            Toast.makeText(this, "Enter valid mob no", Toast.LENGTH_SHORT).show();
                                            return;

                                        }
                                    } else if (prefix.equalsIgnoreCase("india")) {
                                        if (et_guest_phone.getText().toString().length() == 18) {
                                            if (et_guest_phone.getText().toString().indexOf(0) != 0) {
                                                number = et_guest_phone.getText().toString();
                                            } else {
                                                Toast.makeText(this, "mobile no should not be started with zero", Toast.LENGTH_SHORT).show();
                                                return;
                                            }

                                        } else {
                                            Toast.makeText(this, "Enter valid mob no", Toast.LENGTH_SHORT).show();
                                            return;

                                        }
                                    }


                                    Intent intent;
                                    if (type.equalsIgnoreCase("MOVIE")) {

                                        if (movieSeatBlockResult != null) {
                                            intent = new Intent(this, MovieBookingActivity.class);
                                            intent.putExtra("EXTRA_MOVIE_NAME", mMovieName);
                                            intent.putExtra("EXTRA_MOVIE_ID", mMovieID);
                                            intent.putExtra("EXTRA_MOVIE_THEATRE", mMovieTheatre);
                                            //intent.putExtra("EXTRA_MOVIE_DATE", mMovieDate);
                                            intent.putExtra("EXTRA_MOVIE_TIME", mMovieTime);
                                            intent.putExtra("EXTRA_SHOW_TIME_ID", mShowTimeId);
                                            intent.putExtra("TOTAL PERSON", (ArrayList) mSelectedSeatsList);
                                            intent.putExtra("TOTAL Amount", totalAmount);
                                            intent.putExtra("name", name);
                                            intent.putExtra("number", number);
                                            intent.putExtra("email", email);
                                            intent.putExtra("movieSeatBlockResult", movieSeatBlockResult);
                                            // String prifix = spn_prefix.getSelectedItem().toString();
                                            intent.putExtra("fromGuest", "1");
                                            intent.putExtra("prifix", countryCode);
                                            startActivity(intent);

                                        } else {
                                            Toast.makeText(this, errorMassege.toString(), Toast.LENGTH_LONG);
                                        }


                                    } else if (type.equalsIgnoreCase("EVENTS")) {
                                        intent = new Intent(this, EventBookingActivity.class);
                                        intent.putExtra("name", name);
                                        intent.putExtra("number", number);
                                        intent.putExtra("email", email);
                                        // String prifix = spn_prefix.getSelectedItem().toString();
                                        intent.putExtra("prifix", countryCode);
                                        intent.putExtra("price ", price);
                                        intent.putExtra("fromGuest", "1");
                                        intent.putExtra("totalTicketsCount ", person);
                                        intent.putExtra(AppConstants.EVENT_DETAIL, eventDetailsVO);
                                        startActivity(intent);
                                    }
                                } else {
                                    Toast.makeText(this, "Enter valid  mobile no", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(this, "Enter Mob No", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(this, "Enter Email Id", Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        Toast.makeText(this, "Enter Name", Toast.LENGTH_SHORT).show();
                    }

                } else {

                    Toast.makeText(this, "Please accept terms and conditions", Toast.LENGTH_SHORT).show();
                }


                break;
        }
    }

    private boolean isValidEmail(String email) {

        if (email == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        SharedPreferences prefmPrefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = prefmPrefs.edit();
        editor.putString("GuestBack", "1");
        editor.commit();
    }


    public class setBlockURL extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {

            try {
                // showid=397665&seats=F04,F03&AppSource=3&movie_id=12800&schedule_date=04/27/2017&show_time=07:30 PM
                String block_seats_qtickets = AppConstants.MOVIE_SEAT_BLOCK_URL + "showid=" + mShowTimeId + "&" + "seats=" +
                        seats + "&" + "AppSource=" + "3";
                String resp;
                if (connectionDetector.isConnectedToInternet()) {
                    try {
                        block_seats_qtickets = block_seats_qtickets.replaceAll("\\s+", "%20");
                        HttpClient httpClient = new DefaultHttpClient();
                        HttpContext localContext = new BasicHttpContext();
                        HttpGet httpGet = new HttpGet(block_seats_qtickets);
                        httpGet.addHeader(BasicScheme.authenticate(new UsernamePasswordCredentials(getResources().getString(R.string.auth_username), getResources().getString(R.string.auth_password)), "UTF-8", false));
                        HttpResponse response = httpClient.execute(httpGet, localContext);
                        HttpEntity entity = response.getEntity();
                        resp = QTUtils.getASCIIContentFromEntity(entity);
                        return resp;
                    } catch (Exception e) {
                        resp = "error";
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            initializeProgressDialog();

        }


        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            dialog.dismiss();
            try {
                if (result != null) {
                    movieSeatBlockResult = new MovieSeatBlockResult();
                    JSONObject json = XML.toJSONObject(result);
                    JSONObject jsonObject = json.getJSONObject("response");
                    JSONObject resultRes = jsonObject.getJSONObject("result");
                    if (resultRes != null) {
                        if (resultRes.getString("status").equalsIgnoreCase("true")) {
                            movieSeatBlockResult.setCurrency(resultRes.getString("currency"));
                            movieSeatBlockResult.setStatus(resultRes.getString("status"));
                            movieSeatBlockResult.setTransaction_Id(resultRes.getString("Transaction_Id"));
                            movieSeatBlockResult.setPageSessionTime(resultRes.getInt("PageSessionTime"));
                            movieSeatBlockResult.setTransactionTime(resultRes.getInt("TransactionTime"));
                            movieSeatBlockResult.setTicketprice(resultRes.getString("ticketprice"));
                            movieSeatBlockResult.setServicecharges(resultRes.getInt("servicecharges"));
                            movieSeatBlockResult.setTotalprice(resultRes.getInt("totalprice"));
                            String serviceCharge = String.valueOf(resultRes.getInt("servicecharges"));
                            if (serviceCharge != null) {
                                servicechargesTV.setText(serviceCharge + ".0" + " " + currencyType);
                                serviceLL.setVisibility(View.VISIBLE);
                            }


                            //new sendLockRequestURL().execute();
                        } else {
                            errorMassege = resultRes.getString("errormsg");
                            Toast.makeText(GuestActivity.this, errorMassege.toString(), Toast.LENGTH_SHORT).show();
                        }

                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            //initNationalitySpinner();
        }

    }


    public class VoucherCodeValidateUrl extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {

            try {
                // showid=397665&seats=F04,F03&AppSource=3&movie_id=12800&schedule_date=04/27/2017&show_time=07:30 PM
                String block_seats_qtickets = "https://api.q-tickets.com/v2.1/checkcouponstatus?" + "email=" + emailId + "&" + "coupon=" + broucherCode + "&" + "showid=" + mShowTimeId;
                String resp;
                if (connectionDetector.isConnectedToInternet()) {
                    try {
                        block_seats_qtickets = block_seats_qtickets.replaceAll("\\s+", "%20");
                        HttpClient httpClient = new DefaultHttpClient();
                        HttpContext localContext = new BasicHttpContext();
                        HttpGet httpGet = new HttpGet(block_seats_qtickets);
                        httpGet.addHeader(BasicScheme.authenticate(new UsernamePasswordCredentials(getResources().getString(R.string.auth_username), getResources().getString(R.string.auth_password)), "UTF-8", false));
                        HttpResponse response = httpClient.execute(httpGet, localContext);
                        HttpEntity entity = response.getEntity();
                        resp = QTUtils.getASCIIContentFromEntity(entity);
                        return resp;
                    } catch (Exception e) {
                        resp = "error";
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            initializeProgressDialog();

        }


        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            dialog.dismiss();
            try {
                if (result != null) {
                    JSONObject json = XML.toJSONObject(result);
                    JSONObject jsonObject = json.getJSONObject("response");
                    if (jsonObject.getString("status").equalsIgnoreCase("true")) {
                        JSONObject resultRes = jsonObject.getJSONObject("result");
                        JSONObject coupons = resultRes.getJSONObject("Coupons");
                        if (coupons != null) {
                            String couponValid = coupons.getString("coupon");
                            if (couponValid.equalsIgnoreCase("valid")) {
                                if (voucher == 0) {
                                    voucher = 1;
                                    String reduceAmount = coupons.getString("balance");
                                    int reduceAmountBal = Integer.parseInt(reduceAmount);
                                    QTUtils.getInstance().saveToSharedPreference(GuestActivity.this, AppConstants.COUPAN_AMOUNT, reduceAmount);
                                    Toast.makeText(GuestActivity.this, "Coupon apply for " + reduceAmountBal + currencyType, Toast.LENGTH_SHORT).show();
                                    // totalAmount=totalAmount-reduceAmountBal;
                                    int totalPrice = movieSeatBlockResult.getTotalprice();
                                    totalPrice = totalPrice - reduceAmountBal;
                                    // double totalPrice=(double) totalAmount;
                                    movieSeatBlockResult.setTotalprice(totalPrice);


                                } else {
                                    Toast.makeText(GuestActivity.this, "Already you use this voucher", Toast.LENGTH_SHORT).show();
                                }


                                Log.d("", totalAmount + "");
                            } else {
                                Toast.makeText(GuestActivity.this, "Voucher not valid", Toast.LENGTH_SHORT).show();
                            }
                        }
                    } else {
                        errorMassege = jsonObject.getString("errormsg");
                        Toast.makeText(GuestActivity.this, errorMassege.toString(), Toast.LENGTH_SHORT).show();
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }
}
