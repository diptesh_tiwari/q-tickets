package com.mobile.android.qtickets.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.mobile.android.qtickets.R;
import com.mobile.android.qtickets.activities.ContactUsActivity;
import com.mobile.android.qtickets.activities.ForgotPasswordActivity;
import com.mobile.android.qtickets.activities.LoginActivity;
import com.mobile.android.qtickets.activities.MyAccountActivity;
import com.mobile.android.qtickets.activities.RegisterActivity;
import com.mobile.android.qtickets.constants.AppConstants;

/**
 * Created by Satish - 360 on 11/16/2016.
 */


public class NavigationDrawerAdapter extends RecyclerView.Adapter<NavigationDrawerAdapter.ViewHolder> {
    private static final int TYPE_HEADER = 0;  // Declaring Variable to Understand which View is being worked on
    // IF the view under inflation and population is header or Item
    private static final int TYPE_ITEM = 1;

    private String mNavTitles[]; // String Array to store the passed titles Value from AddTaskActivity.java

    private Context mContext;
    private NavigationDrawerListener mNavigationDrawerListener;
    private ImageView iv_profile_pic;
    private TextView tv_username;
    private TextView tv_email;


    public NavigationDrawerAdapter(Context context, String Titles[], NavigationDrawerListener navigationDrawerListener) {
        mContext = context;
        mNavTitles = Titles;
        mNavigationDrawerListener = navigationDrawerListener;
    }

    public NavigationDrawerAdapter() {

    }

    // Creating a ViewHolder which extends the RecyclerView View Holder
    // ViewHolder are used to to store the inflated views in order to recycle them

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == TYPE_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.navigationdrawer, parent, false); //Inflating the layout

            ViewHolder vhItem = new ViewHolder(v, viewType); //Creating ViewHolder and passing the object of type view

            return vhItem; // Returning the created object

            //inflate your layout and pass it to view holder

        } else if (viewType == TYPE_HEADER) {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.navigation_header, parent, false); //Inflating the layout

            ViewHolder vhHeader = new ViewHolder(v, viewType); //Creating ViewHolder and passing the object of type view

            return vhHeader; //returning the object created

        }
        return null;

    }

    //Next we override a method which is called when the item in a row is needed to be displayed, here the int position
    // Tells us item at which position is being constructed to be displayed and the holder id of the holder object tell us
    // which view type is being created 1 for item row
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        if (holder.Holderid == 1) {
            // as the list view is going to be called after the header view so we decrement the

			/*if (position == 1) {
                holder.img.setImageResource(R.mipmap.nav_vouchers);
			} else if (position == 2) {
				holder.img.setImageResource(R.mipmap.nav_favorites);
			} else if (position == 3) {
				holder.img.setImageResource(R.mipmap.nav_history);
			} else if (position == 4) {
				holder.img.setImageResource(R.mipmap.nav_rateapp);
			} else if (position == 5) {
				holder.img.setImageResource(R.mipmap.nav_contactus);
			} else if (position == 6) {
				holder.img.setImageResource(R.mipmap.nav_policies);
			} else if (position == 7) {
				holder.img.setImageResource(R.mipmap.nav_share);
			}*/
            // position by 1 and pass it to the holder while setting the text and image
            holder.textView.setText(mNavTitles[position - 1]);
            holder.mLinearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mNavigationDrawerListener.closeNavigationDrawer();
                    switch (holder.getAdapterPosition()) {

                        case 1:
                            if ((mNavTitles[position - 1]).equalsIgnoreCase("My Account")) {
                                mContext.startActivity(new Intent(mContext, MyAccountActivity.class));
                            } else {
                                mContext.startActivity(new Intent(mContext, LoginActivity.class));
                            }

                            break;
                        case 2:
                            mContext.startActivity(new Intent(mContext, RegisterActivity.class));
                            break;
                        case 3:
                            if((mNavTitles[position - 1]).equalsIgnoreCase("Contact Us")){
                                mContext.startActivity(new Intent(mContext, ContactUsActivity.class));
                            }else {
                                mContext.startActivity(new Intent(mContext, ForgotPasswordActivity.class));
                            }
                            break;
                        case 4:
                            if ((mNavTitles[position - 1]).equalsIgnoreCase("Log Out")) {
                                AlertDialog.Builder builder1 = new AlertDialog.Builder(mContext);
                                builder1.setTitle("Log Out");
                                builder1.setMessage("Are you sure you want to Log Out?");
                                builder1.setCancelable(true);

                                SharedPreferences prefmPrefs = PreferenceManager.getDefaultSharedPreferences(mContext.getApplicationContext());
                                SharedPreferences.Editor editor = prefmPrefs.edit();

                                editor.putString(AppConstants.SHARED_USER_TAG, null);
                                editor.putString("login","0");
                                editor.commit();

                                builder1.setPositiveButton(
                                        "Yes",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                Intent setIntent = new Intent(mContext, LoginActivity.class);
                                                setIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                                mContext.startActivity(setIntent);
                                                Toast.makeText(mContext, "Logout Successfully!", Toast.LENGTH_SHORT).show();

                                            }
                                        });

                                builder1.setNegativeButton(
                                        "No",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.cancel();
                                            }
                                        });

                                AlertDialog alert11 = builder1.create();
                                alert11.show();

                            } else {
                                mContext.startActivity(new Intent(mContext, ContactUsActivity.class));
                                break;
                            }
                    }
                }
            });
        } else {

        }
    }

    // Witht the following method we check what type of view is being passed
    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position)) return TYPE_HEADER;
        return TYPE_ITEM;
    }

    // This method returns the number of items present in the list
    @Override
    public int getItemCount() {
        return mNavTitles.length + 1; // the number of items in the list will be +1 the titles including the header view.
    }

    private boolean isPositionHeader(int position) {
        return position == 0;
    }

    /**
     *
     */
    public interface NavigationDrawerListener {

        public void closeNavigationDrawer();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        int Holderid;

        TextView textView, tv_ver;
        LinearLayout mLinearLayout;
        ImageView img;

        public ViewHolder(View itemView, int ViewType) {                 // Creating ViewHolder Constructor with View and viewType As a parameter
            super(itemView);
//            itemView.setClickable(true);
//            itemView.setOnClickListener(this);
            // Here we set the appropriate view in accordance with the the view type as passed when the holder object is created

            if (ViewType == TYPE_ITEM) {
                img = (ImageView) itemView.findViewById(R.id.img_logo);
                textView = (TextView) itemView.findViewById(R.id.tvNavDrawerItems); // Creating TextView object with the id of textView from item_row.xml
                mLinearLayout = (LinearLayout) itemView.findViewById(R.id.llNavDrawer);
                Holderid = 1; // setting holder id as 1 as the object being populated are of type item row
            } else if (ViewType == TYPE_HEADER) {
                iv_profile_pic = (ImageView) itemView.findViewById(R.id.iv_profile_pic_nav);
                tv_username = (TextView) itemView.findViewById(R.id.tv_username_nav);
                tv_email = (TextView) itemView.findViewById(R.id.tv_email_nav);
                Holderid = 0;  // Setting holder id = 0 as the object being populated are of type header view
            } else {
            }
        }

    }
}
