package com.mobile.android.qtickets.event.booking;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.IdRes;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mobile.android.qtickets.R;
import com.mobile.android.qtickets.constants.AppConstants;
import com.mobile.android.qtickets.event.EventDetailsVO;
import com.mobile.android.qtickets.model.BookedDetal;
import com.mobile.android.qtickets.model.CountriesModel;
import com.mobile.android.qtickets.model.User;
import com.mobile.android.qtickets.movie.moviedetails.activity.MovieDetailsActivity;
import com.mobile.android.qtickets.network.InternetConnectionDetector;
import com.mobile.android.qtickets.parsers.CountryParseOperation;
import com.mobile.android.qtickets.utils.Api;
import com.mobile.android.qtickets.utils.CountryCodePicker;
import com.mobile.android.qtickets.utils.QTUtils;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by hexalabssd on 21/03/17.
 */

public class PaymentActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String TAG = PaymentActivity.class.getName() + ".TAG";
    private static final String EXTRA_USER_DETAILS = PaymentActivity.class.getName() + ".extra.USER_DETAILS";
    ArrayList<String> allCountryNationality = new ArrayList<>();
    ArrayList<CountriesModel> countryArr = new ArrayList<>();
    private TextView mPayNowButton, cancelTV,tvTimer;
    //private RadioGroup mPaymentTypeRadioGroup;
    private Spinner spn_nationality;
    private OnPayNowListener mOnPayNowListener;
    private CountryCodePicker spinLoginCountry;
    private int paymentType=4;
    private User mUserDetailsModel;
    private ProgressDialog dialog = null;
    private EventBookingInfo mEventBookingInfo;
    private EventDetailsVO mEventDetailsVO;
    private String countryName;
    private double totalCost;
    private BookedDetal bookedDetal;

    RadioGroup rgCardType;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_book_tickets_pay_now);

        //mPaymentTypeRadioGroup = (RadioGroup) findViewById(R.id.radioGroup_paymentType);
        spn_nationality = (Spinner) findViewById(R.id.spn_nationality);
        spn_nationality = (Spinner) findViewById(R.id.spn_nationality);
        mPayNowButton = (TextView) findViewById(R.id.button_payNow);
        cancelTV = (TextView) findViewById(R.id.cancelTV);
        tvTimer = (TextView)findViewById(R.id.tvTimer);
        //visaType,americanType,mastercardType
        spinLoginCountry = (CountryCodePicker) findViewById(R.id.spinLoginCountry);
        rgCardType = (RadioGroup)findViewById(R.id.rgCardType);
        mPayNowButton.setOnClickListener(this);
        cancelTV.setOnClickListener(this);


        rgCardType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                switch (checkedId){

                    case R.id.rbVisa:
                        paymentType=4;
                        break;

                    case R.id.rbAmercn:
                        paymentType=5;
                        break;

                    case R.id.rbDoha:
                        paymentType=1;
                        break;
                }
            }
        });
        countryName = spinLoginCountry.getSelectedCountryName();
        spinLoginCountry.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected() {
                countryName = spinLoginCountry.getSelectedCountryName();
                Log.d("", "");
            }

        });


        String user = (String) QTUtils.getInstance().getObjectFromSharedPreference(this, AppConstants.SHARED_USER_TAG, String.class);
        if (user != null) {
            try {
                JSONObject json = new JSONObject(user);
                mUserDetailsModel = (User) Api.fromJson(user, User.class);

            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        //intent.putExtra(AppConstants.EVENT_BOOK_INFO, mEventBookingInfo);
        //intent.putExtra(AppConstants.EVENT_DETAIL, mEventDetailsVO);


        Bundle pBundle = getIntent().getExtras();
        if (pBundle != null) {
            mEventBookingInfo = (EventBookingInfo) pBundle.getParcelable(AppConstants.EVENT_BOOK_INFO);
            mEventDetailsVO = (EventDetailsVO) pBundle.getParcelable(AppConstants.EVENT_DETAIL);
            if (mEventBookingInfo != null) {
                totalCost = mEventBookingInfo.getTotalCost();
            } else {
                bookedDetal = (BookedDetal) getIntent().getSerializableExtra(AppConstants.BOOKED_DETAIL);
            }


        }
        final InternetConnectionDetector connectionDetector = new InternetConnectionDetector(this);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (connectionDetector.isConnectedToInternet()) {
                    // Do something after 2s = 2000ms
                    // new CountryListUrl().execute();

                    new setBlockURL().execute();
                }
            }
        }, 300);
    }


    public void setOnPayNowListener(final OnPayNowListener pOnPayNowListener) {
        mOnPayNowListener = pOnPayNowListener;
    }


    protected ArrayList<CountriesModel> parseCountryResponse(String result) {
        ArrayList<CountriesModel> conty = null;
        try {
            //two lines of code for xmlpullparsing
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(result.getBytes());
            CountryParseOperation xmlpull = new CountryParseOperation(byteArrayInputStream);
            conty = xmlpull.countryArr;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return conty;
    }

    private void initializeProgressDialog() {
        dialog = new ProgressDialog(this, R.style.progress_bar_style);
        dialog.getWindow().setGravity(Gravity.CENTER);
        WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
        dialog.getWindow().setAttributes(params);
        dialog.show();
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
    }

    private void initNationalitySpinner() {

        SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        Gson gson = new Gson();
        String country = mPrefs.getString(AppConstants.SHARED_COUNTRIES_TAG, null);
        if (country != null) {
            java.lang.reflect.Type type = new TypeToken<List<CountriesModel>>() {
            }.getType();
            countryArr = gson.fromJson(country, type);
            if (countryArr.size() != 0) {
                bindSpinners();
            }
        }

    }

    private void bindSpinners() {
        for (int i = 0; i < countryArr.size(); i++) {
            allCountryNationality.add(countryArr.get(i).CountryNationality);
        }

        setupNationalityAdapter(allCountryNationality);

    }

    private void setupNationalityAdapter(ArrayList<String> allNationality) {
        // Creating adapter for spinner
        ArrayAdapter<String> countryAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, allNationality);
        // Drop down layout style - list view with radio button
        countryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        spn_nationality.setAdapter(countryAdapter);
        if (mUserDetailsModel != null) {
            spn_nationality.setSelection(countryAdapter.getPosition(mUserDetailsModel.getNationality()));
        }

        new CountDownTimer(540000, 1000) {

            @Override
            public void onTick(long millisUntilFinished) {

                tvTimer.setText(String.format("%02d : %02d",
                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished),
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))
                ));
            }

            @Override
            public void onFinish() {
                try {
                    Intent setIntent = new Intent(PaymentActivity.this, MovieDetailsActivity.class);
                    setIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(setIntent);
                    finish();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }.start();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.button_payNow:
                if (paymentType != 0) {
                    String paymentGatewayUrl = "https://api.q-tickets.com/Qpayment-registration.aspx?";

                    paymentGatewayUrl = paymentGatewayUrl + "Currency=" + "AED&" + "Amount=" + bookedDetal.getBalance() + "&" + "OrderName=" + "online&" + "OrderID=" + bookedDetal.getOrderInfo() + "&" + "nationality=" + countryName + "&" + "paymenttype=" + paymentType;

                    Intent ii = new Intent(PaymentActivity.this, PaymentGatewaysActivity.class);
                    ii.putExtra("url", paymentGatewayUrl);
                    startActivity(ii);

                } else {
                    Toast.makeText(PaymentActivity.this, "Select a payment type", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.spinLoginCountry:

                Log.d("", "");

                break;
            case R.id.cancelTV:
                finish();
                break;
        }
    }

    public interface OnPayNowListener {
        void onClickPayNowButton(final User pUserDetailsModel, final int pPaymentType);
    }

    public class CountryListUrl extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... arg0) {
            try {
                String resp;
                try {
                    String country_url = AppConstants.SERVER_URL_GET_ALL_COUNTRIES;
                    HttpClient httpClient = new DefaultHttpClient();
                    HttpContext localContext = new BasicHttpContext();
                    HttpGet httpGet = new HttpGet(country_url);
                    httpGet.addHeader(BasicScheme.authenticate(new UsernamePasswordCredentials(getResources().getString(R.string.auth_username), getResources().getString(R.string.auth_password)), "UTF-8", false));
                    HttpResponse response = httpClient.execute(httpGet, localContext);
                    HttpEntity entity = response.getEntity();
                    resp = QTUtils.getASCIIContentFromEntity(entity);
                    countryArr = parseCountryResponse(resp);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {

            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            initializeProgressDialog();

        }


        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            dialog.dismiss();
            try {
                if (countryArr.size() > 0) {
                    SharedPreferences prefmPrefs = PreferenceManager.getDefaultSharedPreferences(PaymentActivity.this);
                    SharedPreferences.Editor editor = prefmPrefs.edit();
                    Gson gson = new Gson();
                    String countryjson = gson.toJson(countryArr);
                    editor.putString(AppConstants.SHARED_COUNTRIES_TAG, countryjson);
                    editor.commit();

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            initNationalitySpinner();
        }

    }


    public class setBlockURL extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... arg0) {
            try {
                String resp;
                try {
                    String country_url = AppConstants.WEBSITE_BASE_URL;
                    HttpClient httpClient = new DefaultHttpClient();
                    HttpContext localContext = new BasicHttpContext();
                    HttpGet httpGet = new HttpGet(country_url);
                    httpGet.addHeader(BasicScheme.authenticate(new UsernamePasswordCredentials(getResources().getString(R.string.auth_username), getResources().getString(R.string.auth_password)), "UTF-8", false));
                    HttpResponse response = httpClient.execute(httpGet, localContext);
                    HttpEntity entity = response.getEntity();
                    resp = QTUtils.getASCIIContentFromEntity(entity);
                    countryArr = parseCountryResponse(resp);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {

            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            initializeProgressDialog();

        }


        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            dialog.dismiss();
            try {
                if (countryArr.size() > 0) {
                    SharedPreferences prefmPrefs = PreferenceManager.getDefaultSharedPreferences(PaymentActivity.this);
                    SharedPreferences.Editor editor = prefmPrefs.edit();
                    Gson gson = new Gson();
                    String countryjson = gson.toJson(countryArr);
                    editor.putString(AppConstants.SHARED_COUNTRIES_TAG, countryjson);
                    editor.commit();

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            initNationalitySpinner();
        }

    }

}
