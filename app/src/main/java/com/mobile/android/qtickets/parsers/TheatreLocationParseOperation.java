package com.mobile.android.qtickets.parsers;

import com.mobile.android.qtickets.constants.AppConstants;
import com.mobile.android.qtickets.movie.Movie;
import com.mobile.android.qtickets.movie.TheatreVO;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by Satish - 360 on 11/28/2016.
 */

public class TheatreLocationParseOperation {

	public ArrayList<TheatreVO> theatreArr;
	protected XmlPullParser xmlpullparser;
	String TAG = "XmlPullParsing";
	Movie movie;
	StringBuffer sb;
	int holderForStartAndLength[] = new int[2];

	public TheatreLocationParseOperation(InputStream is) {
		XmlPullParserFactory factory = null;
		movie = new Movie();
		try {
			factory = XmlPullParserFactory.newInstance();
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		factory.setNamespaceAware(true);
		try {
			xmlpullparser = factory.newPullParser();
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			xmlpullparser.setInput(is, "UTF-8");
			processDocument(xmlpullparser);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void processDocument(XmlPullParser xpp) throws XmlPullParserException, IOException {
		int eventType = xpp.getEventType();
		do {
			if (eventType == XmlPullParser.START_DOCUMENT) {
//				System.out.println("Start document");
			} else if (eventType == XmlPullParser.END_DOCUMENT) {
//				System.out.println("End document");
			} else if (eventType == XmlPullParser.START_TAG) {
				processStartElement(xpp);
			} else if (eventType == XmlPullParser.END_TAG) {
				processEndElement(xpp);
			} else if (eventType == XmlPullParser.TEXT) {
				processText(xpp);
			}
			eventType = xpp.next();
		} while (eventType != XmlPullParser.END_DOCUMENT);
	}

	public void processStartElement(XmlPullParser xpp) {
		String name = xpp.getName();
		String uri = xpp.getNamespace();
		sb = new StringBuffer();

		if ("".equals(uri)) {
//			System.out.println("Start element: " + name);

			if (name.equals(AppConstants.THEATRES_TAG)) {
				theatreArr = new ArrayList<TheatreVO>();
			}

			if (name.equals(AppConstants.THEATRE_TAG)) {
				TheatreVO theatreTempVO = new TheatreVO();

				if (xpp.getAttributeValue(null, AppConstants.THEATRE_ID_TAG) != null) {
					theatreTempVO.serverId = xpp.getAttributeValue(null, AppConstants.THEATRE_ID_TAG);
				} else {
					theatreTempVO.serverId = "";
				}
				if (xpp.getAttributeValue(null, AppConstants.THEATRE_NAME_TAG) != null) {
					theatreTempVO.theatreName = xpp.getAttributeValue(null, AppConstants.THEATRE_NAME_TAG);
				} else {
					theatreTempVO.theatreName = "";
				}
				if (xpp.getAttributeValue(null, AppConstants.THEATRE_LOGO_TAG) != null) {
					theatreTempVO.logo = xpp.getAttributeValue(null, AppConstants.THEATRE_LOGO_TAG);
				} else {
					theatreTempVO.logo = "";
				}
				if (xpp.getAttributeValue(null, AppConstants.THEATRE_STATE_TAG) != null) {
					theatreTempVO.state = xpp.getAttributeValue(null, AppConstants.THEATRE_STATE_TAG);
				} else {
					theatreTempVO.state = "";
				}
				if (xpp.getAttributeValue(null, AppConstants.THEATRE_ADDRESS_TAG) != null) {
					theatreTempVO.address = xpp.getAttributeValue(null, AppConstants.THEATRE_ADDRESS_TAG);
				} else {
					theatreTempVO.address = "";
				}

				theatreArr.add(theatreTempVO);
			}

		} else {
//			System.out.println("Start element: {" + uri + "}" + name);
		}
	}

	public void processEndElement(XmlPullParser xpp) {
		String name = xpp.getName();
		String uri = xpp.getNamespace();
		if ("".equals(uri)) {
//			System.out.println("End element: " + name);
			//			if (name.equals(SMSCountryConstants.SHOW_TIME_TAG)) {
			//				showDate.showTimesArr.addAll(showTimeArr);
			//
			//			}

			//			if (name.equals(SMSCountryConstants.SHOW_DATE_TAG)) {
			//				showDate.showTimesArr.addAll(showTimeArr);
			//
			//			}


		} else {
//			System.out.println("End element:   {" + uri + "}" + name);
		}
	}

	public void processText(XmlPullParser xpp) throws XmlPullParserException {
		char ch[] = xpp.getTextCharacters(holderForStartAndLength);
		int start = holderForStartAndLength[0];
		int length = holderForStartAndLength[1];
//		System.out.print("Characters:    \"");
		for (int i = start; i < start + length; i++) {
			switch (ch[i]) {
				case '\\':
					sb.append(ch[i]);
					break;

				case '"':
					sb.append(ch[i]);
					break;
				case '\n':
					sb.append(ch[i]);
					break;
				case '\r':
					sb.append(ch[i]);
					break;
				case '\t':
					sb.append(ch[i]);
					break;
				default:
					sb.append(ch[i]);
					break;
			}
		}
//		System.out.print("\"\n");
	}

}
