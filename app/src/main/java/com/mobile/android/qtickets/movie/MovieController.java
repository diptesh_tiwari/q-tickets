package com.mobile.android.qtickets.movie;

import android.content.Context;

import com.android.volley.VolleyError;
import com.mobile.android.qtickets.movie.seatselection.PostSeatsLayoutRequest;
import com.mobile.android.qtickets.movie.seatselection.SeatLayoutResponse;

import org.xmlpull.v1.XmlPullParser;

import in.hexalab.abstractvolley.base.HResponseListener;

/**
 * Created by hexalabssd on 22/02/17.
 */

public final class MovieController {
	private final Context mContext;
	private final MovieModel mMovieModel;

	public MovieController(final Context pContext) {
		mContext = pContext;
		mMovieModel = new MovieModel(pContext);
	}

	public void fetchSeatLayout(final String showTimeId, final HResponseListener<SeatLayoutResponse> listener) {
		listener.onRequestSubmitted();
		final HResponseListener<XmlPullParser> xmlPullParserHResponseListener = new HResponseListener<XmlPullParser>() {
			@Override
			public void onRequestSubmitted() {
			}

			@Override
			public void onErrorResponse(final VolleyError error) {
				listener.onErrorResponse(error);
			}

			@Override
			public void onResponse(final XmlPullParser response) {
				listener.onResponse(new SeatLayoutResponse(response));
			}
		};
		final PostSeatsLayoutRequest postSeatsLayoutRequest = new PostSeatsLayoutRequest(showTimeId, xmlPullParserHResponseListener);
		mMovieModel.fetchSeatLayout(postSeatsLayoutRequest);
	}
}
