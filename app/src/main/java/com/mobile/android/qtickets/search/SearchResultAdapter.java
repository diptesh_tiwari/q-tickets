package com.mobile.android.qtickets.search;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mobile.android.qtickets.R;
import com.mobile.android.qtickets.constants.AppConstants;
import com.mobile.android.qtickets.movie.list.MovieItemClickListener;
import com.squareup.picasso.Picasso;

/**
 * Created by hexalabssd on 23/01/17.
 */

public final class SearchResultAdapter extends RecyclerView.Adapter<SearchResultViewHolder> {
	private final int[] mSearchResultCardBackgrounds;
	private SearchResult[] mSearchResults;
	private MovieItemClickListener mMovieItemClickListener;

	public SearchResultAdapter(final Context pContext) {
		mSearchResults = new SearchResult[]{};
		TypedArray typedArray = null;
		try {
			typedArray = pContext.getResources().obtainTypedArray(R.array.backgroundColorsSearchResult);
			mSearchResultCardBackgrounds = new int[typedArray.length()];
			for (int i = 0; i < typedArray.length(); i++) {
				mSearchResultCardBackgrounds[i] = typedArray.getColor(i, 0);
			}
		} finally {
			if (typedArray != null) {
				typedArray.recycle();
			}
		}
	}

	@Override
	public SearchResultViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
		return new SearchResultViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_view_search_result, parent, false));
	}

	@Override
	public void onBindViewHolder(final SearchResultViewHolder holder, final int position) {
		final SearchResult searchResult = mSearchResults[position];
		Picasso.with(holder.mPosterImageView.getContext()).load(AppConstants.WEBSITE_BASE_URL + searchResult.getPosterImagePath()).into(holder.mPosterImageView);
		holder.name.setText(searchResult.getName());
		holder.description.setText(Html.fromHtml(searchResult.getDescription()));
		holder.genreVenue.setText(searchResult.getGenreVenue());
		if (mSearchResultCardBackgrounds.length > 0) {
			holder.itemView.setBackgroundColor(mSearchResultCardBackgrounds[position % mSearchResultCardBackgrounds.length]);
		}

		final View.OnClickListener itemClickListener = new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				mMovieItemClickListener.onsetData(searchResult);
			}
		};
		holder.setMovieItemClickListener(itemClickListener);
	}

	@Override
	public int getItemCount() {
		return mSearchResults.length;
	}

	public void updateSearchResult(final SearchResult[] pSearchResults) {
		mSearchResults = pSearchResults;
		notifyDataSetChanged();
	}

	public void setMovieItemClickListener(final MovieItemClickListener pMovieItemClickListener) {
		mMovieItemClickListener = pMovieItemClickListener;
	}
}
