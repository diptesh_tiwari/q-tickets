package com.mobile.android.qtickets.movie.moviedetails;

import com.mobile.android.qtickets.movie.ShowTimeVO;

import java.util.List;

/**
 * Created by hexalabssd on 30/01/17.
 */

public final class Theatre {
	private final String mName;
	private List<ShowTimeVO> mShowTimes;

	public Theatre(final String pName) {
		mName = pName;
	}

	public String getName() {
		return mName;
	}

	public List<ShowTimeVO> getShowTimes() {
		return mShowTimes;
	}

	public void setShowTimes(final List<ShowTimeVO> pShowTimes) {
		mShowTimes = pShowTimes;
	}
}
