package com.mobile.android.qtickets.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.mobile.android.qtickets.R;
import com.mobile.android.qtickets.constants.AppConstants;
import com.mobile.android.qtickets.network.InternetConnectionDetector;
import com.mobile.android.qtickets.parsers.ForgotPasswrodRequestParseOperation;
import com.mobile.android.qtickets.utils.QTUtils;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;

import java.io.ByteArrayInputStream;

/**
 * Created by Satish - 360 on 11/17/2016.
 */

public class ForgotPasswordActivity extends AppCompatActivity implements View.OnClickListener {
	String emailId;
	private ImageView iv_back;
	private TextView btn_submit;
	private ProgressDialog dialog = null;
	private EditText et_email;
	private String status, forgot_msg, errorcode;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_forgot_password);

		et_email = (EditText) findViewById(R.id.et_email_forgotpassword);
		iv_back = (ImageView) findViewById(R.id.btn_back_forgotpassword);
		btn_submit = (TextView) findViewById(R.id.btn_submit_forgotpassword);

		iv_back.setOnClickListener(this);
		btn_submit.setOnClickListener(this);

	}

	@Override
	protected void onStart() {
		super.onStart();
	}

	@Override
	protected void onStop() {
		super.onStop();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
			case R.id.btn_back_forgotpassword:
				onBackPressed();
				finish();
				break;

			case R.id.btn_submit_forgotpassword:
				InternetConnectionDetector connectionDetector = new InternetConnectionDetector(this);
				if (connectionDetector.isConnectedToInternet()) {
					if (et_email.getText().toString().length() == 0) {
						et_email.setError("Please enter Email Id");
					} else {
						new ForgotPasswordUrl().execute();
					}
				} else {
					new AlertDialog.Builder(ForgotPasswordActivity.this).setTitle("Internet Connection Status").setMessage("No Internet Connection").setPositiveButton("OK", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
						}
					}).show();
				}
				break;
		}
	}

	private void initializeProgressDialog() {
		dialog = new ProgressDialog(this, R.style.progress_bar_style);
		dialog.getWindow().setGravity(Gravity.CENTER);
		WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
		dialog.getWindow().setAttributes(params);
		dialog.show();
		dialog.setCancelable(true);
		dialog.setCanceledOnTouchOutside(true);
	}

	protected void parseForgotPasswordResponse(String result) {
		// TODO Auto-generated method stub
		try {
			//two lines of code for xmlpullparsing
			ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(result.getBytes());
			ForgotPasswrodRequestParseOperation xmlpull = new ForgotPasswrodRequestParseOperation(byteArrayInputStream);
			status = xmlpull.status;
			errorcode = xmlpull.errorCode;
			forgot_msg = xmlpull.errormsg;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	public class ForgotPasswordUrl extends AsyncTask<Void, Void, Void> {
		@Override
		protected Void doInBackground(Void... arg0) {
			String resp;
			try {
				String forgot_url = AppConstants.SERVICE_URL_GET_FORGOT_PASSWORD_REQUEST + AppConstants.SERVICE_MSG_GET_FORGOT_PWD_EMAIL_ID_ID + emailId;
				HttpClient httpClient = new DefaultHttpClient();

				Log.e("forgot_url", forgot_url);
				HttpContext localContext = new BasicHttpContext();
				HttpGet httpGet = new HttpGet(forgot_url);
				httpGet.addHeader(BasicScheme.authenticate(new UsernamePasswordCredentials(getResources().getString(R.string.auth_username), getResources().getString(R.string.auth_password)), "UTF-8", false));
				HttpResponse response = httpClient.execute(httpGet, localContext);
				HttpEntity entity = response.getEntity();
				resp = QTUtils.getASCIIContentFromEntity(entity);
				Log.e("response", resp);
				parseForgotPasswordResponse(resp);

			} catch (Exception e) {

			}
			return null;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			initializeProgressDialog();
			emailId = et_email.getText().toString();
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			dialog.dismiss();
			try {
				if (status.equalsIgnoreCase(AppConstants.RESPONSE_TRUE_TAG)) {
					new AlertDialog.Builder(ForgotPasswordActivity.this).setMessage(forgot_msg).setPositiveButton("OK", new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface arg0, int arg1) {
							arg0.dismiss();
							Intent intent= new Intent(ForgotPasswordActivity.this,LoginActivity.class);
							intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
							startActivity(intent);

						}
					}).show();

				} else if (status.equalsIgnoreCase(AppConstants.RESPONSE_FALSE_TAG)) {
					new AlertDialog.Builder(ForgotPasswordActivity.this).setMessage(forgot_msg).setPositiveButton("OK", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {

						}
					}).show();
				}
			} catch (Exception e) {
				Toast.makeText(getApplicationContext(), "Server Not responding..", Toast.LENGTH_SHORT).show();
				e.printStackTrace();
			}
		}
	}
}
