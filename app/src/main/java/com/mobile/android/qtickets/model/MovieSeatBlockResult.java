package com.mobile.android.qtickets.model;

import java.io.Serializable;

/**
 * Created by Indglobal on 4/28/2017.
 */

public class MovieSeatBlockResult implements Serializable {

   private String status;
    private  String Transaction_Id;
    private Integer PageSessionTime;
    private Integer TransactionTime;
    private String currency;
    private String ticketprice;
    private Integer servicecharges;
    private  Integer totalprice;

    public Integer getTotalprice() {
        return totalprice;
    }

    public void setTotalprice(Integer totalprice) {
        this.totalprice = totalprice;
    }

    public Integer getServicecharges() {
        return servicecharges;
    }

    public void setServicecharges(Integer servicecharges) {
        this.servicecharges = servicecharges;
    }

    public Integer getPageSessionTime() {
        return PageSessionTime;
    }

    public void setPageSessionTime(Integer pageSessionTime) {
        PageSessionTime = pageSessionTime;
    }

    public Integer getTransactionTime() {
        return TransactionTime;
    }

    public void setTransactionTime(Integer transactionTime) {
        TransactionTime = transactionTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTransaction_Id() {
        return Transaction_Id;
    }

    public void setTransaction_Id(String transaction_Id) {
        Transaction_Id = transaction_Id;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getTicketprice() {
        return ticketprice;
    }

    public void setTicketprice(String ticketprice) {
        this.ticketprice = ticketprice;
    }


}
