package com.mobile.android.qtickets.event;


import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mobile.android.qtickets.R;
import com.mobile.android.qtickets.constants.AppConstants;
import com.mobile.android.qtickets.network.InternetConnectionDetector;
import com.mobile.android.qtickets.utils.QTUtils;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static android.app.Activity.RESULT_OK;

/**
 * Created by Satish - 360 on 11/18/2016.
 */

public class EventsFragment extends Fragment implements Spinner.OnItemSelectedListener, View.OnClickListener {
    InternetConnectionDetector connectionDetector;
    private Spinner sp_events;
    private RecyclerView mEventsRecyclerview;
    private EventsAdapter mEventsAdapter;
    private LinearLayoutManager mEventsLayoutManager;
    private ArrayList<EventDetailsVO> eventsList = new ArrayList<>();
    private ArrayList<String> alCategory = new ArrayList<String>();
    private ArrayList<String> mVenueList = new ArrayList<String>();
    private ArrayList<EventDetailsVO> neweventsList = new ArrayList<>();
    private ArrayList<String> newalCategory = new ArrayList<String>();
    private ArrayList<String> newmVenueList = new ArrayList<String>();
    private ProgressDialog dialog;
    private String defaultOption = "Events";
    RelativeLayout rlEventFilter;
    String startDate = "", endDate = "", startPrice = "0", endPrice = "0";
    boolean filtrApplied = false;
    List<EventTicketDetails> eventTicketDetails = new ArrayList<>();
    List<String> eventTicketPrices = new ArrayList<>();
    private TextView noResultTV;
    private RelativeLayout eventLL;
    ImageView noEventImageView;

    //vanuesTV
    public EventsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        QTUtils.getInstance().saveToSharedPreference(getActivity(),AppConstants.EVENT_ITEM,null);
        SharedPreferences prefmPrefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        SharedPreferences.Editor editor = prefmPrefs.edit();
        editor.putString(AppConstants.SHARED_EVENTS_TAG, null);
        editor.commit();


        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_events, container, false);
        connectionDetector = new InternetConnectionDetector(getActivity());

        sp_events = (Spinner) view.findViewById(R.id.spn_events);
        noEventImageView = (ImageView) view.findViewById(R.id.noEventImageView);
        sp_events.setOnItemSelectedListener(this);
        mEventsRecyclerview = (RecyclerView) view.findViewById(R.id.rv_events);
        noResultTV = (TextView) view.findViewById(R.id.noResultTV);
        eventLL = (RelativeLayout) view.findViewById(R.id.eventLL);

        noResultTV.setOnClickListener(this);

        rlEventFilter = (RelativeLayout) view.findViewById(R.id.rlEventFilter);

        rlEventFilter.setOnClickListener(this);

        mEventsLayoutManager = new LinearLayoutManager(getContext());
        mEventsRecyclerview.setLayoutManager(mEventsLayoutManager);

//        final DividerItemDecoration dividerVertical = new DividerItemDecoration(getContext(), mEventsLayoutManager.getOrientation());
//        dividerVertical.setDrawable(ContextCompat.getDrawable(getContext(), R.drawable.divider_event_item_vertical));
//        mEventsRecyclerview.addItemDecoration(dividerVertical);
        sp_events.setOnItemSelectedListener(this);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (connectionDetector.isConnectedToInternet()) {
                    // Do something after 2s = 2000ms
                    new EventListUrl().execute();

                }
            }
        }, 300);

        return view;
    }

    private void initEventsList() {
        SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        mPrefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        Gson gson = new Gson();
        String events = mPrefs.getString(AppConstants.SHARED_EVENTS_TAG, "");
        if (!events.equalsIgnoreCase("")) {
            eventsList.clear();
            java.lang.reflect.Type type = new TypeToken<List<EventDetailsVO>>() {
            }.getType();
            eventsList = gson.fromJson(events, type);
        }

        alCategory.clear();
        if (!eventsList.isEmpty() && eventsList.size() != 0) {
            for (int i = 0; i < eventsList.size(); i++) {
                alCategory.add(eventsList.get(i).Category);
            }
                    /*Log.e("alCategory", alCategory.size()+"");*/
            Set<String> hs = new HashSet<String>();
            hs.clear();
            hs.addAll(alCategory);
            alCategory.clear();
            alCategory.addAll(hs);
                /* Log.e("altheaters after filtered", alCategory.size()+"");*/
            alCategory.add(0, defaultOption);
            setUpCategoriesAdapter(alCategory);
        }
    }

    private void newinitEventsList() {
        SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        Gson gson = new Gson();
        String events = mPrefs.getString(AppConstants.SHARED_EVENTS_TAG, "");
        if (!events.equalsIgnoreCase("")) {
            neweventsList.clear();
            java.lang.reflect.Type type = new TypeToken<List<EventDetailsVO>>() {
            }.getType();
            neweventsList = gson.fromJson(events, type);
        }

        newalCategory.clear();
        if (!neweventsList.isEmpty() && neweventsList.size() != 0) {
            for (int i = 0; i < neweventsList.size(); i++) {
                newalCategory.add(neweventsList.get(i).Category);
            }
                    /*Log.e("alCategory", alCategory.size()+"");*/
            Set<String> hs = new HashSet<String>();
            hs.clear();
            hs.addAll(newalCategory);
            newalCategory.clear();
            newalCategory.addAll(hs);
                /* Log.e("altheaters after filtered", alCategory.size()+"");*/
            newalCategory.add(0, defaultOption);
            newsetUpCategoriesAdapter(newalCategory);
        }
    }

    private void setUpCategoriesAdapter(ArrayList<String> alCategory) {
        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getContext(), R.layout.simple_spinner_item, mVenueList);
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        sp_events.setAdapter(dataAdapter);


    }

    private void newsetUpCategoriesAdapter(ArrayList<String> alCategory) {
        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getContext(), R.layout.simple_spinner_item, newmVenueList);
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        sp_events.setAdapter(dataAdapter);

    }

    private void initializeProgressDialog() {
        dialog = new ProgressDialog(getActivity(), R.style.progress_bar_style);
        dialog.getWindow().setGravity(Gravity.CENTER);
        WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
        dialog.getWindow().setAttributes(params);
        dialog.show();
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
    }

    private ArrayList<EventDetailsVO> parseEventsResponse(String result) {
        // TODO Auto-generated method stub
        eventsList.clear();
        ArrayList<EventDetailsVO> eventList = new ArrayList<EventDetailsVO>();
        try {
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(result.getBytes());
            // To get from raw folder
            //       InputStream is =   getResources().openRawResource(R.raw.getmoviesbylangandtheatreid);
            EventDetailsListParseOperation xmlpull = new EventDetailsListParseOperation(byteArrayInputStream);


            for (final EventDetailsVO e : xmlpull.outputArr) {
                eventList.add(e);

            }

           /* for (final EventDetailsVO e : xmlpull.outputArr) {
                if (e.CategoryId.contentEquals("4")) {
                    eventList.add(e);
                }
            }*/
            mVenueList.clear();
            mVenueList.add(0, "Venues");
            for (int i = 0; i < eventList.size(); i++) {
                String venue = eventList.get(i).Venue;
                if (!mVenueList.contains(venue)) {
                    mVenueList.add(venue);
                }
            }


            Log.d("vanue", mVenueList.toString());
        } catch (Exception e) {
            // TODO: handle exception
        }
        return eventList;

    }

    private ArrayList<EventDetailsVO> newparseEventsResponse(String result) {
        // TODO Auto-generated method stub
        neweventsList.clear();
        ArrayList<EventDetailsVO> neweventList = new ArrayList<EventDetailsVO>();
        try {
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(result.getBytes());
            // To get from raw folder
            //       InputStream is =   getResources().openRawResource(R.raw.getmoviesbylangandtheatreid);
            EventDetailsListParseOperation xmlpull = new EventDetailsListParseOperation(byteArrayInputStream);

            for (final EventDetailsVO e : xmlpull.outputArr) {
                neweventList.add(e);

            }


          /*  for (final EventDetailsVO e : xmlpull.outputArr) {
                if (e.CategoryId.contentEquals("4")) {
                    neweventList.add(e);
                }
            }*/
            newmVenueList.clear();
            newmVenueList.add(0, "Venues");
            for (int i = 0; i < neweventList.size(); i++) {
                String venue = neweventList.get(i).Venue;
                if (!neweventList.contains(venue)) {
                    newmVenueList.add(venue);
                }


            }


        } catch (Exception e) {
            // TODO: handle exception
        }
        return neweventList;

    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        String selectedEvents = adapterView.getItemAtPosition(i).toString();
        if (filtrApplied) {
            newsortByCategory(selectedEvents);
        } else {
            sortByCategory(selectedEvents);
        }

//        Toast.makeText(getContext(), "Event Selected : " + selectedEvents, Toast.LENGTH_SHORT).show();
    }

    private void sortByCategory(String selectedEvents) {
        ArrayList<EventDetailsVO> sortedEventTemp = new ArrayList<EventDetailsVO>();
        if (selectedEvents.equalsIgnoreCase("Venues")) {
            selectedEvents = defaultOption;
        }
        if (selectedEvents.equalsIgnoreCase(defaultOption)) {
            sortedEventTemp.addAll(eventsList);
        } else {
            for (int i = 0; i < eventsList.size(); i++) {
                if (eventsList.get(i).Venue.equalsIgnoreCase(selectedEvents)) {
                    sortedEventTemp.add(eventsList.get(i));
                }
            }
        }


        setUpEventsAdapter(sortedEventTemp);
    }

    private void newsortByCategory(String selectedEvents) {
        ArrayList<EventDetailsVO> sortedEventTemp = new ArrayList<EventDetailsVO>();
        if (selectedEvents.equalsIgnoreCase("Venues")) {
            selectedEvents = defaultOption;
        }
        if (selectedEvents.equalsIgnoreCase(defaultOption)) {
            sortedEventTemp.addAll(neweventsList);
        } else {
            for (int i = 0; i < neweventsList.size(); i++) {
                if (neweventsList.get(i).Venue.equalsIgnoreCase(selectedEvents)) {
                    sortedEventTemp.add(neweventsList.get(i));
                }
            }
        }
        setUpEventsAdapter(sortedEventTemp);
    }

    private void setUpEventsAdapter(ArrayList<EventDetailsVO> sortedEventTemp) {
        mEventsRecyclerview.setHasFixedSize(true);
        mEventsRecyclerview.setNestedScrollingEnabled(false);


        if(!sortedEventTemp.isEmpty()){

            mEventsAdapter = new EventsAdapter(getActivity(), sortedEventTemp);
            mEventsRecyclerview.setAdapter(mEventsAdapter);
            noEventImageView.setVisibility(View.GONE);
            mEventsRecyclerview.setVisibility(View.VISIBLE);

        }else {
            noEventImageView.setVisibility(View.VISIBLE);
            mEventsRecyclerview.setVisibility(View.GONE);
        }

    }


    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rlEventFilter:

                Intent ii = new Intent(getActivity(), EventFilterActivity.class);
                ii.putExtra("startDate", "");
                ii.putExtra("endDate", "");
                ii.putExtra("startPrice", "0");
                ii.putExtra("endPrice", "0");
                startActivityForResult(ii, 1);

                break;

            case R.id.noResultTV:
                new EventListUrl().execute();
                break;

        }
    }


    public class EventListUrl extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {

            String url = AppConstants.SERVER_URL_GET_ALL_EVENTT_DETAILS;
            SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
            String currencyType = mPrefs.getString(AppConstants.COUNTRY_TYPE_CURRENCY, null);

            if (currencyType.equalsIgnoreCase(AppConstants.COUNTRY_TYPE_BH)) {
                url = url + "Country=" + "Bahrain";
            } else if (currencyType.equalsIgnoreCase(AppConstants.COUNTRY_TYPE_QA)) {
                url = url + "Country=" + "Qatar";
            } else if (currencyType.equalsIgnoreCase(AppConstants.COUNTRY_TYPE_UAE)) {
                url = url + "Country=" + "Dubai";
            }


            if (connectionDetector.isConnectedToInternet()) {
                String resp;
                try {
                    url = url.replaceAll("\\s+", "%20");
                    HttpClient httpClient = new DefaultHttpClient();
                    HttpContext localContext = new BasicHttpContext();
                    HttpGet httpGet = new HttpGet(url);
                    httpGet.addHeader(BasicScheme.authenticate(new UsernamePasswordCredentials(getResources().getString(R.string.auth_username), getResources().getString(R.string.auth_password)), "UTF-8", false));
                    HttpResponse response = httpClient.execute(httpGet, localContext);
                    HttpEntity entity = response.getEntity();
                    resp = QTUtils.getASCIIContentFromEntity(entity);
                    eventsList = parseEventsResponse(resp);
                } catch (Exception e) {
                    resp = "error";
                }
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            filtrApplied = false;
            initializeProgressDialog();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            try {
                dialog.dismiss();
                if (!eventsList.isEmpty()) {
                    noResultTV.setVisibility(View.GONE);
                    eventLL.setVisibility(View.VISIBLE);

                    for (int i = 0; i < eventsList.size(); i++) {
                        eventTicketDetails = eventsList.get(i).evntTktDetilas;
                        if (!eventTicketDetails.isEmpty()) {
                            for (int j = 0; j < eventTicketDetails.size(); j++) {
                                String price = eventTicketDetails.get(j).TicketPrice;
                                eventTicketPrices.add(price);
                                Log.d("", eventTicketPrices.toString());
                            }
                        }
                    }

                    SharedPreferences prefmPrefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
                    SharedPreferences.Editor editor = prefmPrefs.edit();
                    Gson gson = new Gson();
                    String eventjson = gson.toJson(eventsList);
                    String eventTicketPricejson = gson.toJson(eventTicketPrices);

                    editor.putString(AppConstants.SHARED_EVENTS_TAG, eventjson);
                    editor.putString(AppConstants.SHARED_EVENTS_TICKET_PRICES, eventTicketPricejson);

                    editor.commit();
                } else {
                    eventsList.clear();
                }
            } catch (Exception e) {
                e.printStackTrace();
//                Toast.makeText(getActivity(), "Server not Responding", Toast.LENGTH_SHORT).show();
            }
            initEventsList();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if ((requestCode == 1 && resultCode == RESULT_OK && data != null)) {
            startPrice = data.getStringExtra("startPrice");
            endPrice = data.getStringExtra("endPrice");
            startDate = data.getStringExtra("startDate");
            endDate = data.getStringExtra("endDate");

            if (connectionDetector.isConnectedToInternet()) {
                new FiltrEventListUrl().execute();

            } else {
                Toast.makeText(getActivity(), "No internet connection!", Toast.LENGTH_SHORT).show();
            }

        }

    }

    public class FiltrEventListUrl extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {

            String url = AppConstants.SERVER_URL_FILTER_EVENT;
            url = url + "minPrice=" + startPrice + "&maxPrice=" + endPrice + "&startDate=" + startDate + "&endDate=" + endDate;
            if (connectionDetector.isConnectedToInternet()) {
                String resp;
                try {
                    url = url.replaceAll("\\s+", "%20");
                    HttpClient httpClient = new DefaultHttpClient();
                    HttpContext localContext = new BasicHttpContext();
                    HttpGet httpGet = new HttpGet(url);
                    //httpGet.addHeader(BasicScheme.authenticate(new UsernamePasswordCredentials(getResources().getString(R.string.auth_username), getResources().getString(R.string.auth_password)), "UTF-8", false));
                    HttpResponse response = httpClient.execute(httpGet, localContext);
                    HttpEntity entity = response.getEntity();
                    resp = QTUtils.getASCIIContentFromEntity(entity);
                    neweventsList = newparseEventsResponse(resp);
                } catch (Exception e) {
                    resp = "error";
                }
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            filtrApplied = true;
            initializeProgressDialog();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            try {
                dialog.dismiss();
                if (neweventsList.size() > 0) {
                    noResultTV.setVisibility(View.GONE);
                    eventLL.setVisibility(View.VISIBLE);
                    SharedPreferences prefmPrefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
                    SharedPreferences.Editor editor = prefmPrefs.edit();
                    Gson gson = new Gson();
                    String eventjson = gson.toJson(neweventsList);
                    editor.putString(AppConstants.SHARED_EVENTS_TAG, eventjson);
                    editor.commit();
                } else {
                    noResultTV.setVisibility(View.VISIBLE);
                    eventLL.setVisibility(View.GONE);
                    neweventsList.clear();
                }
            } catch (Exception e) {
                e.printStackTrace();
//                Toast.makeText(getActivity(), "Server not Responding", Toast.LENGTH_SHORT).show();
            }
            newinitEventsList();
        }
    }
}