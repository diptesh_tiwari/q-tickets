package com.mobile.android.qtickets.parsers;

import com.mobile.android.qtickets.constants.AppConstants;
import com.mobile.android.qtickets.model.UserDetailsModel;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Satish - 360 on 11/24/2016.
 */

public class RegisterParseOperation extends BaseParseOperation {
	public UserDetailsModel user;
	protected XmlPullParser xmlpullparser;
	String TAG = "XmlPullParsingRegister";
	StringBuffer sb;
	int holderForStartAndLength[] = new int[2];

	public RegisterParseOperation(InputStream is) {

		XmlPullParserFactory factory = null;
		try {
			factory = XmlPullParserFactory.newInstance();
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		factory.setNamespaceAware(true);
		try {
			xmlpullparser = factory.newPullParser();
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			xmlpullparser.setInput(is, "UTF-8");
			processDocument(xmlpullparser);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void processDocument(XmlPullParser xpp) throws XmlPullParserException, IOException {
		int eventType = xpp.getEventType();
		do {
			if (eventType == XmlPullParser.START_DOCUMENT) {
				System.out.println("Start document");
			} else if (eventType == XmlPullParser.END_DOCUMENT) {
				System.out.println("End document");
			} else if (eventType == XmlPullParser.START_TAG) {
				processStartElement(xpp);
			} else if (eventType == XmlPullParser.END_TAG) {
				processEndElement(xpp);
			} else if (eventType == XmlPullParser.TEXT) {
				processText(xpp);
			}
			eventType = xpp.next();
		} while (eventType != XmlPullParser.END_DOCUMENT);
	}

	public void processStartElement(XmlPullParser xpp) {
		String name = xpp.getName();
		String uri = xpp.getNamespace();
		sb = new StringBuffer();

		if ("".equals(uri)) {
			System.out.println("Start element: " + name);

			if (name.equals(AppConstants.SERVER_RESPONSE_TAG)) {
				if (xpp.getAttributeValue(null, AppConstants.SERVER_STATUS_TAG) != null) {
					status = xpp.getAttributeValue(null, AppConstants.SERVER_STATUS_TAG);
				} else {
					status = "";
				}
				if (xpp.getAttributeValue(null, AppConstants.SERVER_ERROR_CODE_TAG) != null) {
					errorCode = xpp.getAttributeValue(null, AppConstants.SERVER_ERROR_CODE_TAG);
				} else {
					errorCode = "";
				}

				if (xpp.getAttributeValue(null, AppConstants.SERVER_ERROR_MESSAGE_TAG) != null) {
					errormsg = xpp.getAttributeValue(null, AppConstants.SERVER_ERROR_MESSAGE_TAG);
				} else {
					errormsg = "";
				}

			}

			if (name.equals(AppConstants.SERVER_RESULT_TAG)) {

				if (xpp.getAttributeValue(null, AppConstants.SERVER_ERROR_CODE_TAG) != null) {
					errorCode = xpp.getAttributeValue(null, AppConstants.SERVER_ERROR_CODE_TAG);
				} else {
					errorCode = "";
				}

				if (xpp.getAttributeValue(null, AppConstants.SERVER_ERROR_MESSAGE_TAG) != null) {
					errormsg = xpp.getAttributeValue(null, AppConstants.SERVER_ERROR_MESSAGE_TAG);
				} else {
					errormsg = "";
				}

			}
			if (name.equals(AppConstants.USER_TAG)) {
				user = new UserDetailsModel();

				if (xpp.getAttributeValue(null, AppConstants.USER_ID_TAG) != null) {
					user.id = xpp.getAttributeValue(null, AppConstants.USER_ID_TAG);
				} else {
					user.id = "";
				}
				if (xpp.getAttributeValue(null, AppConstants.USER_NAME_TAG) != null) {
					user.userName = xpp.getAttributeValue(null, AppConstants.USER_NAME_TAG);
				} else {
					user.userName = "";
				}
				if (xpp.getAttributeValue(null, AppConstants.USER_PREFIX_TAG) != null) {
					user.prefix = xpp.getAttributeValue(null, AppConstants.USER_PREFIX_TAG);
				} else {
					user.prefix = "";
				}
				if (xpp.getAttributeValue(null, AppConstants.USER_MOBILE_TAG) != null) {
					user.phoneNumber = xpp.getAttributeValue(null, AppConstants.USER_MOBILE_TAG);
				} else {
					user.phoneNumber = "";
				}
				if (xpp.getAttributeValue(null, AppConstants.USER_ADDRESS_TAG) != null) {
					user.address = xpp.getAttributeValue(null, AppConstants.USER_ADDRESS_TAG);
				} else {
					user.address = "";
				}
				if (xpp.getAttributeValue(null, AppConstants.USER_EMAIL_TAG) != null) {
					user.emailId = xpp.getAttributeValue(null, AppConstants.USER_EMAIL_TAG);
				} else {
					user.emailId = "";
				}

				if (xpp.getAttributeValue(null, AppConstants.USER_VERIFY_TAG) != null) {
					user.verify = xpp.getAttributeValue(null, AppConstants.USER_VERIFY_TAG);
				} else {
					user.verify = "";
				}
				if (xpp.getAttributeValue(null, AppConstants.USER_NATIONALITY_TAG) != null) {
					user.nationality = xpp.getAttributeValue(null, AppConstants.USER_NATIONALITY_TAG);
				} else {
					user.nationality = "";
				}

			}

		} else {
			System.out.println("Start element: {" + uri + "}" + name);
		}

	}

	public void processEndElement(XmlPullParser xpp) {
		String name = xpp.getName();
		String uri = xpp.getNamespace();
		if ("".equals(uri)) {
			System.out.println("End element: " + name);

		} else System.out.println("End element:   {" + uri + "}" + name);
	}

	public void processText(XmlPullParser xpp) throws XmlPullParserException {
		char ch[] = xpp.getTextCharacters(holderForStartAndLength);
		int start = holderForStartAndLength[0];
		int length = holderForStartAndLength[1];
		System.out.print("Characters:    \"");
		for (int i = start; i < start + length; i++) {
			switch (ch[i]) {
				case '\\':
					sb.append(ch[i]);
					break;

				case '"':
					sb.append(ch[i]);
					break;
				case '\n':
					sb.append(ch[i]);
					break;
				case '\r':
					sb.append(ch[i]);
					break;
				case '\t':
					sb.append(ch[i]);
					break;
				default:
					sb.append(ch[i]);
					break;
			}
		}
		System.out.print("\"\n");
	}
}
