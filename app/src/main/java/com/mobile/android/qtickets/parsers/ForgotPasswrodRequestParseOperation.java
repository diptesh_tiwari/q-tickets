package com.mobile.android.qtickets.parsers;

import com.mobile.android.qtickets.constants.AppConstants;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Satish - 360 on 11/23/2016.
 */

public class ForgotPasswrodRequestParseOperation extends BaseParseOperation {


	protected XmlPullParser xmlpullparser;
	String TAG = "ForgotPasswordXmlParsing";
	StringBuffer sb;
	int holderForStartAndLength[] = new int[2];

	public ForgotPasswrodRequestParseOperation(InputStream is) {

		XmlPullParserFactory factory = null;
		try {
			factory = XmlPullParserFactory.newInstance();
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		factory.setNamespaceAware(true);
		try {
			xmlpullparser = factory.newPullParser();
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			xmlpullparser.setInput(is, "UTF-8");
			processDocument(xmlpullparser);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void processDocument(XmlPullParser xpp) throws XmlPullParserException, IOException {
		int eventType = xpp.getEventType();
		do {
			if (eventType == XmlPullParser.START_DOCUMENT) {
				System.out.println("Start document");
			} else if (eventType == XmlPullParser.END_DOCUMENT) {
				System.out.println("End document");
			} else if (eventType == XmlPullParser.START_TAG) {
				processStartElement(xpp);
			} else if (eventType == XmlPullParser.END_TAG) {
				processEndElement(xpp);
			} else if (eventType == XmlPullParser.TEXT) {
				processText(xpp);
			}
			eventType = xpp.next();
		} while (eventType != XmlPullParser.END_DOCUMENT);
	}

	public void processStartElement(XmlPullParser xpp) {
		String name = xpp.getName();
		String uri = xpp.getNamespace();
		sb = new StringBuffer();

		if ("".equals(uri)) {
			System.out.println("Start element: " + name);

			if (name.equals(AppConstants.SERVER_RESULT_TAG)) {
				if (xpp.getAttributeValue(null, AppConstants.SERVER_STATUS_TAG) != null) {
					status = xpp.getAttributeValue(null, AppConstants.SERVER_STATUS_TAG);
				} else {
					status = "";
				}
				if (xpp.getAttributeValue(null, AppConstants.SERVER_ERROR_CODE_TAG) != null) {
					errorCode = xpp.getAttributeValue(null, AppConstants.SERVER_ERROR_CODE_TAG);
				} else {
					errorCode = "";
				}
				if (xpp.getAttributeValue(null, AppConstants.SERVER_ERROR_MESSAGE_TAG) != null) {
					errormsg = xpp.getAttributeValue(null, AppConstants.SERVER_ERROR_MESSAGE_TAG);
				} else {
					errormsg = "";
				}
			}

		} else {
			System.out.println("Start element: {" + uri + "}" + name);
		}
	}

	public void processEndElement(XmlPullParser xpp) {
		String name = xpp.getName();
		String uri = xpp.getNamespace();
		if ("".equals(uri)) {
			System.out.println("End element: " + name);
		} else System.out.println("End element:   {" + uri + "}" + name);
	}

	public void processText(XmlPullParser xpp) throws XmlPullParserException {
		char ch[] = xpp.getTextCharacters(holderForStartAndLength);
		int start = holderForStartAndLength[0];
		int length = holderForStartAndLength[1];
		System.out.print("Characters:    \"");
		for (int i = start; i < start + length; i++) {
			switch (ch[i]) {
				case '\\':
					sb.append(ch[i]);
					break;

				case '"':
					sb.append(ch[i]);
					break;
				case '\n':
					sb.append(ch[i]);
					break;
				case '\r':
					sb.append(ch[i]);
					break;
				case '\t':
					sb.append(ch[i]);
					break;
				default:
					sb.append(ch[i]);
					break;
			}
		}
		System.out.print("\"\n");
	}
}
