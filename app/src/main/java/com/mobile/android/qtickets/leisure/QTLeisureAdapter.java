package com.mobile.android.qtickets.leisure;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.mobile.android.qtickets.R;
import com.mobile.android.qtickets.constants.AppConstants;
import com.mobile.android.qtickets.event.EventDetailsVO;
import com.mobile.android.qtickets.event.EventsDetailsActivity;
import com.mobile.android.qtickets.utils.QTUtils;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Satish - 360 on 11/21/2016.
 */

public class QTLeisureAdapter extends RecyclerView.Adapter<QTLeisureAdapter.ViewHolder> {
	private Context mContext;
	private ArrayList<EventDetailsVO> eventsList;

	public QTLeisureAdapter(Context ctx, ArrayList<EventDetailsVO> listEvents) {
		mContext = ctx;
		eventsList = listEvents;
	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View v = LayoutInflater.from(mContext).inflate(R.layout.rvadapter_events, parent, false);
		ViewHolder mViewHolder = new ViewHolder(v, viewType);
		return mViewHolder;
	}

	@Override
	public void onBindViewHolder(final ViewHolder holder, final int position) {
		String url = eventsList.get(position).bannerURL;
		url = url.replace("App_Images", "movie_Images");
		Picasso.with(mContext).load(url).into(holder.iv_eventImage);
		holder.tv_eventName.setText(eventsList.get(position).eventname);
		String myString = eventsList.get(position).Venue;
		String upperString = myString.substring(0,1).toUpperCase() + myString.substring(1).toLowerCase();
		holder.tv_location.setText(upperString);

		String date = eventsList.get(position).startDate;
		Calendar calendar = Calendar.getInstance();
		try {
			Date dd = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(date);
			calendar.setTime(dd);
			String monthName = new SimpleDateFormat("MMM").format(calendar.getTime());
			String dateno = new SimpleDateFormat("dd").format(calendar.getTime());
			String day = new SimpleDateFormat("EEE").format(calendar.getTime());
			holder.tv_date_events.setText(monthName+"\n"+dateno+"\n"+day);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		String startDate = eventsList.get(position).startDate;
		String endDate = eventsList.get(position).endDate;
		SimpleDateFormat dfEventDate = new SimpleDateFormat("yyyy-MM-dd");
		try {
			Date newDate = dfEventDate.parse(startDate);
			Date endDateEvent = dfEventDate.parse(endDate);
			dfEventDate = new SimpleDateFormat("dd MMM yyyy");
			startDate = dfEventDate.format(newDate);
			endDate = dfEventDate.format(endDateEvent);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		holder.tv_edetails_date.setText(startDate + " - " + endDate);

		//String newDate = format.format(calendar.getTime());

		holder.itemView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent intn = new Intent(mContext, EventsDetailsActivity.class);
				Gson gson = new Gson();
				String event = gson.toJson(eventsList.get(position));
				QTUtils.getInstance().saveToSharedPreference(mContext,AppConstants.EVENT_ITEM,event);
				intn.putExtra(AppConstants.INTENT_SHARED_EVENT_TAG, event);
				mContext.startActivity(intn);
			}
		});

	}

	@Override
	public int getItemCount() {
		return eventsList.size();
	}

	public class ViewHolder extends RecyclerView.ViewHolder {
		private ImageView iv_eventImage;
		private TextView tv_eventName, tv_location,tv_date_events,tv_edetails_date;

		public ViewHolder(View itemView, int viewType) {
			super(itemView);
			iv_eventImage = (ImageView) itemView.findViewById(R.id.iv_events_image);
			tv_eventName = (TextView) itemView.findViewById(R.id.tv_title_events);
			tv_location = (TextView) itemView.findViewById(R.id.tv_events_location);
			tv_date_events = (TextView)itemView.findViewById(R.id.tv_date_events);
			tv_edetails_date = (TextView)itemView.findViewById(R.id.tv_edetails_date);
		}
	}
}
