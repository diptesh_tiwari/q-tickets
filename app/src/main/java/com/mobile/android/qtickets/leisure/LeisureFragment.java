package com.mobile.android.qtickets.leisure;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mobile.android.qtickets.R;
import com.mobile.android.qtickets.constants.AppConstants;
import com.mobile.android.qtickets.event.EventDetailsListParseOperation;
import com.mobile.android.qtickets.event.EventDetailsVO;
import com.mobile.android.qtickets.event.EventsAdapter;
import com.mobile.android.qtickets.network.InternetConnectionDetector;
import com.mobile.android.qtickets.sports.SportsAdapter;
import com.mobile.android.qtickets.sports.SportsFragment;
import com.mobile.android.qtickets.utils.QTUtils;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Satish - 360 on 11/18/2016.
 */

public class LeisureFragment extends Fragment {

	InternetConnectionDetector connectionDetector;
	private RecyclerView mLeisureRecyclerview;
	private ImageView noEventImageView;
	private QTLeisureAdapter mLeisureAdapter;
	private ArrayList<EventDetailsVO> eventsList;
	private ProgressDialog dialog;

	public LeisureFragment() {
	}

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

		QTUtils.getInstance().saveToSharedPreference(getActivity(),AppConstants.EVENT_ITEM,null);

		SharedPreferences prefmPrefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
		SharedPreferences.Editor editor = prefmPrefs.edit();
		editor.putString(AppConstants.SHARED_EVENTS_LEISURE, null);
		editor.commit();

		View view = inflater.inflate(R.layout.fragment_leisure, container, false);
		connectionDetector = new InternetConnectionDetector(getActivity());




		mLeisureRecyclerview = (RecyclerView) view.findViewById(R.id.rv_leisure);
		noEventImageView = (ImageView) view.findViewById(R.id.noEventImageView);

		final Handler handler = new Handler();
		handler.postDelayed(new Runnable() {
			@Override
			public void run() {
				if (connectionDetector.isConnectedToInternet()) {
					// Do something after 2s = 2000ms
					new LeiuseListUrl().execute();

				}
			}
		}, 300);

		return view;
	}

	private void initializeProgressDialog() {
		dialog = new ProgressDialog(getActivity(), R.style.progress_bar_style);
		dialog.getWindow().setGravity(Gravity.CENTER);
		WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
		dialog.getWindow().setAttributes(params);
		dialog.show();
		dialog.setCancelable(true);
		dialog.setCanceledOnTouchOutside(true);
	}

	private void setupLeisureAdapter(ArrayList<EventDetailsVO> sortedEventTemp) {
		mLeisureRecyclerview.setHasFixedSize(true);
		mLeisureRecyclerview.setNestedScrollingEnabled(false);

		final LinearLayoutManager mEventsLayoutManager = new LinearLayoutManager(getContext());
		mLeisureRecyclerview.setLayoutManager(mEventsLayoutManager);

//		final DividerItemDecoration dividerVertical = new DividerItemDecoration(getContext(), mEventsLayoutManager.getOrientation());
//		dividerVertical.setDrawable(ContextCompat.getDrawable(getContext(), R.drawable.divider_event_item_vertical));
//		mLeisureRecyclerview.addItemDecoration(dividerVertical);

		ArrayList<EventDetailsVO> eventsListTemp= new ArrayList<>();

		for (int i = 0; i <eventsList.size() ; i++) {
			EventDetailsVO detailsVO= eventsList.get(i);
			if(detailsVO.CategoryId.equalsIgnoreCase("12")){
				eventsListTemp.add(detailsVO);
			}

		}

		if(!eventsListTemp.isEmpty()){

			mLeisureAdapter = new QTLeisureAdapter(getActivity(), eventsListTemp);
			mLeisureRecyclerview.setAdapter(mLeisureAdapter);
			mLeisureRecyclerview.setVisibility(View.VISIBLE);
			noEventImageView.setVisibility(View.GONE);

		}else {
			mLeisureRecyclerview.setVisibility(View.GONE);
			noEventImageView.setVisibility(View.VISIBLE);
		}



	}

	public class LeiuseListUrl extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... voids) {

			String url = AppConstants.SERVER_URL_GET_ALL_EVENTT_DETAILS;

			//String url = AppConstants.SERVER_URL_GET_ALL_EVENTT_DETAILS;
			SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
			String currencyType = mPrefs.getString(AppConstants.COUNTRY_TYPE_CURRENCY, null);

			if(currencyType.equalsIgnoreCase(AppConstants.COUNTRY_TYPE_BH)){
				url=url+"Country="+"Bahrain";
			}else   if(currencyType.equalsIgnoreCase(AppConstants.COUNTRY_TYPE_QA)){
				url=url+"Country="+"Qatar";
			}else   if(currencyType.equalsIgnoreCase(AppConstants.COUNTRY_TYPE_UAE)){
				url=url+"Country="+"Dubai";
			}
			if (connectionDetector.isConnectedToInternet()) {
				String resp;
				try {
					url = url.replaceAll("\\s+", "%20");
					HttpClient httpClient = new DefaultHttpClient();
					HttpContext localContext = new BasicHttpContext();
					HttpGet httpGet = new HttpGet(url);
					httpGet.addHeader(BasicScheme.authenticate(new UsernamePasswordCredentials(getResources().getString(R.string.auth_username), getResources().getString(R.string.auth_password)), "UTF-8", false));
					HttpResponse response = httpClient.execute(httpGet, localContext);
					HttpEntity entity = response.getEntity();
					resp = QTUtils.getASCIIContentFromEntity(entity);
					eventsList = parseEventsResponse(resp);
				} catch (Exception e) {
					resp = "error";
				}
			}
			return null;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			initializeProgressDialog();
		}

		@Override
		protected void onPostExecute(Void aVoid) {
			super.onPostExecute(aVoid);
			try {
				dialog.dismiss();
				if (eventsList.size() > 0) {
					SharedPreferences prefmPrefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
					SharedPreferences.Editor editor = prefmPrefs.edit();
					Gson gson = new Gson();
					String eventjson = gson.toJson(eventsList);
					editor.putString(AppConstants.SHARED_EVENTS_LEISURE, eventjson);
					editor.commit();
				} else {
					eventsList.clear();
				}
			} catch (Exception e) {
				e.printStackTrace();
//                Toast.makeText(getActivity(), "Server not Responding", Toast.LENGTH_SHORT).show();
			}
			initEventsList();
		}
	}

	private ArrayList<EventDetailsVO> parseEventsResponse(String result) {
		// TODO Auto-generated method stub
		ArrayList<EventDetailsVO> eventList = new ArrayList<EventDetailsVO>();
		try {
			ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(result.getBytes());
			// To get from raw folder
			//			InputStream is =   getResources().openRawResource(R.raw.getmoviesbylangandtheatreid);
			EventDetailsListParseOperation xmlpull = new EventDetailsListParseOperation(byteArrayInputStream);

			for (final EventDetailsVO e : xmlpull.outputArr) {
				if (e.CategoryId.contentEquals("12")) {
					eventList.add(e);
				}
			}


		} catch (Exception e) {
			// TODO: handle exception
		}
		return eventList;

	}

	private void initEventsList() {
		SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
		Gson gson = new Gson();
		String events = mPrefs.getString(AppConstants.SHARED_EVENTS_LEISURE, "");
		if (!events.equalsIgnoreCase("")) {

			java.lang.reflect.Type type = new TypeToken<List<EventDetailsVO>>() {
			}.getType();
			eventsList = gson.fromJson(events, type);
		}
		setupLeisureAdapter(eventsList);

	}


}
