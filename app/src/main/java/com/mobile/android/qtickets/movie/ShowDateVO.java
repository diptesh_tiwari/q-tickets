package com.mobile.android.qtickets.movie;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by Satish - 360 on 11/28/2016.
 */

public class ShowDateVO implements Parcelable {
	public static final Creator<ShowDateVO> CREATOR = new Creator<ShowDateVO>() {

		public ShowDateVO createFromParcel(Parcel in) {
			return new ShowDateVO(in);
		}

		public ShowDateVO[] newArray(int size) {
			return new ShowDateVO[size];
		}

	};
	public int localShowDateId;
	public String serverId, showDate;
	public ArrayList<ShowTimeVO> showTimesArr;

	public ShowDateVO() {
		// TODO Auto-generated constructor stub
//		showTimesArr = new ArrayList<ShowTimeVO>();

	}

	public ShowDateVO(Parcel in) {
		// TODO Auto-generated constructor stub
		readFromParcel(in);
	}

	private void readFromParcel(Parcel in) {
		localShowDateId = in.readInt();
		serverId = in.readString();
		showDate = in.readString();
		showTimesArr = new ArrayList<ShowTimeVO>();

		in.readList(showTimesArr, getClass().getClassLoader());
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		dest.writeInt(localShowDateId);
		dest.writeString(serverId);
		dest.writeString(showDate);
		dest.writeList(showTimesArr);

	}

}
