package com.mobile.android.qtickets.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.mobile.android.qtickets.R;
import com.mobile.android.qtickets.movie.TheatreVO;

import java.util.ArrayList;

/**
 * Created by Satish - 360 on 12/1/2016.
 */

public class CustomAdapter extends ArrayAdapter<TheatreVO> implements View.OnClickListener {

	//    private ArrayList<ShowTimeVO> times;
	Context mContext;
	private ArrayList<TheatreVO> dataSet;
	private int lastPosition = -1;

	public CustomAdapter(ArrayList<TheatreVO> data, Context context) {
		super(context, R.layout.rvadapter_movie_details, data);
		this.dataSet = data;
		this.mContext = context;
//        this.times = alShowTimes;
	}

	@Override
	public void onClick(View view) {

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// Get the data item for this position
		TheatreVO dataModel = getItem(position);
		// Check if an existing view is being reused, otherwise inflate the view
		ViewHolder viewHolder; // view lookup cache stored in tag

		final View result;

		if (convertView == null) {

			viewHolder = new ViewHolder();
			LayoutInflater inflater = LayoutInflater.from(getContext());
			convertView = inflater.inflate(R.layout.rvadapter_movie_details, parent, false);
			viewHolder.txtName = (TextView) convertView.findViewById(R.id.tv_mall_name);
			viewHolder.btn1 = (Button) convertView.findViewById(R.id.nortitle1);
			viewHolder.btn2 = (Button) convertView.findViewById(R.id.nortitle2);
			viewHolder.btn3 = (Button) convertView.findViewById(R.id.nortitle3);
			viewHolder.btn4 = (Button) convertView.findViewById(R.id.nortitle4);
			viewHolder.btn5 = (Button) convertView.findViewById(R.id.nortitle5);
			viewHolder.btn6 = (Button) convertView.findViewById(R.id.nortitle6);
			viewHolder.btn7 = (Button) convertView.findViewById(R.id.nortitle7);
			viewHolder.btn8 = (Button) convertView.findViewById(R.id.nortitle8);

			result = convertView;

			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
			result = convertView;
		}

       /* Animation animation = AnimationUtils.loadAnimation(mContext, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
		result.startAnimation(animation);*/
		lastPosition = position;

		viewHolder.txtName.setText(dataModel.theatreName);
//        viewHolder.btn1.setText(times.get(position).toString());
//        viewHolder.txtVersion.setText(dataModel.getVersion_number());
//        viewHolder.info.setOnClickListener(this);
//        viewHolder.info.setTag(position);
		// Return the completed view to render on screen
		return convertView;
	}

	// View lookup cache
	private static class ViewHolder {
		TextView txtName;
		private Button btn1, btn2, btn3, btn4, btn5, btn6, btn7, btn8;
	}

}
