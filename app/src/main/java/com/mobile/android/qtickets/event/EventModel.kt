package com.mobile.android.qtickets.event

import `in`.hexalab.abstractvolley.ApiRequestManager
import android.content.Context
import com.mobile.android.qtickets.event.booking.BookEventRequest

/**
 * Created by hexalabssd on 27/03/17.
 */
class EventModel(var mContext: Context) {

    fun bookEvent(pBookEventRequest: BookEventRequest) {
        ApiRequestManager.addToRequestQueue(mContext, pBookEventRequest)
    }
}