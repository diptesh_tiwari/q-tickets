package com.mobile.android.qtickets.movie.list;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubePlayer;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mobile.android.qtickets.R;
import com.mobile.android.qtickets.activities.TabsActivity;
import com.mobile.android.qtickets.constants.AppConstants;
import com.mobile.android.qtickets.movie.Movie;
import com.mobile.android.qtickets.movie.MovieParseOperation;
import com.mobile.android.qtickets.movie.TheatreVO;
import com.mobile.android.qtickets.movie.moviedetails.activity.MovieDetailsActivity;
import com.mobile.android.qtickets.network.InternetConnectionDetector;
import com.mobile.android.qtickets.parsers.TheatreLocationParseOperation;
import com.mobile.android.qtickets.utils.QTUtils;
import com.thefinestartist.ytpa.YouTubePlayerActivity;
import com.thefinestartist.ytpa.enums.Orientation;
import com.thefinestartist.ytpa.utils.YouTubeUrlParser;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Satish - 360 on 11/18/2016.
 */

public class MoviesListFragment extends Fragment implements Spinner.OnItemSelectedListener,View.OnClickListener {
	InternetConnectionDetector connectionDetector;
	String lastmodified;
	ArrayList<Movie> mAllMovies = new ArrayList<Movie>();
	ArrayList<String> altheaters = new ArrayList<String>();
	ArrayList<String> allanguages = new ArrayList<String>();
	ArrayList<Movie> sortedMoviesArr = new ArrayList<Movie>();
	ArrayList<TheatreVO> theaterArr = new ArrayList<TheatreVO>();
	String mtheatername = "All Cinema Halls", mLanuage = "All Languages";
	String status, error_msg;
	private Spinner sp_languages, sp_cinemaMall;
	private RecyclerView mMoviesRecyclerView;
	private MoviesAdapter mMoviesAdapter;
	private MoviesLayoutManager mMoviesLayoutManager;
	private ProgressDialog dialog = null;

	private String movieType="nowShow";
	private String moviesUrl;
	private LinearLayout linearLayout2;
	ViewGroup.LayoutParams params;
	public static TextView upComingShowTV,nowShowTV;


	public String getMovieType() {
		return movieType;
	}

	public void setMovieType(String movieType) {
		this.movieType = movieType;
		if(movieType.equalsIgnoreCase(AppConstants.NOW_SHOW)){
			linearLayout2.setVisibility(View.VISIBLE);
			params.height = 70;
			params.width = params.MATCH_PARENT;
			linearLayout2.setLayoutParams(params);
			linearLayout2.setBackgroundColor(getResources().getColor(R.color.btm_tab_color));
			//linearLayout2.setMinimumHeight(R.dimen.img_menu_width_height);
			moviesUrl=AppConstants.SERVER_URL_GET_ALL_MOVIES;
			mAllMovies.clear();
			altheaters.clear();
			allanguages.clear();
			sortedMoviesArr.clear();
			theaterArr.clear();


			new MoviesListUrl().execute();

		}else if (movieType.equalsIgnoreCase(AppConstants.UPCOMING)){
			linearLayout2.setVisibility(View.INVISIBLE);
			params.height = 5;
			params.width = params.MATCH_PARENT;
			linearLayout2.setLayoutParams(params);
		//linearLayout2.setBackgroundColor(getResources().getColor(R.color.backgroundTheatreName));
			moviesUrl=AppConstants.SERVER_URL_GET_ALL_UP_COMING_CINEMAS;

			mAllMovies.clear();
			altheaters.clear();
			allanguages.clear();
			sortedMoviesArr.clear();
			theaterArr.clear();
			new MoviesListUrl().execute();
		}
	}



	public MoviesListFragment() {
		// Required empty public constructor
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);

	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_movies, container, false);

		linearLayout2 = (LinearLayout)view.findViewById(R.id.linearLayout2);
		params = linearLayout2.getLayoutParams();
		params.height = 70;
		params.width = params.MATCH_PARENT;
		linearLayout2.setLayoutParams(params);
		linearLayout2.setVisibility(View.VISIBLE);

		sp_languages = (Spinner) view.findViewById(R.id.spn_languages);
		sp_cinemaMall = (Spinner) view.findViewById(R.id.spn_cinema_mall);
		nowShowTV= (TextView) view.findViewById(R.id.nowShowTV);
		upComingShowTV= (TextView) view.findViewById(R.id.upComingShowTV);
		mMoviesRecyclerView = (RecyclerView) view.findViewById(R.id.rv_movies);

		mMoviesLayoutManager = new MoviesLayoutManager(getContext(), 2);
		mMoviesRecyclerView.setLayoutManager(mMoviesLayoutManager);

//		final DividerItemDecoration dividerVertical = new DividerItemDecoration(getContext(), mMoviesLayoutManager.getOrientation());
//		dividerVertical.setDrawable(ContextCompat.getDrawable(getContext(), R.drawable.divider_movie_item_vertical));
//		mMoviesRecyclerView.addItemDecoration(dividerVertical);
//
//		final DividerItemDecoration dividerHorizontal = new DividerItemDecoration(getContext(), MoviesLayoutManager.HORIZONTAL);
//		dividerHorizontal.setDrawable(ContextCompat.getDrawable(getContext(), R.drawable.divider_movie_items_horizontal));
//		mMoviesRecyclerView.addItemDecoration(dividerHorizontal);

		connectionDetector = new InternetConnectionDetector(getActivity());
		sp_languages.setOnItemSelectedListener(this);
		sp_cinemaMall.setOnItemSelectedListener(this);

		final Handler handler = new Handler();
		handler.postDelayed(new Runnable() {
			@Override
			public void run() {
				if (connectionDetector.isConnectedToInternet()) {
					// Do something after 2s = 2000ms
					moviesUrl=AppConstants.SERVER_URL_GET_ALL_MOVIES;
					new MoviesListUrl().execute();

				}
			}
		}, 300);

		nowShowTV.setOnClickListener(this);
		upComingShowTV.setOnClickListener(this);

		return view;
	}


	private void initMovieList() {
		try {
			SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(this.getActivity());
			mPrefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
			Gson gson = new Gson();
			String movies = mPrefs.getString(AppConstants.SHARED_MOVIES_TAG, "");
			if (!movies.equalsIgnoreCase("")) {

				java.lang.reflect.Type type = new TypeToken<List<Movie>>() {
				}.getType();
				mAllMovies = gson.fromJson(movies, type);

			}

			String theaters = mPrefs.getString(AppConstants.SHARED_THEATER_TAG, "");
			if (!theaters.equalsIgnoreCase("")) {

				java.lang.reflect.Type type = new TypeToken<List<TheatreVO>>() {
				}.getType();
				theaterArr = gson.fromJson(theaters, type);

			}
			if (theaterArr.size() != 0) {
				for (int i = 0; i < theaterArr.size(); i++) {
					altheaters.add(theaterArr.get(i).theatreName);
				}
				altheaters.add(0, mtheatername);
				setupTheatreAdapter(altheaters);
			}


			if (mAllMovies.size() != 0) {
				for (int i = 0; i < mAllMovies.size(); i++) {
					allanguages.add(mAllMovies.get(i).language);
				}
			  /*  	Log.e("allanguages", allanguages.size()+"");*/
				Set<String> hslng = new HashSet<String>();
				hslng.addAll(allanguages);
				allanguages.clear();
				allanguages.addAll(hslng);
                    /*Log.e("allanguages after filtered", allanguages.size()+"");*/
				allanguages.add(0, mLanuage);
				setupLangAdapter(allanguages);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setupTheatreAdapter(ArrayList<String> altheaters) {
		// Creating adapter for spinner
		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getContext(), R.layout.simple_spinner_item, altheaters);
		// Drop down layout style - list view with radio button
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		// attaching data adapter to spinner
		sp_cinemaMall.setAdapter(dataAdapter);
	}


	private void setupLangAdapter(ArrayList<String> allanguages) {
		// Creating adapter for spinner
		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getContext(), R.layout.simple_spinner_item, allanguages);
		// Drop down layout style - list view with radio button
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		// attaching data adapter to spinner
		sp_languages.setAdapter(dataAdapter);
	}

	private void initializeProgressDialog() {
		dialog = new ProgressDialog(getActivity(), R.style.progress_bar_style);
		dialog.getWindow().setGravity(Gravity.CENTER);
		WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
		dialog.getWindow().setAttributes(params);
		dialog.show();
		dialog.setCancelable(true);
		dialog.setCanceledOnTouchOutside(true);
	}

	private void getTheatresList() {
		String url = AppConstants.SERVICE_URL_GET_ALL_CINEMAS;
        /*Log.e("url", url);*/
		if (connectionDetector.isConnectedToInternet()) {
			String resp;
			try {
				HttpClient httpClient = new DefaultHttpClient();
				HttpContext localContext = new BasicHttpContext();
				HttpGet httpGet = new HttpGet(url);
				httpGet.addHeader(BasicScheme.authenticate(new UsernamePasswordCredentials(getResources().getString(R.string.auth_username), getResources().getString(R.string.auth_password)), "UTF-8", false));
				HttpResponse response = httpClient.execute(httpGet, localContext);
				HttpEntity entity = response.getEntity();
				resp = QTUtils.getASCIIContentFromEntity(entity);
                    /*Log.e("resp", resp);*/
				theaterArr = parseTheatreLocationsResponse(resp);
			} catch (Exception e) {
				resp = "error";
			}
		}
	}

	private ArrayList<TheatreVO> parseTheatreLocationsResponse(String result) {
		// TODO Auto-generated method stub
		ArrayList<TheatreVO> theatresList = new ArrayList<TheatreVO>();
		try {
			//two lines of code for xmlpullparsing
			ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(result.getBytes());
			TheatreLocationParseOperation xmlpull = new TheatreLocationParseOperation(byteArrayInputStream);
			theatresList = xmlpull.theatreArr;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return theatresList;
	}

	private ArrayList<Movie> parseMoviesResponse(String result) {
		// TODO Auto-generated method stub
		ArrayList<Movie> moviesList = new ArrayList<Movie>();
		try {
			ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(result.getBytes());
			// To get from raw folder
			//			InputStream is =   getResources().openRawResource(R.raw.getmoviesbylangandtheatreid);
			MovieParseOperation xmlpull = new MovieParseOperation(byteArrayInputStream);
			moviesList = xmlpull.outputArr;
			status = xmlpull.status;
			error_msg = xmlpull.errormsg;
			lastmodified = xmlpull.last_modified;
		} catch (Exception e) {
			// TODO: handle exception
		}
	return moviesList;

	}

	@Override
	public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
		switch (adapterView.getId()) {
			case R.id.spn_languages:
//                String selectedLang = adapterView.getItemAtPosition(i).toString();
				sortByLanguage();
//                Toast.makeText(getContext(), "Lang Selected : " + selectedLang, Toast.LENGTH_LONG).show();
				break;

			case R.id.spn_cinema_mall:
//                String selectedMall = adapterView.getItemAtPosition(i).toString();
				sortByLanguage();
//                Toast.makeText(getContext(), "Cinema Selected : " + selectedMall, Toast.LENGTH_LONG).show();
				break;
		}
	}

	private void sortByLanguage() {
		ArrayList<Movie> sortedMoviesTemp = new ArrayList<Movie>();
		if (mLanuage.equalsIgnoreCase(sp_languages.getSelectedItem().toString())) {
			sortedMoviesTemp.addAll(mAllMovies);
		} else {
			for (int i = 0; i < mAllMovies.size(); i++) {
				if (mAllMovies.get(i).language.equalsIgnoreCase(sp_languages.getSelectedItem().toString())) {
					sortedMoviesTemp.add(mAllMovies.get(i));
				}
			}

		}
		sortByTheatre(sortedMoviesTemp);
	}

	private void sortByTheatre(ArrayList<Movie> sortedMoviesTemp) {
		sortedMoviesArr = new ArrayList<>();
		if (mtheatername.equals(sp_cinemaMall.getSelectedItem().toString())) {
			sortedMoviesArr.addAll(sortedMoviesTemp);
		} else {
			for (int i = 0; i < sortedMoviesTemp.size(); i++) {
				for (int j = 0; j < sortedMoviesTemp.get(i).movieTheatresArr.size(); j++) {
					if (sortedMoviesTemp.get(i).movieTheatresArr.get(j).theatreName.equalsIgnoreCase(sp_cinemaMall.getSelectedItem().toString())) {
						sortedMoviesArr.add(sortedMoviesTemp.get(i));
					}
				}
			}
		}
		setUpMoviesAdapter(sortedMoviesArr);
	}

	private void setUpMoviesAdapter(ArrayList<Movie> sortedMoviesArr) {
		mMoviesRecyclerView.setHasFixedSize(true);

		mMoviesAdapter = new MoviesAdapter(getActivity(), sortedMoviesArr);
		mMoviesAdapter.setMovieItemClickListener(new MovieItemClickListener() {
			@Override
			public void onMovieItemClick(final Movie pMovie) {

				SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
				String currencyType = mPrefs.getString(AppConstants.COUNTRY_TYPE_CURRENCY, null);

				if(currencyType.equalsIgnoreCase(AppConstants.COUNTRY_TYPE_BH)){

					showTrailer(pMovie);

				}else   if(currencyType.equalsIgnoreCase(AppConstants.COUNTRY_TYPE_QA)){
					openMovieDetails(pMovie);
				}else   if(currencyType.equalsIgnoreCase(AppConstants.COUNTRY_TYPE_UAE)){

					showTrailer(pMovie);
				}
			}

			@Override
			public void onsetData(Object object) {

			}
		});

		mMoviesRecyclerView.setAdapter(mMoviesAdapter);
		mMoviesAdapter.setMovieType(movieType);


	}

	private void showTrailer(Movie pMovie) {

		if (TextUtils.isEmpty(pMovie.TrailerURL)) {
			Toast.makeText(getActivity(), "Trailer video is not available.", Toast.LENGTH_SHORT).show();
			return;
		}

		Intent intent = new Intent(getActivity(), YouTubePlayerActivity.class);

// Youtube video ID (Required, You can use YouTubeUrlParser to parse Video Id from url)
		intent.putExtra(YouTubePlayerActivity.EXTRA_VIDEO_ID, YouTubeUrlParser.getVideoId(pMovie.TrailerURL));

// Youtube player style (DEFAULT as default)
		intent.putExtra(YouTubePlayerActivity.EXTRA_PLAYER_STYLE, YouTubePlayer.PlayerStyle.DEFAULT);

// Screen Orientation Setting (AUTO for default)
// AUTO, AUTO_START_WITH_LANDSCAPE, ONLY_LANDSCAPE, ONLY_PORTRAIT
		intent.putExtra(YouTubePlayerActivity.EXTRA_ORIENTATION, Orientation.ONLY_LANDSCAPE);

// TheatreAndShowsAdapter audio interface when watchMovie adjust volume (true for default)
		intent.putExtra(YouTubePlayerActivity.EXTRA_SHOW_AUDIO_UI, true);

// If the video is not playable, use Youtube app or Internet Browser to play it
// (true for default)
		intent.putExtra(YouTubePlayerActivity.EXTRA_HANDLE_ERROR, true);

		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);

	}

	private void openMovieDetails(final Movie pMovie) {
		Gson gson = new Gson();
		String movieJson = gson.toJson(pMovie);
		SharedPreferences prefmPrefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
		SharedPreferences.Editor editor = prefmPrefs.edit();
		editor.putString(AppConstants.SELECTED_MOVIE_DETAIL, movieJson);
		editor.commit();
		MovieDetailsActivity.start(getContext(), movieJson);
	}

	@Override
	public void onNothingSelected(AdapterView<?> adapterView) {

	}

	@Override
	public void onClick(View view) {

		switch (view.getId()) {
			case R.id.nowShowTV:
				((TabsActivity)getActivity()).nowShowClick();
				break;

			case R.id.upComingShowTV:
				((TabsActivity)getActivity()).upcmngClick();
				break;
		}
	}

	public class MoviesListUrl extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... voids) {
			String resp;
			try {
				getTheatresList();
				HttpClient httpClient = new DefaultHttpClient();
				HttpContext localContext = new BasicHttpContext();
				HttpGet httpGet = new HttpGet(moviesUrl);
				httpGet.addHeader(BasicScheme.authenticate(new UsernamePasswordCredentials(getResources().getString(R.string.auth_username), getResources().getString(R.string.auth_password)), "UTF-8", false));
				HttpResponse response = httpClient.execute(httpGet, localContext);
				HttpEntity entity = response.getEntity();
				resp = QTUtils.getASCIIContentFromEntity(entity);
				mAllMovies = parseMoviesResponse(resp);

			} catch (Exception e) {
				resp = "error";
			}
			return null;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			initializeProgressDialog();
		}

		@Override
		protected void onPostExecute(Void aVoid) {
			super.onPostExecute(aVoid);
			try {
				dialog.dismiss();
				if (status.equalsIgnoreCase(AppConstants.RESPONSE_TRUE_TAG)) {
//                if (mAllMovies.size() > 0) {
					SharedPreferences prefmPrefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
					SharedPreferences.Editor editor = prefmPrefs.edit();
					Gson gson = new Gson();
					String moviejson = gson.toJson(mAllMovies);
					String theaterjson = gson.toJson(theaterArr);
					editor.putString(AppConstants.SHARED_MOVIES_TAG, moviejson);
					editor.putString(AppConstants.SHARED_THEATER_TAG, theaterjson);
					editor.putString(AppConstants.SHARED_MOVIE_LAST_MODIFIED_TAG, lastmodified);
					editor.commit();

//                }
				} else if (status.equalsIgnoreCase(AppConstants.RESPONSE_FALSE_TAG)) {
					new AlertDialog.Builder(getActivity()).setMessage(error_msg).setPositiveButton("OK", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {

						}
					}).show();
//                    else {
//                    mAllMovies.clear();
//                    theaterArr.clear();
//                    new MoviesListUrl().execute();
//                }
				}
			} catch (Exception e) {
//                Toast.makeText(getActivity(), "Server not responding..", Toast.LENGTH_SHORT).show();
				e.printStackTrace();
			}
			initMovieList();
		}
	}


}
