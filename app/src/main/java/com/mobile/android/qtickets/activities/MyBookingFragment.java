package com.mobile.android.qtickets.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Xml;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;
import com.mobile.android.qtickets.CustomFonts.RadioRegular;
import com.mobile.android.qtickets.R;
import com.mobile.android.qtickets.adapters.BookedAdapter;
import com.mobile.android.qtickets.adapters.BookedEventAdapter;
import com.mobile.android.qtickets.constants.AppConstants;
import com.mobile.android.qtickets.event.EventDetailsVO;
import com.mobile.android.qtickets.event.EventTicketDetails;
import com.mobile.android.qtickets.event.EventsAdapter;
import com.mobile.android.qtickets.event.EventsFragment;
import com.mobile.android.qtickets.model.BookedEventItem;
import com.mobile.android.qtickets.model.BookedItem;
import com.mobile.android.qtickets.model.UserDetailsModel;
import com.mobile.android.qtickets.movie.Movie;
import com.mobile.android.qtickets.movie.MovieParseOperation;
import com.mobile.android.qtickets.network.InternetConnectionDetector;
import com.mobile.android.qtickets.parsers.BaseParseOperation;
import com.mobile.android.qtickets.utils.QTUtils;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by Android on 4/27/17.
 */

public class MyBookingFragment extends Fragment {

    LayoutInflater inflater;

    InternetConnectionDetector connectionDetector;
    private RecyclerView rv_booked;
    private BookedAdapter bookedAdapter;
    private BookedEventAdapter bookedEventAdapter;
    BookedEventItem bookedEventItem;
    BookedItem bookedItem;
    ArrayList<BookedItem> bookedItemArrayList = new ArrayList<BookedItem>();
    ArrayList<BookedEventItem> bookedEventItemArrayList = new ArrayList<>();
    private ProgressDialog dialog;

    RadioGroup rgBookedType;
    RelativeLayout rlNoItems;
    TextView tvStartBooking;

    UserDetailsModel user;
    String userId = "";

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        inflater = (LayoutInflater) activity.getLayoutInflater();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.my_bookings_fragment, container, false);
        connectionDetector = new InternetConnectionDetector(getActivity());

        rv_booked = (RecyclerView) view.findViewById(R.id.rv_booked);
        rgBookedType = (RadioGroup)view.findViewById(R.id.rgBookedType);
        rlNoItems = (RelativeLayout)view.findViewById(R.id.rlNoItems);
        tvStartBooking = (TextView)view.findViewById(R.id.tvStartBooking);

        SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
        Gson gson = new Gson();
        String usrDetails = mPrefs.getString(AppConstants.SHARED_USER_TAG,null);

        if (usrDetails != null) {
            java.lang.reflect.Type type = new TypeToken<UserDetailsModel>(){}.getType();
            try {
                user = gson.fromJson(usrDetails,type);
                userId = user.id;

            }catch (Exception e){
                e.printStackTrace();
            }

        }


        rgBookedType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, @IdRes int i) {
                switch (i){
                    case R.id.rbMovies:
                        rv_booked.setAdapter(null);
                        rv_booked.invalidate();
                        bookedAdapter = new BookedAdapter(getActivity(), bookedItemArrayList);
                        RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(getActivity().getApplicationContext());
                        rv_booked.setLayoutManager(mLayoutManager1);
                        rv_booked.setAdapter(bookedAdapter);
                        break;

                    case R.id.rbEvents:
                        if (bookedEventItemArrayList.size()==0){
                            rv_booked.setAdapter(null);
                            rv_booked.invalidate();
                            new BookedEventListUrl().execute();
                        }else {
                            rv_booked.setAdapter(null);
                            rv_booked.invalidate();
                            bookedEventAdapter = new BookedEventAdapter(getActivity(), bookedEventItemArrayList);
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
                            rv_booked.setLayoutManager(mLayoutManager);
                            rv_booked.setAdapter(bookedEventAdapter);
                        }
                        break;
                }
            }
        });

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (connectionDetector.isConnectedToInternet()) {
                    // Do something after 2s = 2000ms
                    new BookedMovieListUrl().execute();

                }
            }
        }, 300);

        return view;
    }

    public class BookedMovieListUrl extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... urls) {

            String url = AppConstants.SERVER_URL_GET_BOOKEDLIST;
            if (connectionDetector.isConnectedToInternet()) {
                try {
                    url = url.replaceAll("\\s+", "%20");
                    url = url+"userid="+userId;
                    HttpClient httpClient = new DefaultHttpClient();
                    HttpContext localContext = new BasicHttpContext();
                    HttpGet httpGet = new HttpGet(url);
                   // httpGet.addHeader(BasicScheme.authenticate(new UsernamePasswordCredentials(getResources().getString(R.string.auth_username), getResources().getString(R.string.auth_password)), "UTF-8", false));
                    HttpResponse response = httpClient.execute(httpGet, localContext);
                    HttpEntity entity = response.getEntity();
                    String response_str = QTUtils.getASCIIContentFromEntity(entity);
                    return response_str;
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            initializeProgressDialog();
        }

        protected void onPostExecute(String result) {
            try {

                if (result !=null ){
                    JSONObject jsonObj = null;
                    try {
                        jsonObj = XML.toJSONObject(result);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    if (jsonObj != null){

                        JSONObject response = jsonObj.getJSONObject("response");
                        String status = response.getString("status");

                        if (status.equalsIgnoreCase("true")){

                            JSONObject BookingHistories = response.getJSONObject("BookingHistories");

                            JSONArray bookinghistory = BookingHistories.getJSONArray("bookinghistory");

                            for (int j = 0; j < bookinghistory.length(); j++) {

                                JSONObject object = bookinghistory.getJSONObject(j);

                                String id = object.getString("id");
                                String movie = object.getString("movie");
                                String theater = object.getString("theater");
                                String area = object.getString("area");
                                String bookedtime = object.getString("bookedtime");
                                String showdate = object.getString("showdate");
                                String seats = object.getString("seats");
                                String seatsCount = object.getString("seatsCount");
                                String confirmationCode = object.getString("confirmationCode");
                                String barcodeURL = object.getString("barcodeURL");
                                String ticket_Cost = object.getString("ticket_Cost");
                                String total_Cost = object.getString("total_Cost");
                                String bookingfees = object.getString("bookingfees");
                                String movieImageURL = object.getString("movieImageURL");
                                String moviebannerURL = object.getString("moviebannerURL");
                                String movieServerID = object.getString("movieServerID");
                                String checkcancelstatus = object.getString("checkcancelstatus");

                                bookedItem = new BookedItem(id, movie, theater, area, bookedtime, showdate,seats, seatsCount, confirmationCode,
                                        barcodeURL, ticket_Cost, total_Cost, bookingfees,movieImageURL,moviebannerURL,movieServerID,
                                        checkcancelstatus);

                                bookedItemArrayList.add(bookedItem);

                            }

                            bookedAdapter = new BookedAdapter(getActivity(), bookedItemArrayList);
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
                            rv_booked.setLayoutManager(mLayoutManager);
                            rv_booked.setAdapter(bookedAdapter);

                            if (bookedItemArrayList.size()==0){
                                rlNoItems.setVisibility(View.VISIBLE);
                            }else {
                                rlNoItems.setVisibility(View.GONE);
                            }

                            dialog.dismiss();

                        }else {
                            if (bookedItemArrayList.size()==0){
                                rlNoItems.setVisibility(View.VISIBLE);
                            }else {
                                rlNoItems.setVisibility(View.GONE);
                            }

                            dialog.dismiss();
                        }
                    }else {
                        if (bookedItemArrayList.size()==0){
                            rlNoItems.setVisibility(View.VISIBLE);
                        }else {
                            rlNoItems.setVisibility(View.GONE);
                        }

                        dialog.dismiss();
                    }
                }else {
                    if (bookedItemArrayList.size()==0){
                        rlNoItems.setVisibility(View.VISIBLE);
                    }else {
                        rlNoItems.setVisibility(View.GONE);
                    }

                    dialog.dismiss();
                }


            } catch (Exception e) {
                e.printStackTrace();
                if (bookedItemArrayList.size()==0){
                    rlNoItems.setVisibility(View.VISIBLE);
                }else {
                    rlNoItems.setVisibility(View.GONE);
                }

                dialog.dismiss();
               Toast.makeText(getActivity(), "Server not Responding", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public class BookedEventListUrl extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... urls) {

            String url = AppConstants.SERVER_URL_GET_BOOKEDEVENTLIST;
            if (connectionDetector.isConnectedToInternet()) {
                try {
                    url = url.replaceAll("\\s+", "%20");
                    url = url+"userid="+userId;
                    HttpClient httpClient = new DefaultHttpClient();
                    HttpContext localContext = new BasicHttpContext();
                    HttpGet httpGet = new HttpGet(url);
                    // httpGet.addHeader(BasicScheme.authenticate(new UsernamePasswordCredentials(getResources().getString(R.string.auth_username), getResources().getString(R.string.auth_password)), "UTF-8", false));
                    HttpResponse response = httpClient.execute(httpGet, localContext);
                    HttpEntity entity = response.getEntity();
                    String response_str = QTUtils.getASCIIContentFromEntity(entity);
                    return response_str;
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            initializeProgressDialog();
        }

        protected void onPostExecute(String result) {
            try {

                if (result !=null ){
                    JSONObject jsonObj = null;
                    try {
                        jsonObj = XML.toJSONObject(result);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    if (jsonObj != null){

                        JSONObject response = jsonObj.getJSONObject("response");
                        String status = response.getString("status");

                        if (status.equalsIgnoreCase("true")){

                            JSONObject BookingHistories = response.getJSONObject("BookingHistories");

                            JSONArray bookinghistory = BookingHistories.getJSONArray("bookinghistory");

                            for (int j = 0; j < bookinghistory.length(); j++) {

                                JSONObject object = bookinghistory.getJSONObject(j);

                                String id = object.getString("id");
                                String Eventid = object.getString("Eventid");
                                String total_Cost = object.getString("total_Cost");
                                String BookedOn = object.getString("BookedOn");
                                String seatsCount = object.getString("seatsCount");
                                String Event = object.getString("Event");
                                String location = object.getString("location");
                                String eventDate = object.getString("eventDate");
                                String eventTime = object.getString("eventTime");
                                String confirmationCode = object.getString("confirmationCode");
                                String eventImageURL = object.getString("eventImageURL");

                                bookedEventItem = new BookedEventItem(id,Eventid,total_Cost,BookedOn,
                                        seatsCount,Event,location,eventDate,eventTime,
                                        confirmationCode,eventImageURL);

                                bookedEventItemArrayList.add(bookedEventItem);

                            }

                            bookedEventAdapter = new BookedEventAdapter(getActivity(), bookedEventItemArrayList);
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
                            rv_booked.setLayoutManager(mLayoutManager);
                            rv_booked.setAdapter(bookedEventAdapter);

                            if (bookedEventItemArrayList.size()==0){
                                rlNoItems.setVisibility(View.VISIBLE);
                            }else {
                                rlNoItems.setVisibility(View.GONE);
                            }

                            dialog.dismiss();

                        }else {
                            if (bookedEventItemArrayList.size()==0){
                                rlNoItems.setVisibility(View.VISIBLE);
                            }else {
                                rlNoItems.setVisibility(View.GONE);
                            }
                            dialog.dismiss();
                        }
                    }else {
                        if (bookedEventItemArrayList.size()==0){
                            rlNoItems.setVisibility(View.VISIBLE);
                        }else {
                            rlNoItems.setVisibility(View.GONE);
                        }
                        dialog.dismiss();
                    }
                }else {
                    if (bookedEventItemArrayList.size()==0){
                        rlNoItems.setVisibility(View.VISIBLE);
                    }else {
                        rlNoItems.setVisibility(View.GONE);
                    }
                    dialog.dismiss();
                }


            } catch (Exception e) {
                e.printStackTrace();
                dialog.dismiss();
                Toast.makeText(getActivity(), "Server not Responding", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void initializeProgressDialog() {
        dialog = new ProgressDialog(getActivity(), R.style.progress_bar_style);
        dialog.getWindow().setGravity(Gravity.CENTER);
        WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
        dialog.getWindow().setAttributes(params);
        dialog.show();
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
    }



}