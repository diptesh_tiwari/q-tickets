package com.mobile.android.qtickets.movie.list;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.gson.Gson;
import com.mobile.android.qtickets.AppController;
import com.mobile.android.qtickets.R;
import com.mobile.android.qtickets.constants.AppConstants;
import com.mobile.android.qtickets.event.EventDetailsListParseOperation;
import com.mobile.android.qtickets.event.EventDetailsVO;
import com.mobile.android.qtickets.event.EventTicketDetails;
import com.mobile.android.qtickets.event.EventsDetailsActivity;
import com.mobile.android.qtickets.movie.Movie;
import com.mobile.android.qtickets.movie.MovieParseOperation;
import com.mobile.android.qtickets.movie.moviedetails.activity.MovieDetailsActivity;
import com.mobile.android.qtickets.network.InternetConnectionDetector;
import com.mobile.android.qtickets.search.SearchActivity;
import com.mobile.android.qtickets.search.SearchResult;
import com.mobile.android.qtickets.search.SearchResultAdapter;
import com.mobile.android.qtickets.search.SearchResultResponse;
import com.mobile.android.qtickets.utils.QTUtils;
import com.thefinestartist.ytpa.YouTubePlayerActivity;
import com.thefinestartist.ytpa.enums.Orientation;
import com.thefinestartist.ytpa.utils.YouTubeUrlParser;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;

import in.hexalab.abstractvolley.base.HResponseListener;

/**
 * Created by android2 on 17/04/17.
 */

public class SearchableActivity extends AppCompatActivity implements TextView.OnEditorActionListener, View.OnClickListener {

    private String mKeyword;
    private AppController mAppController;
    private RecyclerView mSearchResultRecyclerView;
    private TextView mEmptyMessageTextView, cancelTV;
    private EditText mSearchEditText;
    private SearchResultAdapter mSearchResultAdapter;
    InternetConnectionDetector connectionDetector;
    private ProgressDialog dialog = null;
    private String movieId = null;
    private String eventId = null;
    String status, error_msg;
    String lastmodified;
    ArrayList<Movie> mAllMovies = new ArrayList<Movie>();
    private ArrayList<EventDetailsVO> eventsList = new ArrayList<>();
    List<EventTicketDetails> eventTicketDetails = new ArrayList<>();
    List<String> eventTicketPrices = new ArrayList<>();
    private ArrayList<String> mVenueList = new ArrayList<String>();

    private final HResponseListener<SearchResultResponse> mSearchResultResponseListener = new HResponseListener<SearchResultResponse>() {
        @Override
        public void onRequestSubmitted() {

        }

        @Override
        public void onErrorResponse(final VolleyError error) {
            showSearchErrorToast("Error while searching for '" + mKeyword + "'.");
            error.printStackTrace();
        }

        @Override
        public void onResponse(final SearchResultResponse response) {
            updateSearchResult(response.getSearchItems());
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_layout);

        mSearchEditText = (EditText) findViewById(R.id.editText_search);
        cancelTV = (TextView) findViewById(R.id.cancelTV);
        mSearchResultRecyclerView = (RecyclerView) findViewById(R.id.recyclerView_searchResult);
        mEmptyMessageTextView = (TextView) findViewById(R.id.textView_emptyMessage);

        mSearchEditText.setCursorVisible(true);
        mSearchEditText.setOnEditorActionListener(this);

        setupSearchResultRecyclerView();
        mAppController = new AppController(this);
        cancelTV.setOnClickListener(this);
        connectionDetector = new InternetConnectionDetector(this);


    }


    private void setupSearchResultRecyclerView() {
        mSearchResultRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mSearchResultAdapter = new SearchResultAdapter(this);

        mSearchResultAdapter.setMovieItemClickListener(new MovieItemClickListener() {
            @Override
            public void onMovieItemClick(final Movie pMovie) {

            }

            @Override
            public void onsetData(Object object) {
                openMovieDetails(object);
            }
        });
        mSearchResultRecyclerView.setAdapter(mSearchResultAdapter);
    }


    private void openMovieDetails(final Object object) {
        Gson gson = new Gson();
        String movieJson = gson.toJson(object);

        try {
            JSONObject jsonObject = new JSONObject(movieJson);
            String url=jsonObject.getString("url");
            if(url.equalsIgnoreCase("movies")) {
                movieId = jsonObject.getString("id");
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (connectionDetector.isConnectedToInternet()) {
                            new getMovieDetailUrl().execute();
                        }
                    }
                }, 300);


            }else {
                eventId = jsonObject.getString("id");
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (connectionDetector.isConnectedToInternet()) {
                            new EventListUrl().execute();
                        }
                    }
                }, 300);

            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }



    public class EventListUrl extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {

            String url = AppConstants.SERVER_URL_GET_ALL_EVENTT_BY_ID + "Event_id=" + eventId;
         /*   SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(SearchableActivity.this);
            String currencyType = mPrefs.getString(AppConstants.COUNTRY_TYPE_CURRENCY, null);

            if (currencyType.equalsIgnoreCase(AppConstants.COUNTRY_TYPE_BH)) {
                url = url + "Country=" + "Bahrain";
            } else if (currencyType.equalsIgnoreCase(AppConstants.COUNTRY_TYPE_QA)) {
                url = url + "Country=" + "Qatar";
            } else if (currencyType.equalsIgnoreCase(AppConstants.COUNTRY_TYPE_UAE)) {
                url = url + "Country=" + "Dubai";
            }*/


            if (connectionDetector.isConnectedToInternet()) {
                String resp;
                try {
                    url = url.replaceAll("\\s+", "%20");
                    HttpClient httpClient = new DefaultHttpClient();
                    HttpContext localContext = new BasicHttpContext();
                    HttpGet httpGet = new HttpGet(url);
                    httpGet.addHeader(BasicScheme.authenticate(new UsernamePasswordCredentials(getResources().getString(R.string.auth_username), getResources().getString(R.string.auth_password)), "UTF-8", false));
                    HttpResponse response = httpClient.execute(httpGet, localContext);
                    HttpEntity entity = response.getEntity();
                    resp = QTUtils.getASCIIContentFromEntity(entity);
                    eventsList = parseEventsResponse(resp);
                } catch (Exception e) {
                    resp = "error";
                }
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            initializeProgressDialog();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            try {
                dialog.dismiss();
                if (!eventsList.isEmpty()) {

                    for (int i = 0; i < eventsList.size(); i++) {
                        eventTicketDetails = eventsList.get(i).evntTktDetilas;
                        if (!eventTicketDetails.isEmpty()) {
                            for (int j = 0; j < eventTicketDetails.size(); j++) {
                                String price = eventTicketDetails.get(j).TicketPrice;
                                eventTicketPrices.add(price);
                                Log.d("", eventTicketPrices.toString());
                            }
                        }
                    }

                    SharedPreferences prefmPrefs = PreferenceManager.getDefaultSharedPreferences(SearchableActivity.this);
                    SharedPreferences.Editor editor = prefmPrefs.edit();
                    Gson gson = new Gson();
                    String eventjson = gson.toJson(eventsList);
                    String eventTicketPricejson = gson.toJson(eventTicketPrices);

                    //editor.putString(AppConstants.SHARED_EVENTS_TAG, eventjson);
                    editor.putString(AppConstants.SHARED_EVENTS_TICKET_PRICES, eventTicketPricejson);
                    editor.commit();

                    Intent intn = new Intent(SearchableActivity.this, EventsDetailsActivity.class);
                    EventDetailsVO eventDetailsVO=eventsList.get(0);
                    String event = gson.toJson(eventDetailsVO);
                    intn.putExtra(AppConstants.INTENT_SHARED_EVENT_TAG, event);
                    startActivity(intn);

                } else {
                    eventsList.clear();
                }
            } catch (Exception e) {
                e.printStackTrace();
//                Toast.makeText(getActivity(), "Server not Responding", Toast.LENGTH_SHORT).show();
            }
            //initEventsList();
        }
    }


    private ArrayList<EventDetailsVO> parseEventsResponse(String result) {
        // TODO Auto-generated method stub
        eventsList.clear();
        ArrayList<EventDetailsVO> eventList = new ArrayList<EventDetailsVO>();
        try {
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(result.getBytes());
            // To get from raw folder
            //       InputStream is =   getResources().openRawResource(R.raw.getmoviesbylangandtheatreid);
            EventDetailsListParseOperation xmlpull = new EventDetailsListParseOperation(byteArrayInputStream);


            for (final EventDetailsVO e : xmlpull.outputArr) {
                eventList.add(e);

            }

            for (final EventDetailsVO e : xmlpull.outputArr) {
                if (e.CategoryId.contentEquals("4")) {
                    eventList.add(e);
                }
            }
           mVenueList.clear();
            mVenueList.add(0, "Venues");
            for (int i = 0; i < eventList.size(); i++) {
                String venue = eventList.get(i).Venue;
                if (!mVenueList.contains(venue)) {
                    mVenueList.add(venue);
                }
            }


            Log.d("vanue", mVenueList.toString());
        } catch (Exception e) {
            // TODO: handle exception
        }
        return eventList;

    }


    public class getMovieDetailUrl extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            String resp;
            try {
                String url = AppConstants.GET_MOVIE_BY_ID + "Movie_id=" + movieId;
                url = url.replaceAll("\\s+", "%20");
                HttpClient httpClient = new DefaultHttpClient();
                HttpContext localContext = new BasicHttpContext();
                HttpGet httpGet = new HttpGet(url);
                httpGet.addHeader(BasicScheme.authenticate(new UsernamePasswordCredentials(getResources().getString(R.string.auth_username), getResources().getString(R.string.auth_password)), "UTF-8", false));
                HttpResponse response = httpClient.execute(httpGet, localContext);
                HttpEntity entity = response.getEntity();
                resp = QTUtils.getASCIIContentFromEntity(entity);
                mAllMovies = parseMoviesResponse(resp);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            initializeProgressDialog();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            try {
                dialog.dismiss();
                if (status.equalsIgnoreCase(AppConstants.RESPONSE_TRUE_TAG)) {
//                if (mAllMovies.size() > 0) {
                    // SharedPreferences prefmPrefs = PreferenceManager.getDefaultSharedPreferences(SearchableActivity.this);
                    //SharedPreferences.Editor editor = prefmPrefs.edit();
                    Gson gson = new Gson();
                    Movie movie = mAllMovies.get(0);
                    String moviejson = gson.toJson(movie);
                    SharedPreferences prefmPrefs = PreferenceManager.getDefaultSharedPreferences(SearchableActivity.this);
                    SharedPreferences.Editor editor = prefmPrefs.edit();
                    editor.putString(AppConstants.SELECTED_MOVIE_DETAIL, moviejson);
                    editor.commit();

                    // SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(SearchableActivity.this);
                    String currencyType = prefmPrefs.getString(AppConstants.COUNTRY_TYPE_CURRENCY, null);

                    if (currencyType.equalsIgnoreCase(AppConstants.COUNTRY_TYPE_BH)) {

                        showTrailer(movie);

                    } else if (currencyType.equalsIgnoreCase(AppConstants.COUNTRY_TYPE_QA)) {
                        openMovieDetails(movie);
                    } else if (currencyType.equalsIgnoreCase(AppConstants.COUNTRY_TYPE_UAE)) {

                        showTrailer(movie);
                    }

                }
            } catch (Exception e) {

                e.printStackTrace();
            }

        }
    }


    private void showTrailer(Movie pMovie) {

        if (TextUtils.isEmpty(pMovie.TrailerURL)) {
            Toast.makeText(SearchableActivity.this, "Trailer video is not available.", Toast.LENGTH_SHORT).show();
            return;
        }

        Intent intent = new Intent(SearchableActivity.this, YouTubePlayerActivity.class);

// Youtube video ID (Required, You can use YouTubeUrlParser to parse Video Id from url)
        intent.putExtra(YouTubePlayerActivity.EXTRA_VIDEO_ID, YouTubeUrlParser.getVideoId(pMovie.TrailerURL));

// Youtube player style (DEFAULT as default)
        intent.putExtra(YouTubePlayerActivity.EXTRA_PLAYER_STYLE, YouTubePlayer.PlayerStyle.DEFAULT);

// Screen Orientation Setting (AUTO for default)
// AUTO, AUTO_START_WITH_LANDSCAPE, ONLY_LANDSCAPE, ONLY_PORTRAIT
        intent.putExtra(YouTubePlayerActivity.EXTRA_ORIENTATION, Orientation.ONLY_LANDSCAPE);

// TheatreAndShowsAdapter audio interface when watchMovie adjust volume (true for default)
        intent.putExtra(YouTubePlayerActivity.EXTRA_SHOW_AUDIO_UI, true);

// If the video is not playable, use Youtube app or Internet Browser to play it
// (true for default)
        intent.putExtra(YouTubePlayerActivity.EXTRA_HANDLE_ERROR, true);

        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);

    }

    private void openMovieDetails(final Movie pMovie) {
        Gson gson = new Gson();
        String movieJson = gson.toJson(pMovie);
        SharedPreferences prefmPrefs = PreferenceManager.getDefaultSharedPreferences(SearchableActivity.this);
        SharedPreferences.Editor editor = prefmPrefs.edit();
        editor.putString(AppConstants.SELECTED_MOVIE_DETAIL, movieJson);
        editor.commit();
        MovieDetailsActivity.start(SearchableActivity.this, movieJson);
    }

    private ArrayList<Movie> parseMoviesResponse(String result) {
        // TODO Auto-generated method stub
        ArrayList<Movie> moviesList = new ArrayList<Movie>();
        try {
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(result.getBytes());
            // To get from raw folder
            //			InputStream is =   getResources().openRawResource(R.raw.getmoviesbylangandtheatreid);
            MovieParseOperation xmlpull = new MovieParseOperation(byteArrayInputStream);
            moviesList = xmlpull.outputArr;
            status = xmlpull.status;
            error_msg = xmlpull.errormsg;
            lastmodified = xmlpull.last_modified;
        } catch (Exception e) {
            // TODO: handle exception
        }
        return moviesList;

    }

    private void initializeProgressDialog() {
        dialog = new ProgressDialog(this, R.style.progress_bar_style);
        dialog.getWindow().setGravity(Gravity.CENTER);
        WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
        params.y = 200;
        dialog.getWindow().setAttributes(params);
        dialog.show();
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }


    private void fetchSearchResults(final String pKeyword) {
        mAppController.getSearchResult(pKeyword, mSearchResultResponseListener);
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

        final String keyword = v.getText().toString();
        fetchSearchResults(keyword);
        return false;
    }


    private void updateSearchResult(SearchResult[] pSearchResults) {
        if (pSearchResults.length > 0) {
            mSearchResultAdapter.updateSearchResult(pSearchResults);
            showSearchResults();
            hideEmptyMessage();
        } else {
            hideSearchResults();
            showEmptyMessage();
        }
    }

    private void showEmptyMessage() {
        mEmptyMessageTextView.setVisibility(View.VISIBLE);
    }

    private void hideEmptyMessage() {
        mEmptyMessageTextView.setVisibility(View.INVISIBLE);
    }

    private void showSearchResults() {
        mSearchResultRecyclerView.setVisibility(View.VISIBLE);
    }

    private void hideSearchResults() {
        mSearchResultRecyclerView.setVisibility(View.INVISIBLE);
    }

    private void showSearchErrorToast(final String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View v) {
        finish();
    }
}
