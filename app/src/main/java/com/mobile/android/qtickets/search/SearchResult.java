package com.mobile.android.qtickets.search;

import com.google.gson.annotations.SerializedName;

/**
 * Created by hexalabssd on 23/01/17.
 */

public final class SearchResult {

	@SerializedName("id")
	private String mId; // ": "2492",

	@SerializedName("link_path")
	private String mLinkPath; // ": "https://www.youtube.com/embed/TaOE9emaz-8?autoplay=0",

	@SerializedName("name")
	private String mName; // ": "XXX: RETURN OF XANDER CAGE (IMAX-3D)",

	@SerializedName("description")
	private String mDescription; // ": "Xander Cage is left for dead after an incident, though he secretly returns to action for a new, tough assignment with his handler Augustus Gibbons.",

	@SerializedName("gen_ven")
	private String mGenreVenue; // ": "Action,Adventure,Thriller",

	@SerializedName("language")
	private String mLanguage; // ": "English",

	@SerializedName("poster")
	private String mPosterImagePath; // ": "movie_images/XXX_RETURN_OF_XANDER_CAGE_(IMAX-3D)_thumb.jpg",

	@SerializedName("url")
	private String mUrl; // ": "movies"

	public String getId() {
		return mId;
	}

	public String getLinkPath() {
		return mLinkPath;
	}

	public String getName() {
		return mName;
	}

	public String getDescription() {
		return mDescription;
	}

	public String getGenreVenue() {
		return mGenreVenue;
	}

	public String getLanguage() {
		return mLanguage;
	}

	public String getPosterImagePath() {
		return mPosterImagePath;
	}

	public String getUrl() {
		return mUrl;
	}
}
