package com.mobile.android.qtickets.event;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.mobile.android.qtickets.R;

import java.util.ArrayList;

/**
 * Created by Satish - 360 on 11/21/2016.
 */

public class QTEventDetailsAdapter extends RecyclerView.Adapter<QTEventDetailsAdapter.ViewHolder> implements View.OnClickListener, Spinner.OnItemSelectedListener {
	private Context mContext;
	private ArrayList<EventTicketDetails> eventsList;

	public QTEventDetailsAdapter(Context ctx, ArrayList<EventTicketDetails> listEvents) {
		mContext = ctx;
		eventsList = listEvents;
	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View v = LayoutInflater.from(mContext).inflate(R.layout.rvadapter_event_details, parent, false);
		ViewHolder mViewHolder = new ViewHolder(v, viewType);
		return mViewHolder;
	}

	@Override
	public void onBindViewHolder(final ViewHolder holder, final int position) {
		holder.tv_ticketType.setText(eventsList.get(position).TicketName);
		holder.tv_price.setText("QAR " + eventsList.get(position).TicketPrice);

		int nooftktpertrans = Integer.parseInt(eventsList.get(position).NoOfTicketsPerTransaction);
		ArrayList<String> altkts = new ArrayList<String>();
		for (int i = 1; i <= nooftktpertrans; i++) {
			altkts.add((i * Integer.parseInt(eventsList.get(position).Admit)) + "");
		}
		altkts.add(0, "Select");
		//setupEventPriceAdapters(holder, altkts);
		//holder.sp_eventDetails.setOnItemSelectedListener(this);

	}

	@Override
	public int getItemCount() {
		return eventsList.size();
	}

	private void setupEventPriceAdapters(ViewHolder holder, ArrayList<String> altkts) {
		// Creating adapter for spinner
		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, altkts);
		// Drop down layout style - list view with radio button
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		// attaching data adapter to spinner
		holder.sp_eventDetails.setAdapter(dataAdapter);
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		}

	}

	@Override
	public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
		String numOfSeats = adapterView.getItemAtPosition(i).toString();
//        Toast.makeText(mContext, "Seats Required : " + numOfSeats, Toast.LENGTH_SHORT).show();

	}

	@Override
	public void onNothingSelected(AdapterView<?> adapterView) {

	}

	public class ViewHolder extends RecyclerView.ViewHolder {
		private Spinner sp_eventDetails;
		private TextView tv_ticketType, tv_price;

		public ViewHolder(View itemView, int viewType) {
			super(itemView);

			//sp_eventDetails = (Spinner) itemView.findViewById(R.id.sp_seats_event_details);
			tv_ticketType = (TextView) itemView.findViewById(R.id.tv_ticketType);
			tv_price = (TextView) itemView.findViewById(R.id.tv_price_edetails);

		}

	}
}
