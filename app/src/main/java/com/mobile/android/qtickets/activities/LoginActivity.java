package com.mobile.android.qtickets.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.gson.Gson;
import com.mobile.android.qtickets.R;
import com.mobile.android.qtickets.adapters.NavigationDrawerAdapter;
import com.mobile.android.qtickets.constants.AppConstants;
import com.mobile.android.qtickets.model.UserDetailsModel;
import com.mobile.android.qtickets.network.InternetConnectionDetector;
import com.mobile.android.qtickets.parsers.LoginParseOperation;
import com.mobile.android.qtickets.parsers.RegisterParseOperation;
import com.mobile.android.qtickets.utils.QTUtils;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static com.mobile.android.qtickets.utils.QTUtils.getASCIIContentFromEntity;

/**
 * Created by Satish - 360 on 11/16/2016.
 */

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
	UserDetailsModel user;
	String username, password, GCMTOCKEN;
	SharedPreferences SM;
	private TextView tv_forgotpassword, tv_register,btn_login;
	private EditText et_username, et_password;
	//private Button btn_login;
	private ProgressDialog dialog = null;
	private String status, error_msg, errorcode;
	//private TextView mFacebookLoginButton;
	private LoginButton mFacebookLoginButton;
	private CallbackManager mFacebookCallbackManager;
	private String error_code;
	private ImageView backBtn;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);

		SM = getSharedPreferences("userrecord", 0);
		Boolean islogin = SM.getBoolean("userlogin", false);

		tv_forgotpassword = (TextView) findViewById(R.id.tv_forgotpassword);
		tv_register = (TextView) findViewById(R.id.tv_register);
		btn_login = (TextView) findViewById(R.id.btn_login);
		et_username = (EditText) findViewById(R.id.et_emailid_login);
		et_password = (EditText) findViewById(R.id.et_password_login);
		mFacebookLoginButton = (LoginButton) findViewById(R.id.button_facebookLogin);
		backBtn = (ImageView) findViewById(R.id.backBtn);
		backBtn.setOnClickListener(this);

	/*	String first = "If you're not yet a member, ";
		String next = "<font color='#0027c4'>Register with us</font>";
		tv_register.setText(Html.fromHtml(first + next));*/
		mFacebookLoginButton.setOnClickListener(this);
		btn_login.setOnClickListener(this);
		tv_forgotpassword.setOnClickListener(this);
		tv_register.setOnClickListener(this);

		//setupFacebookLogin();
	}

	private void setupFacebookLogin() {
		//mFacebookLoginButton.setReadPermissions("email", "pages_messaging_phone_number", "user_birthday", "public_profile");
		mFacebookLoginButton.setReadPermissions("email", "pages_messaging_phone_number", "public_profile");
		mFacebookCallbackManager = CallbackManager.Factory.create();
		mFacebookLoginButton.registerCallback(mFacebookCallbackManager, new FacebookCallback<LoginResult>() {
			@Override
			public void onSuccess(final LoginResult pLoginResult) {
				GraphRequest graphRequest = GraphRequest.newMeRequest(pLoginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
					@Override
					public void onCompleted(final JSONObject object, final GraphResponse response) {
						try {
						/*	String birthday = object.getString("birthday");
							if (birthday!=null&&birthday.split("/").length == 3) {
								final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyy", Locale.getDefault());
								final Date birthDate = simpleDateFormat.parse(birthday);
								simpleDateFormat.applyPattern("dd/MM/yyy");
								birthday = simpleDateFormat.format(birthDate);
							} else {
								birthday = "";
							}*/
							//final RegisterUrl registerUrl = new RegisterUrl(object.getString("first_name"), object.getString("last_name"), object.getString("email"), "", "", birthday, object.getString("gender"), object.getString("id"));
							final RegisterUrl registerUrl = new RegisterUrl(object.getString("first_name"), object.getString("last_name"), object.getString("email"), "", "", "", object.getString("gender"), object.getString("id"));
							registerUrl.execute();
						} catch (Exception pE) {
							pE.printStackTrace();
						}

					}
				});
				final Bundle bundle = new Bundle();
				bundle.putString("fields", "birthday, email, first_name, last_name, gender, locale, location");
				graphRequest.setParameters(bundle);
				graphRequest.executeAsync();
			}

			@Override
			public void onCancel() {

			}

			@Override
			public void onError(final FacebookException error) {

			}
		});
	}

	@Override
	protected void onStart() {
		super.onStart();
	}

	@Override
	protected void onStop() {
		super.onStop();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	@Override
	protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		mFacebookCallbackManager.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	private void initializeProgressDialog() {
		dialog = new ProgressDialog(this, R.style.progress_bar_style);
		dialog.getWindow().setGravity(Gravity.CENTER);
		WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
		dialog.getWindow().setAttributes(params);
		dialog.show();
		dialog.setCancelable(true);
		dialog.setCanceledOnTouchOutside(true);
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
			case R.id.btn_login:
				InternetConnectionDetector connectionDetector = new InternetConnectionDetector(this);
				if (connectionDetector.isConnectedToInternet()) {
					final String emailAddresStr = et_username.getText().toString();
					if (TextUtils.isEmpty(emailAddresStr)) {
						et_username.setError("Please enter email address");
					} else if (!emailAddresStr.matches(AppConstants.REGEX_EMAIL_ADDRESS)) {
						et_username.setError("Please enter a valid email address");
					} else if (et_password.getText().toString().length() == 0) {
						et_password.setError("Please enter password");
					} else {
						new LoginUrl().execute();
					}

				} else {
					new AlertDialog.Builder(LoginActivity.this).setTitle("Internet Connection Status").setMessage("No Internet Connection, Please try after sometime").setPositiveButton("OK", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
						}
					}).show();
				}
				break;

			case R.id.tv_forgotpassword:
				Intent forgotIntent = new Intent(LoginActivity.this, ForgotPasswordActivity.class);
				startActivity(forgotIntent);
				break;

			case R.id.tv_register:
				Intent registerIntent = new Intent(LoginActivity.this, RegisterActivity.class);
				startActivity(registerIntent);
				break;
			case R.id.backBtn:
				onBackPressed();
				finish();
				break;
			case R.id.button_facebookLogin:
				setupFacebookLogin();
				break;
		}
	}

	protected void parseUserLoginResponse(String result) {
		// TODO Auto-generated method stub
		try {
			//two lines of code for xmlpullparsing
			ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(result.getBytes());
			LoginParseOperation xmlpull = new LoginParseOperation(byteArrayInputStream);
			user = xmlpull.user;
			status = xmlpull.status;
			errorcode = xmlpull.errorCode;
			error_msg = xmlpull.errormsg;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	protected void parseUserRegistrationResponse(String result) {
		// TODO Auto-generated method stub
		Log.e("status.....", status + "");
		try {
			//two lines of code for xmlpullparsing
			ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(result.getBytes());
			RegisterParseOperation xmlpull = new RegisterParseOperation(byteArrayInputStream);
			status = xmlpull.status;
			error_code = xmlpull.errorCode;
			error_msg = xmlpull.errormsg;
			user = xmlpull.user;
			Log.e("status..............", status + "");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public class LoginUrl extends AsyncTask<Void, Void, Void> {
		@Override
		protected Void doInBackground(Void... arg0) {
			String resp;
			try {
				String login_url = AppConstants.SERVICE_URL_GET_USER_LOGIN + AppConstants.SERVICE_MSG_GET_USER_LOGIN_EMAIL +
						username + AppConstants.SERVICE_MSG_GET_USER_LOGIN_PASSWORD + password + AppConstants.SERVICE_MSG_GET_USER_LOGIN_SOURCE + "4" + AppConstants.SERVICE_MSG_GET_USER_LOGIN_TOCKEN + GCMTOCKEN;
				login_url = login_url.replaceAll("\\s+", "%20");
				HttpClient httpClient = new DefaultHttpClient();
				HttpContext localContext = new BasicHttpContext();
				HttpGet httpGet = new HttpGet(login_url);
				httpGet.addHeader(BasicScheme.authenticate(new UsernamePasswordCredentials(getResources().getString(R.string.auth_username), getResources().getString(R.string.auth_password)), "UTF-8", false));
				HttpResponse response = httpClient.execute(httpGet, localContext);
				HttpEntity entity = response.getEntity();
				resp = QTUtils.getASCIIContentFromEntity(entity);
				parseUserLoginResponse(resp);
			} catch (Exception e) {

			}
			return null;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			initializeProgressDialog();
			username = et_username.getText().toString().trim();
			password = et_password.getText().toString().trim();
		}


		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			dialog.dismiss();
			try {
				if (status.equalsIgnoreCase(AppConstants.RESPONSE_TRUE_TAG)) {
					SharedPreferences prefmPrefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
					SharedPreferences.Editor editor = prefmPrefs.edit();
					Gson gson = new Gson();
					String userdata = gson.toJson(user);
					editor.putString(AppConstants.SHARED_USER_TAG, userdata);
					editor.putString("login", "1");
					editor.commit();
					//QTUtils.getInstance().saveObjectToSharedPreference(LoginActivity.this,AppConstants.SHARED_USER_TAG,userdata);
					/*Intent loginIntent = new Intent(LoginActivity.this, DashboardActivity.class);
					startActivity(loginIntent);*/
					setResult(Activity.RESULT_OK);
					finish();
				} else if (status.equalsIgnoreCase(AppConstants.RESPONSE_FALSE_TAG)) {
					new AlertDialog.Builder(LoginActivity.this).setMessage(error_msg).setPositiveButton("OK", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {

						}
					}).show();
				}

			} catch (Exception e) {
				Toast.makeText(getApplicationContext(), "Server Not responding..", Toast.LENGTH_SHORT).show();
				e.printStackTrace();
			}

		}

	}

	public class RegisterUrl extends AsyncTask<Void, Void, Void> {
		private String firstName;
		private String lastName;
		private String emailId;
		private String mPassword = "";
		private String mConfirmPassword = "";
		private String phoneNumber;
		private String mPrefix = "";
		private String mNationality;
		private String dateOfBirth;
		private String mGender;
		private String fid;

		public RegisterUrl(final String pFirstName, final String pLastName, final String pEmailId, final String pPhoneNumber, final String pNationality, final String pDateOfBirth, final String pGender, final String pFid) {
			firstName = pFirstName;
			lastName = pLastName;
			emailId = pEmailId;
			phoneNumber = pPhoneNumber;
			mNationality = pNationality;
			dateOfBirth = pDateOfBirth;
			mGender = pGender;
			fid = pFid;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			initializeProgressDialog();
		}

		@Override
		protected Void doInBackground(Void... arg0) {
			String resp;
			try {
				String registration_url = AppConstants.SERVICE_URL_GET_USER_REGISTRATION + AppConstants.SERVICE_MSG_GET_USER_REGISTRATION_FIRST_NAME + firstName + AppConstants.SERVICE_MSG_GET_USER_REGISTRATION_LAST_NAME + lastName + AppConstants.SERVICE_MSG_GET_USER_REGISTRATION_PREFIX + mPrefix + AppConstants.SERVICE_MSG_GET_USER_REGISTRATION_PHONE + phoneNumber + AppConstants.SERVICE_MSG_GET_USER_REGISTRATION_EMAIL_ID + emailId + AppConstants.SERVICE_MSG_GET_USER_REGISTRATION_PASSWORD + mPassword + AppConstants.SERVICE_MSG_GET_USER_REGISTRATION__CONFIRM_PASSWORD + mConfirmPassword + AppConstants.SERVICE_MSG_GET_USER_REGISTRATION__FID + fid + AppConstants.SERVICE_MSG_GET_USER_REGISTRATION__NATIONNALITY + mNationality + AppConstants.SERVICE_MSG_GET_USER_REGISTRATION__DOB + dateOfBirth + AppConstants.SERVICE_MSG_GET_USER_REGISTRATION__GENDER + mGender + AppConstants.SERVICE_MSG_GET_USER_REGISTRATION_SOURCE + "4" + AppConstants.SERVICE_MSG_GET_USER_REGISTRATION_TOCKEN + GCMTOCKEN;
				registration_url = registration_url.replaceAll("\\s+", "%20");
				HttpClient httpClient = new DefaultHttpClient();
				HttpContext localContext = new BasicHttpContext();
				HttpGet httpGet = new HttpGet(registration_url);
				httpGet.addHeader(BasicScheme.authenticate(new UsernamePasswordCredentials(getResources().getString(R.string.auth_username), getResources().getString(R.string.auth_password)), "UTF-8", false));
				HttpResponse response = httpClient.execute(httpGet, localContext);
				HttpEntity entity = response.getEntity();
				resp = getASCIIContentFromEntity(entity);
				Log.e("url", registration_url);
				Log.e("response", resp + "");
				parseUserRegistrationResponse(resp);
			} catch (Exception e) {
				e.printStackTrace();

			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			dialog.dismiss();
			try {
				if (status.equalsIgnoreCase(AppConstants.RESPONSE_TRUE_TAG)) {
					SharedPreferences prefmPrefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
					SharedPreferences.Editor editor = prefmPrefs.edit();
					Gson gson = new Gson();
					String userdata = gson.toJson(user);
					editor.putString(AppConstants.SHARED_USER_TAG, userdata);
					editor.putString("login", "1");
					editor.commit();
					setResult(Activity.RESULT_OK);
					//NavigationDrawerAdapter navigationDrawerAdapter= new  NavigationDrawerAdapter();
					//navigationDrawerAdapter.notifyDataSetChanged();
					finish();
					//supportFinishAfterTransition();
				} else if (status.equalsIgnoreCase(AppConstants.RESPONSE_FALSE_TAG)) {
					new AlertDialog.Builder(LoginActivity.this).setMessage(error_msg).setPositiveButton("OK", null).show();
				}
			} catch (Exception e) {
				Toast.makeText(getApplicationContext(), "Server Not responding..", Toast.LENGTH_SHORT).show();
				e.printStackTrace();
			}
		}
	}
}
