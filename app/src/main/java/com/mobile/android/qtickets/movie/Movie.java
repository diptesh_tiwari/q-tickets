package com.mobile.android.qtickets.movie;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.ArrayList;

/**
 * Created by Satish - 360 on 11/28/2016.
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Movie implements Parcelable {

	public static final Creator<Movie> CREATOR = new Creator<Movie>() {

		public Movie createFromParcel(Parcel in) {
			return new Movie(in);
		}

		public Movie[] newArray(int size) {
			return new Movie[size];
		}

	};
	public int localMovieId;
	public String serverId, movieName, releaseDate, thumbnailURL, language, duration,
			censor, description, thumbnail, iphonethumb, ipadthumb, bannerurl,synopsis,id, willnotwatch, willwatch;
	public String movieType, IMDB_rating;
	public String bg_color_code, bg_border_code, btn_color, title_color_code;
	public String CastAndCrew, TrailerURL, Movieurl, AgeRestrictRating;
	public ArrayList<TheatreVO> movieTheatresArr;

	public Movie() {
		// TODO Auto-generated constructor stub
//		movieTheatresArr = new ArrayList<MovieTheatreVO>();

	}

	public Movie(Parcel in) {
		// TODO Auto-generated constructor stub
		readFromParcel(in);
	}

	private void readFromParcel(Parcel in) {
		movieTheatresArr = new ArrayList<TheatreVO>();
		localMovieId = in.readInt();
		serverId = in.readString();
		willwatch = in.readString();
		willnotwatch = in.readString();
		id = in.readString();
		movieName = in.readString();
		releaseDate = in.readString();
		thumbnailURL = in.readString();
		language = in.readString();
		duration = in.readString();
		censor = in.readString();
		description = in.readString();
		synopsis = in.readString();
		movieType = in.readString();
		bg_color_code = in.readString();
		bg_border_code = in.readString();
		btn_color = in.readString();
		title_color_code = in.readString();
		CastAndCrew = in.readString();
		TrailerURL = in.readString();
		IMDB_rating = in.readString();
		thumbnail = in.readString();
		iphonethumb = in.readString();
		ipadthumb = in.readString();
		Movieurl = in.readString();
		bannerurl = in.readString();
		AgeRestrictRating = in.readString();
		in.readList(movieTheatresArr, getClass().getClassLoader());
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		dest.writeInt(localMovieId);
		dest.writeString(serverId);
		dest.writeString(willnotwatch);
		dest.writeString(willwatch);
		dest.writeString(id);
		dest.writeString(movieName);
		dest.writeString(releaseDate);
		dest.writeString(thumbnailURL);
		dest.writeString(language);
		dest.writeString(duration);
		dest.writeString(censor);
		dest.writeString(description);
		dest.writeString(synopsis);
		dest.writeString(movieType);
		dest.writeString(bg_color_code);
		dest.writeString(bg_border_code);
		dest.writeString(btn_color);
		dest.writeString(title_color_code);
		dest.writeString(CastAndCrew);
		dest.writeString(TrailerURL);
		dest.writeString(IMDB_rating);
		dest.writeString(thumbnail);
		dest.writeString(iphonethumb);
		dest.writeString(ipadthumb);
		dest.writeString(Movieurl);
		dest.writeString(bannerurl);
		dest.writeString(AgeRestrictRating);
		dest.writeList(movieTheatresArr);

	}

}
