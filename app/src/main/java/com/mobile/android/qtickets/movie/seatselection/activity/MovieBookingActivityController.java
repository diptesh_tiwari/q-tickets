package com.mobile.android.qtickets.movie.seatselection.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.mobile.android.qtickets.R;
import com.mobile.android.qtickets.activities.LoginActivity;
import com.mobile.android.qtickets.constants.AppConstants;
import com.mobile.android.qtickets.interfes.OnEventCallBack;
import com.mobile.android.qtickets.model.UserDetailsModel;

import java.util.Locale;

/**
 * Created by hexalabssd on 20/03/17.
 */

abstract class MovieBookingActivityController extends AppCompatActivity implements View.OnClickListener,OnEventCallBack {

	private static final int CONTAINER_ID = R.id.frameLayout_loginPayment;
	private TextView mEventTitle,buttonLoginTV;
	private TextView mLocation;
	private TextView mDate;
	//private TextView mTime;
	private TextView mNumberOfPersons,textView_eventNoseat;
	private TextView mTotalCost;
	private PaymentDetailsListener mPaymentDetailsListener;
	String TAG="EventBookingActivityController";
	public ImageView backBtn;
	private String currencyType;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);


		SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(this);
		currencyType = mPrefs.getString(AppConstants.COUNTRY_TYPE_CURRENCY, null);


		setContentView(R.layout.activity_event_booking);

		mEventTitle = (TextView) findViewById(R.id.textView_eventTitle);
		mLocation = (TextView) findViewById(R.id.textView_eventLocation);
		mDate = (TextView) findViewById(R.id.textView_eventDate);
		//mTime = (TextView) findViewById(R.id.textView_eventTime);
		mNumberOfPersons = (TextView) findViewById(R.id.textView_eventNoOfPersons);
		textView_eventNoseat = (TextView) findViewById(R.id.textView_eventNoseat);
		mTotalCost = (TextView) findViewById(R.id.textView_totalCost);
		buttonLoginTV = (TextView) findViewById(R.id.buttonLoginTV);
		backBtn = (ImageView) findViewById(R.id.backBtn);
		buttonLoginTV.setOnClickListener(this);
		backBtn.setOnClickListener(this);
	}

	/*	@Override
	protected void initContentView() {

	}

	@Override
	protected void initContentViewListeners() {

	}

	@Override
	protected void initActionBar() {
		final ActionBar actionBar = getSupportActionBar();
		if (actionBar != null) {
			actionBar.setDisplayHomeAsUpEnabled(true);
			actionBar.setHomeAsUpIndicator(R.drawable.back_btn);
		}
	}

	@Override
	protected void initNavigationDrawer() {

	}*/

	@Override
	public boolean onSupportNavigateUp() {
		onBackPressed();
		return super.onSupportNavigateUp();
	}

	protected void setEventTitle(final String pEventTitle) {
		mEventTitle.setText(pEventTitle);
	}

	protected void setLocation(final String pEventLocation) {
		mLocation.setText(pEventLocation);
	}

	protected void setDate(String mMovieDate, final String pTime) {
		mDate.setText(mMovieDate+",  "+pTime);
	}

	protected void setTime(final String pTime) {

        //mTime.setText(pTime);
	}

	protected void setNumberOfPersons(final Integer pNumberOfPersons) {
		mNumberOfPersons.setText(String.format(Locale.getDefault(), "Number of %s: %d", getResources().getQuantityString(R.plurals.person, pNumberOfPersons), pNumberOfPersons));
	}

	protected void setSeat(String seats) {
		textView_eventNoseat.setText("Seat:"+seats);
	}

	protected void setTotalCost(final double pTotalCost) {
		mTotalCost.setText(String.format(Locale.getDefault(), " %.0f  "+currencyType, pTotalCost));
	}

	/*protected void loadLoginView() {
		final FragmentManager fragmentManager = getSupportFragmentManager();
		final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		final BookingLoginFragment bookingLoginFragment = BookingLoginFragment.newInstance();
		bookingLoginFragment.setOnProceedForPaymentListener(new BookingLoginFragment.OnProceedForPaymentListener() {
			@Override
			public void onClickProceedPaymentButton(@Nullable final UserDetailsModel pUserDetailsModel) {
				if (mPaymentDetailsListener != null) {
					mPaymentDetailsListener.onClickProceedPaymentButton(pUserDetailsModel);
				}
			}
		});

		fragmentTransaction.replace(CONTAINER_ID, bookingLoginFragment, BookingLoginFragment.TAG);
		fragmentTransaction.commit();
	}*/

	protected void loadPaymentView(@Nullable UserDetailsModel pUserDetailsModel) {
/*		final FragmentManager fragmentManager = getSupportFragmentManager();
		final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		final MoviePaymentActivity paymentFragment = MoviePaymentActivity.newInstance(pUserDetailsModel);
		paymentFragment.setOnPayNowListener(new MoviePaymentActivity.OnPayNowListener() {
			@Override
			public void onClickPayNowButton(final UserDetailsModel pUserDetailsModel, final int pPaymentType) {
				if (mPaymentDetailsListener != null) {
					mPaymentDetailsListener.onClickPayNowButton(pUserDetailsModel, pPaymentType);
				}
			}
		});
		fragmentTransaction.replace(CONTAINER_ID, paymentFragment, MoviePaymentActivity.TAG);
		fragmentTransaction.commit();*/
	}

	protected void setPaymentDetailsListener(final PaymentDetailsListener pPaymentDetailsListener) {
		mPaymentDetailsListener = pPaymentDetailsListener;
	}


	protected interface PaymentDetailsListener {
		void onClickProceedPaymentButton(@Nullable final UserDetailsModel pUserDetailsModel);

		void onClickPayNowButton(final UserDetailsModel pUserDetailsModel, final int pPaymentType);
	}

	@Override
	public void onEvent(int id) {
		if(id==R.id.loginTV){
			startActivity();
		}else if(R.id.singnUpTV==id){
			startActivity();
		}else if(R.id.singnUpTV==id){
			startActivity();
		}

	}

	private  void startActivity() {
		Intent intent= new Intent(this, LoginActivity.class);
		startActivityForResult(intent,AppConstants.LOGIN);

	}

	@Override
	public void onEvent(int id, Object object) {

	}


	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode== Activity.RESULT_OK){
			if (requestCode==AppConstants.LOGIN){
				Toast.makeText(this,"Success",Toast.LENGTH_SHORT).show();
			}

		}
	}
}
