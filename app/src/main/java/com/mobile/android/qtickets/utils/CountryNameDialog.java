package com.mobile.android.qtickets.utils;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;

import com.mobile.android.qtickets.R;

import java.util.List;

/**
 * Created by hbb20 on 11/1/16.
 */
class CountryNameDialog {
    public static void CountryNameDialog(CountryNamePicker countryNamePicker) {
        Context context = countryNamePicker.getContext();
        final Dialog dialog = new Dialog(context);
        countryNamePicker.refreshCustomMasterList();
        countryNamePicker.refreshPreferredCountries();
        List<CountryName> masterCountries = CountryName.getCustomMasterCountryList(countryNamePicker);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setContentView(R.layout.layout_picker_name_dialog);
        RecyclerView recyclerView_countryDialog = (RecyclerView) dialog.findViewById(R.id.recycler_countryDialog);

        final TextView textViewTitle = (TextView) dialog.findViewById(R.id.textView_title);
        textViewTitle.setText(countryNamePicker.getDialogTitle());
        final EditText editText_search = (EditText) dialog.findViewById(R.id.editText_search);
        editText_search.setHint(countryNamePicker.getSearchHintText());
        TextView textView_noResult = (TextView) dialog.findViewById(R.id.textView_noresult);
        textView_noResult.setText(countryNamePicker.getNoResultFoundText());
        final CountryNameAdapter cca = new CountryNameAdapter(context, masterCountries, countryNamePicker, editText_search, textView_noResult, dialog);
        recyclerView_countryDialog.setLayoutManager(new LinearLayoutManager(context));
        recyclerView_countryDialog.setAdapter(cca);
        dialog.show();
    }
}