package com.mobile.android.qtickets.CustomFonts;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.RadioButton;
import android.widget.TextView;

/**
 * Created by Android on 4/20/17.
 */

public class RadioButtonRegular extends RadioButton {
    public RadioButtonRegular(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/SSPro_Regular.otf"));
    }
}