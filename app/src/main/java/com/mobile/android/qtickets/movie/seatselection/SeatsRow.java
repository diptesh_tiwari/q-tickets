package com.mobile.android.qtickets.movie.seatselection;

import java.io.Serializable;
import java.util.List;

/**
 * Created by hexalabssd on 24/02/17.
 */

public final class SeatsRow  {
	private final String mLetter;

	public Double getmCost() {
		return mCost;
	}

	public void setmCost(Double mCost) {
		this.mCost = mCost;
	}

	private Double mCost;
	private int mNumberOfSeats;
	private int mAvailableCount;
	private List<String> mAvailableSeats;
	private boolean mIsFamily;
	private List<String> mGangwaySeats;
	private int[] mGangwayCounts;
	private List<String> mAllSeats;
	private boolean mIsGangway;
	private boolean mIsInitialGangway;
	private int mIsInitialGangwayCount;




	public SeatsRow(final String pLetter) {

		mLetter = pLetter;
	}

	public int getNumberOfSeats() {
		return mNumberOfSeats;
	}

	public void setNumberOfSeats(final int pNumberOfSeats) {
		mNumberOfSeats = pNumberOfSeats;
	}

	public int getAvailableCount() {
		return mAvailableCount;
	}

	public void setAvailableCount(final int pAvailableCount) {
		mAvailableCount = pAvailableCount;
	}

	public List<String> getAvailableSeats() {
		return mAvailableSeats;
	}

	public void setAvailableSeats(final List<String> pAvailableSeats) {
		mAvailableSeats = pAvailableSeats;
	}

	public boolean isFamily() {
		return mIsFamily;
	}

	public void setFamily(final boolean pFamily) {
		mIsFamily = pFamily;
	}

	public List<String> getGangwaySeats() {
		return mGangwaySeats;
	}

	public void setGangwaySeats(final List<String> pGangwaySeats) {

		mGangwaySeats = pGangwaySeats;
	}

	public int[] getGangwayCounts() {
		return mGangwayCounts;
	}

	public void setGangwayCounts(final int[] pGangwayCounts) {
		mGangwayCounts = pGangwayCounts;
	}

	public List<String> getAllSeats() {
		return mAllSeats;
	}

	public void setAllSeats(final List<String> pAllSeats) {
		mAllSeats = pAllSeats;
	}

	public boolean isGangway() {
		return mIsGangway;
	}

	public void setIsGangway(final boolean pGangway) {
		mIsGangway = pGangway;
	}

	public boolean isInitialGangway() {
		return mIsInitialGangway;
	}

	public void setInitialGangway(final boolean pInitialGangway) {
		mIsInitialGangway = pInitialGangway;
	}

	public int getIsInitialGangwayCount() {
		return mIsInitialGangwayCount;
	}

	public void setIsInitialGangwayCount(final int pIsInitialGangwayCount) {
		mIsInitialGangwayCount = pIsInitialGangwayCount;
	}

	public String getLetter() {

		return mLetter;
	}
}
