package com.mobile.android.qtickets.activities;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mobile.android.qtickets.R;
import com.mobile.android.qtickets.constants.AppConstants;
import com.mobile.android.qtickets.model.CountriesModel;
import com.mobile.android.qtickets.network.InternetConnectionDetector;
import com.mobile.android.qtickets.parsers.CountryParseOperation;
import com.mobile.android.qtickets.utils.QTUtils;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Satish - 360 on 12/14/2016.
 */

public class QTBlockSeatsActivity extends AppCompatActivity implements View.OnClickListener, Spinner.OnItemSelectedListener {
	InternetConnectionDetector connectionDetector;
	ArrayList<CountriesModel> countryArr = new ArrayList<CountriesModel>();
	ArrayList<String> allCountryNationality = new ArrayList<String>();
	private Spinner sp_nationality;
	private ProgressDialog dialog;
	private ImageView iv_back;
	private EditText et_email, et_name, et_phone;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_block_seats);

		connectionDetector = new InternetConnectionDetector(this);

		initViews();

	}

	private void initViews() {
		sp_nationality = (Spinner) findViewById(R.id.spn_prefix_book_tickets);
		iv_back = (ImageView) findViewById(R.id.iv_back_block_tickets);

		iv_back.setOnClickListener(this);
		sp_nationality.setOnItemSelectedListener(this);

		final Handler handler = new Handler();
		handler.postDelayed(new Runnable() {
			@Override
			public void run() {
				if (connectionDetector.isConnectedToInternet()) {
					// Do something after 2s = 2000ms
					new CountryListUrl().execute();
				}
			}
		}, 300);
	}

	private void initializeProgressDialog() {
		dialog = new ProgressDialog(this, R.style.progress_bar_style);
		dialog.getWindow().setGravity(Gravity.TOP);
		WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
		params.y = 200;
		dialog.getWindow().setAttributes(params);
		dialog.show();
		dialog.setCancelable(true);
		dialog.setCanceledOnTouchOutside(true);
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
			case R.id.iv_back_block_tickets:
				onBackPressed();
				break;
		}
	}

	@Override
	public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

	}

	@Override
	public void onNothingSelected(AdapterView<?> adapterView) {

	}

	private void initNationalitySpinner() {

		SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		mPrefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		Gson gson = new Gson();
		String country = mPrefs.getString(AppConstants.SHARED_COUNTRIES_TAG, null);
		if (country != null) {
			java.lang.reflect.Type type = new TypeToken<List<CountriesModel>>() {
			}.getType();
			countryArr = gson.fromJson(country, type);
			if (countryArr.size() != 0) {
				bindSpinners();
			}
		}

	}

	private void bindSpinners() {
		for (int i = 0; i < countryArr.size(); i++) {
			allCountryNationality.add("+" + countryArr.get(i).Countryprefix + " " + countryArr.get(i).CountryNationality);
		}

		setupNationalityAdapter(allCountryNationality);
		Log.v("Countries :", allCountryNationality.toString());

	}

	private void setupNationalityAdapter(ArrayList<String> allNationality) {
		// Creating adapter for spinner
		ArrayAdapter<String> countryAdapter = new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_spinner_item, allNationality);
		// Drop down layout style - list view with radio button
		countryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		// attaching data adapter to spinner
		sp_nationality.setAdapter(countryAdapter);
	}

	protected ArrayList<CountriesModel> parseCountryResponse(String result) {
		// TODO Auto-generated method stub
		ArrayList<CountriesModel> conty = null;
		try {
			//two lines of code for xmlpullparsing
			ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(result.getBytes());
			CountryParseOperation xmlpull = new CountryParseOperation(byteArrayInputStream);
			conty = xmlpull.countryArr;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return conty;
	}

	public class CountryListUrl extends AsyncTask<Void, Void, Void> {
		@Override
		protected Void doInBackground(Void... arg0) {
			try {
				String resp;
				try {
					String country_url = AppConstants.SERVER_URL_GET_ALL_COUNTRIES;
					HttpClient httpClient = new DefaultHttpClient();
					HttpContext localContext = new BasicHttpContext();
					HttpGet httpGet = new HttpGet(country_url);
					httpGet.addHeader(BasicScheme.authenticate(new UsernamePasswordCredentials(getResources().getString(R.string.auth_username), getResources().getString(R.string.auth_password)), "UTF-8", false));
					HttpResponse response = httpClient.execute(httpGet, localContext);
					HttpEntity entity = response.getEntity();
					resp = QTUtils.getASCIIContentFromEntity(entity);
					countryArr = parseCountryResponse(resp);
				} catch (Exception e) {
					e.printStackTrace();
				}
			} catch (Exception e) {

			}
			return null;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			initializeProgressDialog();

		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			dialog.dismiss();
			try {
				if (countryArr.size() > 0) {
					SharedPreferences prefmPrefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
					SharedPreferences.Editor editor = prefmPrefs.edit();
					Gson gson = new Gson();
					String countryjson = gson.toJson(countryArr);
					editor.putString(AppConstants.SHARED_COUNTRIES_TAG, countryjson);
					editor.commit();

				} else {
					countryArr.clear();
					new CountryListUrl().execute();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			initNationalitySpinner();
		}

	}
}
