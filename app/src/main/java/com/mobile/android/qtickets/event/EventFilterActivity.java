package com.mobile.android.qtickets.event;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mobile.android.qtickets.R;
import com.mobile.android.qtickets.constants.AppConstants;
import com.mobile.android.qtickets.utils.DateTime;
import com.mobile.android.qtickets.utils.QTUtils;
import com.mobile.android.qtickets.utils.RangeSeekBar;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by Indglobal on 4/22/2017.
 */

public class EventFilterActivity extends AppCompatActivity implements View.OnClickListener {

    int preMin = -1;
    int preMax = -1;
    private TextView minRs, maxRs, tvStartDate, tvEndDate, tvApplyFilter;
    ImageView ivStartDate, ivEndDate, backBtn;
    private RangeSeekBar<Integer> seekBar;
    private int slctminPrice, slctmaxPrice;
    private RadioGroup dateType;
    private String startDate, endDate;
    private String todayDateDefault, endDateDefault;
    private int startPrice = 1, endPrice = 5000;
    private String currencyType;
    private String toDayDate, toMorrowDay, weekendDayStrDay, weekendDaySunday;
    List<String> eventTicketPrices= new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.event_filter);

        Intent ii = getIntent();
        startDate = ii.getStringExtra("startDate");
        endDate = ii.getStringExtra("endDate");
        slctminPrice = Integer.parseInt(ii.getStringExtra("startPrice"));
        slctmaxPrice = Integer.parseInt(ii.getStringExtra("endPrice"));

        dateType = (RadioGroup) findViewById(R.id.rgBookedDays);
        minRs = (TextView) findViewById(R.id.tvMinRs);
        maxRs = (TextView) findViewById(R.id.tvMaxRs);
        tvStartDate = (TextView) findViewById(R.id.tvStartDate);
        tvEndDate = (TextView) findViewById(R.id.tvEndDate);
        ivStartDate = (ImageView) findViewById(R.id.ivStartDate);
        ivEndDate = (ImageView) findViewById(R.id.ivEndDate);
        tvApplyFilter = (TextView) findViewById(R.id.tvApplyFilter);
        backBtn = (ImageView) findViewById(R.id.backBtn);


        SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        currencyType = mPrefs.getString(AppConstants.COUNTRY_TYPE_CURRENCY, null);

        Gson gson = new Gson();
        String eventsDetailsPrices = mPrefs.getString(AppConstants.SHARED_EVENTS_TICKET_PRICES, "");
        if (!eventsDetailsPrices.equalsIgnoreCase("")) {
            eventTicketPrices.clear();

        }







        dateType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                Calendar c = null;
                SimpleDateFormat sdf = null;
                switch (checkedId) {

                    case R.id.rbAll:
                        tvStartDate.setText("Select start date");
                        tvEndDate.setText("Select end date");

                        break;

                    case R.id.rbToday:
                        toDayDate = DateTime.getToday();
                        tvStartDate.setText(toDayDate);
                        tvEndDate.setText(toDayDate);
                        break;

                    case R.id.rbTomorow:
                        c = Calendar.getInstance();
                        sdf = new SimpleDateFormat("yyyy-MM-dd");
                        Date tomorrowDate = c.getTime();
                        c.setTime(tomorrowDate);
                        c.add(Calendar.DATE, 1);


                        try {
                            tomorrowDate = sdf.parse(sdf.format(c.getTime()));
                            toMorrowDay = DateTime.getDate(tomorrowDate);

                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        tvStartDate.setText(toMorrowDay);
                        tvEndDate.setText(toMorrowDay);


                        break;
                    case R.id.rbWeekend:
                        c = Calendar.getInstance();
                        c.set(Calendar.DAY_OF_WEEK, Calendar.FRIDAY);

                        sdf = new SimpleDateFormat("yyyy-MM-dd");
                        Date weekendDate = null;

                        try {
                            weekendDate = sdf.parse(sdf.format(c.getTime()));

                            weekendDayStrDay = DateTime.getDate(weekendDate);

                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        Log.d("", weekendDayStrDay);

                        c.setTime(weekendDate);
                        c.add(Calendar.DATE, 1);
                        //weekendDate = c.getTime();

                        try {
                            weekendDate = sdf.parse(sdf.format(c.getTime()));
                            weekendDaySunday = DateTime.getDate(weekendDate);

                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        tvStartDate.setText(weekendDayStrDay);
                        tvEndDate.setText(weekendDaySunday);

                        break;
                }
            }
        });


        onSeekExecute(1, 5000);

        if (slctminPrice == 0) {
            seekBar.setSelectedMinValue(1);
            minRs.setText("Min " + currencyType + " ." + 1);
        } else {
            seekBar.setSelectedMinValue(slctminPrice);
            minRs.setText("Min " + currencyType + " ." + slctminPrice);
        }

        if (slctmaxPrice == 0) {
            seekBar.setSelectedMaxValue(5000);
            maxRs.setText("Max " + currencyType + " ." + 5000);
        } else {
            seekBar.setSelectedMaxValue(slctmaxPrice);
            maxRs.setText("Max " + currencyType + " ." + slctmaxPrice);
        }

        if (!startDate.equalsIgnoreCase("")) {
            tvStartDate.setText(startDate);
        }

        if (!endDate.equalsIgnoreCase("")) {
            tvEndDate.setText(endDate);
        }

        ivStartDate.setOnClickListener(this);
        ivEndDate.setOnClickListener(this);
        tvApplyFilter.setOnClickListener(this);
        backBtn.setOnClickListener(this);


        Date today = new Date(System.currentTimeMillis());
        todayDateDefault = new SimpleDateFormat("yyyy-MM-dd").format(today);


        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.YEAR, 5); // to get previous year add -1
        Date endYear = cal.getTime();
        endDateDefault = new SimpleDateFormat("yyyy-MM-dd").format(endYear);


        // Log.d(todayDate.toString(),endDate.toString());

    }

    private void onSeekExecute(int minValue, int maxValue) {
        seekBar = new RangeSeekBar<>(minValue, maxValue, EventFilterActivity.this);

        seekBar.setOnRangeSeekBarChangeListener(new RangeSeekBar.OnRangeSeekBarChangeListener<Integer>() {
            @Override
            public void onRangeSeekBarValuesChanged(RangeSeekBar<?> bar, Integer minValue, Integer maxValue) {
                // handle changed range values

                int diff = maxValue - minValue;
                minRs.setVisibility(View.VISIBLE);
                maxRs.setVisibility(View.VISIBLE);
                minRs.setText("Min " + currencyType + " ." + minValue);
                maxRs.setText("Max " + currencyType + " ." + maxValue);
                startPrice = minValue;
                endPrice = maxValue;
      /*          if (diff == 4 || diff < 10) {
                    bar.setEnabled(false);
                    if (minValue != preMin) {
                        seekBar.setSelectedMinValue(preMin);
                    } else if (maxValue != preMax) {
                        seekBar.setSelectedMaxValue(preMax);
                    }
                    AlertDialog.Builder alert = new AlertDialog.Builder(EventFilterActivity.this);
                    alert.setNegativeButton(getResources().getString(android.R.string.ok), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            seekBar.setEnabled(true);
                        }
                    });
                    alert.setCancelable(false);
                    alert.setMessage(Html.fromHtml("Range must be between 10 ") + currencyType).show();

                    // bar.setEnabled(false);
                } else {
                    preMin = minValue;
                    preMax = maxValue;
                }*/

            }
        });
        // add RangeSeekBar to pre-defined layout
        ViewGroup layout = (ViewGroup) EventFilterActivity.this.findViewById(R.id.seeklayout);
        layout.addView(seekBar);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        switch (id) {
            case R.id.ivStartDate:
                openCalander("1", startDate);
                break;

            case R.id.ivEndDate:
                openCalander("2", endDate);
                break;

            case R.id.tvApplyFilter:
                Intent output = new Intent();
                output.putExtra("startPrice", startPrice + "");
                output.putExtra("endPrice", endPrice + "");
                if (!TextUtils.isEmpty(tvStartDate.getText().toString())) {
                    output.putExtra("startDate", tvStartDate.getText().toString());
                } else {
                    output.putExtra("startDate", todayDateDefault);
                }
                if (!TextUtils.isEmpty(tvEndDate.getText().toString())) {
                    output.putExtra("endDate", tvEndDate.getText().toString());
                } else {
                    output.putExtra("endDate", endDateDefault);
                }
                setResult(RESULT_OK, output);
                finish();
                break;

            case R.id.backBtn:
                onBackPressed();
                break;
        }
    }

    private void openCalander(final String fromto, String date) {
        final Calendar mcurrentDate = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        if (!date.equalsIgnoreCase("")) {
            try {
                Date date1 = sdf.parse(date);
                mcurrentDate.setTime(date1);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        int mYear = mcurrentDate.get(Calendar.YEAR);
        int mMonth = mcurrentDate.get(Calendar.MONTH);
        int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(
                EventFilterActivity.this, new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                mcurrentDate.set(Calendar.YEAR, year);
                mcurrentDate.set(Calendar.MONTH, monthOfYear);
                mcurrentDate.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                String myFormat = "yyyy-MM-dd";
                String showDateFormt = "dd/MM/yyyy";
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                String selectDate = sdf.format(mcurrentDate.getTime());

                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(showDateFormt, Locale.US);
                String showDate = simpleDateFormat.format(mcurrentDate.getTime());

                if (fromto.equalsIgnoreCase("1")) {
                    tvStartDate.setText(selectDate);
                    startDate = selectDate;

                } else {
                    tvEndDate.setText(selectDate);
                    endDate = selectDate;
                }

            }
        }, mYear, mMonth, mDay);


        datePickerDialog.show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}