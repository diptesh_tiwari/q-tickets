package com.mobile.android.qtickets.movie;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;
import com.mobile.android.qtickets.R;
import com.mobile.android.qtickets.activities.QTBlockSeatsActivity;
import com.mobile.android.qtickets.constants.AppConstants;
import com.mobile.android.qtickets.model.BookingClassVO;
import com.mobile.android.qtickets.model.BookingRowVO;
import com.mobile.android.qtickets.model.BookingSeatVO;
import com.mobile.android.qtickets.model.SeatLayoutVO;
import com.mobile.android.qtickets.parsers.SeatLayoutParseOperation;
import com.mobile.android.qtickets.utils.QTUtils;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Satish - 360 on 12/5/2016.
 */

public class SelectSeatsActivity extends AppCompatActivity implements View.OnClickListener, Spinner.OnItemSelectedListener {

	ArrayList<SeatLayoutVO> theaterlayoutArr;
	String showTimeId;
	ImageView iv_screen;
	TableLayout grid;
	boolean isPressed = false;
	String seatsSelected;
	String seatCount;
	int total, count = 0;
	float cost = 0;
	private TextView tv_MovieTitle, tv_TheatreName, tv_MovieDate, tv_MovieTime;
	private Spinner sp_seatsMovie;
	private ProgressDialog dialog;
	private Button btn_bookNow;
	/**
	 * ATTENTION: This was auto-generated to implement the App Indexing API. See
	 * https://g.co/AppIndexing/AndroidStudio for more information.
	 */
	private GoogleApiClient client;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_select_seats);

		initViews();
		setupActionbar();
		// ATTENTION: This was auto-generated to implement the App Indexing API.
		// See https://g.co/AppIndexing/AndroidStudio for more information.
		client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
	}

	private void initViews() {

		Intent myInt = getIntent();
		String movieName = myInt.getStringExtra("movieName");
		String movieTheatre = myInt.getStringExtra("theatreName");
		String movieDate = myInt.getStringExtra("movieDate");
		String movieTime = myInt.getStringExtra("movieTime");
		showTimeId = myInt.getStringExtra("movieShowTimeId");

		Log.v("ShowTimeID", showTimeId);

		sp_seatsMovie = (Spinner) findViewById(R.id.spn_seats_movie);
		grid = (TableLayout) findViewById(R.id.grid);
		grid.setOnClickListener(this);
		sp_seatsMovie.setOnItemSelectedListener(this);
		btn_bookNow = (Button) findViewById(R.id.btn_book_now);
		btn_bookNow.setOnClickListener(this);

		initTicketsSpinner();

		tv_MovieTitle = (TextView) findViewById(R.id.tv_movie_title);
		tv_TheatreName = (TextView) findViewById(R.id.tv_theatre_name);
		tv_MovieDate = (TextView) findViewById(R.id.tv_movie_date);
		tv_MovieTime = (TextView) findViewById(R.id.tv_movie_time);
//		iv_back = (ImageView) findViewById(R.id.iv_back_book_tickets);
		iv_screen = (ImageView) findViewById(R.id.iv_screen);

//		iv_back.setOnClickListener(this);

		tv_MovieTitle.setText(movieName);
		tv_TheatreName.setText(movieTheatre);
		tv_MovieDate.setText(movieDate);
		tv_MovieTime.setText(movieTime.substring(0, 8));

	}

	private void initTicketsSpinner() {

		ArrayList<String> seatsTicket = new ArrayList<String>();
		seatsTicket.add("Select Number of seats");
		seatsTicket.add("01 seat");
		seatsTicket.add("02 seats");
		seatsTicket.add("03 seats");
		seatsTicket.add("04 seats");
		seatsTicket.add("05 seats");
		seatsTicket.add("06 seats");
		seatsTicket.add("07 seats");
		seatsTicket.add("08 seats");
		seatsTicket.add("09 seats");
		seatsTicket.add("10 seats");
		seatsTicket.add("11 seats");
		seatsTicket.add("12 seats");


		// Creating adapter for spinner
		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, R.layout.spin_seats_item, seatsTicket);
		// Drop down layout style - list view with radio button
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		// attaching data adapter to spinner
		sp_seatsMovie.setAdapter(dataAdapter);
	}

	private void setupActionbar() {
		final ActionBar actionBar = getSupportActionBar();
		if (actionBar != null) {
			actionBar.setDisplayHomeAsUpEnabled(true);
			actionBar.setHomeAsUpIndicator(R.drawable.back_btn);
		}
	}

	@Override
	public void onStart() {
		super.onStart();

		// ATTENTION: This was auto-generated to implement the App Indexing API.
		// See https://g.co/AppIndexing/AndroidStudio for more information.
		client.connect();
		AppIndex.AppIndexApi.start(client, getIndexApiAction());
	}

	/**
	 * ATTENTION: This was auto-generated to implement the App Indexing API. See
	 * https://g.co/AppIndexing/AndroidStudio for more information.
	 */
	public Action getIndexApiAction() {
		Thing object = new Thing.Builder().setName("QTBookMovieTickets Page") // TODO: Define a title for the content shown.
				// TODO: Make sure this auto-generated URL is correct.
				.setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]")).build();
		return new Action.Builder(Action.TYPE_VIEW).setObject(object).setActionStatus(Action.STATUS_TYPE_COMPLETED).build();
	}

	@Override
	public void onStop() {
		super.onStop();

		// ATTENTION: This was auto-generated to implement the App Indexing API.
		// See https://g.co/AppIndexing/AndroidStudio for more information.
		AppIndex.AppIndexApi.end(client, getIndexApiAction());
		client.disconnect();
	}

	@Override
	public boolean onSupportNavigateUp() {
		onBackPressed();
		return super.onSupportNavigateUp();
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
			case R.id.btn_book_now:

				total = (int) (count * cost);

				Toast.makeText(SelectSeatsActivity.this, total, Toast.LENGTH_SHORT).show();
				Intent myInt = new Intent(SelectSeatsActivity.this, QTBlockSeatsActivity.class);
				startActivity(myInt);

				break;
		}

	}

	@Override
	public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
		seatsSelected = adapterView.getItemAtPosition(i).toString();
		if (seatsSelected != "Select Number of seats") {
			grid.setVisibility(View.VISIBLE);
			grid.removeAllViews();
			final Handler handler = new Handler();
			handler.postDelayed(new Runnable() {
				@Override
				public void run() {
					new BackGourndTask_GetSaetlayout().execute();
				}
			}, 300);
		} else {
			Toast.makeText(getApplicationContext(), "Please select seats", Toast.LENGTH_SHORT).show();
			grid.setVisibility(View.GONE);
			if (theaterlayoutArr != null) {
				theaterlayoutArr.clear();
			}
		}
	}

	@Override
	public void onNothingSelected(AdapterView<?> adapterView) {

	}

	private void initializeProgressDialog() {
		dialog = new ProgressDialog(this, R.style.progress_bar_style);
		dialog.getWindow().setGravity(Gravity.CENTER);
		WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
		dialog.getWindow().setAttributes(params);
		dialog.show();
		dialog.setCancelable(true);
		dialog.setCanceledOnTouchOutside(true);
	}

	private View.OnClickListener onClick(final String seatsSelected, final ImageView testImage, final int i, final String j, final float a) {
		Log.v("Seats", seatsSelected);
		return new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if (isPressed) {
					testImage.setImageResource(R.drawable.ic_seat_selected);
					if (seatCount != null) {
						seatCount = Integer.toString(i);
					} else {
						seatCount = seatCount + "," + i;
					}
					count++;
					cost = cost + a;
					Toast.makeText(SelectSeatsActivity.this, j + i, Toast.LENGTH_SHORT).show();

				} else testImage.setImageResource(R.drawable.ic_seat_empty);
				isPressed = !isPressed;

			}
		};
	}

	private void parseTheatreLayoutResponse(String resp) {
		// TODO Auto-generated method stub
		theaterlayoutArr = new ArrayList<SeatLayoutVO>();
		try {
			ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(resp.getBytes());
			//			InputStream is =   getResources().openRawResource(R.raw.getmoviesbylangandtheatreid);
			SeatLayoutParseOperation xmlpull = new SeatLayoutParseOperation(byteArrayInputStream);
			theaterlayoutArr = xmlpull.outputArr;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public class BackGourndTask_GetSaetlayout extends AsyncTask<Void, Void, Void> {
		@Override
		protected Void doInBackground(Void... arg0) {
			String resp;
			try {
//                String seatlayouturl = "https://api.q-tickets.com/V2.0/gettheatrelayout?showtimeid=296525";
//                seatlayouturl = seatlayouturl.replaceAll("\\s+", "%20");
				String seatlayouturl = AppConstants.SERVICE_URL_GET_THEATRE_LAYOUT + AppConstants.SERVICE_MSG_GET_THEATRE_LAYOUT_SHOW_TIME_ID + showTimeId;
				seatlayouturl = seatlayouturl.replaceAll("\\s+", "%20");
				Log.v("SeatsuRL", seatlayouturl);
				HttpClient httpClient = new DefaultHttpClient();
				HttpContext localContext = new BasicHttpContext();
				HttpGet httpGet = new HttpGet(seatlayouturl);
				httpGet.addHeader(BasicScheme.authenticate(new UsernamePasswordCredentials(getResources().getString(R.string.auth_username), getResources().getString(R.string.auth_password)), "UTF-8", false));
				HttpResponse response = httpClient.execute(httpGet, localContext);
				HttpEntity entity = response.getEntity();
				resp = QTUtils.getASCIIContentFromEntity(entity);
				parseTheatreLayoutResponse(resp);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			initializeProgressDialog();

		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			dialog.dismiss();
			try {
				if (theaterlayoutArr.isEmpty()) {
					Toast.makeText(SelectSeatsActivity.this, "No Seats Provided", Toast.LENGTH_SHORT).show();
				} else {
					for (int i = 0; i < theaterlayoutArr.size(); i++) {
						final List<BookingClassVO> bookingClassesArr = theaterlayoutArr.get(i).bookingClassesArr;
						for (int j = 0; j < bookingClassesArr.size(); j++) {
							final List<BookingRowVO> bookingRowsArr = bookingClassesArr.get(j).bookingRowsArr;
							Collections.sort(bookingRowsArr, new Comparator<BookingRowVO>() {
								@Override
								public int compare(final BookingRowVO o1, final BookingRowVO o2) {
									return o1.getRowName().compareTo(o2.getRowName());
								}
							});
							for (int k = 0; k < bookingRowsArr.size(); ++k) {
								final TableRow seatsRow = (TableRow) LayoutInflater.from(SelectSeatsActivity.this).inflate(R.layout.item_view_movie_seats_row, grid, false);
								seatsRow.setGravity(Gravity.CENTER_VERTICAL);
								seatsRow.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.MATCH_PARENT, 1f));

								final BookingRowVO bookingRowVO = bookingRowsArr.get(k);
								final String rowName = bookingRowVO.getRowName();
								for (int l = 0; l < rowName.length(); ++l) {
									final TextView tv_RowNames = new TextView(getApplicationContext());
									tv_RowNames.setText(rowName);
									tv_RowNames.setTextColor(ContextCompat.getColor(SelectSeatsActivity.this, android.R.color.primary_text_light));
									tv_RowNames.setPadding(8, 8, 8, 8);
//									tv_RowNames.setTextSize(20);
									seatsRow.addView(tv_RowNames);
									final ArrayList<BookingSeatVO> seatsArr = bookingRowVO.getSeatsArr();
									for (int m = 0; m < seatsArr.size(); ++m) {
										final ToggleButton seatToggleButton = (ToggleButton) LayoutInflater.from(SelectSeatsActivity.this).inflate(R.layout.view_movie_seat, seatsRow, false);
//                                        seatToggleButton.setId(10);
//                                        seatsRow.setTag(10);
										seatToggleButton.setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(SelectSeatsActivity.this, R.drawable.selector_movie_seat_normal), null, null, null);
//										seatToggleButton.setPadding(8, 8, 8, 8);
										seatsRow.addView(seatToggleButton);
										float a = bookingClassesArr.get(j).getCost();
//										seatToggleButton.setOnClickListener(onClick(seatsSelected.substring(0, 2), seatToggleButton, m + 1, rowName, a));
									}

                                   /* for (int m = 0; m < theaterlayoutArr.get(i).bookingClassesArr.get(j).bookingRowsArr.get(k).availableSeatsCount; ++m) {
										testImage = new ImageView(getApplicationContext());
                                        testImage.setId(m);
                                        testImage.setImageResource(R.drawable.ic_seat_booked);
                                        seatsRow.addView(testImage);
                                        testImage.setOnClickListener(onClick(testImage, m + 1, theaterlayoutArr.get(i).bookingClassesArr.get(j).bookingRowsArr.get(k).getRowName()));
                                    }*/

								}
//                                testImage.setOnClickListener(this);
								// seatsRow.setOnClickListener(this);
								grid.addView(seatsRow);
							}

						}
					}
					iv_screen.setVisibility(View.VISIBLE);
					btn_bookNow.setVisibility(View.VISIBLE);

				}

			} catch (Exception e) {
				Toast.makeText(getApplicationContext(), "Server Not responding..", Toast.LENGTH_SHORT).show();
				e.printStackTrace();
			}
		}

	}
}
