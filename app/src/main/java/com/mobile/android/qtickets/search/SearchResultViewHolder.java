package com.mobile.android.qtickets.search;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.mobile.android.qtickets.R;

/**
 * Created by hexalabssd on 23/01/17.
 */

final class SearchResultViewHolder extends RecyclerView.ViewHolder {
	protected ImageView mPosterImageView;
	protected TextView name;
	protected TextView description;
	protected TextView genreVenue;

	SearchResultViewHolder(final View itemView) {
		super(itemView);
		mPosterImageView = (ImageView) itemView.findViewById(R.id.imageView_poster);
		name = (TextView) itemView.findViewById(R.id.textView_movieName);
		description = (TextView) itemView.findViewById(R.id.textView_description);
		genreVenue = (TextView) itemView.findViewById(R.id.textView_genre_venue);
	}

	void setMovieItemClickListener(final View.OnClickListener pClickListener) {
		itemView.setOnClickListener(pClickListener);
	}
}
