package com.mobile.android.qtickets.movie.moviedetails;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hexalabssd on 30/01/17.
 */

public final class TheatreAndShowsAdapter extends RecyclerView.Adapter {
	private final List<Theatre> mTheatreList;
	private OnShowTimeItemClickListener mOnShowTimeItemClickListener;

	public TheatreAndShowsAdapter() {
		mTheatreList = new ArrayList<>();
	}

	public void setTheatreList(final List<Theatre> pTheatreList) {
		if (pTheatreList != null && !pTheatreList.isEmpty()) {
			mTheatreList.clear();
			mTheatreList.addAll(pTheatreList);
			notifyDataSetChanged();
		}
	}

	@Override
	public RecyclerView.ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
		return new TheatreAndShowsViewHolder(parent);
	}

	@Override
	public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
		final Theatre theatre = mTheatreList.get(position);
		final TheatreAndShowsViewHolder theatreAndShowsViewHolder = (TheatreAndShowsViewHolder) holder;
		theatreAndShowsViewHolder.setTheatreName(theatre.getName());
		final ShowTimeListAdapter showTimesListAdapter = new ShowTimeListAdapter(theatreAndShowsViewHolder.getContext(), theatre.getShowTimes());
		showTimesListAdapter.setOnShowTimeItemClickListener(new ShowTimeListAdapter.OnShowTimeItemClickListener() {
			@Override
			public void onShowTimeItemClick(final String showTime, String showTimeId) {
				if (mOnShowTimeItemClickListener != null) {
					mOnShowTimeItemClickListener.OnShowTimeItemClick(theatre.getName(), showTime, showTimeId);
				}
			}
		});
		theatreAndShowsViewHolder.setShowTimesListAdapter(showTimesListAdapter);
	}

	@Override
	public int getItemCount() {
		return mTheatreList.size();
	}

	public void setOnShowTimeItemClickListener(final OnShowTimeItemClickListener pOnShowTimeItemClickListener) {
		mOnShowTimeItemClickListener = pOnShowTimeItemClickListener;
	}

	public interface OnShowTimeItemClickListener {
		void OnShowTimeItemClick(String theatreName, String showTime, String showTimeId);
	}

}
