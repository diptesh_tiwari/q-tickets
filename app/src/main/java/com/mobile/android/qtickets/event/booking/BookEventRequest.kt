package com.mobile.android.qtickets.event.booking

import `in`.hexalab.abstractvolley.base.HResponseListener
import `in`.hexalab.abstractvolley.base.HXmlRequest
import org.xmlpull.v1.XmlPullParser

/**
 * Created by hexalabssd on 27/03/17.
 */
class BookEventRequest(listener: HResponseListener<XmlPullParser>) : HXmlRequest(Method.POST, "https://api.q-tickets.com/V2.0/eventbookings", listener) {
}