package com.mobile.android.qtickets.movie.list;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.mobile.android.qtickets.R;
import com.mobile.android.qtickets.constants.AppConstants;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Indglobal on 4/19/2017.
 */

public class SearchItemDetaail extends AppCompatActivity implements View.OnClickListener {
    private TextView movieNameTV,languageTV,descriptionTV;
    private ImageView backBtn,iv_moviebanner_mdetails;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_detail);
        movieNameTV= (TextView) findViewById(R.id.movieNameTV);
        languageTV= (TextView) findViewById(R.id.languageTV);
        descriptionTV= (TextView) findViewById(R.id.descriptionTV);
        backBtn= (ImageView) findViewById(R.id.backBtn);
        iv_moviebanner_mdetails= (ImageView) findViewById(R.id.iv_moviebanner_mdetails);
        backBtn.setOnClickListener(this);
        String data= (String) getIntent().getSerializableExtra(AppConstants.DATA);
        try {
            //languagelanguage,name,description,poster
            JSONObject json = new JSONObject(data);
            String movieName=json.getString("name");
            String description=json.getString("description");
            String language=json.getString("language");
            String poster=json.getString("poster");
            if(poster!=null){
                Picasso.with(iv_moviebanner_mdetails.getContext()).load(AppConstants.WEBSITE_BASE_URL + poster).into(iv_moviebanner_mdetails);

            }

            movieNameTV.setText(movieName!=null?movieName:"");
            descriptionTV.setText(description!=null?description:"");
            languageTV.setText(language!=null?language:"");
            //movieNameTV.setText(movieName!=null?movieName:"");

            Log.d("",data.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("","");
    }


    @Override
    public void onClick(View v) {
        finish();
    }
}
