package com.mobile.android.qtickets.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Satish - 360 on 12/7/2016.
 */

public class BookedDetal implements Serializable {

	private String transactionid;
	private Integer balance;
	private String msg;
	private String OrderInfo;
	private boolean status;

	public Integer getBalance() {
		return balance;
	}

	public void setBalance(Integer balance) {
		this.balance = balance;
	}

	public String getTransactionid() {
		return transactionid;
	}

	public void setTransactionid(String transactionid) {
		this.transactionid = transactionid;
	}


	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getOrderInfo() {
		return OrderInfo;
	}

	public void setOrderInfo(String orderInfo) {
		OrderInfo = orderInfo;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}
}
