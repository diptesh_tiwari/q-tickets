package com.mobile.android.qtickets.movie.moviedetails;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.mobile.android.qtickets.R;
import com.mobile.android.qtickets.movie.ShowTimeVO;

import java.util.List;

/**
 * Created by hexalabssd on 30/01/17.
 */

final class ShowTimeListAdapter extends ArrayAdapter<ShowTimeVO> {

	private OnShowTimeItemClickListener mOnShowTimeItemClickListener;

	ShowTimeListAdapter(final Context context, final List<ShowTimeVO> objects) {
		super(context, R.layout.item_view_show_times, objects);
	}

	@NonNull
	@Override
	public View getView(final int position, View convertView, final ViewGroup parent) {
		final ShowTimeViewHolder showTimeViewHolder;
		if (convertView == null) {
			showTimeViewHolder = new ShowTimeViewHolder(parent);
			convertView = showTimeViewHolder.itemView;
			convertView.setTag(showTimeViewHolder);
		} else {
			showTimeViewHolder = (ShowTimeViewHolder) convertView.getTag();
		}

		final ShowTimeVO showTimeVO = getItem(position);
		showTimeViewHolder.setShowTimeText(showTimeVO.showTime);
		if (showTimeVO.showType.contentEquals("available")) {
			showTimeViewHolder.setOnShowTimeItemClickListener(new View.OnClickListener() {
				@Override
				public void onClick(final View v) {
					if (mOnShowTimeItemClickListener != null) {
						mOnShowTimeItemClickListener.onShowTimeItemClick(showTimeVO.showTime, showTimeVO.serverId);
					}
				}
			});
		} else {
			showTimeViewHolder.setAvailablity(false);
		}
		return convertView;
	}

	public void setOnShowTimeItemClickListener(OnShowTimeItemClickListener pOnShowTimeItemClickListener) {
		mOnShowTimeItemClickListener = pOnShowTimeItemClickListener;
	}

	public interface OnShowTimeItemClickListener {
		void onShowTimeItemClick(String showTime, String showTimeId);
	}
}
