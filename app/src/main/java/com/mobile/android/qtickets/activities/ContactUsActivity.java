package com.mobile.android.qtickets.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.mobile.android.qtickets.R;


/**
 * Created by Satish - 360 on 11/22/2016.
 */

public class ContactUsActivity extends AppCompatActivity implements View.OnClickListener {
	private ImageView iv_back;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_contact_us);

		iv_back = (ImageView) findViewById(R.id.iv_back_contactus);
		iv_back.setOnClickListener(this);
//        tv_email.setOnClickListener(this);

	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
			case R.id.iv_back_contactus:
				Intent homeIntent = new Intent(ContactUsActivity.this, DashboardActivity.class);
				startActivity(homeIntent);
				finish();
				break;
		}
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		Intent homeIntent = new Intent(ContactUsActivity.this, DashboardActivity.class);
		startActivity(homeIntent);
		finish();
	}
}
