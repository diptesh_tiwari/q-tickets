package com.mobile.android.qtickets.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Satish - 360 on 12/7/2016.
 */

public class SeatLayoutVO implements Serializable {

	public ArrayList<BookingClassVO> bookingClassesArr;
	int maxBooking;
	float bookingFees;
	String screenURl;

	public SeatLayoutVO() {
		super();
		bookingClassesArr = new ArrayList<BookingClassVO>();
	}

	public SeatLayoutVO(int maxBooking, float bookingFees, String screenURl) {
		super();
		this.maxBooking = maxBooking;
		this.bookingFees = bookingFees;
		this.screenURl = screenURl;
	}

	public ArrayList<BookingClassVO> getBookingClassesArr() {
		return bookingClassesArr;
	}

	public void setBookingClassesArr(ArrayList<BookingClassVO> bookingClassesArr) {
		this.bookingClassesArr = bookingClassesArr;
	}

	public int getMaxBooking() {
		return maxBooking;
	}

	public void setMaxBooking(int maxBooking) {
		this.maxBooking = maxBooking;
	}

	public float getBookingFees() {
		return bookingFees;
	}

	public void setBookingFees(float bookingFees) {
		this.bookingFees = bookingFees;
	}

	public String getScreenURl() {
		return screenURl;
	}

	public void setScreenURl(String screenURl) {
		this.screenURl = screenURl;
	}

	public enum SeatStatus {

		AVAILABLE(1),    //green color
		NOT_AVAILABLE,    // already booked seat.
		RESERVED,        //yellow color, user selected seat to book the same
		DISABLED,
		IS_FAMILY; // this is used for the seats which are not for Internet booking

		private int numVal;

		private SeatStatus() {
			// TODO Auto-generated constructor stub
		}

		SeatStatus(int numVal) {
			this.numVal = numVal;
		}

		public int getNumVal() {
			return numVal;
		}

	}
}
