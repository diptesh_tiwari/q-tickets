package com.mobile.android.qtickets.movie;

import android.content.Context;

import com.mobile.android.qtickets.movie.seatselection.PostSeatsLayoutRequest;

import in.hexalab.abstractvolley.ApiRequestManager;

/**
 * Created by hexalabssd on 22/02/17.
 */

final class MovieModel {
	private final Context mContext;

	MovieModel(final Context pContext) {
		mContext = pContext;
	}

	void fetchSeatLayout(final PostSeatsLayoutRequest pPostSeatsLayoutRequest) {
		ApiRequestManager.addToRequestQueue(mContext, pPostSeatsLayoutRequest);
	}
}
