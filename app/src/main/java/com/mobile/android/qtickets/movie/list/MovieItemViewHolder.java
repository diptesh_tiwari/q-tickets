package com.mobile.android.qtickets.movie.list;

import android.support.annotation.LayoutRes;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mobile.android.qtickets.R;
import com.squareup.picasso.Picasso;

/**
 * Created by hexalabssd on 01/03/17.
 */
class MovieItemViewHolder extends RecyclerView.ViewHolder {
	private ImageView mMoviePosterImageView;
	private TextView mMovieNameTextView;
	private TextView mDurationTextView,bookTV;
	private String movieType;

	public String getMovieType() {
		return movieType;
	}

	public void setMovieType(String movieType) {

		this.movieType = movieType;
	}

	MovieItemViewHolder(@LayoutRes final int layoutResId, ViewGroup parent) {
		super(LayoutInflater.from(parent.getContext()).inflate(layoutResId, parent, false));
		mMoviePosterImageView = (ImageView) itemView.findViewById(R.id.imageView_moviePoster);
		mMovieNameTextView = (TextView) itemView.findViewById(R.id.textView_movieName);
		mDurationTextView = (TextView) itemView.findViewById(R.id.textView_movieDuration);
		bookTV = (TextView) itemView.findViewById(R.id.bookTV);


		if(movieType!=null&&movieType.equalsIgnoreCase("nowShow")){

			bookTV.setVisibility(View.VISIBLE);

		}else if(movieType!=null&&movieType.equalsIgnoreCase("upComingShow")) {

			bookTV.setVisibility(View.GONE);
		}else {

			bookTV.setVisibility(View.VISIBLE);

		}
	}

	void setMovieName(final String pMovieName) {
		mMovieNameTextView.setText(pMovieName);
	}

	void setDuration(final String pDuration) {
		mDurationTextView.setText(pDuration);
	}

	void setBanner(final String pBannerUrl) {

		if (!TextUtils.isEmpty(pBannerUrl)){
			Picasso.with(mMoviePosterImageView.getContext()).load(pBannerUrl).into(mMoviePosterImageView);
	}
	}

	void setMovieItemClickListener(final View.OnClickListener pClickListener) {
		itemView.setOnClickListener(pClickListener);
	}
}
