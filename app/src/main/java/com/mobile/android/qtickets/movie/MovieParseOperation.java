package com.mobile.android.qtickets.movie;

import com.mobile.android.qtickets.constants.AppConstants;
import com.mobile.android.qtickets.parsers.BaseParseOperation;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by Satish - 360 on 11/28/2016.
 */

public class MovieParseOperation extends BaseParseOperation {

	public ArrayList<Movie> outputArr;
	public String status = "";
	public String last_modified = "";
	protected XmlPullParser xmlpullparser;
	Movie movie;
	String TAG = "XmlPullParsing";
	StringBuffer sb;
	int holderForStartAndLength[] = new int[2];

	public MovieParseOperation(InputStream is) {
		outputArr = new ArrayList<Movie>();
		movie = new Movie();
		XmlPullParserFactory factory = null;
		try {
			factory = XmlPullParserFactory.newInstance();
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		factory.setNamespaceAware(true);
		try {
			xmlpullparser = factory.newPullParser();
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			xmlpullparser.setInput(is, "UTF-8");
			processDocument(xmlpullparser);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void processDocument(XmlPullParser xpp) throws XmlPullParserException, IOException {
		int eventType = xpp.getEventType();
		do {
			if (eventType == XmlPullParser.START_DOCUMENT) {
//				System.out.println("Start document");
			} else if (eventType == XmlPullParser.END_DOCUMENT) {
//				System.out.println("End document");
			} else if (eventType == XmlPullParser.START_TAG) {
				processStartElement(xpp);
			} else if (eventType == XmlPullParser.END_TAG) {
				processEndElement(xpp);
			} else if (eventType == XmlPullParser.TEXT) {
				processText(xpp);
			}
			eventType = xpp.next();
		} while (eventType != XmlPullParser.END_DOCUMENT);
	}

	public void processStartElement(XmlPullParser xpp) {
		String name = xpp.getName();
		String uri = xpp.getNamespace();
		sb = new StringBuffer();

		if ("".equals(uri)) {
//			System.out.println("Start element: " + name);
			if (name.equals(AppConstants.SERVER_RESPONSE_TAG)) {
				if (xpp.getAttributeValue(null, AppConstants.SERVER_STATUS_TAG) != null) {
					status = xpp.getAttributeValue(null, AppConstants.SERVER_STATUS_TAG);
				}
				if (xpp.getAttributeValue(null, AppConstants.LAST_MODIFIED_TAG) != null) {
					last_modified = xpp.getAttributeValue(null, AppConstants.LAST_MODIFIED_TAG);
				}
			}
			if (name.equals(AppConstants.MOVIE_TAG)) {
				Movie tmpMovieVO = new Movie();
				if (xpp.getAttributeValue(null, AppConstants.MOVIE_ID_TAG) != null) {
					tmpMovieVO.serverId = xpp.getAttributeValue(null, AppConstants.MOVIE_ID_TAG);
				} else {
					tmpMovieVO.serverId = "";
				}

				if (xpp.getAttributeValue(null, AppConstants.MOVIE_NAME_TAG) != null) {
					tmpMovieVO.movieName = xpp.getAttributeValue(null, AppConstants.MOVIE_NAME_TAG);
				} else {
					tmpMovieVO.movieName = "";
				}
				if (xpp.getAttributeValue(null, AppConstants.MOVIE_REGISTRATION_DATE_TAG) != null) {
					tmpMovieVO.releaseDate = xpp.getAttributeValue(null, AppConstants.MOVIE_REGISTRATION_DATE_TAG);
				}
				else {
					tmpMovieVO.releaseDate = "";
				}
				/*} else if (xpp.getAttributeValue(null, AppConstants.MOVIE_REGISTRATION_DATE_TAG_UPCOMING) != null) {
					tmpMovieVO.releaseDate = xpp.getAttributeValue(null, AppConstants.MOVIE_REGISTRATION_DATE_TAG_UPCOMING);
				}*/
				if (xpp.getAttributeValue(null, AppConstants.MOVIE_IPHONE_THUMB) != null) {
					tmpMovieVO.iphonethumb = xpp.getAttributeValue(null, AppConstants.MOVIE_IPHONE_THUMB);
				} else {
					tmpMovieVO.iphonethumb = "";
				}
				if (xpp.getAttributeValue(null, AppConstants.MOVIE_IPAD_THUMB) != null) {
					tmpMovieVO.ipadthumb = xpp.getAttributeValue(null, AppConstants.MOVIE_IPAD_THUMB);
				} else {
					tmpMovieVO.ipadthumb = "";
				}


				if (xpp.getAttributeValue(null, AppConstants.MOVIE_THUMB_URL_TAG) != null) {
					tmpMovieVO.thumbnailURL = xpp.getAttributeValue(null, AppConstants.MOVIE_THUMB_URL_TAG);
				} else {
					tmpMovieVO.thumbnailURL = "";
				}
				if (xpp.getAttributeValue(null, AppConstants.MOVIE_LANGUAGE_ID_TAG) != null) {
					tmpMovieVO.language = xpp.getAttributeValue(null, AppConstants.MOVIE_LANGUAGE_ID_TAG);
				} else if (xpp.getAttributeValue(null, AppConstants.MOVIE_LANGUAGE_ID_TAG_UPCOMING) != null) {
					tmpMovieVO.language = xpp.getAttributeValue(null, AppConstants.MOVIE_LANGUAGE_ID_TAG_UPCOMING);
				}else {
					tmpMovieVO.language = "";

				}
				if (xpp.getAttributeValue(null, AppConstants.MOVIE_CENSOR_TAG) != null) {
					tmpMovieVO.censor = xpp.getAttributeValue(null, AppConstants.MOVIE_CENSOR_TAG);
				} else {
					tmpMovieVO.censor = "";
				}
				if (xpp.getAttributeValue(null, AppConstants.MOVIE_DURATION_TAG) != null) {
					tmpMovieVO.duration = xpp.getAttributeValue(null, AppConstants.MOVIE_DURATION_TAG);
				} else {
					tmpMovieVO.duration = "";
				}
				if (xpp.getAttributeValue(null, AppConstants.MOVIE_DESCRIPTION_TAG) != null) {
					tmpMovieVO.description = xpp.getAttributeValue(null, AppConstants.MOVIE_DESCRIPTION_TAG);
				}else if (xpp.getAttributeValue(null, AppConstants.MOVIE_SYNOPSIS_TAG) != null) {
					tmpMovieVO.synopsis = xpp.getAttributeValue(null, AppConstants.MOVIE_SYNOPSIS_TAG);
				} else {
					tmpMovieVO.description = "";
				}
				if (xpp.getAttributeValue(null, AppConstants.MOVIE_TYPE_TAG) != null) {
					tmpMovieVO.movieType = xpp.getAttributeValue(null, AppConstants.MOVIE_TYPE_TAG);
				}else if (xpp.getAttributeValue(null, AppConstants.UPCOMING_MOVIE_TYPE_TAG) != null) {
					tmpMovieVO.movieType = xpp.getAttributeValue(null, AppConstants.UPCOMING_MOVIE_TYPE_TAG);
				}else {
					tmpMovieVO.movieType = AppConstants.MOVIE_TYPE_NO_NAME;
				}
				if (xpp.getAttributeValue(null, AppConstants.MOVIE_BG_COLOR_CODE_TAG) != null) {
					tmpMovieVO.bg_color_code = xpp.getAttributeValue(null, AppConstants.MOVIE_BG_COLOR_CODE_TAG);
				} else {
					tmpMovieVO.bg_color_code = "";
				}
				if (xpp.getAttributeValue(null, AppConstants.MOVIE_BG_BORDER_COLOR_TAG) != null) {
					tmpMovieVO.bg_border_code = xpp.getAttributeValue(null, AppConstants.MOVIE_BG_BORDER_COLOR_TAG);
				} else {
					tmpMovieVO.bg_border_code = "";
				}
				if (xpp.getAttributeValue(null, AppConstants.MOVIE_BTN_COLOR_TAG) != null) {
					tmpMovieVO.btn_color = xpp.getAttributeValue(null, AppConstants.MOVIE_BTN_COLOR_TAG);
				} else {
					tmpMovieVO.btn_color = "";
				}
				if (xpp.getAttributeValue(null, AppConstants.MOVIE_TITLE_COLOR_CODE_TAG) != null) {
					tmpMovieVO.title_color_code = xpp.getAttributeValue(null, AppConstants.MOVIE_TITLE_COLOR_CODE_TAG);
				} else {
					tmpMovieVO.title_color_code = "";
				}
				if (xpp.getAttributeValue(null, AppConstants.MOVIE_CASTE_AND_CREW_TAG) != null) {
					tmpMovieVO.CastAndCrew = xpp.getAttributeValue(null, AppConstants.MOVIE_CASTE_AND_CREW_TAG);
				} else {
					tmpMovieVO.CastAndCrew = "";
				}
				if (xpp.getAttributeValue(null, AppConstants.MOVIE_TRIALER_URL_TAG) != null) {
					tmpMovieVO.TrailerURL = xpp.getAttributeValue(null, AppConstants.MOVIE_TRIALER_URL_TAG);
				} else {
					tmpMovieVO.TrailerURL = "";
				}

				if (xpp.getAttributeValue(null, AppConstants.MOVIE_MOVIEURL_TAG) != null) {
					tmpMovieVO.Movieurl = xpp.getAttributeValue(null, AppConstants.MOVIE_MOVIEURL_TAG);
				} else {
					tmpMovieVO.Movieurl = "";
				}
				if (xpp.getAttributeValue(null, AppConstants.MOVIE_IMDB_rating_TAG) != null) {
					tmpMovieVO.IMDB_rating = xpp.getAttributeValue(null, AppConstants.MOVIE_IMDB_rating_TAG);
				} else {
					tmpMovieVO.IMDB_rating = "";
				}

				if (xpp.getAttributeValue(null, AppConstants.MOVIE_THUMBNAIL_TAG) != null) {
					tmpMovieVO.thumbnail = xpp.getAttributeValue(null, AppConstants.MOVIE_THUMBNAIL_TAG);
				} else {
					tmpMovieVO.thumbnail = "";
				}
				if (xpp.getAttributeValue(null, AppConstants.MOVIE_BANAERURL_TAG) != null) {
					tmpMovieVO.bannerurl = xpp.getAttributeValue(null, AppConstants.MOVIE_BANAERURL_TAG);
				} else {
					tmpMovieVO.bannerurl = "";
				}

				if (xpp.getAttributeValue(null, AppConstants.WILL_NOT_WATCH_TAG) != null) {
					tmpMovieVO.willnotwatch = xpp.getAttributeValue(null, AppConstants.WILL_NOT_WATCH_TAG);
				} else {
					tmpMovieVO.willnotwatch = "";
				}
				if (xpp.getAttributeValue(null, AppConstants.WILL_WATCH_TAG) != null) {
					tmpMovieVO.willwatch = xpp.getAttributeValue(null, AppConstants.WILL_WATCH_TAG);
				} else {
					tmpMovieVO.willwatch = "";
				}

				if (xpp.getAttributeValue(null, AppConstants.MOVIE_AGE_RESTICTED_RATING) != null) {
					tmpMovieVO.AgeRestrictRating = xpp.getAttributeValue(null, AppConstants.MOVIE_AGE_RESTICTED_RATING);
				} else {
					tmpMovieVO.AgeRestrictRating = "";
				}
				tmpMovieVO.movieTheatresArr = new ArrayList<TheatreVO>();
				outputArr.add(tmpMovieVO);
			}

			if (name.equals(AppConstants.THEATRE_TAG)) {

				Movie tmpMovie = outputArr.get(outputArr.size() - 1);

				TheatreVO tmpMovieTheatreVO = new TheatreVO();
				if (xpp.getAttributeValue(null, AppConstants.THEATRE_ID_TAG) != null) {
					tmpMovieTheatreVO.serverId = xpp.getAttributeValue(null, AppConstants.THEATRE_ID_TAG);
				} else {
					tmpMovieTheatreVO.serverId = "";
				}
				if (xpp.getAttributeValue(null, AppConstants.THEATRE_NAME_TAG) != null) {
					tmpMovieTheatreVO.theatreName = xpp.getAttributeValue(null, AppConstants.THEATRE_NAME_TAG);
				} else {
					tmpMovieTheatreVO.theatreName = "";
				}
				if (xpp.getAttributeValue(null, AppConstants.THEATRE_ADDRESS_TAG) != null) {
					tmpMovieTheatreVO.address = xpp.getAttributeValue(null, AppConstants.THEATRE_ADDRESS_TAG);
				} else {
					tmpMovieTheatreVO.address = "";
				}
				if (xpp.getAttributeValue(null, AppConstants.THEATRE_LOGO_TAG) != null) {
					tmpMovieTheatreVO.logo = xpp.getAttributeValue(null, AppConstants.THEATRE_LOGO_TAG);
				} else {
					tmpMovieTheatreVO.logo = "";
				}

				if (xpp.getAttributeValue(null, AppConstants.THEATRE_ARABICNAME_TAG) != null) {
					tmpMovieTheatreVO.arabicname = xpp.getAttributeValue(null, AppConstants.THEATRE_ARABICNAME_TAG);
				} else {
					tmpMovieTheatreVO.arabicname = "";
				}
//				tmpMovie.movieTheatresArr = new ArrayList<MovieTheatreVO>();
				tmpMovieTheatreVO.showDatesArr = new ArrayList<ShowDateVO>();
				tmpMovie.movieTheatresArr.add(tmpMovieTheatreVO);

			}
			if (name.equals(AppConstants.SHOW_DATE_TAG)) {

				Movie tmpMovie = outputArr.get(outputArr.size() - 1);
				ArrayList<TheatreVO> tmpMovieTheatresArr = tmpMovie.movieTheatresArr;

				TheatreVO tmpMovieTheatreVO = tmpMovieTheatresArr.get(tmpMovieTheatresArr.size() - 1);

				ShowDateVO tmpShowDateVO = new ShowDateVO();
				if (xpp.getAttributeValue(null, AppConstants.SHOW_DATE_ID_TAG) != null) {
					tmpShowDateVO.serverId = xpp.getAttributeValue(null, AppConstants.SHOW_DATE_ID_TAG);
				} else {
					tmpShowDateVO.serverId = "";
				}
				if (xpp.getAttributeValue(null, AppConstants.SHOW_DATE_DATE_TAG) != null) {
					tmpShowDateVO.showDate = xpp.getAttributeValue(null, AppConstants.SHOW_DATE_DATE_TAG);
				} else {
					tmpShowDateVO.showDate = "";
				}
//				tmpMovieTheatreVO.showDatesArr = new ArrayList<ShowDateVO>();
				tmpShowDateVO.showTimesArr = new ArrayList<ShowTimeVO>();
				tmpMovieTheatreVO.showDatesArr.add(tmpShowDateVO);
			}
			if (name.equals(AppConstants.SHOW_TIME_TAG)) {
				Movie tmpMovie = outputArr.get(outputArr.size() - 1);
				ArrayList<TheatreVO> tmpMovieTheatresArr = tmpMovie.movieTheatresArr;
				TheatreVO tmpMovieTheatreVO = tmpMovieTheatresArr.get(tmpMovieTheatresArr.size() - 1);
				ArrayList<ShowDateVO> tmpShowDatesArr = tmpMovieTheatreVO.showDatesArr;

				ShowDateVO tmpShowDateVO = tmpShowDatesArr.get(tmpShowDatesArr.size() - 1);

				ShowTimeVO tmpShowTimeVO = new ShowTimeVO();
				if (xpp.getAttributeValue(null, AppConstants.SHOW_TIME_ID_TAG) != null) {
					tmpShowTimeVO.serverId = xpp.getAttributeValue(null, AppConstants.SHOW_TIME_ID_TAG);
				} else {
					tmpShowTimeVO.serverId = "";
				}
				if (xpp.getAttributeValue(null, AppConstants.SHOW_TIME_TIME_TAG) != null) {
					tmpShowTimeVO.showTime = xpp.getAttributeValue(null, AppConstants.SHOW_TIME_TIME_TAG);
				} else {
					tmpShowTimeVO.showTime = "";
				}
				if (xpp.getAttributeValue(null, AppConstants.SHOW_TIME_AVAILABLE_TAG) != null) {
					tmpShowTimeVO.availableCount = Integer.parseInt(xpp.getAttributeValue(null, AppConstants.SHOW_TIME_AVAILABLE_TAG));
				} else {
					tmpShowTimeVO.availableCount = 0;
				}
				if (xpp.getAttributeValue(null, AppConstants.SHOW_TIME_TYPE_TAG) != null) {
					tmpShowTimeVO.showType = xpp.getAttributeValue(null, AppConstants.SHOW_TIME_TYPE_TAG);
				} else {
					tmpShowTimeVO.showType = "";
				}
				if (xpp.getAttributeValue(null, AppConstants.SHOW_TIME_TOTAL_TAG) != null) {
					tmpShowTimeVO.total = Integer.parseInt(xpp.getAttributeValue(null, AppConstants.SHOW_TIME_TOTAL_TAG));
				} else {
					tmpShowTimeVO.total = 0;
				}
//				if (xpp.getAttributeValue(null,QTicketsConstants.SHOW_TIME_ENABLE_TAG)!=null) {
//					if (xpp.getAttributeValue(null,QTicketsConstants.SHOW_TIME_ENABLE_TAG).equalsIgnoreCase("true")) {
//						tmpShowTimeVO.isEnable = true;
//					}
//					else {
//						tmpShowTimeVO.isEnable = false;
//					}
//				}
//				else {
//					tmpShowTimeVO.isEnable = false;
//				}
				if (xpp.getAttributeValue(null, AppConstants.SHOW_TIME_ENABLE_TAG) != null) {
					tmpShowTimeVO.isEnable = xpp.getAttributeValue(null, AppConstants.SHOW_TIME_ENABLE_TAG);
				} else {
					tmpShowTimeVO.isEnable = "";
				}

				if (xpp.getAttributeValue(null, AppConstants.SHOW_TIME_SCREEN_ID_TAG) != null) {
					tmpShowTimeVO.screenId = xpp.getAttributeValue(null, AppConstants.SHOW_TIME_SCREEN_ID_TAG);
				} else {
					tmpShowTimeVO.screenId = "";
				}
				if (xpp.getAttributeValue(null, AppConstants.SHOW_TIME_SCREEN_NAME_TAG) != null) {
					tmpShowTimeVO.screenName = xpp.getAttributeValue(null, AppConstants.SHOW_TIME_SCREEN_NAME_TAG);
				} else {
					tmpShowTimeVO.screenName = "";
				}
//				tmpShowDateVO.showTimesArr = new ArrayList<ShowTimeVO>();
				tmpShowDateVO.showTimesArr.add(tmpShowTimeVO);
			}

		} else {
//			System.out.println("Start element: {" + uri + "}" + name);
		}
	}

	public void processEndElement(XmlPullParser xpp) {
		String name = xpp.getName();
		String uri = xpp.getNamespace();
		if ("".equals(uri)) {
//			System.out.println("End element: " + name);
		} else {
//			System.out.println("End element:   {" + uri + "}" + name);
		}
	}

	public void processText(XmlPullParser xpp) throws XmlPullParserException {
		char ch[] = xpp.getTextCharacters(holderForStartAndLength);
		int start = holderForStartAndLength[0];
		int length = holderForStartAndLength[1];
//		System.out.print("Characters:    \"");
		for (int i = start; i < start + length; i++) {
			switch (ch[i]) {
				case '\\':
					sb.append(ch[i]);
					break;

				case '"':
					sb.append(ch[i]);
					break;
				case '\n':
					sb.append(ch[i]);
					break;
				case '\r':
					sb.append(ch[i]);
					break;
				case '\t':
					sb.append(ch[i]);
					break;
				default:
					sb.append(ch[i]);
					break;
			}
		}
//		System.out.print("\"\n");
	}

}
