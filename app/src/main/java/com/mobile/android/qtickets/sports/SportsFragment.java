package com.mobile.android.qtickets.sports;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mobile.android.qtickets.R;
import com.mobile.android.qtickets.constants.AppConstants;
import com.mobile.android.qtickets.event.EventDetailsListParseOperation;
import com.mobile.android.qtickets.event.EventDetailsVO;
import com.mobile.android.qtickets.event.EventsAdapter;
import com.mobile.android.qtickets.network.InternetConnectionDetector;
import com.mobile.android.qtickets.parsers.SportDetailsListParseOperation;
import com.mobile.android.qtickets.utils.QTUtils;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Satish - 360 on 11/18/2016.
 */

public class SportsFragment  extends Fragment {
	InternetConnectionDetector connectionDetector;

	private RecyclerView mSportsRecyclerview;
	private ImageView noEventImageView;
	private SportsAdapter mSportsAdapter;
	private RecyclerView.LayoutManager mSportsLayoutManager;
	//private ArrayList<String> alCategory = new ArrayList<String>();
	private ArrayList<EventDetailsVO> sportsList;
	private ProgressDialog dialog;
	private String defaultOption = "All Categories";

	public SportsFragment() {

	}

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

		QTUtils.getInstance().saveToSharedPreference(getActivity(),AppConstants.EVENT_ITEM,null);
		SharedPreferences prefmPrefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
		SharedPreferences.Editor editor = prefmPrefs.edit();
		editor.putString(AppConstants.SHARED_EVENTS_SPORTS, null);
		editor.commit();

		View view = inflater.inflate(R.layout.fragment_sports, container, false);
		connectionDetector = new InternetConnectionDetector(getActivity());

		mSportsRecyclerview = (RecyclerView) view.findViewById(R.id.rv_sports);
		noEventImageView = (ImageView) view.findViewById(R.id.noEventImageView);
		final Handler handler = new Handler();
		handler.postDelayed(new Runnable() {
			@Override
			public void run() {
				if (connectionDetector.isConnectedToInternet()) {
					// Do something after 2s = 2000ms
					new SportsListUrl().execute();

				}
			}
		}, 300);
		return view;
	}

	private void sortByCategory(String selectedEvents) {
		ArrayList<EventDetailsVO> sortedEventTemp = new ArrayList<EventDetailsVO>();
		if (selectedEvents.equalsIgnoreCase(defaultOption)) {
			sortedEventTemp.addAll(sportsList);
		} else {
			for (int i = 0; i < sportsList.size(); i++) {
				if (sportsList.get(i).Category.equalsIgnoreCase(selectedEvents)) {
					sortedEventTemp.add(sportsList.get(i));
				}
			}
		}
		setUpEventsAdapter(sortedEventTemp);
	}

	private void setUpEventsAdapter(ArrayList<EventDetailsVO> sortedEventTemp) {
		mSportsRecyclerview.setHasFixedSize(true);
		mSportsRecyclerview.setNestedScrollingEnabled(false);

		final LinearLayoutManager mEventsLayoutManager = new LinearLayoutManager(getContext());
		mSportsRecyclerview.setLayoutManager(mEventsLayoutManager);

		ArrayList<EventDetailsVO> sportssListTemp= new ArrayList<>();

		for (int i = 0; i <sortedEventTemp.size() ; i++) {
			EventDetailsVO detailsVO= sortedEventTemp.get(i);
			if(detailsVO.CategoryId.equalsIgnoreCase("8")){
				sportssListTemp.add(detailsVO);
			}

		}
		if(!sportssListTemp.isEmpty()){
			noEventImageView.setVisibility(View.GONE);
			mSportsRecyclerview.setVisibility(View.VISIBLE);
			mSportsAdapter = new SportsAdapter(getActivity(), sportssListTemp);
			mSportsRecyclerview.setAdapter(mSportsAdapter);

		}else {
			noEventImageView.setVisibility(View.VISIBLE);
			mSportsRecyclerview.setVisibility(View.GONE);
		}

	}

   /*private void setupSportsAdapter() {
      mSportsRecyclerview.setHasFixedSize(true);
      mSportsRecyclerview.setNestedScrollingEnabled(false);

      mSportsLayoutManager = new LinearLayoutManager(getContext());
      mSportsRecyclerview.setLayoutManager(mSportsLayoutManager);

      mSportsAdapter = new SportsAdapter(getActivity(), sportsList);
      mSportsRecyclerview.setAdapter(mSportsAdapter);
   }*/

	private ArrayList<EventDetailsVO> parseSportsResponse(String result) {
		// TODO Auto-generated method stub
		ArrayList<EventDetailsVO> sportList = new ArrayList<EventDetailsVO>();
		try {
			ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(result.getBytes());
			// To get from raw folder
			//       InputStream is =   getResources().openRawResource(R.raw.getmoviesbylangandtheatreid);
			SportDetailsListParseOperation xmlpull = new SportDetailsListParseOperation(byteArrayInputStream);

			for (final EventDetailsVO e : xmlpull.outputArr) {
				if (e.CategoryId.contentEquals("8")) {
					sportList.add(e);
				}
			}


		} catch (Exception e) {
			// TODO: handle exception
		}
		return sportList;

	}

	private void initializeProgressDialog() {
		dialog = new ProgressDialog(getActivity(), R.style.progress_bar_style);
		dialog.getWindow().setGravity(Gravity.CENTER);
		WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
		dialog.getWindow().setAttributes(params);
		dialog.show();
		dialog.setCancelable(true);
		dialog.setCanceledOnTouchOutside(true);
	}

	private void initEventsList() {
		SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
		mPrefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
		Gson gson = new Gson();
		String events = mPrefs.getString(AppConstants.SHARED_EVENTS_SPORTS, "");
		if (!events.equalsIgnoreCase("")) {

			java.lang.reflect.Type type = new TypeToken<List<EventDetailsVO>>() {
			}.getType();
			sportsList = gson.fromJson(events, type);
		}
		setUpEventsAdapter(sportsList);

      /*if (eventsList.size() != 0) {
         for (int i = 0; i < eventsList.size(); i++) {
            alCategory.add(eventsList.get(i).Category);
         }
               *//*Log.e("alCategory", alCategory.size()+"");*//*
         Set<String> hs = new HashSet<String>();
         hs.addAll(alCategory);
         alCategory.clear();
         alCategory.addAll(hs);
                *//*   Log.e("altheaters after filtered", alCategory.size()+"");*//*
         alCategory.add(0, defaultOption);
         setUpCategoriesAdapter(alCategory);
      }*/
	}

   /*private void setUpCategoriesAdapter(ArrayList<String> alCategory) {
      // Creating adapter for spinner
      ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, alCategory);
      // Drop down layout style - list view with radio button
      dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
      // attaching data adapter to spinner
      sp_events.setAdapter(dataAdapter);
   }*/

	public class SportsListUrl extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... voids) {

			String url = AppConstants.SERVER_URL_GET_ALL_EVENTT_DETAILS;
			//String url = AppConstants.SERVER_URL_GET_ALL_EVENTT_DETAILS;
			SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
			String currencyType = mPrefs.getString(AppConstants.COUNTRY_TYPE_CURRENCY, null);

			if(currencyType.equalsIgnoreCase(AppConstants.COUNTRY_TYPE_BH)){
				url=url+"Country="+"Bahrain";
			}else   if(currencyType.equalsIgnoreCase(AppConstants.COUNTRY_TYPE_QA)){
				url=url+"Country="+"Qatar";
			}else   if(currencyType.equalsIgnoreCase(AppConstants.COUNTRY_TYPE_UAE)){
				url=url+"Country="+"Dubai";
			}
			if (connectionDetector.isConnectedToInternet()) {
				String resp;
				try {
					url = url.replaceAll("\\s+", "%20");
					HttpClient httpClient = new DefaultHttpClient();
					HttpContext localContext = new BasicHttpContext();
					HttpGet httpGet = new HttpGet(url);
					httpGet.addHeader(BasicScheme.authenticate(new UsernamePasswordCredentials(getResources().getString(R.string.auth_username), getResources().getString(R.string.auth_password)), "UTF-8", false));
					HttpResponse response = httpClient.execute(httpGet, localContext);
					HttpEntity entity = response.getEntity();
					resp = QTUtils.getASCIIContentFromEntity(entity);
					sportsList = parseSportsResponse(resp);
				} catch (Exception e) {
					resp = "error";
				}
			}
			return null;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			initializeProgressDialog();
		}

		@Override
		protected void onPostExecute(Void aVoid) {
			super.onPostExecute(aVoid);
			try {
				dialog.dismiss();
				if (sportsList.size() > 0) {
					SharedPreferences prefmPrefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
					SharedPreferences.Editor editor = prefmPrefs.edit();
					Gson gson = new Gson();
					String eventjson = gson.toJson(sportsList);
					editor.putString(AppConstants.SHARED_EVENTS_SPORTS, eventjson);
					editor.commit();
				} else {
					sportsList.clear();
				}
			} catch (Exception e) {
				e.printStackTrace();
//                Toast.makeText(getActivity(), "Server not Responding", Toast.LENGTH_SHORT).show();
			}
			initEventsList();
		}
	}
}