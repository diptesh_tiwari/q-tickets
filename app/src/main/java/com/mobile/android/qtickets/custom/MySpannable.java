package com.mobile.android.qtickets.custom;

import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;

/**
 * Created by Satish - 360 on 11/22/2016.
 */

public class MySpannable extends ClickableSpan {

	private boolean isUnderline = true;

	/**
	 * Constructor
	 */
	public MySpannable(boolean isUnderline) {
		this.isUnderline = isUnderline;
	}

	@Override
	public void onClick(View widget) {

	}

	@Override
	public void updateDrawState(TextPaint ds) {

		ds.setUnderlineText(isUnderline);

	}
}
