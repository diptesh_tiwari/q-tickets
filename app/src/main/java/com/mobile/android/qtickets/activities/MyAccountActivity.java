package com.mobile.android.qtickets.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mobile.android.qtickets.R;
import com.mobile.android.qtickets.constants.AppConstants;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Android on 4/27/17.
 */

public class MyAccountActivity  extends AppCompatActivity implements View.OnClickListener{

    Context context = MyAccountActivity.this;
    ImageView ivBack;
    TextView tvLogout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);

        ivBack = (ImageView)findViewById(R.id.ivBack);
        tvLogout = (TextView)findViewById(R.id.tvLogout);


        ivBack.setOnClickListener(this);
        tvLogout.setOnClickListener(this);

        setupViewPager();

    }

    private void setupViewPager() {
        final ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        RelativeLayout tabOne = (RelativeLayout) LayoutInflater.from(context).inflate(R.layout.myprfl_tab_title,null);
        tabOne.setSelected(true);
        tabLayout.getTabAt(0).setCustomView(tabOne);

        RelativeLayout tabsecond = (RelativeLayout) LayoutInflater.from(context).inflate(R.layout.mybking_tab_title,null);
        tabLayout.getTabAt(1).setCustomView(tabsecond);

        RelativeLayout tabfour = (RelativeLayout) LayoutInflater.from(context).inflate(R.layout.vrfymble_tab_title,null);
        tabLayout.getTabAt(2).setCustomView(tabfour);
    }

    private void setupViewPager(ViewPager viewPager) {

        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        Intent mIntent = getIntent();
        int pstn = mIntent.getIntExtra("postion", 0);
        adapter.addFrag(new MyProfileFragment(), "My Profile");
        adapter.addFrag(new MyBookingFragment(), "My Bookings");
        adapter.addFrag(new VerifyMobileFragment(),"Verify Mobile");

        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(pstn);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        switch (id){
            case R.id.ivBack:
                onBackPressed();
                break;

            case R.id.tvLogout:
                AlertDialog.Builder builder1 = new AlertDialog.Builder(MyAccountActivity.this);
                builder1.setTitle("Log Out");
                builder1.setMessage("Are you sure you want to Log Out?");
                builder1.setCancelable(true);



                builder1.setPositiveButton(
                        "Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                SharedPreferences prefmPrefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                                SharedPreferences.Editor editor = prefmPrefs.edit();

                                editor.putString(AppConstants.SHARED_USER_TAG, null);
                                editor.putString("login","0");
                                editor.commit();

                                Intent setIntent = new Intent(MyAccountActivity.this, LoginActivity.class);
                                setIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                startActivity(setIntent);
                                Toast.makeText(MyAccountActivity.this, "Logout Successfully!", Toast.LENGTH_SHORT).show();
                                finish();
                            }
                        });

                builder1.setNegativeButton(
                        "No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert11 = builder1.create();
                alert11.show();

                break;
        }
    }


    static class ViewPagerAdapter extends FragmentPagerAdapter {

        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
