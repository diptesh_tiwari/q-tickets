package com.mobile.android.qtickets;

import android.content.Context;

import com.mobile.android.qtickets.search.GetSearchResultRequest;
import com.mobile.android.qtickets.search.SearchResultResponse;

import in.hexalab.abstractvolley.base.HResponseListener;

/**
 * Created by hexalabssd on 23/01/17.
 */

public final class AppController {
	private final Context mContext;
	private final AppModel mAppModel;
	private GetSearchResultRequest getSearchResultRequest;

	public AppController(final Context pContext) {
		mContext = pContext;
		mAppModel = new AppModel(pContext);
	}

	public void getSearchResult(final String keyword, final HResponseListener<SearchResultResponse> pListener) {
		getSearchResultRequest = new GetSearchResultRequest(keyword, pListener);
		mAppModel.getSearchResult(getSearchResultRequest);
	}

	public void cancelSearch() {
		if (getSearchResultRequest != null) {
			getSearchResultRequest.cancel();
		}
	}
}
