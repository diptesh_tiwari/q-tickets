package com.mobile.android.qtickets.interfes;

public interface OnEventCallBack {

   void onEvent(int id);
   void onEvent(int id, Object object);

}