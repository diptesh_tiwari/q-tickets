package com.mobile.android.qtickets.movie.list;

import android.view.ViewGroup;
import android.widget.TextView;

import com.mobile.android.qtickets.R;

/**
 * Created by hexalabssd on 01/03/17.
 */

final class MovieHorizontalItemViewHolder extends MovieItemViewHolder {

	private TextView mCensorTextView;


	MovieHorizontalItemViewHolder(final ViewGroup parent) {
		super(R.layout.item_view_movie_horizontal, parent);
		mCensorTextView = (TextView) itemView.findViewById(R.id.textView_censor);
	}

	void setCensor(final String pCensor) {
		mCensorTextView.setText(pCensor);
	}

}
