package com.mobile.android.qtickets.parsers;

import com.mobile.android.qtickets.constants.AppConstants;
import com.mobile.android.qtickets.event.EventDetailsVO;
import com.mobile.android.qtickets.event.EventTicketDetails;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by Indglobal on 4/29/2017.
 */

public class SportDetailsListParseOperation  extends BaseParseOperation {

    public ArrayList<EventDetailsVO> outputArr;
    public String status = "";
    public String last_modified = "";
    protected XmlPullParser xmlpullparser;
    EventDetailsVO event;
    String TAG = "XmlPullParsing";
    StringBuffer sb;
    int holderForStartAndLength[] = new int[2];

    public SportDetailsListParseOperation(InputStream is) {
        outputArr = new ArrayList<EventDetailsVO>();
        event = new EventDetailsVO();
        XmlPullParserFactory factory = null;
        try {
            factory = XmlPullParserFactory.newInstance();
        } catch (XmlPullParserException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        factory.setNamespaceAware(true);
        try {
            xmlpullparser = factory.newPullParser();
        } catch (XmlPullParserException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        try {
            xmlpullparser.setInput(is, "UTF-8");
            processDocument(xmlpullparser);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public void processDocument(XmlPullParser xpp) throws XmlPullParserException, IOException {
        int eventType = xpp.getEventType();
        do {
            if (eventType == XmlPullParser.START_DOCUMENT) {
//          System.out.println("Start document");
            } else if (eventType == XmlPullParser.END_DOCUMENT) {
//          System.out.println("End document");
            } else if (eventType == XmlPullParser.START_TAG) {
                processStartElement(xpp);
            } else if (eventType == XmlPullParser.END_TAG) {
                processEndElement(xpp);
            } else if (eventType == XmlPullParser.TEXT) {
                processText(xpp);
            }
            eventType = xpp.next();
        } while (eventType != XmlPullParser.END_DOCUMENT);
    }

    public void processStartElement(XmlPullParser xpp) {
        String name = xpp.getName();
        String uri = xpp.getNamespace();
        sb = new StringBuffer();

        if ("".equals(uri)) {

            if (name.equals(AppConstants.SERVER_RESPONSE_TAG)) {
                if (xpp.getAttributeValue(null, AppConstants.SERVER_STATUS_TAG) != null) {
                    status = xpp.getAttributeValue(null, AppConstants.SERVER_STATUS_TAG);
                }
                if (xpp.getAttributeValue(null, AppConstants.LAST_MODIFIED_TAG) != null) {
                    last_modified = xpp.getAttributeValue(null, AppConstants.LAST_MODIFIED_TAG);
                }
            }
            if (name.equals(AppConstants.EVENET_DETAIL_TAG)) {
                EventDetailsVO tmpEventvoVO = new EventDetailsVO();
                if (xpp.getAttributeValue(null, AppConstants.EVENET_EVENTID_TAG) != null) {
                    tmpEventvoVO.eventid = xpp.getAttributeValue(null, AppConstants.EVENET_EVENTID_TAG);
                } else {
                    tmpEventvoVO.eventid = "";
                }

                if (xpp.getAttributeValue(null, AppConstants.EVENET_EVENTNAME_TAG) != null) {
                    tmpEventvoVO.eventname = xpp.getAttributeValue(null, AppConstants.EVENET_EVENTNAME_TAG);
                } else {
                    tmpEventvoVO.eventname = "";
                }
                if (xpp.getAttributeValue(null, AppConstants.EVENET_EVENT_DESCRIPTION_TAG) != null) {
                    tmpEventvoVO.EDescription = xpp.getAttributeValue(null, AppConstants.EVENET_EVENT_DESCRIPTION_TAG);
                } else {
                    tmpEventvoVO.EDescription = "";
                }
                if (xpp.getAttributeValue(null, AppConstants.EVENET_START_DATE_TAG) != null) {
                    tmpEventvoVO.startDate = xpp.getAttributeValue(null, AppConstants.EVENET_START_DATE_TAG);
                } else {
                    tmpEventvoVO.startDate = "";
                }
                if (xpp.getAttributeValue(null, AppConstants.EVENET_END_DATE_TAG) != null) {
                    tmpEventvoVO.endDate = xpp.getAttributeValue(null, AppConstants.EVENET_END_DATE_TAG);
                } else {
                    tmpEventvoVO.endDate = "";
                }
                if (xpp.getAttributeValue(null, AppConstants.EVENET_END_TIMER_TAG) != null) {
                    tmpEventvoVO.endTime = xpp.getAttributeValue(null, AppConstants.EVENET_END_TIMER_TAG);
                } else {
                    tmpEventvoVO.endTime = "";
                }
                if (xpp.getAttributeValue(null, AppConstants.EVENET_START_TIMER_TAG) != null) {
                    tmpEventvoVO.StartTime = xpp.getAttributeValue(null, AppConstants.EVENET_START_TIMER_TAG);
                } else {
                    tmpEventvoVO.StartTime = "";
                }
                if (xpp.getAttributeValue(null, AppConstants.EVENET_CONTASCT_PERSON_TAG) != null) {
                    tmpEventvoVO.ContactPerson = xpp.getAttributeValue(null, AppConstants.EVENET_CONTASCT_PERSON_TAG);
                } else {
                    tmpEventvoVO.ContactPerson = "";
                }
                if (xpp.getAttributeValue(null, AppConstants.EVENET_CONTASCT_PERSON_EMAIL_TAG) != null) {
                    tmpEventvoVO.ContactPersonEmail = xpp.getAttributeValue(null, AppConstants.EVENET_CONTASCT_PERSON_EMAIL_TAG);
                } else {
                    tmpEventvoVO.ContactPersonEmail = "";
                }
                if (xpp.getAttributeValue(null, AppConstants.EVENET_CONTASCT_PERSON_PHONENO_TAG) != null) {
                    tmpEventvoVO.ContactPersonNo = xpp.getAttributeValue(null, AppConstants.EVENET_CONTASCT_PERSON_PHONENO_TAG);
                } else {
                    tmpEventvoVO.ContactPersonNo = "";
                }

                if (xpp.getAttributeValue(null, AppConstants.EVENET_LATITUDE_TAG) != null) {
                    tmpEventvoVO.Latitude = xpp.getAttributeValue(null, AppConstants.EVENET_LATITUDE_TAG);
                } else {
                    tmpEventvoVO.Latitude = "";
                }
                if (xpp.getAttributeValue(null, AppConstants.EVENET_LONGITITUDE_TAG) != null) {
                    tmpEventvoVO.Longitude = xpp.getAttributeValue(null, AppConstants.EVENET_LONGITITUDE_TAG);
                } else {
                    tmpEventvoVO.Longitude = "";
                }
                if (xpp.getAttributeValue(null, AppConstants.EVENET_ADMINSTION_TAG) != null) {
                    tmpEventvoVO.admission = xpp.getAttributeValue(null, AppConstants.EVENET_ADMINSTION_TAG);
                } else {
                    tmpEventvoVO.admission = "";
                }
                if (xpp.getAttributeValue(null, AppConstants.EVENET_ALOCHOL_TAG) != null) {
                    tmpEventvoVO.alochol = xpp.getAttributeValue(null, AppConstants.EVENET_ALOCHOL_TAG);
                } else {
                    tmpEventvoVO.alochol = "";
                }
                if (xpp.getAttributeValue(null, AppConstants.EVENET_EVENTURL_TAG) != null) {
                    tmpEventvoVO.EventUrl = xpp.getAttributeValue(null, AppConstants.EVENET_EVENTURL_TAG);
                } else {
                    tmpEventvoVO.EventUrl = "";
                }
                if (xpp.getAttributeValue(null, AppConstants.EVENET_THUMBURL_TAG) != null) {
                    tmpEventvoVO.thumbUrl = xpp.getAttributeValue(null, AppConstants.EVENET_THUMBURL_TAG);
                } else {
                    tmpEventvoVO.thumbUrl = "";
                }
                if (xpp.getAttributeValue(null, AppConstants.EVENET_VENUE_TAG) != null) {
                    tmpEventvoVO.Venue = xpp.getAttributeValue(null, AppConstants.EVENET_VENUE_TAG);
                } else {
                    tmpEventvoVO.Venue = "";
                }
                if (xpp.getAttributeValue(null, AppConstants.EVENET_VDESCRIPTION_TAG) != null) {
                    tmpEventvoVO.VDescription = xpp.getAttributeValue(null, AppConstants.EVENET_VDESCRIPTION_TAG);
                } else {
                    tmpEventvoVO.VDescription = "";
                }
                if (xpp.getAttributeValue(null, AppConstants.EVENET_EVENTQTURLPATH_TAG) != null) {
                    tmpEventvoVO.EventQTurlPath = xpp.getAttributeValue(null, AppConstants.EVENET_EVENTQTURLPATH_TAG);
                } else {
                    tmpEventvoVO.EventQTurlPath = "";
                }
                if (xpp.getAttributeValue(null, AppConstants.EVENET_VENUEID_TAG) != null) {
                    tmpEventvoVO.VenueId = xpp.getAttributeValue(null, AppConstants.EVENET_VENUEID_TAG);
                } else {
                    tmpEventvoVO.VenueId = "";
                }
                if (xpp.getAttributeValue(null, AppConstants.EVENET_THUMBURLFULL_TAG) != null) {
                    tmpEventvoVO.thumbURL = xpp.getAttributeValue(null, AppConstants.EVENET_THUMBURLFULL_TAG);
                } else {
                    tmpEventvoVO.thumbURL = "";
                }
                if (xpp.getAttributeValue(null, AppConstants.EVENET_BANERURl_TAG) != null) {
                    tmpEventvoVO.bannerURL = xpp.getAttributeValue(null, AppConstants.EVENET_BANERURl_TAG);
                } else {
                    tmpEventvoVO.bannerURL = "";
                }
                if (xpp.getAttributeValue(null, AppConstants.EVENET_POSTERURL_TAG) != null) {
                    tmpEventvoVO.posterURL = xpp.getAttributeValue(null, AppConstants.EVENET_POSTERURL_TAG);
                } else {
                    tmpEventvoVO.posterURL = "";
                }
                if (xpp.getAttributeValue(null, AppConstants.EVENET_MOBILEURL_TAG) != null) {
                    tmpEventvoVO.mobileURL = xpp.getAttributeValue(null, AppConstants.EVENET_MOBILEURL_TAG);
                } else {
                    tmpEventvoVO.mobileURL = "";
                }
                if (xpp.getAttributeValue(null, AppConstants.EVENET_CATEGORY_TAG) != null) {
                    tmpEventvoVO.Category = xpp.getAttributeValue(null, AppConstants.EVENET_CATEGORY_TAG);
                } else {
                    tmpEventvoVO.Category = "";
                }
                if (xpp.getAttributeValue(null, AppConstants.EVENET_CATEGORYID_TAG) != null) {
                    tmpEventvoVO.CategoryId = xpp.getAttributeValue(null, AppConstants.EVENET_CATEGORYID_TAG);
                } else {
                    tmpEventvoVO.CategoryId = "";
                }
                if (xpp.getAttributeValue(null, AppConstants.EVENET_SEATLAYOUT_TAG) != null) {
                    tmpEventvoVO.SeatLayout = xpp.getAttributeValue(null, AppConstants.EVENET_SEATLAYOUT_TAG);
                } else {
                    tmpEventvoVO.SeatLayout = "";
                }
                if (xpp.getAttributeValue(null, AppConstants.EVENET_BOOKINGTYPE_TAG) != null) {
                    tmpEventvoVO.bookingType = xpp.getAttributeValue(null, AppConstants.EVENET_BOOKINGTYPE_TAG);
                } else {
                    tmpEventvoVO.bookingType = "";
                }
                if (xpp.getAttributeValue(null, AppConstants.EVENET_EVENTTYPE_TAG) != null) {
                    tmpEventvoVO.EventType = xpp.getAttributeValue(null, AppConstants.EVENET_EVENTTYPE_TAG);
                } else {
                    tmpEventvoVO.EventType = "";
                }
                if (xpp.getAttributeValue(null, AppConstants.EVENET_CATEGORYICON_TAG) != null) {
                    tmpEventvoVO.categoryIcon = xpp.getAttributeValue(null, AppConstants.EVENET_CATEGORYICON_TAG);
                } else {
                    tmpEventvoVO.categoryIcon = "";
                }
                if (xpp.getAttributeValue(null, AppConstants.EVENET_COLORCODE_TAG) != null) {
                    tmpEventvoVO.colorcode = xpp.getAttributeValue(null, AppConstants.EVENET_COLORCODE_TAG);
                } else {
                    tmpEventvoVO.colorcode = "";
                }
                if (xpp.getAttributeValue(null, AppConstants.EVENET_BGCOLR_TAG) != null) {
                    tmpEventvoVO.bgcolorcode = xpp.getAttributeValue(null, AppConstants.EVENET_BGCOLR_TAG);
                } else {
                    tmpEventvoVO.bgcolorcode = "";
                }
                if (xpp.getAttributeValue(null, AppConstants.EVENET_BGBORDERCOLRCODE_TAG) != null) {
                    tmpEventvoVO.bgbordercolorcode = xpp.getAttributeValue(null, AppConstants.EVENET_BGBORDERCOLRCODE_TAG);
                } else {
                    tmpEventvoVO.bgbordercolorcode = "";
                }
                if (xpp.getAttributeValue(null, AppConstants.EVENET_BTNCOLORCODE_TAG) != null) {
                    tmpEventvoVO.btncolorcode = xpp.getAttributeValue(null, AppConstants.EVENET_BTNCOLORCODE_TAG);
                } else {
                    tmpEventvoVO.btncolorcode = "";
                }
                if (xpp.getAttributeValue(null, AppConstants.EVENET_TITELCOLORCODE_TAG) != null) {
                    tmpEventvoVO.titlecolorcode = xpp.getAttributeValue(null, AppConstants.EVENET_TITELCOLORCODE_TAG);
                } else {
                    tmpEventvoVO.titlecolorcode = "";
                }

                if (xpp.getAttributeValue(null, AppConstants.EVENET_DATE_MODIFIED_TAG) != null) {
                    tmpEventvoVO.date_modified = xpp.getAttributeValue(null, AppConstants.EVENET_DATE_MODIFIED_TAG);
                } else {
                    tmpEventvoVO.date_modified = "";
                }
                tmpEventvoVO.evntTktDetilas = new ArrayList<EventTicketDetails>();
                outputArr.add(tmpEventvoVO);
            }
            if (name.equals(AppConstants.TICKET_TAG)) {
                EventDetailsVO tmpevnt = outputArr.get(outputArr.size() - 1);
                EventTicketDetails tmptktdetils = new EventTicketDetails();
                if (xpp.getAttributeValue(null, AppConstants.TICKET_PRICEID_TAG) != null) {
                    tmptktdetils.tktpriceid = xpp.getAttributeValue(null, AppConstants.TICKET_PRICEID_TAG);
                } else {
                    tmptktdetils.tktpriceid = "";
                }
                if (xpp.getAttributeValue(null, AppConstants.TICKET_MASTERID_TAG) != null) {
                    tmptktdetils.tktmasterid = xpp.getAttributeValue(null, AppConstants.TICKET_MASTERID_TAG);
                } else {
                    tmptktdetils.tktmasterid = "";
                }
                if (xpp.getAttributeValue(null, AppConstants.TICKET_NAME_TAG) != null) {
                    tmptktdetils.TicketName = xpp.getAttributeValue(null, AppConstants.TICKET_NAME_TAG);
                } else {
                    tmptktdetils.TicketName = "";
                }
                if (xpp.getAttributeValue(null, AppConstants.TICKET_TOTAL_TICKETS_TAG) != null) {
                    tmptktdetils.TotalTickets = xpp.getAttributeValue(null, AppConstants.TICKET_TOTAL_TICKETS_TAG);
                } else {
                    tmptktdetils.TotalTickets = "";
                }

                if (xpp.getAttributeValue(null, AppConstants.TICKET_AVAILBLITY_TAG) != null) {
                    tmptktdetils.Availability = xpp.getAttributeValue(null, AppConstants.TICKET_AVAILBLITY_TAG);
                } else {
                    tmptktdetils.Availability = "";
                }

                if (xpp.getAttributeValue(null, AppConstants.TICKET_SERVICE_CHARGE_TAG) != null) {
                    tmptktdetils.ServiceCharge = xpp.getAttributeValue(null, AppConstants.TICKET_SERVICE_CHARGE_TAG);
                } else {
                    tmptktdetils.ServiceCharge = "";
                }
                if (xpp.getAttributeValue(null, AppConstants.TICKET_PRICE_TAG) != null) {
                    tmptktdetils.TicketPrice = xpp.getAttributeValue(null, AppConstants.TICKET_PRICE_TAG);
                } else {
                    tmptktdetils.TicketPrice = "";
                }
                if (xpp.getAttributeValue(null, AppConstants.TICKET_ADMIT_TAG) != null) {
                    tmptktdetils.Admit = xpp.getAttributeValue(null, AppConstants.TICKET_ADMIT_TAG);
                } else {
                    tmptktdetils.Admit = "";
                }
                if (xpp.getAttributeValue(null, AppConstants.TICKET_DATE_TAG) != null) {
                    tmptktdetils.Date = xpp.getAttributeValue(null, AppConstants.TICKET_DATE_TAG);
                } else {
                    tmptktdetils.Date = "";
                }
                if (xpp.getAttributeValue(null, AppConstants.TICKET_NOOF_TICKETS_FOR_TRANSACTION_TAG) != null) {
                    tmptktdetils.NoOfTicketsPerTransaction = xpp.getAttributeValue(null, AppConstants.TICKET_NOOF_TICKETS_FOR_TRANSACTION_TAG);
                } else {
                    tmptktdetils.NoOfTicketsPerTransaction = "";
                }

                if (xpp.getAttributeValue(null, AppConstants.TICKET_TYPE_TAG) != null) {
                    tmptktdetils.TicketType = xpp.getAttributeValue(null, AppConstants.TICKET_TYPE_TAG);
                } else {
                    tmptktdetils.TicketType = "";
                }
                tmpevnt.evntTktDetilas.add(tmptktdetils);
//
            }

        }


    }

    public void processEndElement(XmlPullParser xpp) {
        String name = xpp.getName();
        String uri = xpp.getNamespace();
        if ("".equals(uri)) {
//       System.out.println("End element: " + name);
        } else {
//       System.out.println("End element:   {" + uri + "}" + name);
        }
    }

    public void processText(XmlPullParser xpp) throws XmlPullParserException {
        char ch[] = xpp.getTextCharacters(holderForStartAndLength);
        int start = holderForStartAndLength[0];
        int length = holderForStartAndLength[1];
//    System.out.print("Characters:    \"");
        for (int i = start; i < start + length; i++) {
            switch (ch[i]) {
                case '\\':
                    sb.append(ch[i]);
                    break;

                case '"':
                    sb.append(ch[i]);
                    break;
                case '\n':
                    sb.append(ch[i]);
                    break;
                case '\r':
                    sb.append(ch[i]);
                    break;
                case '\t':
                    sb.append(ch[i]);
                    break;
                default:
                    sb.append(ch[i]);
                    break;
            }
        }
//    System.out.print("\"\n");
    }
}