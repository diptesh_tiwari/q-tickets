package com.mobile.android.qtickets.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Gravity;
import android.view.WindowManager;

import com.mobile.android.qtickets.R;
import com.mobile.android.qtickets.constants.AppConstants;
import com.mobile.android.qtickets.network.InternetConnectionDetector;
import com.mobile.android.qtickets.utils.QTUtils;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONObject;
import org.json.XML;

/**
 * Created by Android on 4/26/17.
 */

public class TicketConfirmationActivity extends Activity{


    InternetConnectionDetector connectionDetector;
    private ProgressDialog dialog = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ticket_confirmation);




        connectionDetector = new InternetConnectionDetector(this);
       final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
          public void run() {
               if (connectionDetector.isConnectedToInternet()) {
                    new movieSuccessURL().execute();
             }
          }
       }, 300);
    }


    public class movieSuccessURL extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {

            try {
                // showid=397665&seats=F04,F03&AppSource=3&movie_id=12800&schedule_date=04/27/2017&show_time=07:30 PM
                String seat_confirm_url = AppConstants.MOVIE_SEAT_BLOCK_URL+ "booking_id=" + "1445959";
                String resp;
                if (connectionDetector.isConnectedToInternet()) {
                    try {
                        seat_confirm_url = seat_confirm_url.replaceAll("\\s+", "%20");
                        HttpClient httpClient = new DefaultHttpClient();
                        HttpContext localContext = new BasicHttpContext();
                        HttpGet httpGet = new HttpGet(seat_confirm_url);
                        httpGet.addHeader(BasicScheme.authenticate(new UsernamePasswordCredentials(getResources().getString(R.string.auth_username), getResources().getString(R.string.auth_password)), "UTF-8", false));
                        HttpResponse response = httpClient.execute(httpGet, localContext);
                        HttpEntity entity = response.getEntity();
                        resp = QTUtils.getASCIIContentFromEntity(entity);
                        return resp;
                    } catch (Exception e) {
                        resp = "error";
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            initializeProgressDialog();

        }


        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            dialog.dismiss();
            try {
                if (result != null) {
                    JSONObject json = XML.toJSONObject(result);
                    JSONObject jsonObject = json.getJSONObject("response");
                    JSONObject bookingHistories = jsonObject.getJSONObject("BookingHistories");
                    JSONObject bookinghistory = bookingHistories.getJSONObject("bookinghistory");
                    if (bookinghistory != null) {

                        Log.d("","");

                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            //initNationalitySpinner();
        }

    }



    private void initializeProgressDialog() {
        dialog = new ProgressDialog(this, R.style.progress_bar_style);
        dialog.getWindow().setGravity(Gravity.CENTER);
        WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
        dialog.getWindow().setAttributes(params);
        dialog.show();
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
    }
}
