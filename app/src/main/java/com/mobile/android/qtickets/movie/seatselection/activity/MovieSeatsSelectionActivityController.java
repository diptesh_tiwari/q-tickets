package com.mobile.android.qtickets.movie.seatselection.activity;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.mobile.android.qtickets.R;
import com.mobile.android.qtickets.movie.MovieController;
import com.mobile.android.qtickets.movie.seatselection.SeatsRow;

import java.util.List;

import in.hexalab.abstractcomponents.AppBarAbstractActivity;

/**
 * Created by hexalabssd on 02/02/17.
 */

abstract class MovieSeatsSelectionActivityController extends AppBarAbstractActivity {
	private final AdapterView.OnItemSelectedListener mOnSeatItemSelected = new AdapterView.OnItemSelectedListener() {
		@Override
		public void onItemSelected(final AdapterView<?> parent, final View view, final int position, final long id) {
			// TODO Clear current seat selection
		}

		@Override
		public void onNothingSelected(final AdapterView<?> parent) {

		}
	};
	protected MovieController mMovieController;
	private Spinner mSeatCountsSpinner;
	public TableLayout mSeatsTableLayout;
	private OnSeatSelectionChangedListener mOnSeatSelectionChangedListener;
	private ImageView ivBack;
	private TextView moviePriceTV,moviesSeatsTV;
	TextView tvSeatSlctdNmbrs;

	public abstract void onClickConfirmBookingButton(View pView);

	@Override
	protected void onCreate(@Nullable final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mMovieController = new MovieController(this);
	}

	@Override
	protected void initContentView() {
		setContentView(R.layout.activity_movie_seats_selection);

		mSeatCountsSpinner = (Spinner) findViewById(R.id.spinner_seatCounts);
		mSeatsTableLayout = (TableLayout) findViewById(R.id.grid);
		ivBack = (ImageView)findViewById(R.id.ivBack);
		//moviePriceTV= (TextView) findViewById(R.id.moviePriceTV);
		//moviesSeatsTV= (TextView) findViewById(R.id.moviesSeatsTV);
		tvSeatSlctdNmbrs = (TextView)findViewById(R.id.tvSeatSlctdNmbrs);

		ivBack.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onBackPressed();
			}
		});
	}

	@Override
	protected void initContentViewListeners() {
		mSeatCountsSpinner.setOnItemSelectedListener(mOnSeatItemSelected);
	}

//	@Override
//	protected void initActionBar() {
//		final ActionBar actionBar = getSupportActionBar();
//		if (actionBar != null) {
//			actionBar.setDisplayHomeAsUpEnabled(true);
//			actionBar.setHomeAsUpIndicator(R.drawable.back_btn);
//		}
//	}

	@Override
	protected void initNavigationDrawer() {

	}

	@Override
	public boolean onSupportNavigateUp() {
		onBackPressed();
		return super.onSupportNavigateUp();
	}



	protected void addSeatRow(SeatsRow pRow) {
		final TableRow seatsRow = (TableRow) LayoutInflater.from(this).inflate(R.layout.item_view_movie_seats_row, mSeatsTableLayout, false);
		seatsRow.setGravity(Gravity.CENTER_VERTICAL);
		seatsRow.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.MATCH_PARENT, 1f));

		final TextView rowNameTextView = new TextView(getApplicationContext());
		rowNameTextView.setText(pRow.getLetter());
		rowNameTextView.setTextColor(ContextCompat.getColor(this, android.R.color.primary_text_light));
		rowNameTextView.setPadding(8, 8, 8, 8);
		seatsRow.addView(rowNameTextView);

		if (pRow.isInitialGangway()) {
			addGangwaySeats(pRow.getIsInitialGangwayCount(), seatsRow);
		}

		final List<String> allSeats = pRow.getAllSeats();
		String previouslyAddedSeat = "";
		for (final String seat : allSeats) {
			if (pRow.isGangway()) {
				final List<String> gangwaySeats = pRow.getGangwaySeats();
				if (gangwaySeats.contains(previouslyAddedSeat)) {
					addGangwaySeats(pRow.getGangwayCounts()[gangwaySeats.indexOf(previouslyAddedSeat)], seatsRow);
				}
			}

			CheckBox cbSeats = (CheckBox)LayoutInflater.from(this).inflate(R.layout.view_movie_seat, seatsRow, false);

			Drawable selectorSeatDrawable = ContextCompat.getDrawable(this, R.drawable.selector_movie_seat_normal);
			if (pRow.isFamily()) {
				selectorSeatDrawable = ContextCompat.getDrawable(this, R.drawable.selector_movie_seat_family);
			}
			if (!pRow.getAvailableSeats().contains(seat)) {
				cbSeats.setEnabled(false);
				cbSeats.setOnCheckedChangeListener(null);
			} else {
				final Double mTicketsCost=pRow.getmCost();
				cbSeats.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
					@Override
					public void onCheckedChanged(final CompoundButton buttonView, final boolean isChecked) {
						if (mOnSeatSelectionChangedListener != null) {
							if (isChecked) {
								mOnSeatSelectionChangedListener.onSeatSelected(seat,mTicketsCost);
							} else {
								mOnSeatSelectionChangedListener.onSeatUnselected(seat,mTicketsCost);
							}
						}
					}
				});
			}
			cbSeats.setBackground(selectorSeatDrawable);
			String seatNumber = seat.substring(1);
			cbSeats.setText(seatNumber);
			seatsRow.addView(cbSeats);
			previouslyAddedSeat = seat;
		}

		mSeatsTableLayout.addView(seatsRow);
	}

	private void addGangwaySeats(final int gangwayCount, final TableRow seatsRow) {
		for (int i = 0; i < gangwayCount; i++) {
			CheckBox cbSeats = (CheckBox)LayoutInflater.from(this).inflate(R.layout.view_movie_seat, seatsRow, false);
			cbSeats.setBackground(ContextCompat.getDrawable(this, R.drawable.selector_movie_seat_normal));
			cbSeats.setEnabled(false);
			cbSeats.setVisibility(View.INVISIBLE);
			seatsRow.addView(cbSeats);
		}
	}

/*	protected void addSeatRow(final SeatsRow pRow) {
		final TableRow seatsRow = (TableRow) LayoutInflater.from(this).inflate(R.layout.item_view_movie_seats_row, mSeatsTableLayout, false);
		seatsRow.setGravity(Gravity.CENTER_VERTICAL);
		seatsRow.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.MATCH_PARENT, 1f));

		final TextView rowNameTextView = new TextView(getApplicationContext());
		rowNameTextView.setText(pRow.getLetter());
		rowNameTextView.setTextColor(ContextCompat.getColor(this, android.R.color.primary_text_light));
		rowNameTextView.setPadding(8, 8, 8, 8);
		seatsRow.addView(rowNameTextView);

		if (pRow.isInitialGangway()) {
			addGangwaySeats(pRow.getIsInitialGangwayCount(), seatsRow);
		}

		final List<String> allSeats = pRow.getAllSeats();
		String previouslyAddedSeat = "";
		for (final String seat : allSeats) {
			if (pRow.isGangway()) {
				final List<String> gangwaySeats = pRow.getGangwaySeats();
				if (gangwaySeats.contains(previouslyAddedSeat)) {
					addGangwaySeats(pRow.getGangwayCounts()[gangwaySeats.indexOf(previouslyAddedSeat)], seatsRow);
				}
			}

			final ToggleButton seatToggleButton = (ToggleButton) LayoutInflater.from(this).inflate(R.layout.view_movie_seat, seatsRow, false);
			Drawable selectorSeatDrawable = ContextCompat.getDrawable(this, R.drawable.seat_bg);
			if (pRow.isFamily()) {
				selectorSeatDrawable = ContextCompat.getDrawable(this, R.drawable.selector_movie_seat_family);
			}
			if (!pRow.getAvailableSeats().contains(seat)) {
				seatToggleButton.setEnabled(false);
				seatToggleButton.setOnCheckedChangeListener(null);
			} else {
				final Double price=pRow.getmCost();
				seatToggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
					@Override
					public void onCheckedChanged(final CompoundButton buttonView, final boolean isChecked) {
						if (mOnSeatSelectionChangedListener != null) {
							if (isChecked) {
								mOnSeatSelectionChangedListener.onSeatSelected(seat,price);
							} else {
								mOnSeatSelectionChangedListener.onSeatUnselected(seat,price);
							}
						}
					}
				});
			}
			seatToggleButton.setCompoundDrawablesWithIntrinsicBounds(selectorSeatDrawable, null, null, null);
			seatsRow.addView(seatToggleButton);
			previouslyAddedSeat = seat;
		}

		mSeatsTableLayout.addView(seatsRow);
	}

	private void addGangwaySeats(final int gangwayCount, final TableRow seatsRow) {
		for (int i = 0; i < gangwayCount; i++) {
			final ToggleButton seatToggleButton = (ToggleButton) LayoutInflater.from(this).inflate(R.layout.view_movie_seat, seatsRow, false);
			seatToggleButton.setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(this, R.drawable.selector_movie_seat_normal), null, null, null);
			seatToggleButton.setEnabled(false);
			seatToggleButton.setVisibility(View.INVISIBLE);
			seatsRow.addView(seatToggleButton);
		}
	}*/

	public void setOnSeatSelectionChangedListener(final OnSeatSelectionChangedListener pOnSeatSelectionChangedListener) {
		mOnSeatSelectionChangedListener = pOnSeatSelectionChangedListener;
	}

	protected interface OnSeatSelectionChangedListener {
		//void onSeatSelected(final String seat);

		//void onSeatUnselected(final String seat);

		void onSeatSelected(final String seat, Double mCost);

		void onSeatUnselected(final String seat, Double price);
	}
}
