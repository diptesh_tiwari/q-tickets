package com.mobile.android.qtickets.model;


import java.util.Date;

/**
 * Created by Indglobal on 4/29/2017.
 */

public class DateItem{

    public Date datess;
    public String available;

    public DateItem(Date datess, String available) {
        this.datess = datess;
        this.available = available;
    }

    public Date getDatess() {
        return datess;
    }

    public void setDatess(Date datess) {
        this.datess = datess;
    }

    public String getAvailable() {
        return available;
    }

    public void setAvailable(String available) {
        this.available = available;
    }
}
