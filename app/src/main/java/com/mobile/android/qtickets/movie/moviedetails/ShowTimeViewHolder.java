package com.mobile.android.qtickets.movie.moviedetails;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mobile.android.qtickets.R;

/**
 * Created by hexalabssd on 30/01/17.
 */

final class ShowTimeViewHolder extends RecyclerView.ViewHolder {
	private TextView mShowTimeTextView;

	ShowTimeViewHolder(ViewGroup parent) {
		super(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_view_show_times, parent, false));
		mShowTimeTextView = (TextView) itemView.findViewById(R.id.textView_showTime);
	}

	void setShowTimeText(final String pShowTimeText) {
		mShowTimeTextView.setText(pShowTimeText);
	}

	void setOnShowTimeItemClickListener(View.OnClickListener pShowTimeItemClickListener) {
		itemView.setOnClickListener(pShowTimeItemClickListener);
	}

	public void setAvailablity(final boolean pAvailablity) {
		mShowTimeTextView.setEnabled(pAvailablity);
	}
}
