package com.mobile.android.qtickets.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.mobile.android.qtickets.R;
import com.mobile.android.qtickets.movie.ShowTimeVO;

import java.util.ArrayList;

/**
 * Created by Satish - 360 on 11/30/2016.
 */

public class QTTimingsAdapter extends RecyclerView.Adapter<QTTimingsAdapter.ViewHolder> implements View.OnClickListener {
	private Context mContext;
	private ArrayList<ShowTimeVO> showsList;

	public QTTimingsAdapter(Context ctx, ArrayList<ShowTimeVO> listShows) {
		mContext = ctx;
		showsList = listShows;
	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View v = LayoutInflater.from(mContext).inflate(R.layout.rvadapter_show_timings, parent, false);
		ViewHolder mViewHolder = new ViewHolder(v, viewType);
		return mViewHolder;
	}

	@Override
	public void onBindViewHolder(final ViewHolder holder, final int position) {
//        Picasso.with(mContext).load(dataBranches.get(position).getImageNews()).into(holder.iv_News_Image);
//        holder.tv_NewsTitle.setText(dataBranches.get(position).getNewsTitle());
//        holder.tv_NewsDescription.setText(dataBranches.get(position).getNewsDescription());

//        Picasso.with(mContext).load(R.drawable.restaurant).into(holder.iv_News_Image);
//        holder.tv_NewsTitle.setText("News Title");
//        holder.tv_NewsDescription.setText(R.string.sample_text);

		holder.btn_shows.setText(showsList.get(position).showTime);

	}

	@Override
	public int getItemCount() {
		return showsList.size();
	}

	@Override
	public void onClick(View view) {

	}

	public class ViewHolder extends RecyclerView.ViewHolder {
		private Button btn_shows;

		public ViewHolder(View itemView, int viewType) {
			super(itemView);
			btn_shows = (Button) itemView.findViewById(R.id.btn_timings);
		}
	}
}
