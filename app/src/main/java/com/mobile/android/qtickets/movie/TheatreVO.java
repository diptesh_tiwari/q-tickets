package com.mobile.android.qtickets.movie;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by Satish - 360 on 11/28/2016.
 */

public class TheatreVO implements Parcelable {
	public static final Creator<TheatreVO> CREATOR = new Creator<TheatreVO>() {

		public TheatreVO createFromParcel(Parcel in) {
			return new TheatreVO(in);
		}

		public TheatreVO[] newArray(int size) {
			return new TheatreVO[size];
		}

	};
	public int localTheatreId;
	public String serverId, theatreName, logo, state, address, arabicname;
	public ArrayList<ShowDateVO> showDatesArr;

	public TheatreVO(int localTheatreId, String serverId, String theatreName, String logo, String state, String address, String phone, String arabicname) {
		super();
		this.localTheatreId = localTheatreId;
		this.serverId = serverId;
		this.theatreName = theatreName;
		this.logo = logo;
		this.state = state;
		this.address = address;
		this.arabicname = arabicname;
	}

	public TheatreVO() {
		// TODO Auto-generated constructor stub
//		showDatesArr = new ArrayList<ShowDateVO>();

	}

	public TheatreVO(Parcel in) {
		// TODO Auto-generated constructor stub
		readFromParcel(in);
	}

	public void readFromParcel(Parcel in) {
		localTheatreId = in.readInt();
		serverId = in.readString();
		theatreName = in.readString();
		logo = in.readString();
		state = in.readString();
		address = in.readString();
		arabicname = in.readString();
		showDatesArr = new ArrayList<ShowDateVO>();
		in.readList(showDatesArr, getClass().getClassLoader());
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub

		dest.writeInt(localTheatreId);
		dest.writeString(serverId);
		dest.writeString(theatreName);
		dest.writeString(logo);
		dest.writeString(state);
		dest.writeString(address);
		dest.writeString(arabicname);
		dest.writeList(showDatesArr);

	}
}
