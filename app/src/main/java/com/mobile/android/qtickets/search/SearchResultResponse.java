package com.mobile.android.qtickets.search;

import com.google.gson.annotations.SerializedName;

import in.hexalab.abstractvolley.base.HResponseBody;

/**
 * Created by hexalabssd on 23/01/17.
 */

public final class SearchResultResponse extends HResponseBody {

	@SerializedName("items")
	private SearchResult[] mSearchItems;

	public SearchResult[] getSearchItems() {
		return mSearchItems;
	}
}
