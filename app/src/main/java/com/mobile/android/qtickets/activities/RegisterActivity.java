package com.mobile.android.qtickets.activities;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mobile.android.qtickets.R;
import com.mobile.android.qtickets.constants.AppConstants;
import com.mobile.android.qtickets.model.CountriesModel;
import com.mobile.android.qtickets.model.UserDetailsModel;
import com.mobile.android.qtickets.network.InternetConnectionDetector;
import com.mobile.android.qtickets.parsers.CountryParseOperation;
import com.mobile.android.qtickets.parsers.RegisterParseOperation;
import com.mobile.android.qtickets.utils.QTUtils;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;

import java.io.ByteArrayInputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import static com.mobile.android.qtickets.utils.QTUtils.getASCIIContentFromEntity;

/**
 * Created by Satish - 360 on 11/17/2016.
 */

public class RegisterActivity  extends AppCompatActivity implements View.OnClickListener, Spinner.OnItemSelectedListener {
    String GCMTOCKEN, status, error_msg, error_code;
    UserDetailsModel user;
    ArrayList<CountriesModel> countryArr = new ArrayList<>();
    ArrayList<String> allCountryNationality = new ArrayList<>();
    ArrayList<String> allPrefix = new ArrayList<>();
    InternetConnectionDetector connectionDetector;
    //private ImageView iv_back;
    private TextView tv_login;
    private EditText et_dob, et_firstName, et_lastName, et_email, et_phone, et_password, et_confirmPassword,nationalityTV;
    private Spinner spn_gender, spn_prefix, spn_nationality;
    private DatePickerDialog dobPicker;
    private SimpleDateFormat dateFormatter;
    private TextView btn_register;
    private String firstName, lastName, dateOfBirth, emailId, phoneNumber, mPassword, mConfirmPassword, mPrefix, mGender, mNationality, fid = "";
    private ProgressDialog dialog = null;
    private ImageView backBtn;
    private String gender = "Male";

    RadioGroup rgGender;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        connectionDetector = new InternetConnectionDetector(this);

        SharedPreferences mPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        GCMTOCKEN = mPreferences.getString(AppConstants.SHARED_GCM_REG_ID_TAG, "");

        //iv_back = (ImageView) findViewById(R.id.btn_back_register);
        tv_login = (TextView) findViewById(R.id.tv_login);
        spn_gender = (Spinner) findViewById(R.id.spn_gender);
        spn_prefix = (Spinner) findViewById(R.id.spn_prefix);
        spn_nationality = (Spinner) findViewById(R.id.spn_nationality);
        et_dob = (EditText) findViewById(R.id.tv_dob_register);
        et_firstName = (EditText) findViewById(R.id.et_first_name_register);
        et_lastName = (EditText) findViewById(R.id.et_last_name_register);
        et_email = (EditText) findViewById(R.id.et_email_register);
        et_phone = (EditText) findViewById(R.id.et_phone_register);
        et_password = (EditText) findViewById(R.id.et_password_register);
        et_confirmPassword = (EditText) findViewById(R.id.et_confirm_password_register);
        //nationalityTV = (EditText) findViewById(R.id.nationalityTV);
        btn_register = (TextView) findViewById(R.id.btn_register);
        backBtn = (ImageView) findViewById(R.id.backBtn);
        rgGender = (RadioGroup)findViewById(R.id.rgGender);

        //iv_back.setOnClickListener(this);
        tv_login.setOnClickListener(this);
        spn_gender.setOnItemSelectedListener(this);
        spn_prefix.setOnItemSelectedListener(this);
        spn_nationality.setOnItemSelectedListener(this);
        et_dob.setOnClickListener(this);
        btn_register.setOnClickListener(this);
        backBtn.setOnClickListener(this);

        rgGender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, @IdRes int i) {
                switch (i){
                    case R.id.rbMale:
                        gender = "Male";
                        break;

                    case R.id.rbFemale:
                        gender = "Female";
                        break;
                }
            }
        });


        String first = "If you're already a member, ";
        String next = "<font color='#0027c4'>Login to your Account</font>";
        //tv_login.setText(Html.fromHtml(first + next));

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (connectionDetector.isConnectedToInternet()) {
                    // Do something after 2s = 2000ms
                    new CountryListUrl().execute();
                }
            }
        }, 300);

        initGenderSpinner();
        setDateTimeField();
    }

    private void setDateTimeField() {
        dateFormatter = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        Calendar newCalendar = Calendar.getInstance();
        dobPicker = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                et_dob.setText(dateFormatter.format(newDate.getTime()));
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
    }

    private void initGenderSpinner() {
        List<String> gender = new ArrayList<String>();
        gender.add("--Select Gender--");
        gender.add("Male");
        gender.add("Female");

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, gender);
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        spn_gender.setAdapter(dataAdapter);
    }

    @Override
    protected void onStart() {
        super.onStart();// ATTENTION: This was auto-generated to implement the App Indexing API.
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public void onClickDateOfBirthEditText(View pView) {
        final Calendar calendar = Calendar.getInstance();
        new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(final DatePicker view, final int year, final int month, final int dayOfMonth) {
                calendar.set(year, month, dayOfMonth);
                et_dob.setText(new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(calendar.getTime()));
            }
        }, calendar.get(Calendar.YEAR) - 20, calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    private void initializeProgressDialog() {
        dialog = new ProgressDialog(this, R.style.progress_bar_style);
        dialog.getWindow().setGravity(Gravity.TOP);
        WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
        params.y = 200;
        dialog.getWindow().setAttributes(params);
        dialog.show();
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
    }

    private void initNationalitySpinner() {

        SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        Gson gson = new Gson();
        String country = mPrefs.getString(AppConstants.SHARED_COUNTRIES_TAG, null);
        if (country != null) {
            java.lang.reflect.Type type = new TypeToken<List<CountriesModel>>() {
            }.getType();
            countryArr = gson.fromJson(country, type);
            if (countryArr.size() != 0) {
                bindSpinners();
            }
        }

    }

    private void bindSpinners() {
        for (int i = 0; i < countryArr.size(); i++) {
            allCountryNationality.add(countryArr.get(i).CountryNationality);
//            alphonecodes.addEventTicket("+" + countryArr.get(i).Countryprefix + "  " + countryArr.get(i).Countryname);
            allPrefix.add("+" + countryArr.get(i).Countryprefix);
        }

        setupNationalityAdapter(allCountryNationality);
        Log.v("Countries :", allCountryNationality.toString());

        setupPrefixAdapter(allPrefix);

    }

    private void setupPrefixAdapter(ArrayList<String> allPrefixes) {
        // Creating adapter for spinner
        ArrayAdapter<String> countryAdapter = new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_spinner_item, allPrefixes);
        // Drop down layout style - list view with radio button
        countryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        spn_prefix.setAdapter(countryAdapter);
    }

    private void setupNationalityAdapter(ArrayList<String> allNationality) {
        // Creating adapter for spinner
        ArrayAdapter<String> countryAdapter = new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_spinner_item, allNationality);
        // Drop down layout style - list view with radio button
        countryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        spn_nationality.setAdapter(countryAdapter);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            /*case R.id.btn_back_register:
                Intent backIntent = new Intent(RegisterActivity.this, LoginActivity.class);
            startActivity(backIntent);
            break;*/
            case R.id.backBtn:
                finish();
                break;
            case R.id.tv_login:
                Intent loginIntent = new Intent(RegisterActivity.this, LoginActivity.class);
                startActivity(loginIntent);
                break;

            case R.id.tv_dob_register:
                dobPicker.getDatePicker().setMaxDate(System.currentTimeMillis());
                dobPicker.show();
                break;

            case R.id.btn_register:

                if (connectionDetector.isConnectedToInternet()) {
                    if (validateAllFields()) {
                        new RegisterUrl(this).execute();
                    }
                } else {
                    new AlertDialog.Builder(RegisterActivity.this).setMessage("No Internet Connection").setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).show();
                }
                break;
        }
    }

    private boolean validateAllFields() {
        // TODO Auto-generated method stub
        if (!et_firstName.getText().toString().isEmpty()) {
            if (!et_lastName.getText().toString().isEmpty()) {
                if (!et_phone.getText().toString().isEmpty()) {
                    if (!et_email.getText().toString().isEmpty()) {
                        if (!et_password.getText().toString().isEmpty()) {
                            if (!et_confirmPassword.getText().toString().isEmpty()) {
                                if (et_password.getText().toString().trim().toLowerCase().equalsIgnoreCase(et_confirmPassword.getText().toString().trim().toLowerCase())) {
                                    return true;
                                } else {
                                    et_confirmPassword.requestFocus();
                                    et_confirmPassword.setError("Password Miss Matched");
                                    return false;
                                }
                            } else {
                                et_confirmPassword.requestFocus();
                                et_confirmPassword.setError("Enter Confirm Password");
                                return false;
                            }
                        } else {
                            et_password.requestFocus();
                            et_password.setError("Enter Password");
                            return false;
                        }
                    } else {
                        et_email.requestFocus();
                        et_email.setError("Enter email id");
                        return false;
                    }


                } else {
                    et_phone.requestFocus();
                    et_phone.setError("Enter Phone Number");
                    return false;

                }
            } else {
                et_lastName.requestFocus();
                et_lastName.setError("Enter Last Name");
                return false;
            }
        } else {
            et_firstName.requestFocus();
            et_firstName.setError("Enter First Name");
            return false;
        }

    }

    protected ArrayList<CountriesModel> parseCountryResponse(String result) {
        ArrayList<CountriesModel> conty = null;
        try {
            //two lines of code for xmlpullparsing
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(result.getBytes());
            CountryParseOperation xmlpull = new CountryParseOperation(byteArrayInputStream);
            conty = xmlpull.countryArr;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return conty;
    }

    protected void parseUserRegistrationResponse(String result) {
        // TODO Auto-generated method stub
        Log.e("status.....", status + "");
        try {
            //two lines of code for xmlpullparsing
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(result.getBytes());
            RegisterParseOperation xmlpull = new RegisterParseOperation(byteArrayInputStream);
            status = xmlpull.status;
            error_code = xmlpull.errorCode;
            error_msg = xmlpull.errormsg;
            user = xmlpull.user;
            Log.e("status..............", status + "");
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        switch (adapterView.getId()) {
            case R.id.spn_gender:
                String genderSelected = adapterView.getItemAtPosition(i).toString();
                break;

            case R.id.spn_prefix:
                String prefixSelected = adapterView.getItemAtPosition(i).toString();
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    public class CountryListUrl extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... arg0) {
            try {
                String resp;
                try {
                    String country_url = AppConstants.SERVER_URL_GET_ALL_COUNTRIES;
                    HttpClient httpClient = new DefaultHttpClient();
                    HttpContext localContext = new BasicHttpContext();
                    HttpGet httpGet = new HttpGet(country_url);
                    httpGet.addHeader(BasicScheme.authenticate(new UsernamePasswordCredentials(getResources().getString(R.string.auth_username), getResources().getString(R.string.auth_password)), "UTF-8", false));
                    HttpResponse response = httpClient.execute(httpGet, localContext);
                    HttpEntity entity = response.getEntity();
                    resp = QTUtils.getASCIIContentFromEntity(entity);
                    countryArr = parseCountryResponse(resp);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {

            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            initializeProgressDialog();

        }


        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            dialog.dismiss();
            try {
                if (countryArr.size() > 0) {
                    SharedPreferences prefmPrefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                    SharedPreferences.Editor editor = prefmPrefs.edit();
                    Gson gson = new Gson();
                    String countryjson = gson.toJson(countryArr);
                    editor.putString(AppConstants.SHARED_COUNTRIES_TAG, countryjson);
                    editor.commit();

                } else {
                    countryArr.clear();
                    new CountryListUrl().execute();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            initNationalitySpinner();
        }

    }

    public class RegisterUrl extends AsyncTask<Void, Void, Void> {
        private Context mContext;

        public RegisterUrl(final Context pContext) {
            mContext = pContext;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            firstName = et_firstName.getText().toString().trim();
            lastName = et_lastName.getText().toString().trim();
            emailId = et_email.getText().toString().trim();
            mPassword = et_password.getText().toString().trim();
            mConfirmPassword = et_confirmPassword.getText().toString().trim();
            phoneNumber = et_phone.getText().toString().trim();
            mPrefix = spn_prefix.getSelectedItem().toString();
            mPrefix = mPrefix.replaceAll("[-+.^:,]", "");
            mNationality = spn_nationality.getSelectedItem().toString();
            dateOfBirth = et_dob.getText().toString();
            mGender = spn_gender.getSelectedItem().toString();
            if (spn_nationality.getSelectedItemPosition() == 0) {
                mNationality = "";
            }
            initializeProgressDialog();
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            String resp;
            try {
                String registration_url = AppConstants.SERVICE_URL_GET_USER_REGISTRATION +
                        AppConstants.SERVICE_MSG_GET_USER_REGISTRATION_FIRST_NAME + firstName +
                        AppConstants.SERVICE_MSG_GET_USER_REGISTRATION_LAST_NAME +
                        lastName + AppConstants.SERVICE_MSG_GET_USER_REGISTRATION_PREFIX + mPrefix +
                        AppConstants.SERVICE_MSG_GET_USER_REGISTRATION_PHONE + phoneNumber +
                        AppConstants.SERVICE_MSG_GET_USER_REGISTRATION_EMAIL_ID + emailId +
                        AppConstants.SERVICE_MSG_GET_USER_REGISTRATION_PASSWORD + mPassword +
                        AppConstants.SERVICE_MSG_GET_USER_REGISTRATION__CONFIRM_PASSWORD + mConfirmPassword +
                        AppConstants.SERVICE_MSG_GET_USER_REGISTRATION__FID + fid +
                        AppConstants.SERVICE_MSG_GET_USER_REGISTRATION__NATIONNALITY +
                        mNationality + AppConstants.SERVICE_MSG_GET_USER_REGISTRATION__DOB + dateOfBirth
                        + AppConstants.SERVICE_MSG_GET_USER_REGISTRATION__GENDER + gender +
                        AppConstants.SERVICE_MSG_GET_USER_REGISTRATION_SOURCE + "4" +
                        AppConstants.SERVICE_MSG_GET_USER_REGISTRATION_TOCKEN + GCMTOCKEN;
                registration_url = registration_url.replaceAll("\\s+", "%20");
                HttpClient httpClient = new DefaultHttpClient();
                HttpContext localContext = new BasicHttpContext();
                HttpGet httpGet = new HttpGet(registration_url);
                httpGet.addHeader(BasicScheme.authenticate(new UsernamePasswordCredentials(getResources().getString(R.string.auth_username), getResources().getString(R.string.auth_password)), "UTF-8", false));
                HttpResponse response = httpClient.execute(httpGet, localContext);
                HttpEntity entity = response.getEntity();
                resp = getASCIIContentFromEntity(entity);
                Log.e("url", registration_url);
                Log.e("response", resp + "");
                parseUserRegistrationResponse(resp);
            } catch (Exception e) {

            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            dialog.dismiss();
            try {
                if (status.equalsIgnoreCase(AppConstants.RESPONSE_TRUE_TAG)) {
                    SharedPreferences prefmPrefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                    SharedPreferences.Editor editor = prefmPrefs.edit();
                    Gson gson = new Gson();
                    String userdata = gson.toJson(user);
                    editor.putString(AppConstants.SHARED_USER_TAG, userdata);
                    editor.putString("login","1");
                    editor.commit();
                    new AlertDialog.Builder(mContext).setTitle("Success").setMessage("Registration has done. Now please login with your email address and password.").setNeutralButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(final DialogInterface dialog, final int which) {
                            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    }).create().show();
                } else if (status.equalsIgnoreCase(AppConstants.RESPONSE_FALSE_TAG)) {
                    new AlertDialog.Builder(RegisterActivity.this).setMessage(error_msg).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    }).show();
                }
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), "Server Not responding..", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        }
    }
}