package com.mobile.android.qtickets.event.booking;

import android.app.Activity;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.mobile.android.qtickets.QticketsApplication;
import com.mobile.android.qtickets.constants.AppConstants;
import com.mobile.android.qtickets.event.EventsDetailsActivity;
import com.mobile.android.qtickets.utils.QTUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hexalabssd on 20/03/17.
 */

public final class EventBookingInfo implements Parcelable {

	String eventId= QTUtils.getInstance().getFromSharedPreference(QticketsApplication.getInstance(), AppConstants.EVENT_ID);
	String boat_type= QTUtils.getInstance().getFromSharedPreference(QticketsApplication.getInstance(), AppConstants.BOAT_TYPE);


	public static final Creator<EventBookingInfo> CREATOR = new Creator<EventBookingInfo>() {
		@Override
		public EventBookingInfo createFromParcel(Parcel in) {
			return new EventBookingInfo(in);
		}

		@Override
		public EventBookingInfo[] newArray(int size) {
			return new EventBookingInfo[size];
		}
	};
	private final List<EventTicket> mEventTickets;

	public EventBookingInfo() {
		mEventTickets = new ArrayList<>();
	}

	protected EventBookingInfo(Parcel in) {
		mEventTickets = in.createTypedArrayList(EventTicket.CREATOR);
	}



	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeTypedList(mEventTickets);
	}

	public void addEventTicket(final EventTicket pEventTicket) {
		mEventTickets.add(pEventTicket);
	}

	public int getTotalTicketsCount() {
		int totalTicketsCount = 0;
		for (final EventTicket cost : mEventTickets) {
			final int count = cost.getTotalCount();
			totalTicketsCount += count;
		}
		return totalTicketsCount;
	}

	public double getTotalCost() {
		double totalCost = 0;
		for (final EventTicket cost : mEventTickets) {
			if(Integer.parseInt(eventId)==422){
				totalCost += cost.getRate() * 1;
			}else if((Integer.parseInt(eventId)==421)) {
				if(boat_type!=null&&boat_type.equalsIgnoreCase("3")){
					totalCost += cost.getRate() * 1;
				}else {

				}

			}else {
				totalCost += cost.getRate() * cost.getCount();
			}

		}
		return totalCost;
	}
}
