package com.mobile.android.qtickets.CustomFonts;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.EditText;
import android.widget.RadioButton;

/**
 * Created by Android on 4/20/17.
 */

public class EditTextRegular extends EditText {
    public EditTextRegular(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/SSPro_Regular.otf"));
    }
}