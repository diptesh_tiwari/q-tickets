package com.mobile.android.qtickets.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Satish - 360 on 12/7/2016.
 */

public class BookingClassVO implements Serializable {

	public ArrayList<BookingRowVO> bookingRowsArr;
	int noOfRows;
	String classId, className;
	float cost;

	public BookingClassVO() {
		super();
		bookingRowsArr = new ArrayList<BookingRowVO>();
	}

	public BookingClassVO(int noOfRows, String classId, String className, float cost) {
		super();
		this.noOfRows = noOfRows;
		this.classId = classId;
		this.className = className;
		this.cost = cost;
	}

	//		CGRect classFrame;
	public int getNoOfRows() {
		return noOfRows;
	}

	public void setNoOfRows(int noOfRows) {
		this.noOfRows = noOfRows;
	}

	public ArrayList<BookingRowVO> getBookingRowsArr() {
		return bookingRowsArr;
	}

	public void setBookingRowsArr(ArrayList<BookingRowVO> bookingRowsArr) {
		this.bookingRowsArr = bookingRowsArr;
	}

	public String getClassId() {
		return classId;
	}

	public void setClassId(String classId) {
		this.classId = classId;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public float getCost() {
		return cost;
	}

	public void setCost(float cost) {
		this.cost = cost;
	}
}
