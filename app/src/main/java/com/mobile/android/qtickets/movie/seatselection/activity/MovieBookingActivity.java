package com.mobile.android.qtickets.movie.seatselection.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mobile.android.qtickets.R;
import com.mobile.android.qtickets.constants.AppConstants;
import com.mobile.android.qtickets.event.EventDetailsVO;
import com.mobile.android.qtickets.model.BookedDetal;
import com.mobile.android.qtickets.model.CountriesModel;
import com.mobile.android.qtickets.model.MovieSeatBlockResult;
import com.mobile.android.qtickets.model.UserDetailsModel;
import com.mobile.android.qtickets.movie.moviedetails.activity.MovieDetailsActivity;
import com.mobile.android.qtickets.network.InternetConnectionDetector;
import com.mobile.android.qtickets.utils.QTUtils;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONObject;
import org.json.XML;

import java.util.ArrayList;
import java.util.List;

//import com.mobile.android.qtickets.PaymentGatewaysActivity;

public class MovieBookingActivity extends MovieBookingActivityController {
    private static final String EXTRA_EVENT_BOOKING_INFO = MovieBookingActivity.class.getName() + ".extra.EVENT_BOOKING_INFO";
    private static final String EXTRA_EVENT_DETAILS = MovieBookingActivity.class.getName() + ".extra.EVENT_DETAILS";
    InternetConnectionDetector connectionDetector;
    private double totalAmount;
    private ProgressDialog dialog = null;
    private String mMovieName, mMovieID, mMovieTheatre, mMovieDate, mMovieTime, mShowTimeId;
    private String seats;
    private MovieSeatBlockResult movieSeatBlockResult;
  //  private User mUserDetailsModel;
    private BookedDetal bookedDetal;
    UserDetailsModel user;
    String seatLockErrorMessage;
    int lockReq = 0;

    String name, number, email, prifix;
    boolean fromguest = false;

   /* private final PaymentDetailsListener mPaymentDetailsListener = new PaymentDetailsListener() {
        @Override
        public void onClickProceedPaymentButton(@Nullable final UserDetailsModel pUserDetailsModel) {
            loadPaymentView(pUserDetailsModel);
        }

        @Override
        public void onClickPayNowButton(final UserDetailsModel pUserDetailsModel, final int pPaymentType) {
            Toast.makeText(MovieBookingActivity.this, "Pay now button clicked", Toast.LENGTH_SHORT).show();
            String paymentGatewayUrl = "https://api.q-tickets.com/Qpayment-registration.aspx?";


            final String params = String.format(Locale.getDefault(),
                    "Currency=%s" +
                            "&Amount=%.2f" +
                            "&OrderName=online" +
                            "&OrderInfo=IPG" +
                            "&OrderID=%s" +
                            "&nationality=%s" +
                            "&paymenttype=%d"
                    ,
                    "QAR",
                    totalAmount,
                    "A" + new Random().nextInt(),
                    pUserDetailsModel.nationality,
                    pPaymentType
            );

            paymentGatewayUrl += params;

            PaymentGatewayActivity.Companion.start(MovieBookingActivity.this, Uri.parse(paymentGatewayUrl));
        }
    };*/
    private String mesg;
    private String massege;
    private String errormsg;


    private void requestToBookForEvent() {

    }

    //private EventBookingInfo mEventBookingInfo;
    private EventDetailsVO mEventDetailsVO;
    private ArrayList<CountriesModel> countryArr;
    private ArrayList<String> allPrefix = new ArrayList<>();

    /*  public  void start(final Context pContext, final EventBookingInfo pEventBookingInfo, final EventDetailsVO pEventDetailsVO) {

          final Bundle bundle = new Bundle();
          bundle.putParcelable(EXTRA_EVENT_BOOKING_INFO, pEventBookingInfo);
          bundle.putParcelable(EXTRA_EVENT_DETAILS, pEventDetailsVO);
          Intent intent= new Intent(this,MovieBookingActivity.class);
          intent.putExtras(bundle);
          startActivity(intent);
          //(pContext, MovieBookingActivity.class, bundle);
      }
  */
    @Override
    public void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String bookingDate = sharedPreferences.getString(AppConstants.MOVIE_BOOKING_DATE, null);

        Intent ii = getIntent();
        String from = ii.getStringExtra("fromGuest");
        movieSeatBlockResult = (MovieSeatBlockResult) ii.getSerializableExtra("movieSeatBlockResult");
        if (movieSeatBlockResult != null) {
            Integer totalPrice = movieSeatBlockResult.getTotalprice() != null ? movieSeatBlockResult.getTotalprice() : 0;
            setTotalCost(totalPrice);
        }


        if (from != null && from.equalsIgnoreCase("1")) {
            fromguest = true;
            name = ii.getStringExtra("name");
            number = ii.getStringExtra("number");
            email = ii.getStringExtra("email");
            prifix = ii.getStringExtra("prifix");

            mMovieName = getIntent().getStringExtra("EXTRA_MOVIE_NAME");
            mMovieID = getIntent().getStringExtra("EXTRA_MOVIE_ID");
            mMovieTheatre = getIntent().getStringExtra("EXTRA_MOVIE_THEATRE");
            if (bookingDate != null) {
                mMovieDate = bookingDate;
            }

            mMovieTime = getIntent().getStringExtra("EXTRA_MOVIE_TIME");
            mShowTimeId = getIntent().getStringExtra("EXTRA_SHOW_TIME_ID");
            ArrayList<String> mSelectedSeatsList = getIntent().getStringArrayListExtra("TOTAL PERSON");
            totalAmount = getIntent().getDoubleExtra("TOTAL Amount", 0);
            setEventTitle(mMovieName);
            setLocation(mMovieTheatre);
            mMovieDate = mMovieDate.replace("\"", "");
            setDate(mMovieDate, mMovieTime);

            seats = getListAsCsvString(mSelectedSeatsList);
            // setTotalCost(totalAmount);
            setNumberOfPersons(mSelectedSeatsList.size());
            setSeat(seats);

        } else {
            fromguest = false;
            mMovieName = getIntent().getStringExtra("EXTRA_MOVIE_NAME");
            mMovieID = getIntent().getStringExtra("EXTRA_MOVIE_ID");
            mMovieTheatre = getIntent().getStringExtra("EXTRA_MOVIE_THEATRE");
            if (bookingDate != null) {
                mMovieDate = bookingDate;
            }

            //mMovieDate = getIntent().getStringExtra("EXTRA_MOVIE_DATE");
            mMovieTime = getIntent().getStringExtra("EXTRA_MOVIE_TIME");
            mShowTimeId = getIntent().getStringExtra("EXTRA_SHOW_TIME_ID");
            ArrayList<String> mSelectedSeatsList = getIntent().getStringArrayListExtra("TOTAL PERSON");
            totalAmount = getIntent().getDoubleExtra("TOTAL Amount", 0);
            setEventTitle(mMovieName);
            setLocation(mMovieTheatre);
            mMovieDate = mMovieDate.replace("\"", "");
            setDate(mMovieDate, mMovieTime);

            //setDate(mMovieDate, mMovieDate + "," + mMovieTime);
            //setTotalCost(Double.parseDouble(totalAmount));

            seats = getListAsCsvString(mSelectedSeatsList);
            // setTotalCost(totalAmount);
            setNumberOfPersons(mSelectedSeatsList.size());

            SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            Gson gson = new Gson();
            String usrDetails = mPrefs.getString(AppConstants.SHARED_USER_TAG, null);

            if (usrDetails != null) {
                java.lang.reflect.Type type = new TypeToken<UserDetailsModel>() {
                }.getType();
                try {
                    user = gson.fromJson(usrDetails, type);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

        }

        connectionDetector = new InternetConnectionDetector(this);
//        final Handler handler = new Handler();
//        handler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                if (connectionDetector.isConnectedToInternet()) {
//                    new movieSuccessURL().execute();
//                }
//            }
//        }, 300);
    }


    public String getListAsCsvString(List<String> list) {

        StringBuilder sb = new StringBuilder();
        for (String str : list) {
            if (sb.length() != 0) {
                sb.append(",");
            }
            sb.append(str);
        }
        return sb.toString();

    }

   /* public class setBlockURL extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {

            try {
                // showid=397665&seats=F04,F03&AppSource=3&movie_id=12800&schedule_date=04/27/2017&show_time=07:30 PM
                String block_seats_qtickets = AppConstants.MOVIE_SEAT_BLOCK_URL + "showid=" + mShowTimeId + "&" + "seats=" +
                        seats + "&" + "AppSource=" + "3";
                String resp;
                if (connectionDetector.isConnectedToInternet()) {
                    try {
                        block_seats_qtickets = block_seats_qtickets.replaceAll("\\s+", "%20");
                        HttpClient httpClient = new DefaultHttpClient();
                        HttpContext localContext = new BasicHttpContext();
                        HttpGet httpGet = new HttpGet(block_seats_qtickets);
                        httpGet.addHeader(BasicScheme.authenticate(new UsernamePasswordCredentials(getResources().getString(R.string.auth_username), getResources().getString(R.string.auth_password)), "UTF-8", false));
                        HttpResponse response = httpClient.execute(httpGet, localContext);
                        HttpEntity entity = response.getEntity();
                        resp = QTUtils.getASCIIContentFromEntity(entity);
                        // CoreXMLSerializers xmlSerializer = new CoreXMLSerializers();

                        //JSONObject json = XML.toJSONObject(resp);

                        // MovieSeatBlockResult movieSeatBlockResult= (MovieSeatBlockResult) Api.fromJson(resp,MovieSeatBlockResult.class);
                        return resp;
                    } catch (Exception e) {
                        resp = "error";
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            initializeProgressDialog();

        }


        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            dialog.dismiss();
            try {
                if (result != null) {
                    movieSeatBlockResult = new MovieSeatBlockResult();
                    JSONObject json = XML.toJSONObject(result);
                    JSONObject jsonObject = json.getJSONObject("response");
                    JSONObject resultRes = jsonObject.getJSONObject("result");
                    if (resultRes != null) {
                        if (resultRes.getString("status").equalsIgnoreCase("true")) {
                            Toast.makeText(MovieBookingActivity.this, "1 Success", Toast.LENGTH_SHORT).show();
                            movieSeatBlockResult.setCurrency(resultRes.getString("currency"));
                            movieSeatBlockResult.setStatus(resultRes.getString("status"));
                            movieSeatBlockResult.setTransaction_Id(resultRes.getString("Transaction_Id"));
                            movieSeatBlockResult.setPageSessionTime(resultRes.getInt("PageSessionTime"));
                            movieSeatBlockResult.setTransactionTime(resultRes.getInt("TransactionTime"));
                            movieSeatBlockResult.setTicketprice(resultRes.getString("ticketprice"));
                            movieSeatBlockResult.setServicecharges(resultRes.getInt("servicecharges"));
                            movieSeatBlockResult.setTotalprice(resultRes.getInt("servicecharges"));

                            new sendLockRequestURL().execute();
                        } else {
                            massege = resultRes.getString("errormsg");
                            Toast.makeText(MovieBookingActivity.this, massege.toString(), Toast.LENGTH_SHORT).show();
                        }

                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            //initNationalitySpinner();
        }

    }*/

    public class sendLockRequestURL extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {


            try {
                String send_seats_lock = "";
                if (fromguest) {
                    send_seats_lock = AppConstants.MOVIE_SEND_LOCK_URL + "Transaction_Id=" + movieSeatBlockResult.getTransaction_Id() +
                            "&" + "name=" + name + "&" + "email=" + email + "&" + "mobile=" + number
                            + "&" + "prefix=" + prifix + "&" + "VoucherCodes=" + "null";
                } else {
                    send_seats_lock = AppConstants.MOVIE_SEND_LOCK_URL + "Transaction_Id=" + movieSeatBlockResult.getTransaction_Id() +
                            "&" + "name=" + user.userName + "&" + "email=" + user.emailId + "&" + "mobile=" + user.phoneNumber
                            + "&" + "prefix=" + user.prefix + "&" + "VoucherCodes=" + "null";
                }
                // showid=397665&seats=F04,F03&AppSource=3&movie_id=12800&schedule_date=04/27/2017&show_time=07:30 PM

                String resp;
                if (connectionDetector.isConnectedToInternet()) {
                    try {
                        send_seats_lock = send_seats_lock.replaceAll("\\s+", "%20");
                        HttpClient httpClient = new DefaultHttpClient();
                        HttpContext localContext = new BasicHttpContext();
                        HttpGet httpGet = new HttpGet(send_seats_lock);
                        httpGet.addHeader(BasicScheme.authenticate(new UsernamePasswordCredentials(getResources().getString(R.string.auth_username), getResources().getString(R.string.auth_password)), "UTF-8", false));
                        HttpResponse response = httpClient.execute(httpGet, localContext);
                        HttpEntity entity = response.getEntity();
                        resp = QTUtils.getASCIIContentFromEntity(entity);
                        return resp;
                    } catch (Exception e) {
                        resp = "error";
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            initializeProgressDialog();

        }


        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            dialog.dismiss();
            try {
                if (result != null) {
                    movieSeatBlockResult = new MovieSeatBlockResult();
                    JSONObject json = XML.toJSONObject(result);
                    JSONObject jsonObject = json.getJSONObject("response");
                    JSONObject resultRes = jsonObject.getJSONObject("result");
                    if (resultRes != null) {
                        if (resultRes.getString("status").equalsIgnoreCase("true")) {

                            String transactionid = resultRes.getString("Transaction_Id");
                            if (transactionid != null) {
                                new sendLockConfirmReq(transactionid).execute();
                            } else {
                                Toast.makeText(MovieBookingActivity.this, "Transaction id is empty!", Toast.LENGTH_SHORT).show();
                            }

                        } else {
                            errormsg = resultRes.getString("errormsg");
                            Log.d("MSG2", errormsg);
                            Toast.makeText(MovieBookingActivity.this, errormsg, Toast.LENGTH_SHORT).show();
                        }
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            //initNationalitySpinner();
        }

    }


    public class sendLockConfirmReq extends AsyncTask<String, String, String> {
        String transactionid;

        public sendLockConfirmReq(String transactionid) {
            this.transactionid = transactionid;
        }

        @Override
        protected String doInBackground(String... params) {
            String AppVersion = null;
            try {
                AppVersion = getApplicationContext().getPackageManager().getPackageInfo(getApplicationContext().getPackageName(), 0).versionName;
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
                Toast.makeText(MovieBookingActivity.this, "Appversion not found!", Toast.LENGTH_SHORT).show();
            }

            try {
                // showid=397665&seats=F04,F03&AppSource=3&movie_id=12800&schedule_date=04/27/2017&show_time=07:30 PM
                String lock_seats_confirm = AppConstants.MOVIE_SEND_LOCK_CONFIRM_URL + "Transaction_Id=" + transactionid + "&" + "AppSource=" + "3" + "&" + "AppVersion=" + AppVersion;
                String resp;
                if (connectionDetector.isConnectedToInternet()) {
                    try {
                        lock_seats_confirm = lock_seats_confirm.replaceAll("\\s+", "%20");
                        HttpClient httpClient = new DefaultHttpClient();
                        HttpContext localContext = new BasicHttpContext();
                        HttpGet httpGet = new HttpGet(lock_seats_confirm);
                        httpGet.addHeader(BasicScheme.authenticate(new UsernamePasswordCredentials(getResources().getString(R.string.auth_username), getResources().getString(R.string.auth_password)), "UTF-8", false));
                        HttpResponse response = httpClient.execute(httpGet, localContext);
                        HttpEntity entity = response.getEntity();
                        resp = QTUtils.getASCIIContentFromEntity(entity);
                        return resp;
                    } catch (Exception e) {
                        resp = "error";
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(MovieBookingActivity.this, "Exceptions", Toast.LENGTH_SHORT).show();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            initializeProgressDialog();
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            dialog.dismiss();
            try {
                if (result != null) {
                    movieSeatBlockResult = new MovieSeatBlockResult();
                    JSONObject json = XML.toJSONObject(result);
                    JSONObject jsonObject = json.getJSONObject("response");
                    JSONObject resultRes = jsonObject.getJSONObject("result");
                    if (resultRes != null) {
                        if (resultRes.getBoolean("status") == true) {
                           // Toast.makeText(MovieBookingActivity.this, "3 Success", Toast.LENGTH_SHORT).show();
                            bookedDetal = new BookedDetal();
                            bookedDetal.setTransactionid(resultRes.getString("Transaction_Id"));
                            bookedDetal.setBalance(resultRes.getInt("balance"));
                            bookedDetal.setMsg(resultRes.getString("msg"));
                            bookedDetal.setOrderInfo(resultRes.getString("OrderInfo"));
                            bookedDetal.setStatus(resultRes.getBoolean("status"));

                            if (bookedDetal != null) {
                                Intent intent = new Intent(MovieBookingActivity.this, MoviePaymentActivity.class);
                                intent.putExtra(AppConstants.BOOKED_DETAIL, bookedDetal);
                                startActivity(intent);
                            } else {
                                Toast.makeText(MovieBookingActivity.this, "Booking details are empty please try again later!", Toast.LENGTH_SHORT).show();
                            }

                        } else {
                            seatLockErrorMessage = resultRes.getString("msg");
                            lockReq = 1;
                            Toast.makeText(MovieBookingActivity.this, seatLockErrorMessage.toString(), Toast.LENGTH_SHORT).show();
                            //Toast.makeText(MovieBookingActivity.this, "Something went wrong please select seat again", Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        Toast.makeText(MovieBookingActivity.this, "Result object is null", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(MovieBookingActivity.this, "Result is null", Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(MovieBookingActivity.this, "Result Exception", Toast.LENGTH_SHORT).show();
            }
            //initNationalitySpinner();
        }

    }


    private void initializeProgressDialog() {
        dialog = new ProgressDialog(this, R.style.progress_bar_style);
        dialog.getWindow().setGravity(Gravity.CENTER);
        WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
        dialog.getWindow().setAttributes(params);
        dialog.show();
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.buttonLoginTV:
                if (lockReq == 1) {
                    Toast.makeText(this, seatLockErrorMessage.toString(), Toast.LENGTH_SHORT).show();
                    //Toast.makeText(this, "Something went wrong please select seat again", Toast.LENGTH_SHORT).show();
                } else {
                    if (movieSeatBlockResult != null) {

                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (connectionDetector.isConnectedToInternet()) {
                                    // Do something after 2s = 2000ms
                                    //new CountryListUrl().execute();
                                    new sendLockRequestURL().execute();
                                }
                            }
                        }, 300);


                        //new sendLockRequestURL().execute();
                    }
                }


                //   new movieSuccessURL().execute();
//                if (massege != null) {
//                    Toast.makeText(this, massege.toString(), Toast.LENGTH_LONG);
//                    Log.d("MERRORMSGS",massege);
//                } else if (errormsg != null) {
//                    Toast.makeText(this, errormsg.toString(), Toast.LENGTH_LONG);
//                    Log.d("ERRORMSGS",errormsg);
//                } else if (mesg != null) {
//                    Toast.makeText(this, mesg.toString(), Toast.LENGTH_LONG);
//                    Log.d("MSGS",mesg);
//                } else {
//                    if(bookedDetal!=null){
//                        Intent intent = new Intent(this, MoviePaymentActivity.class);
//                        intent.putExtra(AppConstants.BOOKED_DETAIL, bookedDetal);
//                        startActivity(intent);
//                    }else {
//                        Toast.makeText(MovieBookingActivity.this,"Booking details are empty please try again later!",Toast.LENGTH_SHORT).show();
//                    }
//                }

                break;

            case R.id.backBtn:
                onBackPressed();
                break;
        }
    }


    public void onBackPressed() {
        Intent intent = new Intent(this, MovieDetailsActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);

        finish();

    }

}
