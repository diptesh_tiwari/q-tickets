package com.mobile.android.qtickets.model;

/**
 * Created by Android on 4/28/17.
 */

public class BookedEventItem {

    public String id,Eventid,total_Cost,BookedOn,seatsCount,Event,location,eventDate,eventTime,confirmationCode,eventImageURL;

    public BookedEventItem(String id, String eventid, String total_Cost, String bookedOn, String seatsCount, String event, String location, String eventDate, String eventTime, String confirmationCode, String eventImageURL) {
        this.id = id;
        Eventid = eventid;
        this.total_Cost = total_Cost;
        BookedOn = bookedOn;
        this.seatsCount = seatsCount;
        Event = event;
        this.location = location;
        this.eventDate = eventDate;
        this.eventTime = eventTime;
        this.confirmationCode = confirmationCode;
        this.eventImageURL = eventImageURL;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEventid() {
        return Eventid;
    }

    public void setEventid(String eventid) {
        Eventid = eventid;
    }

    public String getTotal_Cost() {
        return total_Cost;
    }

    public void setTotal_Cost(String total_Cost) {
        this.total_Cost = total_Cost;
    }

    public String getBookedOn() {
        return BookedOn;
    }

    public void setBookedOn(String bookedOn) {
        BookedOn = bookedOn;
    }

    public String getSeatsCount() {
        return seatsCount;
    }

    public void setSeatsCount(String seatsCount) {
        this.seatsCount = seatsCount;
    }

    public String getEvent() {
        return Event;
    }

    public void setEvent(String event) {
        Event = event;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getEventDate() {
        return eventDate;
    }

    public void setEventDate(String eventDate) {
        this.eventDate = eventDate;
    }

    public String getEventTime() {
        return eventTime;
    }

    public void setEventTime(String eventTime) {
        this.eventTime = eventTime;
    }

    public String getConfirmationCode() {
        return confirmationCode;
    }

    public void setConfirmationCode(String confirmationCode) {
        this.confirmationCode = confirmationCode;
    }

    public String getEventImageURL() {
        return eventImageURL;
    }

    public void setEventImageURL(String eventImageURL) {
        this.eventImageURL = eventImageURL;
    }
}
