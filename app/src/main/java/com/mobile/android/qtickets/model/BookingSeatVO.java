package com.mobile.android.qtickets.model;

import java.io.Serializable;

/**
 * Created by Satish - 360 on 12/6/2016.
 */

public class BookingSeatVO implements Serializable {

	public BookingRowVO bookingRowVO;
	public String seatNumber;
	public SeatLayoutVO.SeatStatus seatStatus;
	public Boolean row_is_family;
	//		public Boolean getIsGangwaySeat() {
//			return isGangwaySeat;
//		}
//		public void setIsGangwaySeat(Boolean isGangwaySeat) {
//			this.isGangwaySeat = isGangwaySeat;
//		}
	public Boolean isNextGangway;
	int gangwayCount;

	//		CGRect seatFrame;
	public BookingSeatVO(int gangwayCount, String seatNumber, SeatLayoutVO.SeatStatus seatStatus, Boolean isNextGangway, Boolean row_is_family) {
		super();
		this.gangwayCount = gangwayCount;
		this.seatNumber = seatNumber;
		this.seatStatus = seatStatus;
		this.isNextGangway = isNextGangway;
		this.row_is_family = row_is_family;

	}

	public BookingSeatVO() {
		super();
		// TODO Auto-generated constructor stub
		bookingRowVO = new BookingRowVO();
	}

	public Boolean getRow_is_family() {
		return row_is_family;
	}

	public void setRow_is_family(Boolean row_is_family) {
		this.row_is_family = row_is_family;
	}

	public BookingRowVO getBookingRowVO() {
		return bookingRowVO;
	}

	public void setBookingRowVO(BookingRowVO bookingRowVO) {
		this.bookingRowVO = bookingRowVO;
	}

	public int getGangwayCount() {
		return gangwayCount;
	}

	public void setGangwayCount(int gangwayCount) {
		this.gangwayCount = gangwayCount;
	}

	public String getSeatNumber() {
		return seatNumber;
	}

	public void setSeatNumber(String seatNumber) {
		this.seatNumber = seatNumber;
	}

	public SeatLayoutVO.SeatStatus getSeatStatus() {
		return seatStatus;
	}

	public void setSeatStatus(SeatLayoutVO.SeatStatus seatStatus) {
		this.seatStatus = seatStatus;
	}

	public Boolean getIsNextGangway() {
		return isNextGangway;
	}

	public void setIsNextGangway(Boolean isNextGangway) {
		this.isNextGangway = isNextGangway;
	}

}