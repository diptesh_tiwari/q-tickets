package com.mobile.android.qtickets.constants;

import android.content.Context;

/**
 * Created by Satish - 360 on 11/16/2016.
 */

public class AppConstants {

	public static final int SPLASH_SCREEN_DURATION = 2000;


	public static final String SERVER_BASE_URL = "https://api.q-tickets.com/V2.0/";
	public static final String WEBSITE_BASE_URL = "https://www.q-tickets.com/";


	public static final String SERVICE_URL_GET_USER_LOGIN = SERVER_BASE_URL + "loginmobile?";
	public static final String SERVICE_URL_GET_USER_REGISTRATION = SERVER_BASE_URL + "Registration?";
	public static final String SERVICE_URL_GET_FORGOT_PASSWORD_REQUEST = SERVER_BASE_URL + "forgot_password?";
	public static final String SERVER_URL_GET_ALL_COUNTRIES = SERVER_BASE_URL + "getallcountries";
	public static final String SERVICE_URL_GET_THEATRE_LAYOUT = SERVER_BASE_URL + "gettheatrelayout?";
	public static final String SERVER_URL_GET_ALL_MOVIES = SERVER_BASE_URL + "GetMoviesbyLangAndTheatreid";
	public static final String SERVICE_URL_GET_ALL_CINEMAS = SERVER_BASE_URL + "GetAllLocations";
	//public static final String SERVER_URL_GET_ALL_EVENTT_DETAILS = SERVER_BASE_URL + "getalleventsdetails?";
	public static final String SERVER_URL_GET_ALL_EVENTT_DETAILS = SERVER_BASE_URL + "getalleventsdetailsbycountry?";
	public static final String SERVER_URL_GET_ALL_UP_COMING_CINEMAS = SERVER_BASE_URL + "getupcomingmovies";
	public static final String SERVER_URL_WATCH = SERVER_BASE_URL + "insertmoviefeed?";
	public static final String SERVER_URL_GET_WATCH_WILL = SERVER_BASE_URL + "Getmoviefeed?";
	public static final String MOVIE_SEAT_BLOCK_URL = SERVER_BASE_URL + "block_seats_qtickets?";
	public static final String MOVIE_SEND_LOCK_URL = SERVER_BASE_URL + "send_lock_request?";
	public static final String MOVIE_SEND_LOCK_CONFIRM_URL = SERVER_BASE_URL + "lock_confirm_request?";
	public static final String SERVER_URL_GET_BOOKEDLIST = SERVER_BASE_URL+ "userbookings?";
	public static final String SERVER_URL_GET_BOOKEDEVENTLIST = SERVER_BASE_URL+ "usereventbookings?";
	public static final String SERVER_URL_UPDATE_PROFILE = SERVER_BASE_URL+ "profileupdation?";
	public static final String SERVER_URL_CHANGE_PASS = SERVER_BASE_URL+ "changepassword?";
	public static final String SERVER_URL_FILTER_EVENT = SERVER_BASE_URL+ "geteventsbyfilters?";
	public static final String SERVER_URL_EVENT_PAYMENT = SERVER_BASE_URL+ "eventbookings?";
    public static final String SERVER_URL_GET_ALL_EVENTT_BY_ID =SERVER_BASE_URL+ "getalleventsdetailsbyeventid?" ;


    public static String GET_MOVIE_BY_ID=SERVER_BASE_URL+ "getmoviesbymovieid?";
	public static String VOUCHER_VALIDATE_URL=SERVER_BASE_URL+"checkcouponstatus?";

	//http://api.q-tickets.com/V2.0/bookingconfirmaionmovie?booking_id=1445959

	public static final String MOVIE_SEAT_CONFIRM_URL = SERVER_BASE_URL + "bookingconfirmaionmovie?";


//sendLockRequestURL

	//	USER LOGIN input parameters:
	public static final String SERVICE_MSG_GET_USER_LOGIN_EMAIL = "username=";
	public static final String SERVICE_MSG_GET_USER_LOGIN_PASSWORD = "&password=";
	public static final String SERVICE_MSG_GET_USER_LOGIN_SOURCE = "&source=";
	public static final String SERVICE_MSG_GET_USER_LOGIN_TOCKEN = "&token=";
	public static final String SHARED_COUNTRIES_TAG = "Country";
	public static final String SHARED_GCM_REG_ID_TAG = "GCMRegId";
	public static final String SHARED_USER_TAG = "User";

	//Forgot Password
	public static final String SERVICE_MSG_GET_FORGOT_PWD_EMAIL_ID_ID = "email_id=";
	public static final String SERVER_FORGOT_PWD_ERROR_MSG_TAG = "msg";

	// Common status parser tags
	public static final String SERVER_RESPONSE_TAG = "response";
	public static final String SERVER_RESULT_TAG = "result";
	public static final String SERVER_ERROR_TAG = "error";
	public static final String SERVER_STATUS_TAG = "status";
	public static final String SERVER_ERROR_CODE_TAG = "errorcode";
	public static final String SERVER_ERROR_MESSAGE_TAG = "errormsg";
	public static final String SERVER_RANDOMCODE_TAG = "randomCode";
	public static final String SERVER_TRANSID_TAG = "transID";

	public static final String RESPONSE_TRUE_TAG = "true";
	public static final String RESPONSE_FALSE_TAG = "false";

	// User status parser tags
	public static final String USER_TAG = "user";
	public static final String USER_ID_TAG = "id";
	public static final String USER_SERVER_ID_TAG = "user_server_id";

	public static final String USER_NAME_TAG = "name";
	public static final String USER_MOBILE_TAG = "mobile";
	public static final String USER_PREFIX_TAG = "prefix";
	public static final String USER_EMAIL_TAG = "email";
	public static final String USER_ADDRESS_TAG = "address";
	public static final String USER_VERIFY_TAG = "verify";
	public static final String USER_NATIONALITY_TAG = "nationality";

	// USER REGISTRATION	input parameters:

	public static final String SERVICE_MSG_GET_USER_REGISTRATION_FIRST_NAME = "firstname=";
	public static final String SERVICE_MSG_GET_USER_REGISTRATION_LAST_NAME = "&lastname=";
	public static final String SERVICE_MSG_GET_USER_REGISTRATION_PREFIX = "&prefix=";
	public static final String SERVICE_MSG_GET_USER_REGISTRATION_PHONE = "&phone=";
	public static final String SERVICE_MSG_GET_USER_REGISTRATION_EMAIL_ID = "&mailid=";
	public static final String SERVICE_MSG_GET_USER_REGISTRATION_PASSWORD = "&pwd=";
	public static final String SERVICE_MSG_GET_USER_REGISTRATION__CONFIRM_PASSWORD = "&confirmpwd=";
	public static final String SERVICE_MSG_GET_USER_REGISTRATION__FID = "&fid=";
	public static final String SERVICE_MSG_GET_USER_REGISTRATION__NATIONNALITY = "&nationality=";
	public static final String SERVICE_MSG_GET_USER_REGISTRATION__DOB = "&dob=";
	public static final String SERVICE_MSG_GET_USER_REGISTRATION__GENDER = "&gender=";
	public static final String SERVICE_MSG_GET_USER_REGISTRATION_SOURCE = "&source=";
	public static final String SERVICE_MSG_GET_USER_REGISTRATION_TOCKEN = "&token=";

	//COUNTRIES

	public static final String Countriesinfo_TAG = "Countriesinfo";
	public static final String Countriesinfo_COUNTRY_TAG = "country";
	public static final String Countriesinfo_COUNTRYID_TAG = "Countryid";
	public static final String Countriesinfo_Countryname_TAG = "Countryname";
	public static final String Countriesinfo_Countryprefix_TAG = "Countryprefix";
	public static final String Countriesinfo_CountryNationality_TAG = "CountryNationality";

	public static final String LAST_MODIFIED_TAG = "LastModified";

	public static final String SELECTED_CINEMA_TAG = "selected_cinema";
	public static final String SHARED_MOVIE_LAST_MODIFIED_TAG = "lastmodified";

	public static final String SHARED_MOVIES_TAG = "Movies";
	public static final String SHARED_THEATER_TAG = "Theaters";
	public static final String MOVIES_TAG = "Movies";
	public static final String MOVIE_TAG = "movie";
	public static final String MOVIE_ID_TAG = "id";
	//public static final String MOVIE_SERVER_ID_TAG = "id";
	public static final String MOVIE_NAME_TAG = "name";
	public static final String MOVIE_REGISTRATION_DATE_TAG = "rdate";
	public static final String MOVIE_REGISTRATION_DATE_TAG_UPCOMING = "release_date";
	public static final String MOVIE_THUMB_URL_TAG = "thumbURL";
	public static final String MOVIE_LANGUAGE_ID_TAG = "Languageid";
	public static final String MOVIE_LANGUAGE_ID_TAG_UPCOMING = "language";
	public static final String MOVIE_CENSOR_TAG = "Censor";
	public static final String MOVIE_DURATION_TAG = "Duration";
	public static final String MOVIE_DESCRIPTION_TAG = "Description";
	public static final String MOVIE_SYNOPSIS_TAG = "synopsis";
	public static final String MOVIE_TYPE_TAG = "MovieType";
	public static final String UPCOMING_MOVIE_TYPE_TAG = "movie_type";
	public static final String MOVIE_ID = "id";
	public static final String NO_MOVIE_ID ="" ;
	public static final String MOVIE_BG_COLOR_CODE_TAG = "bgcolorcode";
	public static final String MOVIE_BG_BORDER_COLOR_TAG = "bgbordercolorcode";
	public static final String MOVIE_BTN_COLOR_TAG = "btncolorcode";
	public static final String MOVIE_TITLE_COLOR_CODE_TAG = "titlecolorcode";
	public static final String MOVIE_CASTE_AND_CREW_TAG = "CastAndCrew";
	public static final String MOVIE_TRIALER_URL_TAG = "TrailerURL";
	public static final String MOVIE_IMDB_rating_TAG = "IMDB_rating";
	public static final String MOVIE_MOVIEURL_TAG = "Movieurl";
	public static final String MOVIE_THUMBNAIL_TAG = "thumbnail";
	public static final String MOVIE_BANAERURL_TAG = "banner";
	public static final String MOVIE_TYPE_2D = "2D";
	public static final String MOVIE_TYPE_3D = "3D";
	public static final String MOVIE_IPHONE_THUMB = "iphonethumb";
	public static final String MOVIE_IPAD_THUMB = "ipadthumb";
	public static final String MOVIE_TYPE_NO_NAME = "";
	public static final String MOVIE_AGE_RESTICTED_RATING = "AgeRestrictRating";

	// Theatre parser tags
	public static final String THEATRES_TAG = "Theatres";
	public static final String THEATRE_TAG = "Theatre";
	public static final String THEATRE_ID_TAG = "id";
	public static final String THEATRE_NAME_TAG = "name";
	public static final String THEATRE_LOGO_TAG = "logo";
	public static final String THEATRE_STATE_TAG = "state";
	public static final String THEATRE_ADDRESS_TAG = "address";
	public static final String THEATRE_PHONE_TAG = "phone";
	public static final String THEATRE_ARABICNAME_TAG = "arabicname";

	//Booking cancel status
	public static final String BOOKING_CANCEL_TAG = "bookingCancel";
	//TEREMS AND CONDITIONS
	public static final String TERMSAND_CONDITIONS_TAG = "Termsandcondition";
	public static final String CONDITIONS_TAG = "condition";

	public static final String BOOKING_CANCEL_MESSAGE_TAG = "message";
	// ShowDates parser tags
	public static final String SHOW_DATES_TAG = "ShowDates";
	public static final String SHOW_DATE_TAG = "showDate";
	public static final String SHOW_DATE_ID_TAG = "id";
	public static final String SHOW_DATE_DATE_TAG = "Date";

	// ShowDates parser tags
	public static final String SHOW_TIMES_TAG = "ShowTimes";
	public static final String SHOW_TIME_TAG = "showTime";
	public static final String SHOW_TIME_ID_TAG = "id";
	public static final String SHOW_TIME_TIME_TAG = "time";
	public static final String SHOW_TIME_AVAILABLE_TAG = "avaliable";
	public static final String SHOW_TIME_TYPE_TAG = "type";
	public static final String SHOW_TIME_ENABLE_TAG = "enable";
	public static final String SHOW_TIME_SCREEN_ID_TAG = "screenId";
	public static final String SHOW_TIME_SCREEN_NAME_TAG = "screenName";
	public static final String SHOW_TIME_TOTAL_TAG = "total";

	//Events
	public static final String SHARED_EVENTS_TAG = "Events";
	public static final String EVENET_DETAILS_TAG = "EventDetails";
	public static final String EVENET_DETAIL_TAG = "eventdetail";
	public static final String EVENET_EVENTID_TAG = "eventid";
	public static final String EVENET_EVENTNAME_TAG = "eventname";
	public static final String EVENET_EVENT_DESCRIPTION_TAG = "EDescription";
	public static final String EVENET_START_DATE_TAG = "startDate";
	public static final String EVENET_END_DATE_TAG = "endDate";
	public static final String EVENET_END_TIMER_TAG = "endTime";
	public static final String EVENET_START_TIMER_TAG = "StartTime";
	public static final String EVENET_CONTASCT_PERSON_TAG = "ContactPerson";
	public static final String EVENET_CONTASCT_PERSON_EMAIL_TAG = "ContactPersonEmail";
	public static final String EVENET_CONTASCT_PERSON_PHONENO_TAG = "ContactPersonNo";
	public static final String EVENET_LATITUDE_TAG = "Latitude";
	public static final String EVENET_LONGITITUDE_TAG = "Longitude";
	public static final String EVENET_ADMINSTION_TAG = "admission";
	public static final String EVENET_ALOCHOL_TAG = "alochol";
	public static final String EVENET_EVENTURL_TAG = "EventUrl";
	public static final String EVENET_THUMBURL_TAG = "thumbUrl";
	public static final String EVENET_VENUE_TAG = "Venue";
	public static final String EVENET_VDESCRIPTION_TAG = "VDescription";
	public static final String EVENET_EVENTQTURLPATH_TAG = "EventQTurlPath";
	public static final String EVENET_VENUEID_TAG = "VenueId";
	public static final String EVENET_THUMBURLFULL_TAG = "thumbURL";
	public static final String EVENET_BANERURl_TAG = "bannerURL";
	public static final String EVENET_POSTERURL_TAG = "posterURL";
	public static final String EVENET_MOBILEURL_TAG = "mobileURL";
	public static final String EVENET_CATEGORY_TAG = "Category";
	public static final String EVENET_CATEGORYID_TAG = "CategoryId";
	public static final String EVENET_SEATLAYOUT_TAG = "SeatLayout";
	public static final String EVENET_BOOKINGTYPE_TAG = "bookingType";
	public static final String EVENET_EVENTTYPE_TAG = "EventType";
	public static final String EVENET_CATEGORYICON_TAG = "categoryIcon";
	public static final String EVENET_COLORCODE_TAG = "colorcode";
	public static final String EVENET_BGCOLR_TAG = "bgcolorcode";
	public static final String EVENET_BGBORDERCOLRCODE_TAG = "bgbordercolorcode";
	public static final String EVENET_BTNCOLORCODE_TAG = "btncolorcode";
	public static final String EVENET_TITELCOLORCODE_TAG = "titlecolorcode";
	public static final String EVENET_DATE_MODIFIED_TAG = " date_modified";


	//EVENT TICKET DETAILS

	public static final String TICKET_DETAILS_TAG = "TicketDetails";
	public static final String TICKET_TAG = "Ticket";
	public static final String TICKET_PRICEID_TAG = "tktpriceid";
	public static final String TICKET_MASTERID_TAG = "tktmasterid";
	public static final String TICKET_NAME_TAG = "TicketName";
	public static final String TICKET_TOTAL_TICKETS_TAG = "TotalTickets";
	public static final String TICKET_AVAILBLITY_TAG = "Availability";
	public static final String TICKET_SERVICE_CHARGE_TAG = "ServiceCharge";
	public static final String TICKET_PRICE_TAG = "TicketPrice";
	public static final String TICKET_ADMIT_TAG = "Admit";
	public static final String TICKET_DATE_TAG = "Date";
	public static final String TICKET_NOOF_TICKETS_FOR_TRANSACTION_TAG = "NoOfTicketsPerTransaction";
	public static final String TICKET_TYPE_TAG = "TicketType";

	//intent shared tags

	public static final String INTENT_SHARED_MOVIES_TAG = "Movie";
	public static final String INTENT_SHARED_EVENT_TAG = "Event";
	public static final String INTENT_SHARED_SEAT_SELECTION_TAG = "SeatSelectionVO";
	public static final String INTENT_SHARED_BOOKING_DETAILS = "booking_details";
	public static final String INTENT_SHARED_EMAILID = "emailid";
	public static final String INTENT_SHARED_FROM = "fromconditions";
	public static final String INTENT_SHARED_RESERVATION_CODE = "reservationcode";
	public static final String INTENT_SHARED_EVENT_TOTAL_PRICE_TAG = "price";
	public static final String INTENT_SHARED_EVENT_NO_OF_SEATS_TAG = "nofseats";
	public static final String INTENT_SHARED_EVENT_TICKET_ID = "ticket_id";
	public static final String INTENT_SHARED_EVENT_NO_OF_TICKET_PER_ID = "NoOftktPerid";
	public static final String INTENT_SHARED_EVENT_BDATE = "bdate";
	public static final String INTENT_SHARED_EVENT_BTIME = "btime";
	public static final String INTENT_SHARED_EVENT_ORDERID = "orderid";
	public static final String INTENT_SHARED_EVENT_LATITUDE = "latitude";
	public static final String INTENT_SHARED_EVENT_LONGITUDE = "longotude";
	public static final String INTENT_SHARED_EVENT_TiTEL = "eventtitel";

	public static final String INTENT_SHARED_SUMMER_EVENT = "summerevent";
	public static final String SERVICE_URL_GET_EVENT_CONFIRM_TICKETS = SERVER_BASE_URL + "ticketconfirmstatus?";

	//EVENT CONFIRM
	public static final String SERVICE_MSG_EVENT_CONFIRM_ORDERID = "OrderID=";

	// TheatreLayout parser Booking Classes tags
	public static final String SERVICE_MSG_GET_THEATRE_LAYOUT_SHOW_TIME_ID = "showtimeid=";
	public static final String CLASSES_TAG = "Classes";
	public static final String CLASSES_MAX_BOOKING_TAG = "maxBooking";
	public static final String CLASSES_BOOKING_FEES_TAG = "bookingFees";
	public static final String CLASSES_URL_TAG = "url";

	// TheatreLayout parser Booking Class tags
	public static final String CLASS_TAG = "Class";
	public static final String CLASS_ID_TAG = "id";
	public static final String CLASS_NAME_TAG = "name";
	public static final String CLASS_COST_TAG = "cost";
	public static final String CLASS_NO_OF_ROWS_TAG = "noOfRows";

	// TheatreLayout parser Booking Row tags
	public static final String ROW_TAG = "Row";
	public static final String ROW_LETTER_TAG = "letter";
	public static final String ROW_NO_OF_SEATS_TAG = "noOfSeats";
	public static final String ROW_AVAILABLE_COUNT_TAG = "available";
	public static final String ROW_AVAILABLE_SEATS_TAG = "Availableseats";
	public static final String ROW_IS_INITIAL_GANGWAY_TAG = "isInitialGangway";
	public static final String ROW_IS_INITIAL_GANGWAT_COUNT_TAG = "isInitialGangwayCount";
	public static final String ROW_IS_FAMILY_TAG = "isFamily";
	public static final String ROW_GANGWAY_SEATS_TAG = "gangwaySeats";
	public static final String ROW_GANGWAY_COUNTS_TAG = "gangwayCounts";
	public static final String ROW_ALL_SEATS_TAG = "AllSeats";
	public static final String ROW_IS_GANGWAY_TAG = "isGangway";

	public static final String REGEX_EMAIL_ADDRESS = "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,64}$";

    public static final String NOW_SHOW = "nowShow";
	public static final String UPCOMING = "upComingShow";
    public static final String DATA ="DATA" ;

	public static final String WILL_WATCH_TAG ="willwatch" ;
	public static final String WILL_NOT_WATCH_TAG ="willnotwatch" ;
    public static final String MOVIE_BOOKING_DATE = "MOVIE_BOOKING_DATE";
    public static final Integer LOGIN = 1000;
    public static final String PAYMENT_TYPE ="PAYMENT_TYPE" ;
	public static final String USER_DATA = "USER_DATA";
    public static final String EVENT_BOOK_INFO ="EVENT_BOOK_INFO" ;
    public static final String EVENT_DETAIL ="EVENT_DETAIL" ;
	public static final String BOOKED_DETAIL ="BOOKED_DETAIL" ;
    public static final int REGISTER = 1100;

    public static final String TYPE ="TYPE" ;
    public static final String COUNTRY_TYPE_CURRENCY ="COUNTRY_TYPE";
    public static final String COUNTRY_TYPE_QA ="QAR";
    public static final String COUNTRY_TYPE_BH ="BH";
    public static final String COUNTRY_TYPE_UAE="AED";
    public static final String SHARED_EVENTS_TICKET_PRICES ="SHARED_EVENTS_TICKET_PRICES" ;
    public static final String SELECTED_MOVIE_DETAIL ="SELECTED_MOVIE_DETAIL" ;
    public static  final String EVENET_SELECTED_DATE="EVENET_SELECTED_DATE";
	public static final String EVENT_ID ="EVENT_ID" ;
	public static final String SHARED_EVENTS_LEISURE = "SHARED_EVENTS_LEISURE";
	public static final String SHARED_EVENTS_SPORTS ="SHARED_EVENTS_SPORTS" ;
	public static final String COUPAN_AMOUNT = "COUPAN_AMOUNT";
	public static final String EVENTS_DETAIL ="EVENTS_DETAIL" ;
	public static final String EVENT_ITEM="EVENT_ITEM";
	public static final String BOAT_TYPE ="BOAT_TYPE" ;
	public static final String TOTAL_COUNT ="TOTAL_COUNT" ;
	public static final String TOTAL_PRICE ="TOTAL_PRICE" ;
}
