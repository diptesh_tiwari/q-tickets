package com.mobile.android.qtickets.movie.seatselection;

import android.net.Uri;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by hexalabssd on 22/02/17.
 */

public final class SeatLayoutResponse {
	private static final String NAMESPACE = null;
	private static final String TAG_RESPONSE = "response";
	private static final String ATTR_RESPONSE_STATUS = "status";
	private static final String TAG_CLASSES = "Classes";
	private static final String ATTR_CLASSES_MAX_BOOKING = "maxBooking";
	private static final String ATTR_CLASSES_BOOKING_FEES = "bookingFees";
	private static final String ATTR_CLASSES_URL = "url";
	private static final String TAG_CLASS = "Class";
	private static final String ATTR_CLASS_ID = "id";
	private static final String ATTR_CLASS_COST = "cost";
	private static final String ATTR_CLASS_NAME = "name";
	private static final String ATTR_CLASS_ROWS_COUNT = "noOfRows";
	private static final String TAG_ROW = "Row";
	private static final String ATTR_ROW_LETTER = "letter";
	private static final String ATTR_ROW_SEATS_COUNT = "noOfSeats";
	private static final String ATTR_ROW_AVAILABLE_COUNT = "availableCount";
	private static final String ATTR_ROW_AVAILABLE_SEATS = "Availableseats";
	private static final String ATTR_ROW_IS_FAMILY = "isFamily";
	private static final String ATTR_ROW_GANGWAY_SEATS = "gangwaySeats";
	private static final String ATTR_ROW_GANGWAY_COUNTS = "gangwayCounts";
	private static final String ATTR_ROW_ALL_SEATS = "AllSeats";
	private static final String ATTR_ROW_IS_GANGWAY = "isGangway";
	private static final String ATTR_ROW_IS_INITIAL_GANGWAY = "isInitialGangway";
	private static final String ATTR_ROW_IS_INITIAL_GANGWAY_COUNT = "isInitialGangwayCount";

	private final XmlPullParser mXmlPullParser;

	private String mStatus;
	private SeatsClassList mClasses;
	public SeatLayoutResponse(final XmlPullParser pXmlPullParser) {
		mXmlPullParser = pXmlPullParser;
		try {
			mXmlPullParser.nextTag();
			while (mXmlPullParser.getName().contentEquals(TAG_RESPONSE)) {
				if (mXmlPullParser.getEventType() == XmlPullParser.START_TAG) {
					mStatus = mXmlPullParser.getAttributeValue(NAMESPACE, ATTR_RESPONSE_STATUS);
					mClasses = readClasses();
				}
				mXmlPullParser.nextTag();
			}
		} catch (XmlPullParserException pE) {
			pE.printStackTrace();
		} catch (IOException pE) {
			pE.printStackTrace();
		}
	}

	private SeatsClassList readClasses() {
		SeatsClassList classes = null;
		try {
			mXmlPullParser.nextTag();
			while (mXmlPullParser.getName().contentEquals(TAG_CLASSES)) {
				if (mXmlPullParser.getEventType() == XmlPullParser.START_TAG) {
					classes = new SeatsClassList();
					classes.setMaxBooking(Integer.parseInt(mXmlPullParser.getAttributeValue(NAMESPACE, ATTR_CLASSES_MAX_BOOKING)));
					classes.setBookingFees(Integer.parseInt(mXmlPullParser.getAttributeValue(NAMESPACE, ATTR_CLASSES_BOOKING_FEES)));
					classes.setUrl(Uri.parse(mXmlPullParser.getAttributeValue(NAMESPACE, ATTR_CLASSES_URL)));

					classes.add(readClass());
				}
				mXmlPullParser.nextTag();
			}
		} catch (XmlPullParserException pE) {
			pE.printStackTrace();
		} catch (IOException pE) {
			pE.printStackTrace();
		}
		return classes;
	}

	private SeatsClass readClass() {
		SeatsClass aClass = null;
		try {
			mXmlPullParser.nextTag();
			while (mXmlPullParser.getName().contentEquals(TAG_CLASS)) {
				if (mXmlPullParser.getEventType() == XmlPullParser.START_TAG) {
					if(mXmlPullParser.getAttributeValue(NAMESPACE, ATTR_CLASS_COST)!=null){
						aClass = new SeatsClass(Integer.parseInt(mXmlPullParser.getAttributeValue(NAMESPACE, ATTR_CLASS_ID)),
								Double.parseDouble(mXmlPullParser.getAttributeValue(NAMESPACE, ATTR_CLASS_COST)));
						aClass.setName(mXmlPullParser.getAttributeValue(NAMESPACE, ATTR_CLASS_NAME));
						aClass.setNosRows(Integer.parseInt(mXmlPullParser.getAttributeValue(NAMESPACE, ATTR_CLASS_ROWS_COUNT)));

						aClass.setRows(readRows());
					}

				}
				mXmlPullParser.nextTag();
			}

		} catch (XmlPullParserException pE) {
			pE.printStackTrace();
		} catch (IOException pE) {
			pE.printStackTrace();
		}
		return aClass;
	}

	private List<SeatsRow> readRows() {
		List<SeatsRow> seatsRows = new ArrayList<>();
		try {
			mXmlPullParser.nextTag();
			while (mXmlPullParser.getName().contentEquals(TAG_ROW)) {
				if (mXmlPullParser.getEventType() != XmlPullParser.START_TAG) {
					final SeatsRow seatsRow = new SeatsRow(mXmlPullParser.getAttributeValue(NAMESPACE, ATTR_ROW_LETTER));
					seatsRow.setNumberOfSeats(Integer.parseInt(mXmlPullParser.getAttributeValue(NAMESPACE, ATTR_ROW_SEATS_COUNT)));
					seatsRow.setAvailableCount(Integer.parseInt(mXmlPullParser.getAttributeValue(NAMESPACE, ATTR_ROW_AVAILABLE_COUNT)));
					seatsRow.setAvailableSeats(Arrays.asList(mXmlPullParser.getAttributeValue(NAMESPACE, ATTR_ROW_AVAILABLE_SEATS).split(",")));
					seatsRow.setFamily(Integer.parseInt(mXmlPullParser.getAttributeValue(NAMESPACE, ATTR_ROW_IS_FAMILY)) > 0);
					seatsRow.setGangwaySeats(Arrays.asList(mXmlPullParser.getAttributeValue(NAMESPACE, ATTR_ROW_GANGWAY_SEATS).split(",")));
					final String[] gangwayCountsStrArr = mXmlPullParser.getAttributeValue(NAMESPACE, ATTR_ROW_GANGWAY_COUNTS).split(",");
					final int[] gangwayCountsIntArr = new int[gangwayCountsStrArr.length];
					for (int i = 0; i < gangwayCountsStrArr.length; i++) {
						gangwayCountsIntArr[i] = Integer.parseInt(gangwayCountsStrArr[i]);
					}
					seatsRow.setGangwayCounts(gangwayCountsIntArr);
					seatsRow.setAllSeats(Arrays.asList(mXmlPullParser.getAttributeValue(NAMESPACE, ATTR_ROW_ALL_SEATS).split(",")));
					seatsRow.setIsGangway(Boolean.parseBoolean(mXmlPullParser.getAttributeValue(NAMESPACE, ATTR_ROW_IS_GANGWAY)));
					seatsRow.setInitialGangway(Boolean.parseBoolean(mXmlPullParser.getAttributeValue(NAMESPACE, ATTR_ROW_IS_INITIAL_GANGWAY)));
					seatsRow.setIsInitialGangwayCount(Integer.parseInt(mXmlPullParser.getAttributeValue(NAMESPACE, ATTR_ROW_IS_INITIAL_GANGWAY_COUNT)));

					seatsRows.add(seatsRow);
				}
				mXmlPullParser.nextTag();
			}

		} catch (XmlPullParserException pE) {
			pE.printStackTrace();
		} catch (IOException pE) {
			pE.printStackTrace();
		}
		return seatsRows;
	}

	public String getStatus() {
		return mStatus;
	}

	public SeatsClassList getClasses() {
		return mClasses;
	}

}
