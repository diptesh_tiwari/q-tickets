package com.mobile.android.qtickets;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ListView;
import android.widget.Toast;

import com.mobile.android.qtickets.constants.AppConstants;


public class PdfViewer extends AppCompatActivity {
	private WebView webView;
	ProgressDialog pDialog;

//	inside this goes our pdf viewer, just a toy for this test. Requires  more work to make it production ready
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		/*setContentView(R.layout.full_article_layout);

		webView = (WebView) findViewById(R.id.webview);

		webView = (WebView) findViewById(R.id.webview);
		WebSettings settings = webView.getSettings();
		settings.setJavaScriptEnabled(true);

		if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN)
			settings.setAllowUniversalAccessFromFileURLs(true);

		settings.setBuiltInZoomControls(true);
		webView.setWebChromeClient(new WebChromeClient());*/


		String title = getIntent().getStringExtra(AppConstants.DATA);



		if (title!=null) {

			Uri path = Uri.parse(title);
			Intent intent = new Intent(Intent.ACTION_VIEW);
			intent.setDataAndType(path, "application/pdf");
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);

			//init(title);
			//listener();
			//webView.loadUrl("http://qnmc.org/wp-content/uploads/2017/05/Application-3.pdf");

		}
	}



	private void init(String title) {
		//webView = (WebView) findViewById(R.id.webview);
		webView.getSettings().setJavaScriptEnabled(true);

		pDialog = new ProgressDialog(PdfViewer.this);
		pDialog.setTitle("PDF");
		pDialog.setMessage("Loading...");
		pDialog.setIndeterminate(false);
		pDialog.setCancelable(false);
		webView.loadUrl(title);

	}




}
