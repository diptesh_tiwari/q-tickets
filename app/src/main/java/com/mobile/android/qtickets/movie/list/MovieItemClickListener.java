package com.mobile.android.qtickets.movie.list;

import com.mobile.android.qtickets.movie.Movie;

/**
 * Created by Android on 4/19/17.
 */

public interface MovieItemClickListener {
    void onMovieItemClick(final Movie pMovie);
    void onsetData(final Object object);
}
