package com.mobile.android.qtickets.movie.seatselection;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hexalabssd on 24/02/17.
 */

public final class SeatsClass {
	private final int mId;
	private final double mCost;
	private String mName;
	private int mNosRows;
	private List<SeatsRow> mRows;

	public SeatsClass(final int pId, final double pCost) {
		mId = pId;
		mCost = pCost;
		mRows = new ArrayList<>();
	}

	public String getName() {
		return mName;
	}

	public void setName(final String pName) {
		mName = pName;
	}

	public int getNosRows() {
		return mNosRows;
	}

	public void setNosRows(final int pNosRows) {
		mNosRows = pNosRows;
	}

	public int getId() {

		return mId;
	}

	public double getCost() {
		return mCost;
	}

	public List<SeatsRow> getRows() {
		return mRows;
	}

	public void setRows(final List<SeatsRow> pRows) {
		mRows = pRows;
	}

	public void addRow(final SeatsRow pRow) {
		mRows.add(pRow);
	}
}
