package com.mobile.android.qtickets.movie.seatselection;

import com.mobile.android.qtickets.constants.AppConstants;

import org.xmlpull.v1.XmlPullParser;

import in.hexalab.abstractvolley.base.HResponseListener;
import in.hexalab.abstractvolley.base.HXmlRequest;

/**
 * Created by hexalabssd on 22/02/17.
 */

public final class PostSeatsLayoutRequest extends HXmlRequest {
	public PostSeatsLayoutRequest(final String showTimeId, final HResponseListener<XmlPullParser> pSeatLayoutResponseListener) {
		super(AppConstants.SERVER_BASE_URL + "GetSeatLayout", pSeatLayoutResponseListener);
		addBodyParam("showtimeid", showTimeId);
	}
}
