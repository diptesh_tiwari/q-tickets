package com.mobile.android.qtickets.movie.list;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;

/**
 * Created by hexalabssd on 01/03/17.
 */

public final class MoviesLayoutManager extends GridLayoutManager {
	public MoviesLayoutManager(final Context context, final int spanCount) {
		super(context, spanCount);
	}
}
