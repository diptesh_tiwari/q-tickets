package com.mobile.android.qtickets.movie.moviedetails;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.mobile.android.qtickets.R;

/**
 * Created by hexalabssd on 30/01/17.
 */
final class TheatreAndShowsViewHolder extends RecyclerView.ViewHolder {
	private final TextView mTheatreNameTextView;
	private final TableLayout mShowTimesTableLayout;

	TheatreAndShowsViewHolder(@NonNull final ViewGroup parent) {
		super(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_view_theatre_shows, parent, false));
		mTheatreNameTextView = (TextView) itemView.findViewById(R.id.textView_theatreName);
		mShowTimesTableLayout = (TableLayout) itemView.findViewById(R.id.tableLayout_showTimes);
	}

	void setTheatreName(final String pTheatreName) {
		mTheatreNameTextView.setText(pTheatreName);
	}

	void setShowTimesListAdapter(final ListAdapter pShowTimesListAdapter) {
		final int columnCount = 4;
		for (int i = 0; i < pShowTimesListAdapter.getCount(); ) {
			final TableRow tableRow = new TableRow(getContext());
			final TableLayout.LayoutParams layoutParams = new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
			tableRow.setLayoutParams(layoutParams);
			do {
				final int columnIndex = i % columnCount;
				final View view = pShowTimesListAdapter.getView(i, tableRow.getChildAt(columnIndex), mShowTimesTableLayout);
				final TableRow.LayoutParams showTimeItemParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
				showTimeItemParams.column = columnIndex;
				showTimeItemParams.gravity = Gravity.CENTER_HORIZONTAL;
				view.setLayoutParams(showTimeItemParams);
				tableRow.addView(view);
				i++;
				if (i == pShowTimesListAdapter.getCount()) {
					break;
				}
			} while (tableRow.getChildCount() < columnCount);
			mShowTimesTableLayout.addView(tableRow);
		}
	}

	Context getContext() {
		return itemView.getContext();
	}
}
