package com.mobile.android.qtickets.activities;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.widget.VideoView;

import com.google.gson.Gson;
import com.mobile.android.qtickets.R;
import com.mobile.android.qtickets.constants.AppConstants;

/**
 * Created by Satish - 360 on 11/16/2016.
 */

public class SplashActivity extends Activity {

	private VideoView mSplashVideoView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);
		mSplashVideoView = (VideoView) findViewById(R.id.videoView_splash);

		setupSplashVideoPlay();
	}

	private void setupSplashVideoPlay() {
		mSplashVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
			@Override
			public void onPrepared(final MediaPlayer mp) {
				mp.setVolume(0, 0);
			}
		});
		mSplashVideoView.setVideoURI(Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + getPackageName() + "/" + R.raw.splash_video));
		mSplashVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
			@Override
			public void onCompletion(final MediaPlayer mp) {
				startActivity(new Intent(SplashActivity.this, DashboardActivity.class));
			}
		});
		mSplashVideoView.start();
	}
}
