package in.hexalab.abstractvolley;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;

import in.hexalab.abstractvolley.cache.ImageCacheManager;

/**
 * Created by hexalabssd on 14/11/16.
 */
public final class ApiRequestManager {
	private static ApiRequestManager mInstance;
	private RequestQueue mRequestQueue;
	private ImageLoader mImageLoader;

	private ApiRequestManager(final Context pContext) {
		if (mRequestQueue == null) {
			mRequestQueue = Volley.newRequestQueue(pContext.getApplicationContext());
		}

		mImageLoader = new ImageLoader(mRequestQueue, new ImageCacheManager(pContext.getApplicationContext()));
	}

	public static ImageLoader getImageLoader(final Context pContext) {
		return getInstance(pContext).getImageLoader();
	}

	private static ApiRequestManager getInstance(final Context pContext) {
		if (mInstance == null) {
			mInstance = new ApiRequestManager(pContext);
		}
		return mInstance;
	}

	private ImageLoader getImageLoader() {
		return mImageLoader;
	}

	public static <T> void addToRequestQueue(final Context pContext, final Request<T> pRequest) {
		getInstance(pContext).addToRequestQueue(pRequest);
	}

	/**
	 * @param pRequest An object of request class which extends any of the following: {@link
	 *                 in.hexalab.abstractvolley.base.HFormRequest} {@link in.hexalab.abstractvolley.base.HGetRequest}
	 *                 {@link in.hexalab.abstractvolley.base.HJsonRequest}
	 * @param <T>      Type of a class for the response.
	 */
	private <T> void addToRequestQueue(final Request<T> pRequest) {
		mRequestQueue.add(pRequest);
	}
}
