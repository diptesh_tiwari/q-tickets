package in.hexalab.abstractvolley.cache;

import android.content.Context;
import android.graphics.Bitmap;

import com.android.volley.toolbox.ImageLoader;

/**
 * An object of this is being used in {@link in.hexalab.abstractvolley.ApiRequestManager} to create
 * an instance of {@link ImageLoader}.
 */

public final class ImageCacheManager implements ImageLoader.ImageCache {

	private final LruBitmapCache mLruCache;

	public ImageCacheManager(final Context pContext) {
		mLruCache = new LruBitmapCache(LruBitmapCache.getCacheSize(pContext.getApplicationContext()));
	}

	@Override
	public Bitmap getBitmap(final String url) {
		return mLruCache.get(url);
	}

	@Override
	public void putBitmap(final String url, final Bitmap bitmap) {
		mLruCache.put(url, bitmap);
	}
}
