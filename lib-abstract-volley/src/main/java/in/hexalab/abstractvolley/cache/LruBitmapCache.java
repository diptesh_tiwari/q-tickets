package in.hexalab.abstractvolley.cache;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.util.LruCache;
import android.util.DisplayMetrics;

import com.android.volley.toolbox.ImageLoader;

/**
 * Get the object of this from {@link ImageCacheManager}.
 */

final class LruBitmapCache extends LruCache<String, Bitmap> implements ImageLoader.ImageCache {
	public LruBitmapCache(Context ctx) {
		this(getCacheSize(ctx));
	}

	/**
	 * @param maxSize for caches that do not override {@link #sizeOf}, this is the maximum number of
	 *                entries in the cache. For all other caches, this is the maximum sum of the
	 *                sizes of the entries in this cache.
	 */
	LruBitmapCache(final int maxSize) {
		super(maxSize);
	}

	/**
	 * Returns a cache size equal to approximately three screens worth of images.
	 *
	 * @param ctx
	 *
	 * @return required size for image caching
	 */
	static int getCacheSize(Context ctx) {
		final DisplayMetrics displayMetrics = ctx.getResources().getDisplayMetrics();
		final int screenWidth = displayMetrics.widthPixels;
		final int screenHeight = displayMetrics.heightPixels;
		// 4 bytes per pixel
		final int screenBytes = screenWidth * screenHeight * 4;

		return screenBytes * 3;
	}

	@Override
	protected int sizeOf(String key, Bitmap value) {
		return value.getRowBytes() * value.getHeight();
	}

	@Override
	public Bitmap getBitmap(String url) {
		return get(url);
	}

	@Override
	public void putBitmap(String url, Bitmap bitmap) {
		put(url, bitmap);
	}

}
