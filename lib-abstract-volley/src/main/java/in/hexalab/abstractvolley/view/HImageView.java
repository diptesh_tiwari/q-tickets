package in.hexalab.abstractvolley.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.util.AttributeSet;

import com.android.volley.toolbox.NetworkImageView;

import in.hexalab.abstractvolley.R;

/**
 * Created by hexalabssd on 23/01/17.
 */

public final class HImageView extends NetworkImageView {
	private final Context mContext;
	private int mDefaultImageResId;
	private int mErrorImageResId;

	public HImageView(final Context context) {
		super(context);
		mContext = context;
		init(null, 0);
	}

	private void init(@Nullable final AttributeSet attrs, final int defStyle) {
		if (attrs != null) {
			final TypedArray typedArray = mContext.obtainStyledAttributes(attrs, R.styleable.HImageView, defStyle, 0);
			try {
				mDefaultImageResId = typedArray.getResourceId(R.styleable.HImageView_defaultImage, 0);
				mErrorImageResId = typedArray.getResourceId(R.styleable.HImageView_errorImage, 0);
			} finally {
				typedArray.recycle();
			}

			setDefaultImageResId(mDefaultImageResId);
			setErrorImageResId(mErrorImageResId);
		}
	}

	public HImageView(final Context context, final AttributeSet attrs) {
		super(context, attrs);
		mContext = context;
		init(attrs, 0);
	}

	public HImageView(final Context context, final AttributeSet attrs, final int defStyle) {
		super(context, attrs, defStyle);
		mContext = context;
		init(attrs, defStyle);
	}
}
