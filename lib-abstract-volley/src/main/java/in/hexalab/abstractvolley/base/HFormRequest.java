package in.hexalab.abstractvolley.base;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.util.ArrayMap;
import android.text.TextUtils;

import com.android.volley.AuthFailureError;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLConnection;
import java.util.Calendar;
import java.util.Map;

import in.hexalab.abstractvolley.core.HRequest;

/**
 * Created by hexalabssd on 14/11/16.
 */

public class HFormRequest<T extends HResponseBody> extends HRequest<T> {

	private static final String TWO_HYPHENS = "--";
	private static final String LINE_FEED = "\r\n";
	private final Map<String, String> mParams;
	private final Map<String, File> mFileParams;
	private final Map<String, File[]> mFilesParams;
	private final Map<String, Map<String, File>> mFilesWFileNameParams;
	private String mBoundary;

	/**
	 * Use this to make API request with {@code POST} method from {@link Method}.
	 *
	 * @param url
	 * @param pClass
	 * @param listener
	 */
	public HFormRequest(@NonNull final String url, @NonNull final Class<T> pClass, @NonNull final HResponseListener<T> listener) {
		super(Method.POST, url, pClass, listener, listener);
		mParams = new ArrayMap<>();
		mFileParams = new ArrayMap<>();
		mFilesParams = new ArrayMap<>();
		mFilesWFileNameParams = new ArrayMap<>();
	}

	/**
	 * Use this to make API request with any HTTP method.
	 *
	 * @param method   Any HTTP from {@link Method}.
	 * @param url
	 * @param pClass
	 * @param listener
	 */
	public HFormRequest(@NonNull final int method, @NonNull final String url, @NonNull final Class<T> pClass, @NonNull final HResponseListener<T> listener) {
		super(method, url, pClass, listener, listener);
		mParams = new ArrayMap<>();
		mFileParams = new ArrayMap<>();
		mFilesParams = new ArrayMap<>();
		mFilesWFileNameParams = new ArrayMap<>();
	}

	public void addBodyParam(final String name, final String value) {
		mParams.put(name, value);
	}

	public void addFile(@NonNull final String name, @NonNull final File file) {
		mFileParams.put(name, file);
		setBoundary();
	}

	private void setBoundary() {
		if (TextUtils.isEmpty(mBoundary)) {
			mBoundary = String.valueOf(Calendar.getInstance().getTimeInMillis());
		}
	}

	public void addFiles(@NonNull final String name, @NonNull final File[] files) {
		mFilesParams.put(name, files);
		setBoundary();
	}

	public void addFilesWithNames(@NonNull final String name, @NonNull final Map<String, File> filesToUpload) {
		mFilesWFileNameParams.put(name, filesToUpload);
		setBoundary();
	}

	@Override
	public Map<String, String> getParams() {
		return mParams;
	}

	@Override
	public String getBodyContentType() {
		if (!mFileParams.isEmpty() || !mFilesParams.isEmpty() || !mFilesWFileNameParams.isEmpty()) {
			return "multipart/form-data; boundary=" + mBoundary;
		}
		return super.getBodyContentType();
	}

	@Nullable
	@Override
	public byte[] getBody() {
		if (!mFileParams.isEmpty() || !mFilesParams.isEmpty() || !mFilesWFileNameParams.isEmpty()) {
			// TODO Generate body with mParams and encoded file
			// TODO and return it as byte array
			return encodeMultiPartBodyData();
		} else {
			try {
				return super.getBody();
			} catch (AuthFailureError pAuthFailureError) {
				pAuthFailureError.printStackTrace();
				return null;
			}
		}
	}

	@Nullable
	private byte[] encodeMultiPartBodyData() {
		final StringBuilder multipartDataBuilder = new StringBuilder();
		if (!mParams.isEmpty()) {
			for (String key : mParams.keySet()) {
				multipartDataBuilder.append(LINE_FEED);
				multipartDataBuilder.append(TWO_HYPHENS);
				multipartDataBuilder.append(mBoundary);
				multipartDataBuilder.append(String.format(LINE_FEED + "Content-Disposition: form-data; name=\"%s\"", key));
				multipartDataBuilder.append(LINE_FEED + LINE_FEED);
				multipartDataBuilder.append(mParams.get(key));
			}
		}

		if (!mFileParams.isEmpty()) {
			for (String key : mFileParams.keySet()) {
				final File fileToUpload = mFileParams.get(key);

				multipartDataBuilder.append(LINE_FEED);
				multipartDataBuilder.append(TWO_HYPHENS);
				multipartDataBuilder.append(mBoundary);
				multipartDataBuilder.append(String.format(LINE_FEED + "Content-Disposition: form-data; name=\"%s\"; filename=\"%s\"", key, fileToUpload.getName()));
				multipartDataBuilder.append(String.format(LINE_FEED + "Content-Type: %s", URLConnection.guessContentTypeFromName(fileToUpload.getName())));
				multipartDataBuilder.append(LINE_FEED + LINE_FEED);

				multipartDataBuilder.append(encodeFileData(fileToUpload));
			}
		}

		if (!mFilesParams.isEmpty()) {
			for (String name : mFilesParams.keySet()) {
				final File[] filesToUpload = mFilesParams.get(name);
				for (File fileToUpload : filesToUpload) {
					final String fileName = fileToUpload.getName();

					multipartDataBuilder.append(LINE_FEED);
					multipartDataBuilder.append(TWO_HYPHENS);
					multipartDataBuilder.append(mBoundary);
					name += name.endsWith("[]") ? "" : "[]";
					multipartDataBuilder.append(String.format(LINE_FEED + "Content-Disposition: form-data; name=\"%s\"; filename=\"%s\"", name, fileToUpload.getName()));
					multipartDataBuilder.append(String.format(LINE_FEED + "Content-Type: %s", URLConnection.guessContentTypeFromName(fileName)));
					multipartDataBuilder.append(LINE_FEED + LINE_FEED);

					multipartDataBuilder.append(encodeFileData(fileToUpload));
				}
			}
		}

		if (!mFilesWFileNameParams.isEmpty()) {
			for (String name : mFilesWFileNameParams.keySet()) {
				final Map<String, File> filesToUpload = mFilesWFileNameParams.get(name);
				for (String filenameKey : filesToUpload.keySet()) {
					final File fileToUpload = filesToUpload.get(filenameKey);
					final String fileName = fileToUpload.getName();

					multipartDataBuilder.append(LINE_FEED);
					multipartDataBuilder.append(TWO_HYPHENS);
					multipartDataBuilder.append(mBoundary);
					name += name.endsWith("[]") ? "" : "[]";
					multipartDataBuilder.append(String.format(LINE_FEED + "Content-Disposition: form-data; name=\"%s\"; filename=\"%s\"", name, filenameKey + fileName.substring(fileName.lastIndexOf('.'))));
					multipartDataBuilder.append(String.format(LINE_FEED + "Content-Type: %s", URLConnection.guessContentTypeFromName(fileName)));
					multipartDataBuilder.append(LINE_FEED + LINE_FEED);

					multipartDataBuilder.append(encodeFileData(fileToUpload));
				}
			}
		}

		if (!mParams.isEmpty() || !mFileParams.isEmpty() || !mFilesWFileNameParams.isEmpty() || !mFilesParams.isEmpty()) {
			multipartDataBuilder.append(LINE_FEED);
			multipartDataBuilder.append(TWO_HYPHENS);
			multipartDataBuilder.append(mBoundary);
			multipartDataBuilder.append(TWO_HYPHENS);
		}

		try {
			return multipartDataBuilder.toString().getBytes(getParamsEncoding());
		} catch (UnsupportedEncodingException pE) {
			pE.printStackTrace();
			return null;
		}
	}

	@Nullable
	private String encodeFileData(final File file) {
		try {
			final FileInputStream is = new FileInputStream(file);
			final BufferedReader reader = new BufferedReader(new InputStreamReader(is));
			StringBuilder sb = new StringBuilder();
			String line;
			try {
				while ((line = reader.readLine()) != null) {
					sb.append(line);
				}
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					reader.close();
					is.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			return sb.toString();

			/*
			final int maxBufferSize = 1024 * 1024;
//			final FileInputStream uploadFileInputStream = new FileInputStream(file);
			final DataInputStream dataInputStream = new DataInputStream(new FileInputStream(file));
			int byteAvailable = dataInputStream.available();
			int bufferSize = Math.min(byteAvailable, maxBufferSize);
			byte[] buffer = new byte[bufferSize];
//			int bytesRead = uploadFileInputStream.read(buffer, 0, bufferSize);
			dataInputStream.readFully(buffer, 0, bufferSize);

//			while (bytesRead > 0) {
//				outputStream.write(buffer, 0, bufferSize);
//				byteAvailable = uploadFileInputStream.available();
//				bufferSize = Math.min(byteAvailable, maxBufferSize);
//				bytesRead = uploadFileInputStream.read(buffer, 0, bufferSize);
//			}
			dataInputStream.close();

			// TODO Return string of byte array;
			return buffer.toString();*/

		} catch (IOException pE) {
			pE.printStackTrace();
			return null;
		}
	}
}
