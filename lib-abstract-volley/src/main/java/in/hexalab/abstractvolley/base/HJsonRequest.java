package in.hexalab.abstractvolley.base;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.util.ArrayMap;
import android.text.TextUtils;
import android.webkit.MimeTypeMap;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonRequest;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonSyntaxException;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by hexalabssd on 14/11/16.
 */

public abstract class HJsonRequest<T extends HResponseBody> extends JsonRequest<T> {
	private final Class<T> clazz;

	private final Map<String, String> headers;
	private final Map<String, String> urlParams;

	/**
	 * Use this to make API request with body in JSON format and {@code POST} method from {@link
	 * com.android.volley.Request.Method}.
	 *
	 * @param url
	 * @param requestBody
	 * @param clazz
	 * @param listener
	 */
	public HJsonRequest(@NonNull final String url, @Nullable final Body requestBody, @NonNull final Class<T> clazz, @NonNull final HResponseListener<T> listener) {
		super(Method.POST, url, requestBody == null ? null : requestBody.toJsonString(), listener, listener);
		this.clazz = clazz;
		headers = new ArrayMap<>();
		urlParams = new ArrayMap<>();
	}

	/**
	 * Use this to make API request with body in JSON format and any HTTP method.
	 *
	 * @param url         Any HTTP method from {@link com.android.volley.Request.Method}.
	 * @param requestBody
	 * @param clazz
	 * @param listener
	 */
	public HJsonRequest(@NonNull final int method, @NonNull final String url, @Nullable final Body requestBody, @NonNull final Class<T> clazz, @NonNull final HResponseListener<T> listener) {
		super(method, url, requestBody == null ? null : requestBody.toJsonString(), listener, listener);
		this.clazz = clazz;
		headers = new ArrayMap<>();
		urlParams = new ArrayMap<>();
	}

	public void addHeader(final String name, final String value) {
		headers.put(name, value);
	}

	public void addUrlParam(final String name, final String value) {
		urlParams.put(name, value);
	}

	@Override
	public String getUrl() {
		if (!urlParams.isEmpty()) {
			final StringBuilder url = new StringBuilder(super.getUrl());
			final String ext = MimeTypeMap.getFileExtensionFromUrl(super.getUrl());
			url.append(url.toString().endsWith("/") ? "?" : (TextUtils.isEmpty(ext) ? "/?" : "?"));
			url.append(getParamsChain(urlParams.entrySet().iterator(), getParamsEncoding()));
			return url.toString();
		}
		return super.getUrl();
	}

	@Override
	public Map<String, String> getHeaders() throws AuthFailureError {
		return headers;
	}

	private String getParamsChain(final Iterator<Map.Entry<String, String>> paramsEntrySetIterator, final String paramEncoding) {
		final StringBuilder paramsChain = new StringBuilder();
		try {
			while (paramsEntrySetIterator.hasNext()) {
				final Map.Entry<String, String> entry = paramsEntrySetIterator.next();
				paramsChain.append(URLEncoder.encode(entry.getKey(), paramEncoding));
				paramsChain.append('=');
				paramsChain.append(URLEncoder.encode(entry.getValue(), paramEncoding));
				if (paramsEntrySetIterator.hasNext()) {
					paramsChain.append('&');
				}
			}
		} catch (UnsupportedEncodingException pE) {
			pE.printStackTrace();
		}
		return paramsChain.toString();
	}

	@Nullable
	@Override
	protected Response<T> parseNetworkResponse(NetworkResponse response) {
		if (isCanceled()) return null;
		final Gson gson = new Gson();
		try {
			String json = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
			return Response.success(gson.fromJson(json, clazz), HttpHeaderParser.parseCacheHeaders(response));
		} catch (JsonSyntaxException | UnsupportedEncodingException e) {
			return Response.error(new ParseError(e));
		}
	}

	public abstract class Body {
		String toJsonString() {
			return new Gson().toJson(this);
		}

		private JsonElement toJsonElement() {
			return new Gson().toJsonTree(this);
		}
	}

}
