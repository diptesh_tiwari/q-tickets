package in.hexalab.abstractvolley.base;

import com.google.gson.Gson;

/**
 * Created by hexalabssd on 06/01/17.
 */

public abstract class HResponseBody {
	String toJsonString() {
		return new Gson().toJson(this);
	}
}
