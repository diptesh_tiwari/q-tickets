package in.hexalab.abstractvolley.base;

import com.android.volley.Response;

/**
 * Created by hexalabssd on 23/01/17.
 */
public interface HResponseListener<T> extends Response.Listener<T>, Response.ErrorListener {
	void onRequestSubmitted();
}
