package in.hexalab.abstractvolley.base;

import android.support.annotation.NonNull;

import in.hexalab.abstractvolley.core.HRequest;

/**
 * Created by hexalabssd on 14/11/16.
 */

public abstract class HGetRequest<T extends HResponseBody> extends HRequest<T> {

	/**
	 * Use this to make API request with {@code GET} method from {@link
	 * com.android.volley.Request.Method}.
	 *
	 * @param url
	 * @param pClass
	 * @param listener
	 */
	public HGetRequest(@NonNull final String url, @NonNull final Class<T> pClass, @NonNull final HResponseListener<T> listener) {
		super(Method.GET, url, pClass, listener, listener);
	}
}
