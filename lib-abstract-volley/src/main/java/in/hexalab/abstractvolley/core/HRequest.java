package in.hexalab.abstractvolley.core;

import android.support.annotation.Nullable;
import android.support.v4.util.ArrayMap;

import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.Map;

/**
 * Do not extend this class directly. Instead extends any of the following: {@link
 * in.hexalab.abstractvolley.base.HFormRequest} {@link in.hexalab.abstractvolley.base.HGetRequest}
 * {@link in.hexalab.abstractvolley.base.HJsonRequest}
 */

public abstract class HRequest<T> extends Request<T> {

	protected final Response.Listener<T> mListener;
	private final Map<String, String> headers;
	private final Map<String, String> urlParams;
	private Class<T> mClass;

	protected HRequest(final int method, final String url, final Class<T> pClass, final Response.Listener<T> pListener, final Response.ErrorListener errorListener) {
		super(method, url, errorListener);
		headers = new ArrayMap<>();
		urlParams = new ArrayMap<>();
		mClass = pClass;
		mListener = pListener;
	}

	public void addHeader(final String name, final String value) {
		headers.put(name, value);
	}

	public void addUrlParam(final String name, final String value) {
		urlParams.put(name, value);
	}

	@Override
	public String getUrl() {
		if (!urlParams.isEmpty()) {
			return super.getUrl() + getParamsChain(urlParams.entrySet().iterator(), getParamsEncoding());
		}
		return super.getUrl();
	}

	private String getParamsChain(final Iterator<Map.Entry<String, String>> paramsEntrySetIterator, final String paramEncoding) {
		final StringBuilder paramsChain = new StringBuilder("?");
		try {
			while (paramsEntrySetIterator.hasNext()) {
				final Map.Entry<String, String> entry = paramsEntrySetIterator.next();
				paramsChain.append(URLEncoder.encode(entry.getKey(), paramEncoding));
				paramsChain.append('=');
				paramsChain.append(URLEncoder.encode(entry.getValue(), paramEncoding));
				if (paramsEntrySetIterator.hasNext()) {
					paramsChain.append('&');
				}
			}
		} catch (UnsupportedEncodingException pE) {
			pE.printStackTrace();
		}
		return paramsChain.toString();
	}

	@Override
	public Map<String, String> getHeaders() {
		return headers;
	}

	@Nullable
	@Override
	protected Response<T> parseNetworkResponse(final NetworkResponse response) {
		if (isCanceled()) return null;
		final Gson gson = new Gson();
		try {
			String json = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
			return Response.success(gson.fromJson(json, mClass), HttpHeaderParser.parseCacheHeaders(response));
		} catch (UnsupportedEncodingException e) {
			return Response.error(new ParseError(e));
		} catch (JsonSyntaxException e) {
			return Response.error(new ParseError(e));
		}
	}

	@Override
	protected void deliverResponse(final T response) {
		if (mListener != null) {
			mListener.onResponse(response);
		}
	}
}
